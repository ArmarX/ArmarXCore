/**
\page armarx-cli  ArmarX User CLI
\tableofcontents
ArmarX provides a python tool for conveniently performing everyday situations with ArmarX
like \ref armarx-cli-start "starting the Ice runtime environment" or \ref armarx-cli-deploy "deploying to a remote host".

The ArmarX python tools are located in the Core build directory and in the python-armarx deb package.
To retrieve the list of all commands with short explanations start the tools with the -h parameter or to retrieve all the parameters for a command type armarx $command -h

\par Location of the user cli tool:

    ${ArmarXHome}/ArmarXCore/build/bin/armarx

\section armarx-cli-gui  gui command

Use the following command to start an ArmarXGui from anywhere:

    ${ArmarXHome}/ArmarXCore/build/bin/armarx gui



\section armarx-cli-memory  memory command

Use the following command to start/stop the MongoDB database from anywhere:

    ${ArmarXHome}/ArmarXCore/build/bin/armarx memory start
    ${ArmarXHome}/ArmarXCore/build/bin/armarx memory stop



\section armarx-cli-start  start command

Use the following command to start Ice locally:

    ${ArmarXHome}/ArmarXCore/build/bin/armarx start




\section armarx-cli-stop  stop command

Use the following command to stop Ice:

    ${ArmarXHome}/ArmarXCore/build/bin/armarx stop




\section armarx-cli-reset  reset command

Use the following command to stop Ice, clean the Ice database, start Ice locally:

    ${ArmarXHome}/ArmarXCore/build/bin/armarx reset




\subsection armarx-cli-deployment-killIce  killIce command

Use the following command to kill a non-responsive IceGrid started via 'armarx start' :

    ${ArmarXHome}/ArmarXCore/build/bin/armarx killIce


\section armarx-cli-scenario scenario command
ArmarX scenarios are an easy way to start many applications, with specific configurations, together.
Each ArmarX package has a scenario folder, where scenarios are located. The scenario configuration files have an .scx ending.

The ArmarX CLI has the functionality to start/stop/kill these scenarios.
Changing of specific Application parameter should be done with the ScenarioManager GUI Tool.

The CLI tool can be used by only specifying Scenario names, but the package the scenario resides in must be defined in ~/.armarx/default.cfg
in the ArmarX.DefaultPackages parameter. If this is not the case, you should use a specific path to the scx file or the folder it is located in.

\subsection armarx-cli-scenario-list List all Scenarios

Lists all known Scenarios out of Packages that are defined in the ~/.armarx/default.cfg

    \code
        ${ArmarXHome}/ArmarXCore/build/bin/armarx scenario list
    \endcode

\subsection armarx-cli-scenario-status Get the Status of an Scenario

Get the Status of a Scenario with a known Scenario name or with a path to the Scenario scx file

    \code
        ${ArmarXHome}/ArmarXCore/build/bin/armarx scenario status $scenarioName
        ${ArmarXHome}/ArmarXCore/build/bin/armarx scenario status $pathToScx
    \endcode

\subsection armarx-cli-scenario-deploy Deploy an Scenario via Ice

Deploying a Scenario with an known Scenario name or with a path to the Scenario scx file

    \code
        ${ArmarXHome}/ArmarXCore/build/bin/armarx scenario deploy $scenarioName
        ${ArmarXHome}/ArmarXCore/build/bin/armarx scenario deploy $pathToScx
    \endcode

If you want to deploy the Scenario remotely append --remote
Make sure you had a look at the \ref ArmarXCore-Tutorials-ice-remote-deployment "Ice Deployment Tutorial" before using the remote option

\subsection armarx-cli-scenario-start Start an Scenario

Starting a Scenario with an known Scenario name or with a path to the Scenario scx file

    \code
        ${ArmarXHome}/ArmarXCore/build/bin/armarx scenario start $scenarioName
        ${ArmarXHome}/ArmarXCore/build/bin/armarx scenario start $pathToScx
    \endcode

\subsection armarx-cli-scenario-stop Stop an Scenario

Stopping a Scenario with an known Scenario name or with a path to the Scenario scx file.
Stopping only tries to stop the Scenario. If an Applications blocks it might still be running.

Use \ref armarx-cli-scenario-kill to be sure that the Applications stop.

    \code
        ${ArmarXHome}/ArmarXCore/build/bin/armarx scenario stop $scenarioName
        ${ArmarXHome}/ArmarXCore/build/bin/armarx scenario stop $pathToScx
    \endcode

\subsection armarx-cli-scenario-kill Kill an Scenario

Kills a Scenario with a known Scenario name or with a path to the Scenario scx file.

    \code
        ${ArmarXHome}/ArmarXCore/build/bin/armarx scenario kill $scenarioName
        ${ArmarXHome}/ArmarXCore/build/bin/armarx scenario kill $pathToScx
    \endcode

\subsection armarx-cli-scenario-remote Remove an deployed Scenario via Ice

Removing a Scenario with an known Scenario name or with a path to the Scenario scx file

    \code
        ${ArmarXHome}/ArmarXCore/build/bin/armarx scenario remove $scenarioName
        ${ArmarXHome}/ArmarXCore/build/bin/armarx scenario remove $pathToScx
    \endcode

\subsection armarx-cli-scenario-restart Restart an Scenario

Restarts a Scenario with a known Scenario name or with a path to the Scenario scx file.
The named Scenario by default gets killed and startet.
If you want to make sure your Application has enough time for his cleanup you should call stop and start yourself.

    \code
        ${ArmarXHome}/ArmarXCore/build/bin/armarx scenario restart $scenarioName
        ${ArmarXHome}/ArmarXCore/build/bin/armarx scenario restart $pathToScx
    \endcode

\subsection armarx-cli-scenario-generate Generate Deprecated Ice Deployment files

\warning This is only for utilty. It is strongly adviced to use the deploy/remove commands of this CLI or the \ref ArmarXGui-GuiPlugins-ScnearioManagerGuiPlugin "ScenarioManagerGUI"

Generate the old *.icegrid.xml files for deployment with a known Scenario name or with a path to the Scenario scx file.

    \code
        ${ArmarXHome}/ArmarXCore/build/bin/armarx scenario generate $scenarioName
        ${ArmarXHome}/ArmarXCore/build/bin/armarx scenario generate $pathToScx
    \endcode


\section armarx-cli-profiles  ArmarX CLI Profiles


ArmarX provides a mechanism to support interaction with multiple independent IceGrid/ArmarX configurations.
The profile mechanism is integrated into the armarx CLI tool
located in the build binary directory of the ArmarXCore
(e.g. ${ArmarXHome}/ArmarXCore/build/bin/armarx).


\subsection armarx-cli-profiles-files What files does a profile consist of?

An ArmarX profile consists of the following files

\li default.cfg [Properties read by all ArmarX applications]
\li icegrid-sync-variables.icegrid.xml [Contains properties required for \ref armarx-dev-cli-sync "syncing" and \ref armarx-cli-deploy "deploying" ArmarX applications on a remote IceGrid]
\li optional: armarx-sync.ini [contains commandline flags which are automatically used by the \ref armarx-dev-cli-sync "armarx sync" cli command]

These files are located in $HOME/.armarx/profiles/$PROFILE_NAME/ and symlinked to $HOME/.armarx/ during \ref armarx-cli-profiles-switching "profile switching".

The name of the currenlty active profile is stored in the file $HOME/.armarx/armarx.ini


\subsection armarx-cli-profiles-config Show current configuration files

Listing the contents of the currently active configuration files and all available configurations

     ${ArmarXHome}/ArmarXCore/build/bin/armarx profile


\subsection armarx-cli-profiles-listing Listing Profiles

Listing all available configurations (2 possibilities)

     ${ArmarXHome}/ArmarXCore/build/bin/armarx profile
     ${ArmarXHome}/ArmarXCore/build/bin/armarx switch


\subsection armarx-cli-profiles-switching Switching Profiles

Switching to a different profile (non-existing profiles get created on the fly):

     ${ArmarXHome}/ArmarXCore/build/bin/armarx switch $profilename


\page armarx-cli-deployment Deploying ArmarX Scenarios

Deploying an ArmarX scenario means that IceGrid takes care of starting/stopping/restarting ArmarX executables.

\note For deployment to work, a \ref ArmarXCore-Tutorials-IceGridSetup "fully operational IceGrid setup" is required.

Before ArmarX scenarios can be deployed, the related ArmarX packages must be synchronized to the remote PCs running IceGrid nodes first.
Synchronization is performed with the \ref armarx-dev-cli-sync "sync" command of the \ref armarx-dev-cli "ArmarX developer cli" and currently works only with ArmarX packages compiled from sources.

In general it is a good idea to create an \ref armarx-cli-profiles "ArmarX cli profile" for each robot/IceGrid setup, so you can easily switch between them.

Before deploying any scenario, the base directory of the synchronized ArmarX packages on the remote hosts must be specified in the file icegrid-sync-variables.icegrid.xml (available in every \ref armarx-cli-profiles "ArmarX CLI profile").

\note This means that all remote PCs must have an identical user and remote sync directory.

\section armarx-cli-deployment-config Scenario configuration files

Configuration files for the single ArmarX executables are generated in ${PackageName}/scenarios/${ScenarioName}/icegrid/${ExecutableName}.icegrid.xml.
Additionally, two types of scenario deployment files are generated by the build process:

\li ${PackageName}/scenarios/${ScenarioName}/${ScenarioName}.remote.icegrid.xml: use this for remote deployment
\li ${PackageName}/scenarios/${ScenarioName}/${ScenarioName}.icegrid.xml: use this for deployment on an IceGrid instance running on localhost

The typical structure of these files is the following:

    <icegrid>
        <application>
	    <!-- some other IceGrid related tags found here -->

            <!-- include directories for component executables -->
            <node name="NodeMain">
                <include file="icegrid/ExampleUnitObserver.icegrid.xml"/>
                <include file="icegrid/ExampleUnitApp.icegrid.xml"/>
            </node>
        </application>
    </icegrid>

If specific executables should be started on a different IceGrid node, a new node tag can be introduced like this (valid node names can be retrieved with \ref armarx-cli-deployment-list "armarx list"):

    <icegrid>
        <application>
	    <!-- some other IceGrid related tags found here -->

            <!-- include directories for component executables -->
            <node name="NodeMain">
                <include file="icegrid/ExampleUnitObserver.icegrid.xml"/>
            </node>
            <node name="Node1">
                <include file="icegrid/ExampleUnitApp.icegrid.xml"/>
            </node>
        </application>
    </icegrid>


\section armarx-cli-deployment-deploy  deploy command

Use the following command to deploy an ArmarX scenario to remote IceGrid instances:

    ${ArmarXHome}/ArmarXCore/build/bin/armarx deploy path-to-scenario.icegrid.xml



\section armarx-cli-deployment-remove  remove command

Use the following command to remove a scenario from a running IceGrid instance (use the \ref armarx-cli-deployment-list "list command" get a list of running applications):

    ${ArmarXHome}/ArmarXCore/build/bin/armarx remove @scenarioName



\section armarx-cli-deployment-list  list command

Use the following command to list all scenarios running on an IceGrid instance:

    ${ArmarXHome}/ArmarXCore/build/bin/armarx list



\section armarx-cli-deployment-status  status command

Use the following command to show the current IceGrid status:

    ${ArmarXHome}/ArmarXCore/build/bin/armarx status

The output will show the currently active IceGrid registry, the currently active IceGrid nodes and the executables they are managing, as well as the scenarios and all associated executables.



\section armarx-cli-deployment-kill  kill command

Use the following command to kill a deployed ArmarX application:

    ${ArmarXHome}/ArmarXCore/build/bin/armarx kill $applicationname

\note With the default IceGrid config the application will be restarted instantly again.








\page armarx-dev-cli  ArmarX Developer CLI
\tableofcontents

ArmarX provides a python tool for ArmarX developer to conventiently build a package and all its depedencies.
It provides further utility commands for fixing errors like locating a package and finding a c++ symbol.

The ArmarX python tools are located in the Core build directory and in the python-armarx deb package.
To retrieve the list of all commands with short explanations start the tools with the -h parameter.

\par Location of the developer cli tool:

    ${ArmarXHome}/ArmarXCore/build/bin/armarx-dev
\section armarx-cli-build  build command


To run ArmarX programs it is necessary to compile several packages and
their dependencies in the correct order.
Although the normal C++ compiling procedure by calling cmake and make
in the build directory works in ArmarX as well, it can be tedious.

Therefore ArmarX provides a tool, which can be used to build
ArmarX packages including all its dependencies.
The build tool is integrated into the armarx CLI tool
located in the build binary directory of the ArmarXCore
(e.g. ${ArmarXHome}/ArmarXCore/build/bin/armarx).


The following features are available at the moment:
- Extracting the dependency graph and compiling in correct order
- Force CMake to run before compilation (--cmake)
- Interactive compilation if the packages cannot be found
- Pulling from the Git repository before compiling (--pull).
- Stash&Pulling from the Git repository before compiling (--stashnpull). This will perform stash, pull, stash pop on the git repository.
- Multicore compilation (standard behavior is using as many cores as the CPU has) (-j X)
- Compilation with Ninja (--ninja)

To pull and compile for example the RobotAPI, enter the following line (for this to work ArmarXCore needs to be cmaked before):

     ${ArmarXHome}/ArmarXCore/build/bin/armarx-dev build RobotAPI --pull

Running the script without parameters shows the usage information with all available parameters.
The location from which the script is executed, does not matter.

\subsection armarx-dev-cli-external-deps Building of external dependencies
It is also possible to automatically build external dependencies, if the following criteria are met:

- The source code is already checked out
- It is a CMake project
- CMake was already run once
- The project writes the build directory into ~/.cmake/packages

You specify the dependency in ~/.armarx/armarx.ini:

     [Misc]
     dependencies = project_name1:dependency1,dependency2;project_name2:dependency3

For example:

     [Misc]
     dependencies = RobotAPI:Simox

Now, Simox will be always be build before RobotAPI is build. These dependency will also be included in the `exec` command.

\section armarx-dev-cli-exec  exec command

With the `exec` command it is possible to execute shell commands in a given package and all its dependencies.
The dependencies can be skipped with the parameter `--nd`.
If the command contains spaces quotes must be used.





\section armarx-dev-cli-sync  sync command

ArmarX provides a mechanism to synchronize an ArmarX package and all of its dependencies to remote PCs.
Afterwards, ArmarX scenarios can be started remotely via \ref armarx-cli-deployment "IceGrid deployments (armarx deploy)".

This command will install all mentioned ArmarX packages and all dependendent ArmarX packages into a local temporary directory
and synchronize the contents of this directory to one or more remote hosts.
Use the following command for synchronization:

    ${ArmarXHome}/ArmarXCore/build/bin/armarx-dev sync [parameters] package [package ...]

The most important parameters are:

\li --deployHosts: specify here which PCs the packages should be synced to
\li --username: specify which username to use for connecting to remote PCs (default: armar-user)

Additional paramters are:

\li --no-delete: don't remove the content of the local temporary directory before installation
\li --no-dep: don't install package dependencies (usefull if you are only changing a specific ArmarX package)
\li --compile: compile the packages before installation
\li --localSyncDir: if you want to specify which local temporary directory should be used for installation (default: $HOME/sync-armarx/)
\li --remoteSyncDir: which remote directory to synchronize ArmarX to (can be set via the environment variable ARMARX_REMOTE_SYNC_DIR and defaults to: armarx_$USER)

Commandline parameters can be stored in the file $HOME/.armarx/profiles/$PROFILE_NAME/armarx-sync.ini.
Each commandline parameter is specified on a separate line in this file

    --deployHosts=pc1,pc2
    --no-delete




\section armarx-dev-cli-locate  locatePackage command

Use the following command to locate and ArmarX package and print some of its properties:

    ${ArmarXHome}/ArmarXCore/build/bin/armarx-dev locatePackage packagename
    
The result for RobotAPI might look like this:
\verbatim
$ armarx-dev locatePackage RobotAPI   

ArmarX Package <RobotAPI>

        PACKAGE_CONFIG_DIR : ['/home/waechter/armarx/RobotAPI/build']

        PACKAGE_VERSION : ['0.9.1']

        PROJECT_BASE_DIR : ['/home/waechter/armarx/RobotAPI']

        BUILD_DIR : ['/home/waechter/armarx/RobotAPI/build']

        INCLUDE_DIRS : ['/home/waechter/armarx/RobotAPI/source', '/home/waechter/armarx/RobotAPI/build/source']

        LIBRARY_DIRS : ['/home/waechter/armarx/RobotAPI/build/lib']

        LIBRARIES : ['RobotAPIInterfaces RobotAPICore RobotAPIWidgets RobotAPIUnits RobotAPIUnitListenerRelays DebugDrawer RobotAPIRobotStateComponent EarlyVisionGraph ViewSelection WeissHapticSensor XsensIMU LibIMU RobotAPIOperations WeissHapticGroup StatechartProfilesTestGroup KinematicUnitGuiPlugin HandUnitGuiPlugin PlatformUnitGuiPlugin SensorActorWidgetsGuiPlugin HapticUnitGuiPlugin RobotViewerGuiPlugin DebugDrawerViewerGuiPlugin ViewSelectionGuiPlugin']

        CMAKE_DIR : ['/home/waechter/armarx/RobotAPI/etc/cmake']

        BINARY_DIR : ['/home/waechter/armarx/RobotAPI/build/bin']

        SCENARIOS_DIR : ['/home/waechter/armarx/RobotAPI/scenarios']

        DATA_DIR : ['/home/waechter/armarx/RobotAPI/data']

        EXECUTABLE : ['ForceTorqueObserverRun HeadIKUnitRun TCPControlUnitRun WeissHapticUnitAppRun HapticObserverAppRun RobotControlRun RobotControlUIRun KinematicUnitObserverRun KinematicUnitSimulationRun PlatformUnitSimulationRun PlatformUnitObserverRun RobotStateComponentRun HandUnitSimulationRun ForceTorqueUnitSimulationRun XsensIMUAppRun InertialMeasurementUnitObserverAppRun ViewSelectionAppRun']

        INTERFACE_DIRS : ['/home/waechter/armarx/RobotAPI/source']

        DEPENDENCIES : ['ArmarXCore', 'ArmarXGui']

        SOURCE_PACKAGE_DEPENDENCIES : ['ArmarXCore', 'ArmarXGui']

        PACKAGE_DEPENDENCY_PATHS : ['ArmarXCore:/home/waechter/armarx/ArmarXCore/build', 'ArmarXGui:/home/waechter/armarx/ArmarXGui/build']

\endverbatim




\section armarx-dev-cli-findSymbol  findSymbol command

Use the following command to find a symbol in any of the ArmarX packages by specifying the top level package.
ArmarX dependency will automatically searched as well.
This command is useful if you are facing undefined reference. Just pass the undefined reference to this command (or a part of it) and
all package libs will be searched for it.

    ${ArmarXHome}/ArmarXCore/build/bin/armarx-dev findSymbol -p packagename -s symbolname

For example:

    armarx findSymbol -p VisionX -s "memoryx::ObjectInstance::ObjectInstance("

The result output might look like this:
\verbatim
Ice Config: /home/waechter/.armarx/default.cfg
Profile: default
Searching for memoryx::ObjectInstance::ObjectInstance(
Searching in the following projects: ['ArmarXCore', 'ArmarXGui', 'RobotAPI', 'MemoryX', 'VisionX']
Found 4 matching symbol(s) in package MemoryX in file /home/waechter/armarx/MemoryX/build/lib/libMemoryXMemoryTypes.so.0.6.0
#1: memoryx::ObjectInstance::ObjectInstance(memoryx::ObjectInstance const&)
#2: memoryx::ObjectInstance::ObjectInstance(std::string const&, std::string const&)
#3: memoryx::ObjectInstance::ObjectInstance(memoryx::ObjectInstance const&)
#4: memoryx::ObjectInstance::ObjectInstance(std::string const&, std::string const&)
\endverbatim

*/

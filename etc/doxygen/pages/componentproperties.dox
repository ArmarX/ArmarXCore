/*!
 * \page armarx-componentproperties Component parameterization
 * 
 *
 * \section introduction Introduction
 * Components in ArmarX are configured using properties. This page explains how property values can be passed to an application or component.
 * The documentation on how to specify properties for applications and components can be found here: \ref PropertiesDoc
 * 
 * \subsection propertysyntax Property syntax
 * Each property consists of a \c key=value pair, that follow the convention
 * 
 * <tt>\<DomainName\>.\<ComponentName\>.\<PropertyName\>=value</tt>
 * 
 * where
 * - <tt>\<DomainName\></tt> is the domain of the component. For Ice properties the domain is \c Ice, for ArmarX properties, the domain is \c ArmarX, and for VisionX the domain is \c VisionX. The utilized domain is specified by the armarx::Application.
 * - <tt>\<ComponentName\></tt> is the name of the component. The name of the component is determined using armarx::Component::getDefaultName(). This name can be overridden by the application. Both DomainName and ComponentName are taken from the armarx::ComponentIdentifier. Usually ComponentName corresponds to the class name of the most derived class.
 * - <tt>\<PropertyName\></tt> is the name of the property.
 * 
 * \subsection specifyproperties Specifying properties
 * The properties can be specified on startup of an application either on command line or in a configuration file. Properties in the command line always overwrite properties in the configuration file.
 * \subsubsection commandline Properties on command line
 * Properties can be specified in the command line using - -key=value
 * \subsubsection configfile Properties in configuration file
 * A configuration file can be provided which contains an umlimited number of \c key=value pairs. The configuration file is specified with \b - \b -ArmarX.Config=filename on the command line.
 *  
 * \subsection property-inheritance Property Inheritance
 * Assuming an application consists of two or more component and most of them use or share the same properties, Property Inheritance can be used to set common properties. This way, defining the properties redundantly for each component can be avoided. Further more, property changes is simplified as well.
 * Property inheritance can be specified via a reserved property with a namespace. 
 * 
 * <tt>\<DomainName\>.\<ComponentName\>.inheritFrom = \<namespace\></tt>
 * 
 * Example config:
 * 
 * 
\verbatim
    ArmarX.CommonProperties.FrameRate = 30
    ArmarX.CommonProperties.ColorMode = HSV
    ArmarX.CommonProperties.ShutterSpeed = 0.003
    ArmarX.CommonProperties.Aperture = 1.2
    ArmarX.CommonProperties.WhiteBalance = Auto
    ArmarX.CommonProperties.Metering = CenteredMean
    ArmarX.CommonProperties.Resolution = 640x480
    ArmarX.CommonProperties.CropFactor = 1.5

    VisionX.MyCapturer1.inheritFrom = ArmarX.CommonProperties
    VisionX.MyCapturer1.ColorMode = RGB

    VisionX.MyCapturer2.inheritFrom = ArmarX.CommonProperties
    VisionX.MyCapturer2.ShutterSpeed = 0.001

    VisionX.MyCapturer3.FrameRate = 60
    VisionX.MyCapturer3.inheritFrom = ArmarX.CommonProperties
\endverbatim
 * 
 * The result is:
 *
\verbatim
    ArmarX.CommonProperties.FrameRate = 30
    ...
    ...
    ArmarX.CommonProperties.CropFactor = 1.5

    VisionX.MyCapturer1.FrameRate = 30
    VisionX.MyCapturer1.ColorMode = RGB
    VisionX.MyCapturer1.ShutterSpeed = 0.003
    VisionX.MyCapturer1.Aperture = 1.2
    VisionX.MyCapturer1.WhiteBalance = Auto
    VisionX.MyCapturer1.Metering = CenteredMean
    VisionX.MyCapturer1.Resolution = 640x480
    VisionX.MyCapturer1.CropFactor = 1.5

    VisionX.MyCapturer2.FrameRate = 30
    VisionX.MyCapturer2.ColorMode = HSV
    VisionX.MyCapturer2.ShutterSpeed = 0.001
    VisionX.MyCapturer2.Aperture = 1.2
    VisionX.MyCapturer2.WhiteBalance = Auto
    VisionX.MyCapturer2.Metering = CenteredMean
    VisionX.MyCapturer2.Resolution = 640x480
    VisionX.MyCapturer2.CropFactor = 1.5

    VisionX.MyCapturer3.FrameRate = 60
    VisionX.MyCapturer3.ColorMode = HSV
    VisionX.MyCapturer3.ShutterSpeed = 0.003
    VisionX.MyCapturer3.Aperture = 1.2
    VisionX.MyCapturer3.WhiteBalance = Auto
    VisionX.MyCapturer3.Metering = CenteredMean
    VisionX.MyCapturer3.Resolution = 640x480
    VisionX.MyCapturer3.CropFactor = 1.5
\endverbatim
 *
 * \section componentproperties-applications Application Properties Overview
 * A list of all component and aplication properties can be found at: \ref componentproperties
 
 \section ManualProperties Manually created properties documentation
 */

//This file was auto generated

#include "@ARMARX_GUI_PLUGIN_PREFIX@GuiPlugin.h"

#include <@ARMARX_GUI_PLUGIN_WIDGET_CONTROLLER_HEDER@>

namespace armarx
{
    @ARMARX_GUI_PLUGIN_PREFIX@GuiPlugin::@ARMARX_GUI_PLUGIN_PREFIX@GuiPlugin()
    {
        addWidget < @ARMARX_GUI_PLUGIN_PREFIX@WidgetController > ();
    }
}

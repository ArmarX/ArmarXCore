//This file was auto generated
#pragma once

#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

namespace armarx
{
    class ARMARXCOMPONENT_IMPORT_EXPORT @ARMARX_GUI_PLUGIN_PREFIX@GuiPlugin:
        public armarx::ArmarXGuiPlugin
    {
        Q_OBJECT
        Q_INTERFACES(ArmarXGuiInterface)
        Q_PLUGIN_METADATA(IID "ArmarXGuiInterface/1.00")
    public:
        /**
         * All widgets exposed by this plugin are added in the constructor
         * via calls to addWidget()
         */
        @ARMARX_GUI_PLUGIN_PREFIX@GuiPlugin();
    };
}

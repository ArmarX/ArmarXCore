# set this variable to forward to the IceStorm
set(ICE_DEFAULT_HOST "$ENV{ICE_DEFAULT_HOST}")
configure_file("icegrid_default_templates.xml.in" "${PACKAGE_CONFIG_DIR}/icegrid_default_templates.xml" @ONLY)
configure_file("default.cfg.in" "${PACKAGE_CONFIG_DIR}/default.cfg")
configure_file("default.generated.cfg.in" "${PACKAGE_CONFIG_DIR}/default.generated.cfg")
configure_file("IceGridUserVariables.sh.in" "${PACKAGE_CONFIG_DIR}/IceGridUserVariables.sh")
configure_file("armarx-icegridnode.cfg.in" "${PACKAGE_CONFIG_DIR}/armarx-icegridnode.cfg")
configure_file("icegrid_registry.cfg.in" "${PACKAGE_CONFIG_DIR}/icegrid_registry.cfg")
configure_file("icegrid-sync-variables.icegrid.xml.in" "${PACKAGE_CONFIG_DIR}/icegrid-sync-variables.icegrid.xml" @ONLY NEWLINE_STYLE UNIX)


file(COPY "IceStorm.icegrid.xml" DESTINATION "${PACKAGE_CONFIG_DIR}")

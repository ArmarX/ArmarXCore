#!/usr/bin/env python3
##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Mirko Waechter (waechter at kit dot edu)
# @date       2016
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License


import fileinput
import os
import re
import sys


if __name__ == '__main__':

    if len(sys.argv) < 5:
        print("Required parameters: buildDir projectName projectBaseDir searchPath [searchPath2 ...] ")
        exit(1)

    regex = {
        "": r"(@|\\)page ([A-Z0-9a-z\-]+) ",
        "group__": r"(@|\\)defgroup ([A-Z0-9a-z\-]+) ",
        "classarmarx_1_1": r"(@|\\)class ([A-Z0-9a-z\-]+)",
    }

    pageSourceMap = {}
    buildDoxygenPath = "{}/doxygen/html".format(sys.argv[1])
    project = sys.argv[2]
    projectBaseDir = sys.argv[3]
    print(sys.argv)
    searchPaths = sys.argv[4:]  # ["etc/doxygen", "source"]
    gitlabBaseLink = "https://gitlab.com/ArmarX/{}/blob/master/".format(project)
    doxygenSearchText = "<li class=\"footer\">"

    for path in searchPaths:
        for root, dirnames, filenames in os.walk(path):
            # print(root, dirnames, filenames)
            for filename in filenames:
                relFilePath = os.path.join(root, filename)
                try:
                    with open(relFilePath) as file:
                        fileContent = file.read()
                        lines = fileContent.splitlines()

                        lineNumber = 0
                        for line in lines:
                            lineNumber += 1
                            for regexKey in regex:
                                matches = re.finditer(regex[regexKey], line)
                                for matchNum, match in enumerate(matches):
                                    matchNum = matchNum + 1
                                    link = ("https://gitlab.com/ArmarX/ArmarXCore/blob/master/{}#L{}"
                                            .format(os.path.relpath(relFilePath, projectBaseDir), lineNumber))
                                    # print ("Match:" + match.group(2) +" in file " + link)
                                    pageSourceMap[regexKey + match.group(2)] = (
                                        os.path.relpath(relFilePath,projectBaseDir), lineNumber)

                    exit(0)
                except Exception:
                    # print("Reading " + relFilePath + " failed")
                    pass

    changeCount = 0

    for page in pageSourceMap:
        filePath = "{}/{}.html".format(buildDoxygenPath, page)
        replaceText = ("<li class=\"footer\" > <A href='{}{}#L' target='_blank'><b>Edit Page</b></A> - "
                       .format(gitlabBaseLink, pageSourceMap[page][0], pageSourceMap[page][1]))

        if os.path.isfile(filePath):
            # print ("Changing file " +  filePath)
            changeCount += 1
            with fileinput.FileInput(filePath, inplace=True) as file:
                for line in file:
                    print(line.replace(doxygenSearchText, replaceText), end='')

    print("Added {} source page links for project {}.".format(changeCount, project))

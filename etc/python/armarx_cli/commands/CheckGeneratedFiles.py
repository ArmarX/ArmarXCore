##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Fabian Peller (fabian (dot) peller-konrad (at) kit (dot) edu)
# @date       2020
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

__author__ = 'peller'

import os
import re
import time
import subprocess
import xml.dom.minidom

from .Command import Command, CommandType
import argparse
from armarx_cli import ArmarXBuilder

class ExecCommand(Command):
    helpString = "Removes unecessary / old generated files which do not have a corresponding generator (e.g. .ice, .xml) file anymore. Example: armarx-dev checkGeneratedFiles RobotAPI"
    commandName = "checkGeneratedFiles"
    commandType = CommandType.Developer
    requiresIce = False

    requiredArgumentCount = 1
    parser = argparse.ArgumentParser(description=helpString)
    def __init__(self, profile):
        super(ExecCommand, self).__init__(profile)

    @classmethod
    def addtoSubArgParser(cls, subParser):
        subParser.add_argument('package', help='ArmarX package to search for').completer = cls.getCompletionPackages


    def execute(self, args):
        self.addtoSubArgParser(self.parser)
        args = self.parser.parse_args(args)
        generator = ArmarXBuilder.ArmarXMakeGenerator(0)

        packagename = args.package
        builder = ArmarXBuilder.ArmarXBuilder(None, False, False, False, False, False)
        package_data = builder.getArmarXPackageData(packagename)

        if not package_data:
            print(("ArmarX package <" + packagename + "> not found"))
            return

        base_directories = builder.getArmarXPackageDataValue(package_data, "PACKAGE_CONFIG_DIR")

        if not base_directories:
            print(("ArmarX package <" + packagename + "> not found"))
            return

        project_base_dir = builder.getArmarXPackageDataValue(package_data, "PROJECT_BASE_DIR")
        if not project_base_dir or len(project_base_dir) < 1:
            print(("ArmarX package <" + packagename + "> not found"))
            return

        project_build_dir = builder.getArmarXPackageDataValue(package_data, "BUILD_DIR")
        if not project_build_dir or len(project_build_dir) < 1:
            print(("ArmarX package <" + packagename + "> not found"))
            return

        basePath = project_base_dir[0]
        sourcePath = basePath + "/source"
        buildPath = project_build_dir[0]

        #startTime = time.time()
        self.checkIceFiles(packageName=packagename, sourcePath=sourcePath, buildPath=buildPath)
        self.checkAronFiles(packageName=packagename, sourcePath=sourcePath, buildPath=buildPath)
        #executionTime = (time.time() - startTime)
        #print('Execution time in seconds: ' + str(executionTime))


    # check whether a file contains a specific string
    def _checkFileContains(self, file, needle):
        if os.path.exists(file):
            regexp = re.compile(needle)
            with open(file, 'r') as f:
                if regexp.search(f.read()):
                    return True
        return False


    # Check Ice Files
    def checkIceFiles(self, packageName, sourcePath, buildPath):
        """Check for each generated header file whether a correspondig ice file exists. If not, then remove the header (and the cpp if it exists)"""
        # print("Checking .ice files ...")

        iceFileHeader = "// Generated from file .*\.ice"

        # get all generated header files (and check for cpp files only if h file found)
        # *.generated.h is either aron or statechart file and can therefore be skipped
        batcmd = "find {root_path} -type f -name '*.h' | xargs grep -l '{hdr}'".format(root_path=buildPath, hdr=iceFileHeader)

        allHeaderFiles = []
        try:
            cmdout = subprocess.check_output(batcmd, shell=True)
            cmdout = cmdout.decode('ascii')
            if(cmdout != ""):
                allHeaderFiles = str(cmdout).strip().split("\n")
        except subprocess.CalledProcessError as e:
            # error in find. Maybe there are no such files
            print("It seems like there are no generated ice header files.")


        allHeaderFilesWithoutExtension = []
        for file in allHeaderFiles:
            withoutExtension = os.path.splitext(file)[0]
            allHeaderFilesWithoutExtension.append(withoutExtension)

        batcmd = "find {root_path} -type f -name '*.ice'".format(root_path=sourcePath)

        allSliceFiles = []
        try:
            cmdout = subprocess.check_output(batcmd, shell=True)
            cmdout = cmdout.decode('ascii')
            if(cmdout != ""):
                allSliceFiles = str(cmdout).strip().split("\n")
        except subprocess.CalledProcessError as e:
            # error in find. Maybe there are no such files
            print("It seems like there are no ice files.")

        allSliceFilesWithoutExtension = []
        for file in allSliceFiles:
            withoutExtension = os.path.splitext(file)[0]
            allSliceFilesWithoutExtension.append(withoutExtension)

        i = 0
        for a in allHeaderFilesWithoutExtension:
            sameFileInSliceFiles = a.replace("build/", "")
            if not sameFileInSliceFiles in allSliceFilesWithoutExtension:
                i = i+1
                print("Could not find a corresponding '.ice' file for: '" + a + ".h/cpp'. Removing the generated file(s).")
                os.remove(a + ".h")
                if os.path.exists(a + ".cpp"):
                    os.remove(a + ".cpp")

        if i == 0:
            print("ICE  summary: No mismatches found.")
        else:
            print("ICE  summary: Found " + str(i) + " mismatches for package '" + packageName + "'.")

    # Check Aron Files
    def checkAronFiles(self, packageName, sourcePath, buildPath):
        """Check for each generated header file whether a correspondig aron file exists. If not, then remove the header"""

        allFilesToRemove = []
        aronFileSource = " * Original file: .*\.xml"

        # remove all generated files in source
        batcmd = "find {root_path} -type f -name '*.aron.generated.h' | xargs grep -l '{hdr}'".format(root_path=sourcePath, hdr=aronFileSource)

        try:
            cmdout = subprocess.check_output(batcmd, shell=True)
            cmdout = cmdout.decode('ascii')
            if(cmdout != ""):
                allNextFilesToRemove = str(cmdout).strip().split("\n")
                allFilesToRemove += allNextFilesToRemove
        except subprocess.CalledProcessError as e:
            # error in find. Maybe there are no such files
            #print("ArmarXGeneratedFileChecker: It seems like there are no wrongly generated aron files in source.")
            pass

        # remove all generated files in build if no corresponding xml exists
        allHeaderFiles = []
        batcmd = "find {root_path} -type f -name '*.aron.generated.h' | xargs grep -l '{hdr}'".format(root_path=buildPath, hdr=aronFileSource)

        try:
            cmdout = subprocess.check_output(batcmd, shell=True)
            cmdout = cmdout.decode('ascii')
            if(cmdout != ""):
                allHeaderFiles = str(cmdout).strip().split("\n")
        except subprocess.CalledProcessError as e:
            # error in find. Maybe there are no such files
            # print("It seems like there are no wrongly generated aron files in build.")
            pass

        for file in allHeaderFiles:
            with open(file) as f:
                for line in f:
                    if line.startswith(" * Original file: "):
                        corresponding_xml_file = line.replace(" * Original file: " , "").replace("\n", "").strip();
                        if not os.path.exists(corresponding_xml_file):
                            print ("The xml file '" + corresponding_xml_file + "' does not exist!")
                            allFilesToRemove.append(file)
                        break

        i = 0;
        for file in allFilesToRemove:
            print("Could not find a corresponding '.xml' file for: '" + file + "'. Removing the generated header.")
            os.remove(file)
            i += 1

        if i == 0:
            print("ARON summary: No mismatches found.")
        else:
            print("ARON summary: Found " + str(i) + " wrongly generated files for package '" + packageName + "'.")


    @classmethod
    def getHelpString(cls):
        return cls.helpString

##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Fabian Peller (fabian (dot) peller-konrad (at) kit (dot) edu)
# @date       2020
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

__author__ = 'peller'

import os
import subprocess

from .Command import Command, CommandType
import argparse
from armarx_cli import ArmarXBuilder

class AronFileGenerator:
    def __init__(self, inputfile, outputFolder, targetLanguage, enableDebugInfo):
        self.inputFile = inputfile
        self.outputFolder = outputFolder
        self.targetLanguage = targetLanguage
        self.enableDebugInfo = enableDebugInfo

    def runFileGeneration(self):
        builder = ArmarXBuilder.ArmarXBuilder(None, False, False, False, False, False)

        # Find RobotAPI
        robotapi_data = builder.getArmarXPackageData("RobotAPI")

        if not robotapi_data:
            print(("ArmarX package <RobotAPI> not found"))
            return False

        base_directories = builder.getArmarXPackageDataValue(robotapi_data, "PACKAGE_CONFIG_DIR")

        if not base_directories:
            print(("ArmarX package <RobotAPI> not found"))
            return False

        binaryPath = builder.getArmarXPackageDataValue(robotapi_data, "BINARY_DIR")[0]
        executables = builder.getArmarXPackageDataValue(robotapi_data, "EXECUTABLE")[0]
        executables = executables.split(" ")

        if "AronCodeGeneratorAppRun" not in executables or not os.path.exists(binaryPath + "/AronCodeGeneratorAppRun"):
            print(("The executable <AronCodeGeneratorAppRun> not found"))
            return False

        executable = binaryPath + "/AronCodeGeneratorAppRun"

        if(self.enableDebugInfo == "True" or self.enableDebugInfo == "TRUE" or self.enableDebugInfo == "1"):
            p = subprocess.run([executable, "-v", "-f", "-i", self.inputFile, "-o", self.outputFolder], universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        else:
            p = subprocess.run([executable, "-i", self.inputFile, "-o", self.outputFolder], universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        print(p.stdout)
        return True


class ExecCommand(Command):
    helpString = "Generates the aron classes given an XML Aron descriptor file. Example: armarx-dev GenerateAron <inputFile> <outputFolder> <targetLanguage>"
    commandName = "GenerateAron"
    commandType = CommandType.Developer
    requiresIce = False

    requiredArgumentCount = 4
    parser = argparse.ArgumentParser(description=helpString)
    def __init__(self, profile):
        super(ExecCommand, self).__init__(profile)

    @classmethod
    def addtoSubArgParser(cls, subParser):
        supportedLanguages = ("cpp","python_experimental")
        subParser.add_argument('inputFile', help='The input file path')
        subParser.add_argument('outputFolder', help='The output folder path')
        subParser.add_argument('targetLanguage', help='The used language to generate source code', choices=supportedLanguages, default="cpp")
        subParser.add_argument('enableDebugInfo', help='Add verbose output', default="False")


    def execute(self, args):
        self.addtoSubArgParser(self.parser)
        args = self.parser.parse_args(args)

        inputFile = args.inputFile
        outputFolder = args.outputFolder
        targetLanguage = args.targetLanguage
        enableDebugInfo = args.enableDebugInfo

        afg = AronFileGenerator(inputFile, outputFolder, targetLanguage, enableDebugInfo)
        afg.runFileGeneration()

    @classmethod
    def getHelpString(cls):
        return cls.helpString

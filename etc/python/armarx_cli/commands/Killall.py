##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
# @date       2022
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License
from .Command import Command
import argparse

__author__ = 'kartmann'

from .KillAll import KillAll


class Killall(Command):

    commandName = "killall"
    requiresIce = False

    requiredArgumentCount = 0

    parser = argparse.ArgumentParser(description='-')


    def __init__(self, profile):
        super().__init__(profile)
        self.impl = KillAll(profile)

    @classmethod
    def addtoSubArgParser(cls, subParser):
        KillAll.addtoSubArgParser(subParser)

    def execute(self, args):
        self.impl.execute(args=args)

    @classmethod
    def getHelpString(cls):
        return "Alias of 'killAll'"

##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Fabian Peller (fabian (dot) peller-konrad (at) kit (dot) edu)
# @date       2020
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

__author__ = 'peller'

import os
import re
import time
import sys
import subprocess
import xml.dom.minidom

from .Command import Command, CommandType
import argparse
from armarx_cli import ArmarXBuilder

class ExecCommand(Command):
    helpString = "Analyze statechart xmls for dead ends and parameters. Example: armarx-dev analyzeStatecharts"
    commandName = "analyzeStatecharts"
    commandType = CommandType.Developer
    requiresIce = False

    requiredArgumentCount = 1
    parser = argparse.ArgumentParser(description=helpString)
    def __init__(self, profile):
        super(ExecCommand, self).__init__(profile)

    @classmethod
    def addtoSubArgParser(cls, subParser):
        pass


    def execute(self, args):
        self.addtoSubArgParser(self.parser)
        args = self.parser.parse_args(args)
        generator = ArmarXBuilder.ArmarXMakeGenerator(0)

        packages = ['RobotAPI', 'MemoryX', 'VisionX', 'RobotComponents', 'RobotSkillTemplates', 'ArmarXSimulation', 'Armar3', 'armar6_rt', 'Armar6Skills']
        allXMLsOfAllPackages = []

        startTime = time.time()

        # get all xmls
        for packagename in packages:

            builder = ArmarXBuilder.ArmarXBuilder(None, False, False, False, False, False)
            package_data = builder.getArmarXPackageData(packagename)

            if not package_data:
                print(("ArmarX package <" + packagename + "> not found"))
                return

            base_directories = builder.getArmarXPackageDataValue(package_data, "PACKAGE_CONFIG_DIR")

            if not base_directories:
                print(("ArmarX package <" + packagename + "> not found"))
                return

            project_base_dir = builder.getArmarXPackageDataValue(package_data, "PROJECT_BASE_DIR")
            if not project_base_dir or len(project_base_dir) < 1:
                print(("ArmarX package <" + packagename + "> not found"))
                return

            project_build_dir = builder.getArmarXPackageDataValue(package_data, "BUILD_DIR")
            if not project_build_dir or len(project_build_dir) < 1:
                print(("ArmarX package <" + packagename + "> not found"))
                return

            basePath = project_base_dir[0]
            sourcePath = basePath + "/source"
            buildPath = project_build_dir[0]

            batcmd = "find {root_path}/{package}/statecharts -type f -name '*.xml'".format(root_path=sourcePath, package=packagename)
            allXMLs = []
            try:
                cmdout = subprocess.check_output(batcmd, shell=True)
                cmdout = cmdout.decode('ascii')
                if(cmdout != ""):
                    allXMLs = str(cmdout).strip().split("\n")
            except subprocess.CalledProcessError as e:
                # error in find. Maybe there are no such files
                print("ArmarXStatechartAnalyzer: It seems like there are no statechart XML files for package " + packagename + ".")

            allXMLsOfAllPackages.extend(allXMLs)


        # parse all xmls and build substate graph for each xml
        allStates = {}
        x = 0
        for xml_file in allXMLsOfAllPackages:
            state = {}

            doc = xml.dom.minidom.parse(xml_file)
            stateDefinition = doc.getElementsByTagName("State")[0]
            state["file"] = xml_file
            state["name"] = stateDefinition.getAttribute("name")
            state["type"] = stateDefinition.getAttribute("type")

            state["usages"] = 0

            #inputParameters = doc.getElementByTagName("InputParameters")[0]
            #parameters = inputParameters.getElementByTagName("Parameter")

            substates = stateDefinition.getElementsByTagName("Substates")[0]
            allRelevantSubstateTags = []
            allRelevantSubstateTags.extend(substates.getElementsByTagName("LocalState"))
            allRelevantSubstateTags.extend(substates.getElementsByTagName("EndState"))
            allRelevantSubstateTags.extend(substates.getElementsByTagName("RemoteState"))
            substateMap = {}
            for s in allRelevantSubstateTags:
                substate = {}
                substate["name"] = s.getAttribute("name")
                substate["type"] = s.tagName
                substate["is_start"] = False
                substate["incoming_edges"] = 0
                substate["outgoing_edges"] = 0

                substateMap[substate["name"]] = substate
            state["substates"] = substateMap

            if(len(state["substates"]) > 0):
                startState = stateDefinition.getElementsByTagName("StartState")[0]
                state["substates"][startState.getAttribute("substateName")]["is_start"] = True

            transitions = stateDefinition.getElementsByTagName("Transitions")[0]
            transitions = transitions.getElementsByTagName("Transition")
            for t in transitions:
                state["substates"][t.getAttribute("from")]["outgoing_edges"] += 1
                if(t.hasAttribute("to")):
                    state["substates"][t.getAttribute("to")]["incoming_edges"] += 1
                else:
                    event = t.getAttribute("eventName")
                    if event in state["substates"]:
                        state["substates"][event]["incoming_edges"] += 1

            allStates[state["name"]] = state

        unusedStates = []
        deadEndStates = []

        # update usages and get unused and dead states per xml
        for stateKey in allStates:
            state = allStates[stateKey]
            for substateKey in state["substates"]:
                substate = state["substates"][substateKey]
                if substate["incoming_edges"] == 0 and not substate["is_start"]:
                    unusedState = {}
                    unusedState["name"] = substate["name"]
                    unusedState["file"] = state["file"]
                    unusedStates.append(unusedState)
                if substate["outgoing_edges"] == 0 and not substate["type"] == "EndState":
                    deadEndState = {}
                    deadEndState["name"] = substate["name"]
                    deadEndState["file"] = state["file"]
                    deadEndStates.append(deadEndState)

        executionTime = (time.time() - startTime)

        print("ArmarXStatechartAnalyzer: STATECHART Summary: Found " + str(len(unusedStates)) + " unused states.")
        for x in unusedStates:
            print("     - State '" + x["name"] + "' in file " + x["file"])
        print("===================================================================================================")
        print("ArmarXStatechartAnalyzer: STATECHART Summary: Found " + str(len(deadEndStates)) + " deadend states.")
        #print("ArmarXStatechartAnalyzer: STATECHART Summary: Found " + str(i) + " potential parameter errors.")
        print('Execution time in seconds: ' + str(executionTime))


    @classmethod
    def getHelpString(cls):
        return cls.helpString

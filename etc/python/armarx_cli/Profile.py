##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

__author__ = 'kroehner'

import armarx_cli.Util as Util

import os
import shutil
import random
import tempfile
import socket
import subprocess
import re

class Profile:

    profiles_directory = ""
    armarxUserConfigurationDirectory = ""
    templateDirectory = ""
    configuration_files = []


    def __init__(self, profilename):
        self.profilename = profilename


    def exists(self):
        profile_directory = self.getProfileDirectory()
        # check if directory exists
        if not os.path.exists(profile_directory):
            return False
        # check if all configuration files exist
        for configuration_file in self.configuration_files:
            if not os.path.exists(os.path.join(profile_directory, configuration_file)):
                return False
        return True


    def create(self, mongo_port=None, mongo_host="localhost", ice_port=None, ice_host="localhost", ice_default_host=None):
        Util.mkdir_p(self.getProfileDirectory())
        profile_directory = self.getProfileDirectory()
        # create symlinks for all necessary configuration files
        for configuration_file in self.configuration_files:
            symlinkDestination = os.path.join(self.armarxUserConfigurationDirectory, configuration_file)
            symlinkTarget = os.path.join(profile_directory, configuration_file)
            if os.path.isfile(symlinkDestination) and not os.path.islink(symlinkDestination):
                shutil.move(symlinkDestination, symlinkTarget)
            elif not os.path.exists(symlinkTarget):
                templatefile = os.path.join(self.templateDirectory, configuration_file)
                print("copying " + templatefile + " template to: " + symlinkTarget)
                if not os.path.exists(templatefile):
                    print("Template file does not exist: " + templatefile)
                    print("Aborting")
                    return False
                shutil.copyfile(templatefile, symlinkTarget)
                if configuration_file == "default.cfg" :
                    if ice_port is None or mongo_port is None:
                        print("    Randomizing Ports in: " + symlinkTarget)
                        new_port = random.randint(7000, 27000)
                        if ice_port is None   : ice_port = new_port
                        if mongo_port is None : mongo_port = new_port + 1
                    ice_port = str(ice_port)
                    mongo_port = str(mongo_port)
                    if ice_default_host is None:
                        try:
                            ice_target_ip = socket.gethostbyname(ice_host)
                            r = subprocess.run(["ip", "route", "get", str(ice_target_ip)], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                            if len(r.stdout) is not 0:
                                ice_default_host = re.sub(
                                    r"^.*src ([^ ]+) .*$", 
                                    r"\1", 
                                    str(r.stdout)
                                )
                                print("    Detected Ice.Default.Host = ", ice_default_host)
                        except:
                            print("    Could not determine Ice.Default.Host")
                    else:
                        print("    Ice.Default.Host was set to ", ice_default_host)
                        
                    ice_default_host_string = "#Ice.Default.Host=127.0.0.1"
                    if ice_default_host is not None and ice_default_host != "127.0.0.1" and ice_default_host != "localhost":
                            ice_default_host_string = "Ice.Default.Host=" + str(ice_default_host)
                            print("    " + ice_default_host_string)
                    print("    Ice   port is " + ice_port)
                    print("    Ice   host is " + ice_host)
                    print("    Mongo port is " + mongo_port)
                    print("    Mongo host is " + mongo_host)
                    
                    fh, abs_path = tempfile.mkstemp()
                    #replace
                    with os.fdopen(fh,'w') as new_file:
                         with open(symlinkTarget) as old_file:
                            for line in old_file:
                                #ice
                                line = line.replace(
                                    "Ice.Default.Locator=IceGrid/Locator:tcp -p 4061 -h localhost",
                                    "Ice.Default.Locator=IceGrid/Locator:tcp -p " + ice_port + " -h " + ice_host
                                )
                                line = line.replace(
                                    "IceGrid.Registry.Client.Endpoints=tcp -p 4061 -h localhost",
                                    "IceGrid.Registry.Client.Endpoints=tcp -p " + ice_port + " -h " + ice_host +
                                    "\n" + ice_default_host_string
                                )
                                #mongo
                                line = line.replace("ArmarX.MongoHost=localhost", "ArmarX.MongoHost="+ mongo_host)
                                line = line.replace("ArmarX.MongoPort=27017", "ArmarX.MongoPort=" + mongo_port)
                                new_file.write(line)
                    #Remove original file
                    os.remove(symlinkTarget)
                    #Move new file
                    shutil.move(abs_path, symlinkTarget)
        return True

    def update(self):
        Util.mkdir_p(self.getProfileDirectory())
        profile_directory = self.getProfileDirectory()
        # create symlinks for all necessary configuration files
        for configuration_file in self.configuration_files:
            if ".generated." in configuration_file:
                symlinkDestination = os.path.join(self.armarxUserConfigurationDirectory, configuration_file)
                symlinkTarget = os.path.join(profile_directory, configuration_file)
                templatefile = os.path.join(self.templateDirectory, configuration_file)
                if not os.path.exists(templatefile):
                    print("Template file does not exist: " + templatefile)
                    print("Aborting")
                    return False
                shutil.copyfile(templatefile, symlinkTarget)
        return True
    def remove(self):
        pass


    def link(self):
        if not self.exists():
            print("Profile does not exist: " + self.profilename)
            return

        profile_directory = self.getProfileDirectory()
        # create symlinks for all necessary configuration files
        for configuration_file in self.configuration_files:
            symlinkDestination = os.path.join(self.armarxUserConfigurationDirectory, configuration_file)
            symlinkTarget = os.path.join(profile_directory, configuration_file)
            # delete old symlink
            if os.path.lexists(symlinkDestination):
                os.remove(symlinkDestination)
            # create new symlink
            if os.path.exists(symlinkTarget):
                os.symlink(symlinkTarget, symlinkDestination)
            else:
                print("Can not create symlink to: " + symlinkTarget)
                print("File does not exist")


    def getProfileDirectory(self):
        return os.path.join(self.profiles_directory, self.profilename)


    @classmethod
    def setDirectories(cls, profiles_directory, armarxUserConfigurationDirectory, templateDirectory):
        cls.profiles_directory = profiles_directory
        cls.armarxUserConfigurationDirectory = armarxUserConfigurationDirectory
        cls.templateDirectory = templateDirectory


    @classmethod
    def setConfigurationFiles(cls, configuration_files):
        cls.configuration_files = configuration_files


    @classmethod
    def getAvailableProfiles(cls):
        profiles = []
        for profilename in os.listdir(cls.profiles_directory):
            profiles.append(profilename)
        return profiles

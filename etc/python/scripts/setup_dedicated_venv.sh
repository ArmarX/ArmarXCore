#! /bin/bash

clear=""
help=""

venv_name="venv"
reqs_file="requirements.txt"
armarx_dev="armarx-dev"

POSITIONAL=()
while [[ $# -gt 0 ]]
do
    key="$1"

    case $key in
        -h|--help)
        help=1
        shift # past argument
        ;;
        -c|--clear)
        clear=1
        shift # past argument
        ;;
        *)    # unknown option
        POSITIONAL+=("$1") # save it in an array for later
        shift # past argument
        ;;
    esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

package="$1"

if [[ -n "$help" || -z "$1" ]]; then
  echo "Usage: $0 package_name [-c]"
  echo -e "\tpackage_name \t the name of the python package (directory containing the requirements.txt)"
  echo -e "\t-c, --clear \t remove the venv beforehand if it exists"
  exit 1
fi

if [[ -d "$package" ]]; then
    echo "Entering package '$package'."
    cd "$package"
else
    echo "No such directory: $package"
    exit 2
fi

if [[ -d "$venv_name" && -n "$clear" ]]; then
    echo "Removing existing venv '$venv_name' ..."
    rm -r "$venv_name"
fi

if [[ -d "$venv_name" ]]; then
    echo "Using existing venv '$venv_name'."
    source venv/bin/activate
else
    echo "Creating venv '$venv_name'"
    python3 -m venv "$venv_name"
    source venv/bin/activate
    echo "Upgrading pip ..."
    pip install --upgrade pip
fi

if grep -q "$armarx_dev" "$reqs_file"; then
  echo "Detected "$armarx_dev" in $reqs_file."
  tmp_file="~$reqs_file.tmp"
  cp "$reqs_file" "$tmp_file"
  # Delete $armarx_dev from requirements.txt
  sed -i /$armarx_dev/d "$tmp_file"

  echo "Installing $reqs_file (without $armarx_dev) ..."
  pip install -r "$tmp_file"
  rm "$tmp_file"

  echo "Installing $armarx_dev ..."
  pip install --upgrade --index-url https://pypi.humanoids.kit.edu:443/ "$armarx_dev"

else
  echo "Installing $reqs_file ..."
  pip install -r "$reqs_file"
fi

cd ..
echo "Done."

armarx_add_library(@COMPONENT_NAME@
    SOURCES
        ./@COMPONENT_NAME@.cpp
    HEADERS
        ./@COMPONENT_NAME@.h
    DEPENDENCIES_PUBLIC
        ArmarXCoreInterfaces
        ArmarXCore
    # DEPENDENCIES_PRIVATE
        # ...
    # DEPENDENCIES_INTERFACE
        # ...
    # DEPENDENCIES_LEGACY_PUBLIC
        # ...
    # DEPENDENCIES_LEGACY_PRIVATE
        # ...
    # DEPENDENCIES_LEGACY_INTERFACE
        # ...
)

 
armarx_add_test(@COMPONENT_NAME@Test 
    TEST_FILES
        test/@COMPONENT_NAME@Test.cpp
    DEPENDENCIES
        @PACKAGE_NAME@::@COMPONENT_NAME@
)

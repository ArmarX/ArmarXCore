/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    @PACKAGE_NAME@::qt_plugins::WidgetController
 * @author     @AUTHOR_NAME@ ( @AUTHOR_EMAIL@ )
 * @date       @YEAR@
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <@INCLUDE_NAMESPACE@/qt_plugins/@COMPONENT_NAME@/ui_Widget.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>


namespace @PACKAGE_CPP_NAMESPACE@::qt_plugins::@COMPONENT_NAME@
{
    /**
    \page @PACKAGE_NAME@-GuiPlugins-@COMPONENT_NAME@ @COMPONENT_NAME@
    \brief The @COMPONENT_NAME@ allows visualizing ...

    \image html @COMPONENT_NAME@.png
    The user can

    API Documentation \ref WidgetController

    \see @COMPONENT_NAME@GuiPlugin
    */


    /**
     * \class @COMPONENT_NAME@GuiPlugin
     * \ingroup ArmarXGuiPlugins
     * \brief @COMPONENT_NAME@GuiPlugin brief description
     *
     * Detailed description
     */

    /**
     * \class WidgetController
     * \brief WidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        WidgetController :
        public armarx::ArmarXComponentWidgetControllerTemplate<WidgetController>
    {
        Q_OBJECT

    public:

        /// Controller constructor.
        explicit WidgetController();
        /// Controller destructor.
        virtual ~WidgetController();

        //// @see ArmarXWidgetController::loadSettings()
        void loadSettings(QSettings* settings) override;
        /// @see ArmarXWidgetController::saveSettings()
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName();

        /// @see armarx::Component::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::Component::onConnectComponent()
        void onConnectComponent() override;


    public slots:
        /* QT slot declarations */


    signals:
        /* QT signal declarations */


    private:

        /// Widget Form
        Ui::Widget widget;

    };
}

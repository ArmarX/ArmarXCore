/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    @PACKAGE_NAME@::qt_plugins::WidgetController
 * \author     @AUTHOR_NAME@ ( @AUTHOR_EMAIL@ )
 * \date       @YEAR@
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "WidgetController.h"


namespace @PACKAGE_CPP_NAMESPACE@::qt_plugins::@COMPONENT_NAME@
{

    QString WidgetController::GetWidgetName()
    {
        return "@COMPONENT_NAME@";
    }


    WidgetController::WidgetController()
    {
        widget.setupUi(getWidget());
    }


    WidgetController::~WidgetController()
    {
    }


    void WidgetController::loadSettings(QSettings* settings)
    {
        (void) settings;
    }

    void WidgetController::saveSettings(QSettings* settings)
    {
        (void) settings;
    }


    void WidgetController::onInitComponent()
    {
    }


    void WidgetController::onConnectComponent()
    {
    }

}

/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    @PACKAGE_NAME@::@LIBRARY_NAME@
 * @author     @AUTHOR_NAME@ ( @AUTHOR_EMAIL@ )
 * @date       @YEAR@
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "@LIBRARY_NAME@StatechartContext.h"

//#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
//#include <RobotAPI/libraries/core/Pose.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/statechart/Statechart.h>



namespace armarx::@LIBRARY_NAME@
{
    void @LIBRARY_NAME@StatechartContext::onInitStatechartContext()
    {
        // Register dependencies
        //        usingProxy(getProperty<std::string>("KinematicUnitName").getValue());
        //        usingProxy(getProperty<std::string>("KinematicUnitObserverName").getValue());
        //        usingProxy(getProperty<std::string>("RobotStateComponentName").getValue());
        //        usingProxy(getProperty<std::string>("TCPControlUnitName").getValue());
    }

    void @LIBRARY_NAME@StatechartContext::onConnectStatechartContext()
    {
        // retrieve proxies
        //        robotStateComponent = getProxy<RobotStateComponentInterfacePrx>(getProperty<std::string>("RobotStateComponentName").getValue());
        //        kinematicUnitPrx = getProxy<KinematicUnitInterfacePrx>(getProperty<std::string>("KinematicUnitName").getValue());
        //        kinematicUnitObserverPrx = getProxy<KinematicUnitObserverInterfacePrx>(getProperty<std::string>("KinematicUnitObserverName").getValue());
        //        tcpControlPrx = getProxy<TCPControlUnitInterfacePrx>(getProperty<std::string>("TCPControlUnitName").getValue());

        //        // initialize remote robot
        //        remoteRobot.reset(new RemoteRobot(robotStateComponent->getSynchronizedRobot()));
    }

    PropertyDefinitionsPtr @LIBRARY_NAME@StatechartContext::createPropertyDefinitions()
    {
        return PropertyDefinitionsPtr(new @LIBRARY_NAME@StatechartContextProperties(
                                          getConfigIdentifier()));
    }
}

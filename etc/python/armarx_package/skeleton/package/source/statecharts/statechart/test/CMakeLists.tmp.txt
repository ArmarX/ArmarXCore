
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore @COMPONENT_NAME@)
 
armarx_add_test(@COMPONENT_NAME@Test @COMPONENT_NAME@Test.cpp "${LIBS}")
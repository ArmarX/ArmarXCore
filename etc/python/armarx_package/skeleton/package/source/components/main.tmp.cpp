#include <ArmarXCore/libraries/DecoupledSingleComponent/DecoupledMain.h>

int main(int argc, char* argv[])
{
    return armarx::DecoupledMain(argc, argv);
}

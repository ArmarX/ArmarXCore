armarx_component_set_name("@COMPONENT_NAME@")


set(COMPONENT_LIBS
    ArmarXCore
    DecoupledSingleComponent
    ArmarXGuiComponentPlugins
)

set(SOURCES
    #@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.h
    #@TEMPLATE_LINE@@COMPONENT_PATH@/@COMPONENT_NAME@.cpp
    main.cpp
)


armarx_add_component_executable("${SOURCES}")

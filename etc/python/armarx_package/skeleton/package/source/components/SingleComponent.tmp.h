/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    @PACKAGE_NAME@::ArmarXObjects::@COMPONENT_NAME@
 * @author     @AUTHOR_NAME@ ( @AUTHOR_EMAIL@ )
 * @date       @YEAR@
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

#include <ArmarXCore/core/Component.h>

namespace armarx
{

    using namespace armarx::RemoteGui::Client;
    struct @COMPONENT_NAME@Tab : Tab
    {
        Button printStuff;
    };


    struct @COMPONENT_NAME@
            : virtual armarx::Component
            , virtual armarx::LightweightRemoteGuiComponentPluginUser
    {
        std::string getDefaultName() const override;

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

        void createRemoteGuiTab();
        void RemoteGui_update() override;

        @COMPONENT_NAME@Tab tab;

        struct Properties{
            // std::string example_property = "Example";
        } properties;
    };
}

/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    @PACKAGE_NAME@::ArmarXObjects::@COMPONENT_NAME@
 * @author     @AUTHOR_NAME@ ( @AUTHOR_EMAIL@ )
 * @date       @YEAR@
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "@COMPONENT_NAME@.h"

// #include <Eigen/Core>

// #include <pcl/filters/filter.h>

#include <RobotAPI/libraries/core/Pose.h>


namespace armarx
{

    armarx::PropertyDefinitionsPtr @COMPONENT_NAME@::createPropertyDefinitions()
    {
        auto def = PointCloudProcessor::createPropertyDefinitions(getConfigIdentifier());

        // topic subscriber
        // ... def->topic<PlatformUnitListener>(...)

        // proxies
        // ... def->component(...)

        // required properties
        // ... def->required(properties.i, ...)

        // optional properties
        // ... def->optional(...)


        def->topic(debugObserver);

        return def;
    }


    void @COMPONENT_NAME@::onInitPointCloudProcessor()
    {
    }


    void @COMPONENT_NAME@::onConnectPointCloudProcessor()
    {
        enableResultPointClouds<PointT>();
        // Add a name to provide more than one result point cloud.
        // enableResultPointClouds<PointT>("ResultProviderName");
    }


    void @COMPONENT_NAME@::onDisconnectPointCloudProcessor()
    {
    }

    void @COMPONENT_NAME@::onExitPointCloudProcessor()
    {
    }


    void @COMPONENT_NAME@::process()
    {
        // Fetch input point cloud.
        pcl::PointCloud<PointT>::Ptr inputCloud(new pcl::PointCloud<PointT>());

        if (waitForPointClouds())
        {
            getPointClouds<PointT>(inputCloud);
        }
        else
        {
            ARMARX_VERBOSE << "Timeout or error while waiting for point cloud data.";
            return;
        }

        // Do processing.

        StringVariantBaseMap debugValues;
        debugValues["Point Cloud Timestamp"] = new Variant(static_cast<int>(inputCloud->header.stamp));
        debugObserver->setDebugChannel(getName(), debugValues);


        // Publish result point cloud.

        provideResultPointClouds<PointT>(inputCloud);
        // provideResultPointClouds<PointT>(inputCloud, "ResultProviderName");
    }


    std::string @COMPONENT_NAME@::getDefaultName() const
    {
        return "@COMPONENT_NAME@";
    }


}  // namespace armarx

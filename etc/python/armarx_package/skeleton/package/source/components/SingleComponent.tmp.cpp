/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    @PACKAGE_NAME@::ArmarXObjects::@COMPONENT_NAME@
 * @author     @AUTHOR_NAME@ ( @AUTHOR_EMAIL@ )
 * @date       @YEAR@
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "@COMPONENT_NAME@.h"

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

namespace armarx
{
    ARMARX_DECOUPLED_REGISTER_COMPONENT(@COMPONENT_NAME@);

    armarx::PropertyDefinitionsPtr @COMPONENT_NAME@::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def = new ComponentPropertyDefinitions(getConfigIdentifier());

        // def->optional(properties.example_property, "ExampleProperty", "Example property");

        return def;
    }


    void @COMPONENT_NAME@::onInitComponent()
    {
    }


    void @COMPONENT_NAME@::onConnectComponent()
    {
        createRemoteGuiTab();
        RemoteGui_startRunningTask();
    }


    void @COMPONENT_NAME@::onDisconnectComponent()
    {

    }


    void @COMPONENT_NAME@::onExitComponent()
    {

    }

    void @COMPONENT_NAME@::createRemoteGuiTab()
    {
        GridLayout grid;
        int row = 0;
        {
            tab.printStuff.setLabel("Print Stuff");
            grid.add(tab.printStuff, Pos{row, 0}, Span{1, 2});
            row += 1;
        }

        RemoteGui_createTab("@COMPONENT_NAME@", grid, &tab);
    }

    void @COMPONENT_NAME@::RemoteGui_update()
    {
        if (tab.printStuff.wasClicked())
        {
            ARMARX_IMPORTANT << "The button was pressed!";
        }
    }


    std::string @COMPONENT_NAME@::getDefaultName() const
    {
        return "@COMPONENT_NAME@";
    }

}

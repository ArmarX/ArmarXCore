##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::tools
# @author     Jan Issac (jan dot issac at gmail dot com)
# @date       2013
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

from argparse import RawTextHelpFormatter
from argparse import HelpFormatter
from armarx_package.SubCommand import SubCommand
from armarx_package.ManpageFormatter import ManpageFormatter
from armarx_package.Exceptions import UnknownSubCommandException

from armarx_package.Manpage import Manpage

from argcomplete.completers import ChoicesCompleter

class HelpSubCmd(SubCommand):

    ##
    # help sub-command constructor
    #
    def __init__(self, parserContainer):

        self.name = 'help'

        self.manpage = Manpage()
        self.manpage.setProgName('armarx-package')
        self.manpage.setName(self.name)
        self.manpage.setDescription('''
Display help content of armarx-package tool or of a specific sub-command.
This is equivalent to the --help option on each command.
''')

        self.manpage.setBriefDescription('Create an empty ArmarX package')
        self.manpage.setExamples('''
.B 1.
Get help for armarx-package

    $ armarx-package help

.B 2.
Get help content of a sub-command (e.g. init)

    $ armarx-package help init

Alternativly also possible

    $ armarx-package init --help
''')

        self.parser = parserContainer.add_parser(
                               self.name,
                               help="Detailed help content",
                               formatter_class=RawTextHelpFormatter,
                               epilog='Checkout \'armarx-package help help\''\
                                      ' for more details and examples!')

        self.parser.add_argument("subCommand",
                                 help="Get help for the specified sub-command")\
                                 .completer = ChoicesCompleter(('init',
                                                                'add',
                                                                'remove',
                                                                'status',
                                                                'cpp-class',
                                                                'help'))

        self.parser.set_defaults(func=self.execute)

        self.addVerboseArgument(self.parser)


    def execute(self, args):
        try:
            if args.subCommand in args.commands:
                manpage = args.commands[args.subCommand].getManpage()
                parser = args.commands[args.subCommand].getParser()

                # alter the formatter to Manpage formatter
                parser.formatter_class = ManpageFormatter

                # generate and set the arguments/options section
                formatter = parser._get_formatter()
                for action_group in parser._action_groups:
                    formatter.start_section(action_group.title)
                    formatter.add_text(action_group.description)
                    formatter.add_arguments(action_group._group_actions)
                    formatter.end_section()

                manpage.setOptions(formatter.format_help())

                # set synopsis
                manpage.setSynopsis(parser.format_usage())

                # display man page
                manpage.display()

            else:
                raise UnknownSubCommandException(args.subCommand)
        except Exception as exc:
            self.handleException(args, exc)

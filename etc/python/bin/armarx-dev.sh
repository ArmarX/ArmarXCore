#!/bin/bash

own_dir=$(dirname "$(readlink -f "${BASH_SOURCE:-$0}")")
source "$own_dir"/_call_python_script_in_env.sh

_call_python_script_in_env armarx-dev.py "$@"

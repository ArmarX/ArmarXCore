#!/bin/bash

function _call_python_script_in_env()
{
  # The name of the python script to call.
  script_name="$1"
  shift

  # This file's path.
  own_path=$(readlink -f "${BASH_SOURCE:-$0}")
  # This file's directory.
  own_dir="$(dirname "$own_path")"
  # The "python" root directory.
  root_dir="$(dirname "$own_dir")"

  # Find the environment.
  if [[ -d "$root_dir/.venv" ]]; then
    venv="$root_dir/.venv"
  elif [[ -d "$root_dir/venv" ]]; then
    venv="$root_dir/venv"
  else
    # Fall back to system environment.
    venv=""
  fi

  # echo "venv: '$venv'"

  # Activate the environment.
  if [[ -d "$venv" ]]; then
    source "$venv/bin/activate"
  fi

  # Add the python package to PYTHONPATH
  # (might not be necessary as the script is in the right directory).
  export PYTHONPATH="$root_dir:$PYTHONPATH"

  # Call the python script.
  "$root_dir"/"$script_name" "$@"
}

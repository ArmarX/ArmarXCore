#!/usr/bin/env python3
from setuptools import setup, find_packages
import __version__ as v
# from pip.req import parse_requirements

# install_reqs = parse_requirements("requirements.txt")
# test_req = parse_requirements("test_requirements.txt")
# reqs = [str(i.req) for i in install_reqs]
# test_reqs = [str(i.req) for i in test_req]

setup(
    name="armarx",
    version=v.armarx_version,
    description="Python tools for ArmarX",
    author="The ArmarX development team",
    author_email="waechter@kit.edu",
    url="https://armarx.humanoids.kit.edu/",
    # install_requires=reqs,
    # tests_require=test_reqs,
    install_requires=[
        'argcomplete',
        'configparser',
        'zeroc-ice',
        'psutil',
    ],
    tests_require=[
        'nose',
        'mock',
    ],
    test_suite="nose.collector",
    packages=find_packages(exclude=["tests"]),
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'armarx-version = armarx_cli.armarx_version:main',
            'armarx-build = armarx_cli.armarx_build:main',
            'armarx = armarx_cli.armarx_control:main_user',
            'armarx-dev = armarx_cli.armarx_control:main_developer',
            'armarx-package = armarx_package.armarx_package_tool:main'
        ],
    },
)

#!/bin/bash


if [ -z "${ARMARX_WORKSPACE}" ]; then
    ARMARX_DEFAULTS_DIR="$HOME/.armarx/"
else
    if [-z "${ARMARX_CONFIG_DIR_NAME}" ]; then
        ARMARX_DEFAULTS_DIR="${ARMARX_WORKSPACE}/${ARMARX_CONFIG_DIR_NAME}/"
    else
        ARMARX_DEFAULTS_DIR="${ARMARX_WORKSPACE}/armarx_config/"
    fi
fi

ARMARX_USER_VARIABLES_FILE="IceGridUserVariables.sh"
ARMARX_USER="armar-user"
WHERE="$(dirname $0)"
PROGNAME="$(basename $0)"

if [ -e "${ARMARX_DEFAULTS_DIR}/${ARMARX_USER_VARIABLES_FILE}" ]; then
	echo "Using ${ARMARX_DEFAULTS_DIR}/${ARMARX_USER_VARIABLES_FILE}"
else
	echo "Creating ${ARMARX_DEFAULTS_DIR}/${ARMARX_USER_VARIABLES_FILE}"
	mkdir -p ${ARMARX_DEFAULTS_DIR}
	touch ${ARMARX_DEFAULTS_DIR/${ARMARX_USER_VARIABLES_FILE}}

	echo "# user configurable variables used by bash scripts like startApplication.sh" >> "${ARMARX_DEFAULTS_DIR}/${ARMARX_USER_VARIABLES_FILE}"
	echo "export ICE_GRID_REGISTRY_PORT=4061" >> "${ARMARX_DEFAULTS_DIR}/${ARMARX_USER_VARIABLES_FILE}"
	echo "export ICE_GRID_REGISTRY_HOST=armar4-left" >> "${ARMARX_DEFAULTS_DIR}/${ARMARX_USER_VARIABLES_FILE}"
	echo "export ICE_GRID_REGISTRY_NAME=NodeMain" >> "${ARMARX_DEFAULTS_DIR}/${ARMARX_USER_VARIABLES_FILE}"
	echo "export ICE_GRID_NODE1_HOST=armar4-right" >> "${ARMARX_DEFAULTS_DIR}/${ARMARX_USER_VARIABLES_FILE}"
	echo "export ICE_GRID_NODE1_NAME=Node1" >> "${ARMARX_DEFAULTS_DIR}/${ARMARX_USER_VARIABLES_FILE}"
	echo "export ICE_GRID_NODE2_HOST=armar4-head" >> "${ARMARX_DEFAULTS_DIR}/${ARMARX_USER_VARIABLES_FILE}"
	echo "export ICE_GRID_NODE2_NAME=Node2" >> "${ARMARX_DEFAULTS_DIR}/${ARMARX_USER_VARIABLES_FILE}"
	echo "export MONGODB_PORT=27017" >> "${ARMARX_DEFAULTS_DIR}/${ARMARX_USER_VARIABLES_FILE}"
	echo "export MONGODB_HOST=armar4-right" >> "${ARMARX_DEFAULTS_DIR}/${ARMARX_USER_VARIABLES_FILE}"
fi

# import user defined variables
source ${ARMARX_DEFAULTS_DIR}/${ARMARX_USER_VARIABLES_FILE}

if [ -f $HOME/.ssh/ice-start ]; then
	SSH_KEY="$HOME/.ssh/ice-start"
else
	SSH_KEY="$WHERE/ice-start"
fi
[[ $(stat -c %a "$SSH_KEY") == 400 ]] || { chmod 400 "$SSH_KEY" || exit; }

if [[ "$PROGNAME" == "ice-reset.sh" ]]; then
	ARGS="reset"
else
	ARGS=""
fi

HOSTNAME=$(hostname)
case $HOSTNAME in
	armar4-left*)
		NODE_NAME="$ICE_GRID_REGISTRY_NAME"
		HOST="$ICE_GRID_REGISTRY_HOST"
		;;
	armar4-right*)
		NODE_NAME="$ICE_GRID_NODE1_NAME"
		HOST="$ICE_GRID_NODE1_HOST"
		;;
	armar4-head*)
		NODE_NAME="$ICE_GRID_NODE2_NAME"
		HOST="$ICE_GRID_NODE2_HOST"
		;;
esac

isIceNodeRunning() {
	# Check if an IceGridNode is running by doing a 'node ping'
	# redirect output to /dev/null to not confuse users since an error is expected
	# if the node does not exist yet
	local NodeStatus=$(icegridadmin --Ice.Default.Locator="IceGrid/Locator:tcp -p ${ICE_GRID_REGISTRY_PORT} -h ${ICE_GRID_REGISTRY_HOST}" \
		-u x -p y -e "node ping ${1}" 2>&1)
	if [[ $NodeStatus == "node is up" ]]; then
		return 0
	else
		return 1
	fi
}

for client in ICE_GRID_REGISTRY ICE_GRID_NODE1 ICE_GRID_NODE2; do
	name="${client}_NAME"
	if isIceNodeRunning ${!name}; then
		${WHERE}/ice-stop.sh
	fi
	while isIceNodeRunning ${!name}; do
		sleep 10
		echo "Waiting for ${!name} to shut down."
	done
done

for client in ICE_GRID_REGISTRY ICE_GRID_NODE1 ICE_GRID_NODE2; do
	host="${client}_HOST"
	ssh -o UserKnownHostsFile="${WHERE}/known_hosts" -i "$SSH_KEY" -n "${ARMARX_USER}@${!host}" "$ARGS" &
done

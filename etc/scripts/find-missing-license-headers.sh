#!/bin/bash
#set -x

files_to_check=$(find . -type f)

for f in $files_to_check;
do
    if ! grep -q 'This file is part of ArmarX' $f 
    then
        echo $f
    fi
done

#set +x

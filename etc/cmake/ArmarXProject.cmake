cmake_minimum_required(VERSION 3.10.2)

if(NOT "$ENV{CMAKE_UNITY_BUILD_BATCH_SIZE}" STREQUAL "")
    message(STATUS "CMAKE_UNITY_BUILD_BATCH_SIZE -> $ENV{CMAKE_UNITY_BUILD_BATCH_SIZE}")
    set(CMAKE_UNITY_BUILD ON)
    set(CMAKE_UNITY_BUILD_BATCH_SIZE $ENV{CMAKE_UNITY_BUILD_BATCH_SIZE})
endif()

if (${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR})
    message( FATAL_ERROR "In-source builds not allowed. Please make a new directory (e.g. build) and run CMake from there. You may need to remove CMakeCache.txt." )
endif()

option(ARMARX_USE_ARMARX_GLOBAL_CMAKE_BUILD_TYPE "Use the environment variable 'ARMARX_GLOBAL_CMAKE_BUILD_TYPE' to determine the build type" ON)

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "Release") #default
    if(ARMARX_USE_ARMARX_GLOBAL_CMAKE_BUILD_TYPE AND DEFINED ENV{ARMARX_GLOBAL_CMAKE_BUILD_TYPE})
        message(STATUS "Using env var ARMARX_GLOBAL_CMAKE_BUILD_TYPE for build type")
        set(CMAKE_BUILD_TYPE "$ENV{ARMARX_GLOBAL_CMAKE_BUILD_TYPE}")
    endif()

    set(CMAKE_BUILD_TYPE "${CMAKE_BUILD_TYPE}" CACHE STRING
       "Choose the type of build, options are: Debug Release RelWithDebInfo MinSizeRel."
       FORCE)
    SET_PROPERTY(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release" "MinSizeRel" "RelWithDebInfo")
endif()

message(STATUS "CMAKE_BUILD_TYPE = ${CMAKE_BUILD_TYPE}")

##########################################################################
#simox setup
##########################################################################
#get simox as dependency (we want to inherit Simox_BOOST_VERSION)
set(ArmarX_Simox_VERSION 2.3.72)
find_package(Simox ${ArmarX_Simox_VERSION} REQUIRED)
list(APPEND CPACK_DEBIAN_PACKAGE_DEPENDS "simox (= ${ArmarX_Simox_VERSION})")

SET(ArmarX_BOOST_VERSION ${Simox_BOOST_VERSION})
##########################################################################
# include required for ExternalProject_Add()
##########################################################################
include(ExternalProject)
if(DEFINED ARMARX_ENABLE_DEPENDENCY_VERSION_CHECK_DEFAULT)
    option(ARMARX_ENABLE_DEPENDENCY_VERSION_CHECK "Throws an error if dependent ArmarX packages have differing version number (set to FALSE to disable)" ${ARMARX_ENABLE_DEPENDENCY_VERSION_CHECK_DEFAULT})
else()
    option(ARMARX_ENABLE_DEPENDENCY_VERSION_CHECK "Throws an error if dependent ArmarX packages have differing version number (set to FALSE to disable)" FALSE)
endif()
# Set variable here (written in depends_on_armarx_package() and setupDirs.cmake)
# Otherwise they will get overwritten by the other macros, thus leading to missing dependencies
set(ARMARX_PROJECT_DEPENDENCIES "ArmarXCore" CACHE INTERNAL "Project Dependencies on other ArmarX Packages" FORCE)
set(ARMARX_PROJECT_DEPENDENT_DATA_DIRS "" CACHE INTERNAL "Collection of all *_DATA_DIR entries of all dependent projects")
set(ARMARX_PROJECT_DISABLED_TARGETS "" CACHE INTERNAL "Disabled Targets in current ArmarX Package" FORCE)
set(ARMARX_PROJECT_DISABLED_DEPENDENT_TARGETS "${ArmarXCore_DISABLED_TARGETS}" CACHE INTERNAL "Disabled Targets from dependent ArmarX Packages" FORCE)
set(ARMARX_PROJECT_DEPENDENT_TAG_FILES "ArmarXCore.tag=${ArmarXCore_DOXYGEN_TAG_FILE}" CACHE INTERNAL "Dependent Doxygen Tag files for linking with documentation of other packages" FORCE)
set(ARMARX_MISSING_PROJECT_DEPENDENCIES "" CACHE INTERNAL "Missing Project Dependencies on other ArmarX Packages")
if(DEFINED ArmarXCore_SOURCE_DIR)
    if(NOT "${ARMARX_PROJECT_NAME}" EQUAL "ArmarXCore")
        set(ARMARX_PROJECT_SOURCE_PACKAGE_DEPENDENCIES "ArmarXCore" CACHE INTERNAL "Project Source Dependencies on other ArmarX Packages")
    endif()
else()
    set(ARMARX_PROJECT_SOURCE_PACKAGE_DEPENDENCIES "" CACHE INTERNAL "Project Source Dependencies on other ArmarX Packages")
endif()
set(ARMARX_PROJECT_PACKAGE_DEPENDENCY_PATHS "" CACHE INTERNAL "Project Source Dependencies with paths on other ArmarX Packages")
# Use 'make dependencies' to build all external project dependencies
add_custom_target(dependencies
    COMMAND echo "Building external dependencies: ${ARMARX_PROJECT_DEPENDENCIES}"
    COMMENT ""
)


# TODO: Remove this after eventually migrating ArmarXCore to a modern cmake project.
set(ARMARX_LEGACY_CMAKE_PROJECT YES)
macro(armarx_enable_modern_cmake_project)
    message(WARNING "Enabling modern CMake project. Please be aware that the macro 'armarx_enable_modern_cmake_project' will be removed once modern CMake projects will become default in ArmarX.")
    set(ARMARX_LEGACY_CMAKE_PROJECT NO)
endmacro()


macro(armarx_project)
    message(STATUS "\n== Setting up ArmarX project ...")

    # First argument is required positional argument for the project name.
    set(arglist "${ARGN}")
    list(GET arglist 0 NAME)
    list(REMOVE_AT arglist 0)
    set(ARMARX_PROJECT_NAME "${NAME}")

    if(${ARMARX_LEGACY_CMAKE_PROJECT})
        message(STATUS "ArmarX legacy package.")
        set(ARMARX_PROJECT_INCLUDE_PATH "${ARMARX_PROJECT_NAME}")
    else()
        set(multi_param)
        set(single_param
            NAMESPACE)
        set(flag_param
            LEGACY_CMAKE_PROJECT)
        cmake_parse_arguments(AX "${flag_param}" "${single_param}" "${multi_param}" "${arglist}")

        set(ARMARX_PROJECT_NAMESPACE "${AX_NAMESPACE}")
        set(ARMARX_PROJECT_NAME_RAW "${ARMARX_PROJECT_NAME}")

        if(NOT "${ARMARX_PROJECT_NAMESPACE}" STREQUAL "")
            set(ARMARX_PROJECT_NAME_PRETTY "${ARMARX_PROJECT_NAMESPACE}::${ARMARX_PROJECT_NAME}")
            set(ARMARX_PROJECT_NAME "${ARMARX_PROJECT_NAMESPACE}_${ARMARX_PROJECT_NAME}")
            set(ARMARX_PROJECT_INCLUDE_PATH "${ARMARX_PROJECT_NAMESPACE}/${ARMARX_PROJECT_NAME_RAW}")
        else()
            # ARMARX_PROJECT_NAME is intentionally left unchanged
            set(ARMARX_PROJECT_NAME_PRETTY "${ARMARX_PROJECT_NAME}")
            set(ARMARX_PROJECT_INCLUDE_PATH "${ARMARX_PROJECT_NAME}")
        endif()
        message(STATUS "ArmarX next generation package.")
    endif()
    unset(arglist)

    message(STATUS "Configuring ArmarX project `${ARMARX_PROJECT_NAME}`.")

    # The variable ArmarXCore_CMAKE_DIR gets set in ProjectConfig.cmake.in
    # or in the toplevel CMakeLists.txt of ArmarXCore
    
    set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
    project("${ARMARX_PROJECT_NAME}" CXX)
    set(${ARMARX_PROJECT_NAME}_FOUND ON)

    # Add dependency on ArmarXCore but not for ArmarXCore itself
    if(NOT "${ARMARX_PROJECT_NAME}" STREQUAL "ArmarXCore")
        if(DEFINED ArmarXCore_SOURCE_DIR)
            add_external_dependency(ArmarXCore)
        endif()
        list(APPEND CMAKE_MODULE_PATH "${ArmarXCore_CMAKE_DIR}")

        set(ARMARX_PROJECT_PACKAGE_DEPENDENCY_PATHS "ArmarXCore:${ArmarXCore_PACKAGE_CONFIG_DIR}" CACHE INTERNAL "Project Source Dependencies with paths on other ArmarX Packages")
    endif()

    if(EXISTS "${PROJECT_SOURCE_DIR}/etc/cmake/")
        set(${PROJECT_NAME}_CMAKE_DIR "${PROJECT_SOURCE_DIR}/etc/cmake/")
        list(APPEND CMAKE_MODULE_PATH "${${PROJECT_NAME}_CMAKE_DIR}")
        set(PROJECT_USE_FILE "${${PROJECT_NAME}_CMAKE_DIR}/Use${PROJECT_NAME}.cmake")
    endif()

    # Include ArmarX CMake scripts.
    if(${ARMARX_LEGACY_CMAKE_PROJECT})
        set(ArmarXCore_CMAKE_SCRIPTS_DIR "${ArmarXCore_CMAKE_DIR}/legacy")
    else()
        set(ArmarXCore_CMAKE_SCRIPTS_DIR "${ArmarXCore_CMAKE_DIR}/latest")
    endif()
    include("${ArmarXCore_CMAKE_SCRIPTS_DIR}/setup.cmake")

    # include the macros defined in the package specific Use${ARMARX_PROJECT_NAME}.cmake
    if(NOT "${ARMARX_PROJECT_NAME}" STREQUAL "ArmarXCore")
        if (EXISTS ${PROJECT_USE_FILE})
            include(${PROJECT_USE_FILE})
        endif()
    endif()

    if(NOT "${ARMARX_PROJECT_NAME}" STREQUAL "ArmarXCore")
        set(ARMARX_PROJECT_DEPENDENT_DATA_DIRS "${ARMARX_PROJECT_DEPENDENT_DATA_DIRS}" "${PROJECT_DATA_DIR}" "${ArmarXCore_DATA_DIR}" CACHE INTERNAL "Collection of all *_DATA_DIR entries of all dependent projects")
    else()
        set(ARMARX_PROJECT_DEPENDENT_DATA_DIRS "${PROJECT_DATA_DIR}" CACHE INTERNAL "Collection of all *_DATA_DIR entries of all dependent projects")
    endif()
    # This variable is needed in Installation.cmake for setting
    # PROJECT_INCLUDE_DIRS
    # the last entry is necessary for #include directives inside the generated slice files
    set(PROJECT_INCLUDE_DIRECTORIES ${PROJECT_SOURCECODE_DIR} ${PROJECT_BINARY_DIR}/source)

    string(REPLACE ";" "/" DEF_DEPENDENCIES "${ARMARX_PROJECT_DEPENDENCIES}")
    add_definitions(-DDEPENDENCIES=${DEF_DEPENDENCIES})

    foreach(dir ${ArmarXCore_INCLUDE_DIRS})
        string(REGEX MATCHALL ^.*/build/source$ matched "${dir}")
        if(matched)
            include_directories(SYSTEM ${dir})
        else()
            include_directories(${dir})
        endif()
    endforeach()

    foreach(dir ${PROJECT_INCLUDE_DIRECTORIES})
        string(REGEX MATCHALL ^.*/build/source$ matched "${dir}")
        if(matched)
            include_directories(SYSTEM ${dir})
        else()
            include_directories(${dir})
        endif()
    endforeach()

    link_directories(${ARMARX_LIB_DIR} ${ArmarXCore_LIBRARY_DIRS})

    # make path absolute
    get_filename_component(${PROJECT_NAME}_CMAKE_DIR "${${PROJECT_NAME}_CMAKE_DIR}" ABSOLUTE)
    add_custom_target(generated_files)
endmacro()


# This macro is called via depends_on_armarx_package() and allows the rebuilding
# of ArmarX package dependencies with 'make dependencies'
macro(add_external_dependency DEPENDENCY_NAME)
    if(${ARMARX_LEGACY_CMAKE_PROJECT})
        add_external_dependency_legacy(${DEPENDENCY_NAME})
    else()
        add_external_dependency_latest(${DEPENDENCY_NAME})
    endif()
endmacro()


macro(add_external_dependency_latest DEPENDENCY_NAME)
    set(DEPENDENCY_TARGET_NAME "ExternalDependency_${DEPENDENCY_NAME}")
    set(DEPENDENCY_TARGET_SOURCE_DIR "${${DEPENDENCY_NAME}_SOURCE_DIR}")
    set(DEPENDENCY_TARGET_BUILD_DIR "${${DEPENDENCY_NAME}_BUILD_DIR}")

    # add ${DEPENDENCY} as an external build target
    # build the dependencies of ${DEPENDENCY} first, then ${DEPENDENCY} itself
    ExternalProject_Add(${DEPENDENCY_TARGET_NAME}
        PREFIX ${DEPENDENCY_TARGET_SOURCE_DIR}
        SOURCE_DIR ${DEPENDENCY_TARGET_SOURCE_DIR}
        BINARY_DIR ${DEPENDENCY_TARGET_BUILD_DIR}
        TMP_DIR ${DEPENDENCY_TARGET_BUILD_DIR}/cmake-tmp
        STAMP_DIR ${DEPENDENCY_TARGET_BUILD_DIR}/cmake-stamp
        BUILD_IN_SOURCE 0
        BUILD_COMMAND ${CMAKE_BUILD_TOOL} dependencies all
        INSTALL_COMMAND echo "Not installing"
    )

    # This FORCED build step is required
    # without it the dependencies won't be rebuild by cmake
    ExternalProject_Add_Step(${DEPENDENCY_TARGET_NAME} forcebuild
        COMMAND ${CMAKE_COMMAND} -E echo "Force build of ${DEPENDENCY_TARGET_NAME}"
        DEPENDEES configure
        DEPENDERS build
        ALWAYS 1
    )

    # execute this dependency before the collective one
    add_dependencies(dependencies ${DEPENDENCY_TARGET_NAME})

    # exclude the target for external dependencies from the standard execution of 'make'
    set_target_properties(${DEPENDENCY_TARGET_NAME} PROPERTIES EXCLUDE_FROM_ALL TRUE)
endmacro()


macro(add_external_dependency_legacy DEPENDENCY_NAME)
    set(DEPENDENCY_TARGET_NAME "ExternalDependency_${DEPENDENCY_NAME}")
    set(DEPENDENCY_TARGET_SOURCE_DIR "${${DEPENDENCY_NAME}_SOURCE_DIR}")
    set(DEPENDENCY_TARGET_BUILD_DIR "${${DEPENDENCY_NAME}_BUILD_DIR}")

    # add ${DEPENDENCY} as an external build target
    # build the dependencies of ${DEPENDENCY} first, then ${DEPENDENCY} itself
    ExternalProject_Add(${DEPENDENCY_TARGET_NAME}
        PREFIX ${DEPENDENCY_TARGET_SOURCE_DIR}
        SOURCE_DIR ${DEPENDENCY_TARGET_SOURCE_DIR}
        BINARY_DIR ${DEPENDENCY_TARGET_BUILD_DIR}
        TMP_DIR ${DEPENDENCY_TARGET_BUILD_DIR}/cmake-tmp
        STAMP_DIR ${DEPENDENCY_TARGET_BUILD_DIR}/cmake-stamp
        BUILD_IN_SOURCE 0
        BUILD_COMMAND ${CMAKE_BUILD_TOOL} dependencies all
        INSTALL_COMMAND echo "Not installing"
    )

    # This FORCED build step is required
    # without it the dependencies won't be rebuild by cmake
    ExternalProject_Add_Step(${DEPENDENCY_TARGET_NAME} forcebuild
        COMMAND ${CMAKE_COMMAND} -E echo "Force build of ${DEPENDENCY_TARGET_NAME}"
        DEPENDEES configure
        DEPENDERS build
        ALWAYS 1
    )

    # execute this dependency before the collective one
    add_dependencies(dependencies ${DEPENDENCY_TARGET_NAME})

    # exclude the target for external dependencies from the standard execution of 'make'
    set_target_properties(${DEPENDENCY_TARGET_NAME} PROPERTIES EXCLUDE_FROM_ALL TRUE)
endmacro()

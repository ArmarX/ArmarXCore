# this will generate dependency graphs for all targets

#cleanup old output
file(REMOVE_RECURSE ${CMAKE_TARGET_DEPENDENCY_GRAPH_DIR})
file(MAKE_DIRECTORY ${CMAKE_TARGET_DEPENDENCY_GRAPH_DIR})
#plot without external deps
file(WRITE CMakeGraphVizOptions.cmake "set(GRAPHVIZ_EXTERNAL_LIBS FALSE)")
execute_process(COMMAND ${CMAKE_COMMAND} --graphviz=${CMAKE_TARGET_DEPENDENCY_GRAPH_DIR}/deps.dot ..          OUTPUT_QUIET)
#plot with external deps
file(WRITE CMakeGraphVizOptions.cmake "set(GRAPHVIZ_EXTERNAL_LIBS TRUE)")
execute_process(COMMAND ${CMAKE_COMMAND} --graphviz=${CMAKE_TARGET_DEPENDENCY_GRAPH_DIR}/deps.dot.external .. OUTPUT_QUIET)

#render

file(GLOB_RECURSE DOT_FILES "deps.dot*")
foreach(DOT_FILE ${DOT_FILES})
    execute_process(COMMAND dot -Tsvg -O "${DOT_FILE}")
    if(NOT KEEP_DOT_FILES)
        file(REMOVE ${DOT_FILE})
    endif(NOT KEEP_DOT_FILES)
endforeach(DOT_FILE)

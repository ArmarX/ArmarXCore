# This macro uses find_package() to search for ${DEPENDENCY} with the REQUIRED mode.
# Add a second argument to the macro parameters to search for optional packages with the QUIET mode.
#
# If the dependency was found it adds ${DEPENDENCY}_INCLUDE_DIRS
# via include_directories() and the ${DEPENDENCY} itself to ${ARMARX_PROJECT_DEPENDENCIES}.
# ${ARMARX_PROJECT_DEPENDENCIES} in turn is used by ArmarXScenario.cmake for generating scenarios
# and in Installation.cmake to write the dependecies into the necessary configuration files.
macro(depends_on_armarx_package DEPENDENCY)
    if (NOT ARMARX_PROJECT_NAME)
        # FATAL_ERROR if used before armarx_project() macro
        message(FATAL_ERROR "\n\nThe macro depends_on_armarx_package() must be called AFTER the armarx_project() macro.")
    endif()

    string(REPLACE "::" ";" PROJECT_NAME_NAMESPACE_TUPLE "${DEPENDENCY}")
    list(LENGTH PROJECT_NAME_NAMESPACE_TUPLE PROJECT_NAME_NAMESPACE_TUPLE_LEN)
    if (${PROJECT_NAME_NAMESPACE_TUPLE_LEN} EQUAL 1)
        list(GET PROJECT_NAME_NAMESPACE_TUPLE 0 DEP_PROJECT_NAME)
        set(DEP_PROJECT_NAMESPACE "")
    elseif(${PROJECT_NAME_NAMESPACE_TUPLE_LEN} EQUAL 2)
        list(GET PROJECT_NAME_NAMESPACE_TUPLE 0 DEP_PROJECT_NAMESPACE)
        list(GET PROJECT_NAME_NAMESPACE_TUPLE 1 DEP_PROJECT_NAME)
    else()
        message(ERROR "Namespaces must be at most one level deep.")
    endif()

    if("${DEP_PROJECT_NAME}" STREQUAL "${ARMARX_PROJECT_NAME}")
        message(STATUS "skipping self dependency")
    else()
        if (NOT "${CMAKE_SOURCE_DIR}" STREQUAL "${CMAKE_CURRENT_LIST_DIR}")
            message(FATAL_ERROR "\n\nThe macro depends_on_armarx_package() can only be called in the toplevel CMakeLists.txt file of the current ArmarX Package.")
        endif()

        if(${${DEP_PROJECT_NAME}_FOUND})
            message(WARNING "There was already a call to find_package(${DEP_PROJECT_NAME} ${ARMARX_PACKAGE_LIBRARY_VERSION}) in your project")
            message(WARNING "Please remove it in favor of the depends_on_armarx_package(${DEP_PROJECT_NAME}) in your toplevel CMakeLists.txt file.")
        else()
            if (${ARGC} GREATER 1)
                # package is optional if a second argument is given
                find_package(${DEP_PROJECT_NAME} QUIET
                    PATHS "$ENV{HOME}/armarx/${DEP_PROJECT_NAME}/build"
                )
            else ()
                find_package(${DEP_PROJECT_NAME} QUIET
                    PATHS "$ENV{HOME}/armarx/${DEP_PROJECT_NAME}/build"
                )
                if(NOT ${${DEP_PROJECT_NAME}_FOUND})
                    set(ARMARX_MISSING_PROJECT_DEPENDENCIES "${ARMARX_MISSING_PROJECT_DEPENDENCIES}" "${DEP_PROJECT_NAME}" CACHE INTERNAL "Missing Project Dependencies on other ArmarX Packages")
                endif()
            endif()
        endif()

        string(FIND "${ARMARX_PROJECT_SOURCE_PACKAGE_DEPENDENCIES}" "${DEP_PROJECT_NAME}" pos)
        # ${DEP_PROJECT_NAME}_BUILD_DIR is only set if source package and not installed
        # if ${${DEP_PROJECT_NAME}_FOUND} is not available the project was not found at all, assume its a source package
        if(${pos} EQUAL -1 AND (${DEP_PROJECT_NAME}_BUILD_DIR OR NOT ${${DEP_PROJECT_NAME}_FOUND}))
            set(ARMARX_PROJECT_SOURCE_PACKAGE_DEPENDENCIES "${ARMARX_PROJECT_SOURCE_PACKAGE_DEPENDENCIES}" "${DEP_PROJECT_NAME}" CACHE INTERNAL "Project Source Dependencies on other ArmarX Packages")
        endif()

        string(FIND "${ARMARX_PROJECT_PACKAGE_DEPENDENCY_PATHS}" "${DEP_PROJECT_NAME}" pos)
        if(${pos} EQUAL -1)
           set(ARMARX_PROJECT_PACKAGE_DEPENDENCY_PATHS "${ARMARX_PROJECT_PACKAGE_DEPENDENCY_PATHS}" "${DEP_PROJECT_NAME}:${${DEP_PROJECT_NAME}_PACKAGE_CONFIG_DIR}" CACHE INTERNAL "Project Dependencies paths on other ArmarX Packages")
        endif()

        # check if the version number of the found dependency matches the version of the current package
        # throw an error if the version numbers don't match
        if (ARMARX_ENABLE_DEPENDENCY_VERSION_CHECK AND ${${DEP_PROJECT_NAME}_FOUND} AND NOT "${${DEP_PROJECT_NAME}_VERSION}" VERSION_EQUAL "${ARMARX_PACKAGE_LIBRARY_VERSION}")
            message(STATUS "")
            message(STATUS "")
            message(FATAL_ERROR "\n\nVersion mismatch\n ${ARMARX_PROJECT_NAME} version: ${ARMARX_PACKAGE_LIBRARY_VERSION}\n ${DEP_PROJECT_NAME} version: ${${DEP_PROJECT_NAME}_VERSION} instead of ${ARMARX_PACKAGE_LIBRARY_VERSION}). \n Please update the depencency ${DEPENDENCY} or adapt the version number of ${ARMARX_PROJECT_NAME}.\n For details see: https://i61wiki.itec.uka.de/doc/armarxdoc/index.html")
        endif()

        if(${${DEP_PROJECT_NAME}_FOUND})
            set(ARMARX_PROJECT_DEPENDENCIES "${ARMARX_PROJECT_DEPENDENCIES}" "${DEP_PROJECT_NAME}" CACHE INTERNAL "Project Dependencies on other ArmarX Packages")
            set(ARMARX_PROJECT_DEPENDENT_DATA_DIRS "${ARMARX_PROJECT_DEPENDENT_DATA_DIRS}" "${${DEP_PROJECT_NAME}_DATA_DIR}" CACHE INTERNAL "Collection of all *_DATA_DIR entries of all dependent projects")
            set(ARMARX_PROJECT_DISABLED_DEPENDENT_TARGETS "${ARMARX_PROJECT_DISABLED_DEPENDENT_TARGETS}" "${${DEP_PROJECT_NAME}_DISABLED_TARGETS}" CACHE INTERNAL "Disabled Targets from dependent ArmarX Packages")
            list(APPEND CMAKE_MODULE_PATH "${${DEP_PROJECT_NAME}_CMAKE_DIR}")

            foreach(dir ${${DEP_PROJECT_NAME}_INCLUDE_DIRS})
                string(REGEX MATCHALL ^.*/build/source$ matched "${dir}")
                if(matched)
                    include_directories(SYSTEM ${dir})
                else()
                    include_directories(${dir})
                endif()
            endforeach()

            message(STATUS "${DEP_PROJECT_NAME}_USE_FILE: ${${DEP_PROJECT_NAME}_USE_FILE}")
            if(NOT ${DEP_PROJECT_NAME}_USE_FILE)
                message(FATAL_ERROR "${DEP_PROJECT_NAME}_USE_FILE is not set!" )
            endif()
            include(${${DEP_PROJECT_NAME}_USE_FILE})
            # this has to be a string (not a list) so it is understood by doxygen
            set(ARMARX_PROJECT_DEPENDENT_TAG_FILES "${ARMARX_PROJECT_DEPENDENT_TAG_FILES} ${DEP_PROJECT_NAME}.tag=${${DEP_PROJECT_NAME}_DOXYGEN_TAG_FILE}")
            if(DEFINED ${DEP_PROJECT_NAME}_SOURCE_DIR)
                add_external_dependency(${DEP_PROJECT_NAME})
            endif()
        endif()
    endif()
endmacro()

add_custom_target(all_generate)

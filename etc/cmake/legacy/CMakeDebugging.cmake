# Utility and convenience functions and macros
# for ArmarX framework

OPTION(ARMARX_CMAKE_DEBUGGING "if on, prints additional debug info for cmake" OFF)

#print all targets is taken from https://stackoverflow.com/a/34292622

# Get all propreties that cmake supports
execute_process(COMMAND cmake --help-property-list OUTPUT_VARIABLE CMAKE_PROPERTY_LIST)

# Convert command output into a CMake list
STRING(REGEX REPLACE ";" "\\\\;" CMAKE_PROPERTY_LIST "${CMAKE_PROPERTY_LIST}")
STRING(REGEX REPLACE "\n" ";" CMAKE_PROPERTY_LIST "${CMAKE_PROPERTY_LIST}")

function(print_cmake_property_list)
    list(REMOVE_DUPLICATES CMAKE_PROPERTY_LIST)
    message (STATUS "CMAKE_PROPERTY_LIST:")
    printlist("  * " "${CMAKE_PROPERTY_LIST}")
endfunction(print_cmake_property_list)

function(print_cmake_target_properties tgt)
    if(NOT TARGET ${tgt})
      message(STATUS "There is no target named '${tgt}'")
      return()
    endif()
    message (STATUS "        all target properties for ${tgt}")
    # for some reason we need to remove duplicates here (it is not enough to do this at the global scope)
    list(REMOVE_DUPLICATES CMAKE_PROPERTY_LIST)
    foreach (prop ${CMAKE_PROPERTY_LIST})
        string(REPLACE "<CONFIG>" "${CMAKE_BUILD_TYPE}" prop ${prop})
        # Fix https://stackoverflow.com/questions/32197663/how-can-i-remove-the-the-location-property-may-not-be-read-from-target-error-i
        if(NOT (prop STREQUAL "LOCATION" OR prop MATCHES "^LOCATION_" OR prop MATCHES "_LOCATION$") )
            get_target_property(propval ${tgt} ${prop})
            if (NOT propval STREQUAL "NOTFOUND" AND NOT propval STREQUAL "propval-NOTFOUND")
                list(REMOVE_DUPLICATES propval)
                list(LENGTH propval propval_len)
                message (STATUS "            ${prop}")
                printlist(      "                * " "${propval}")
            endif()
        endif()
    endforeach(prop)
endfunction(print_cmake_target_properties)


function(print_cmake_source_properties src)
    message (STATUS "        all source properties for ${src}")
    # for some reason we need to remove duplicates here (it is not enough to do this at the global scope)
    list(REMOVE_DUPLICATES CMAKE_PROPERTY_LIST)
    foreach (prop ${CMAKE_PROPERTY_LIST})
        string(REPLACE "<CONFIG>" "${CMAKE_BUILD_TYPE}" prop ${prop})
        get_source_file_property(propval ${src} ${prop})
        if (NOT "${propval}" STREQUAL "NOTFOUND")
            list(REMOVE_DUPLICATES propval)
            list(LENGTH propval propval_len)
            message (STATUS "            ${prop}")
            printlist(      "                * " "${propval}")
        endif()
    endforeach(prop)
endfunction(print_cmake_source_properties)

function(print_cmake_properties)
    message (STATUS "all cmake properties")
    # for some reason we need to remove duplicates here (it is not enough to do this at the global scope)
    list(REMOVE_DUPLICATES CMAKE_PROPERTY_LIST)
    foreach (prop ${CMAKE_PROPERTY_LIST})
        string(REPLACE "<CONFIG>" "${CMAKE_BUILD_TYPE}" prop ${prop})
        get_cmake_property(propval ${prop})
        if (NOT "${propval}" STREQUAL "NOTFOUND")
            list(REMOVE_DUPLICATES propval)
            list(LENGTH propval propval_len)
            message (STATUS "    ${prop}")
            printlist(      "        * " "${propval}")
        endif()
    endforeach(prop)
endfunction(print_cmake_properties)

#from https://stackoverflow.com/a/9328525

macro(print_all_cmake_variables)
    message(STATUS "ALL CMAKE VARIABLES:")
    get_cmake_property(_variableNames VARIABLES)
    foreach (_variableName ${_variableNames})
        message(STATUS "    ${_variableName}")
        printlist(     "        * " "${${_variableName}}")
    endforeach()
endmacro(print_all_cmake_variables)

macro(print_environment_variables)
    message(STATUS "ALL ENVIRONMENT VARIABLES:")
    execute_process(COMMAND "${CMAKE_COMMAND}" "-E" "environment")
endmacro(print_environment_variables)


if(ARMARX_CMAKE_DEBUGGING)
    print_cmake_property_list()
endif()

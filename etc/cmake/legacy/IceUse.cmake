# Test that Ice works (from orca robotics)
# Defines the following variables:
# Ice_WORKS : 1 if test passed, 0 otherwise.

include(${CMAKE_ROOT}/Modules/CheckCXXSourceCompiles.cmake)

if (NOT ICE_FOUND)
    message(STATUS "Testing Ice - Failed. The Ice library was not detected")
    set(Ice_WORKS 0)
endif()

set(CMAKE_REQUIRED_INCLUDES "${Ice_INCLUDE_DIRS}")
set(CMAKE_REQUIRED_LIBRARIES "${Ice_Ice_LIBRARY};${Ice_IceUtil_LIBRARY}")
check_cxx_source_compiles("#include <Ice/Ice.h>\nint main() { return 0; }" Ice_WORKS)
if (Ice_WORKS)
    set(Ice_WORKS 1)
else()
    set(Ice_WORKS 0)
endif()

# This macro needs to be applied after adding a target to link the target
# against the required libraries.
macro(target_link_ice TARGET)
    target_link_libraries(${TARGET} PUBLIC
        "${Ice_LIBRARIES}"
        "${Ice_Ice_LIBRARY}"
        "${Ice_IceUtil_LIBRARY}"
        "${Ice_IceStorm_LIBRARY}"
        "${Ice_IceBox_LIBRARY}"
        "${Ice_IceGrid_LIBRARY}"
        "${Ice_Glacier2_LIBRARY}")
endmacro()

include(CMakeParseArguments)

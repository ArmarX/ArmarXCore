# Macros for ArmarX Framework Components

macro(armarx_component_set_name NAME)
    set(ARMARX_COMPONENT_NAME ${NAME}     )
    set(ARMARX_COMPONENT_LIB_NAME ${NAME} )
    set(ARMARX_COMPONENT_EXE "${NAME}Run" )
    set(ARMARX_COMPONENT_NODE "NodeMain"  ) # default Ice Node

    armarx_set_target("Component: ${ARMARX_COMPONENT_NAME}")
endmacro()

macro(armarx_add_component)
    #there are two modes:
    # 1. legacy mode (2 params SOURCES + HEADERS)
    # 2. parsing mode
    if("${ARGC}" EQUAL 2)
        set(SOURCES "${ARGV0}")
        set(HEADERS "${ARGV1}")
    else()
        #assume parsing mode
        set(multi_param
            SOURCES
            HEADERS
            COMPONENT_LIBS
            DEPENDEND_LIBS
            LIBS
            ARON_FILES
            INCLUDE_DIRECTORIES)
        set(single_param
            COMPONENT_NAME)
        set(flag_param)
        cmake_parse_arguments(PREFIX "${flag_param}" "${single_param}" "${multi_param}" ${ARGN})
        if(PREFIX_SOURCES)
            set(SOURCES ${PREFIX_SOURCES})
        endif()
        if(PREFIX_HEADERS)
            set(HEADERS ${PREFIX_HEADERS})
        endif()

        if(PREFIX_COMPONENT_LIBS)
            list(APPEND COMPONENT_LIBS ${PREFIX_COMPONENT_LIBS})
        endif()
        if(PREFIX_DEPENDEND_LIBS)
            list(APPEND COMPONENT_LIBS ${PREFIX_DEPENDEND_LIBS})
        endif()
        if(PREFIX_LIBS)
            list(APPEND COMPONENT_LIBS ${PREFIX_LIBS})
        endif()

        if(PREFIX_ARON_FILES)
            set(ARON_FILES ${PREFIX_ARON_FILES})
        endif()

        if(PREFIX_INCLUDE_DIRECTORIES)
            set(INCLUDE_DIRECTORIES ${PREFIX_INCLUDE_DIRECTORIES})
        endif()

        if(PREFIX_COMPONENT_NAME)
            armarx_component_set_name(${PREFIX_COMPONENT_NAME})
        endif()
        if(PREFIX_UNPARSED_ARGUMENTS)
            message(FATAL_ERROR "${COMPONENT_NAME}: encountered unparsed arguments! ${PREFIX_UNPARSED_ARGUMENTS}\nARGN = ${ARGN}")
        endif()
    endif()
    # the library target
    armarx_add_library(
        LIB_NAME ${ARMARX_COMPONENT_LIB_NAME}
        LIBS     ${COMPONENT_LIBS}
        SOURCES  ${SOURCES}
        HEADERS  ${HEADERS}
        ARON_FILES ${ARON_FILES}
        INCLUDE_DIRECTORIES ${INCLUDE_DIRECTORIES}
    )
    list(APPEND COMPONENT_LIBS ${ARMARX_COMPONENT_LIB_NAME})
endmacro()

function(armarx_add_component_executable SOURCES)
    ARMARX_MESSAGE(STATUS "    Building ${ARMARX_COMPONENT_EXE}")
    armarx_add_executable(${ARMARX_COMPONENT_EXE} "${SOURCES}" "${COMPONENT_LIBS}")
endfunction()

macro(armarx_add_component_test TEST_NAME TEST_FILE)
    set(TEST_LINK_LIBRARIES ${ARMARX_COMPONENT_LIB_NAME} ${COMPONENT_LIBS})
    armarx_add_test(${TEST_NAME} ${TEST_FILE} "${TEST_LINK_LIBRARIES}")
endmacro()


function(armarx_generate_and_add_component_executable)
    set(init_comp_name ${ARMARX_COMPONENT_NAME})
    set(init_comp_lib_name ${ARMARX_COMPONENT_LIB_NAME})
    message(STATUS "    Generating application")
    set(multi_param)
    set(single_param
        APPLICATION_NAME
        COMPONENT_NAMESPACE
        COMPONENT_NAME
        COMPONENT_INCLUDE
        PACKAGE_NAME
        COMPONENT_CLASS_NAME
        COMPONENT_LIB_NAME
        COMPONENT_APPLICATION_TYPE
        COMPONENT_APPLICATION_INCLUDE
        CONFIG_NAME
        CONFIG_DOMAIN)
    set(flag_param
        APPLICATION_APP_SUFFIX
        ENABLE_LIB_LOADING)

    cmake_parse_arguments(PREFIX "${flag_param}" "${single_param}" "${multi_param}" ${ARGN})
    if(PREFIX_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "${COMPONENT_NAME}: encountered unparsed arguments! ${PREFIX_UNPARSED_ARGUMENTS}\nARGN = ${ARGN}")
    endif()
    foreach(param ${single_param})
        if(PREFIX_${param})
            set(${param} "${PREFIX_${param}}")
        endif()
    endforeach()

    if(PREFIX_COMPONENT_NAME)
        armarx_component_set_name("${COMPONENT_NAME}")
    endif()
    ############################################################################
    #defaults
    if(NOT COMPONENT_APPLICATION_TYPE)
        set(COMPONENT_APPLICATION_TYPE "armarx::Application")
    endif()
    if(NOT COMPONENT_APPLICATION_INCLUDE)
        set(COMPONENT_APPLICATION_INCLUDE "ArmarXCore/core/application/Application.h")
    endif()
    if(NOT COMPONENT_NAMESPACE)
        set(COMPONENT_NAMESPACE "armarx")
    endif()
    if(NOT COMPONENT_NAME)
        set(COMPONENT_NAME "${ARMARX_COMPONENT_NAME}")
    endif()
    if(NOT APPLICATION_NAME)
        set(APPLICATION_NAME "${ARMARX_COMPONENT_EXE}")
        if(PREFIX_APPLICATION_APP_SUFFIX)
            set(APPLICATION_NAME "${COMPONENT_NAME}AppRun")
        endif()
    endif()
    if(NOT PACKAGE_NAME)
        set(PACKAGE_NAME "${ARMARX_PROJECT_NAME}")
    endif()
    if(NOT COMPONENT_CLASS_NAME)
        set(COMPONENT_CLASS_NAME "${COMPONENT_NAME}")
    endif()
    set(COMPONENT_INCLUDE_TESTED "")
    if(NOT COMPONENT_INCLUDE)
        set(COMPONENT_INCLUDE "${PACKAGE_NAME}/components/${COMPONENT_NAME}/${COMPONENT_CLASS_NAME}.h")
        string(APPEND COMPONENT_INCLUDE_TESTED "tested ${COMPONENT_INCLUDE}")
        if(NOT EXISTS "${COMPONENT_INCLUDE}")
            set(COMPONENT_INCLUDE "${PACKAGE_NAME}/components/${COMPONENT_NAME}/${COMPONENT_NAME}.h")
            string(APPEND COMPONENT_INCLUDE_TESTED " -> file does not exist!\ntested ${COMPONENT_INCLUDE}")
        endif()
        if(NOT EXISTS "${COMPONENT_INCLUDE}")
            set(COMPONENT_INCLUDE "${CMAKE_CURRENT_LIST_DIR}/${COMPONENT_CLASS_NAME}.h")
            string(APPEND COMPONENT_INCLUDE_TESTED " -> file does not exist!\ntested ${COMPONENT_INCLUDE}")
        endif()
        if(NOT EXISTS "${COMPONENT_INCLUDE}")
            set(COMPONENT_INCLUDE "${CMAKE_CURRENT_LIST_DIR}/${COMPONENT_NAME}.h")
            string(APPEND COMPONENT_INCLUDE_TESTED " -> file does not exist!\ntested ${COMPONENT_INCLUDE}")
        endif()
        if(NOT EXISTS "${COMPONENT_INCLUDE}")
            string(APPEND COMPONENT_INCLUDE_TESTED " -> file does not exist!\n")
            foreach(f ${HEADERS} ${LIB_HEADERS})
                set(regex "^(.*/)?${COMPONENT_CLASS_NAME}.h$")
                if(f MATCHES "${regex}")
                    set(COMPONENT_INCLUDE "${CMAKE_CURRENT_LIST_DIR}/${f}")
                    string(APPEND COMPONENT_INCLUDE_TESTED "tested ${COMPONENT_INCLUDE}\n")
                    break()
                else()
                    string(APPEND COMPONENT_INCLUDE_TESTED "tested ${COMPONENT_INCLUDE} -> does not match regex '${regex}'\n")
                endif()
            endforeach()
        endif()
    endif()

    if(NOT COMPONENT_LIB_NAME)
        set(COMPONENT_LIB_NAME "${COMPONENT_NAME}")
    endif()
    if(NOT CONFIG_NAME)
        set(CONFIG_NAME "")
    endif()
    if(NOT CONFIG_DOMAIN)
        set(CONFIG_DOMAIN "ArmarX")
    endif()
    if(PREFIX_ENABLE_LIB_LOADING)
        set(ENABLE_LIB_LOADING true)
    else()
        set(ENABLE_LIB_LOADING false)
    endif()
    ############################################################################
    #checks
    if(NOT COMPONENT_NAME)
        message(FATAL_ERROR "no component name given! set parameter COMPONENT_NAME or call armarx_component_set_name\nARGN = ${ARGN}")
    endif()
    if(NOT APPLICATION_NAME)
        message(FATAL_ERROR "${COMPONENT_NAME}: no application name given! set parameter APPLICATION_NAME or call armarx_component_set_name\nARGN = ${ARGN}")
    endif()
    if(NOT EXISTS "${COMPONENT_INCLUDE}")
        message(FATAL_ERROR "${COMPONENT_NAME}: failed to find include! define it via COMPONENT_INCLUDE\nARGN = ${ARGN}\n${COMPONENT_INCLUDE_TESTED}")
    endif()
    ############################################################################
    string(REPLACE "${CMAKE_SOURCE_DIR}/source/" "" COMPONENT_INCLUDE "${COMPONENT_INCLUDE}")
    #generate
    set(AUTHOR_NAME  "cmake generator")
    set(AUTHOR_EMAIL "none")
    set(outfile "${CMAKE_CURRENT_BINARY_DIR}/generated_main/${APPLICATION_NAME}.cpp")
    configure_file(
        "${ArmarXCore_TEMPLATES_DIR}/ApplicationTemplate/main.cpp.in"
        "${outfile}"
        @ONLY
    )
    ############################################################################
    #add target
    list(APPEND COMPONENT_LIBS ${LIBS})
    foreach(lib ${LIB_NAME} ${COMPONENT_LIB_NAME} ${COMPONENT_NAME} ${init_comp_name} ${init_comp_lib_name})
        if(TARGET ${lib})
            list(APPEND COMPONENT_LIBS ${lib})
        endif()
    endforeach()
    armarx_add_executable(${APPLICATION_NAME} "${outfile}" "${COMPONENT_LIBS}")
endfunction()

function(armarx_eval_mutex_flag_group FLAG_NAME_VAR)
    # Parse arguments.
    set(multi_param FLAG_NAMES)
    set(single_param FLAG_PREFIX FLAG_DEFAULT_VALUE)
    set(flag_param)
    cmake_parse_arguments(PARSE_ARGV 1 AX "${flag_param}" "${single_param}" "${multi_param}")
    if(DEFINED AX_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "${TARGET}: Unknown arguments `${AX_UNPARSED_ARGUMENTS}`.")
    endif()

    # Holds the name of true flag.
    set(TRUE_FLAG "")

    # Iterate over flags and find the true one.
    foreach(FLAG_NAME ${AX_FLAG_NAMES})
        if (${${AX_FLAG_PREFIX}${FLAG_NAME}})
            if(TRUE_FLAG)
                message(FATAL_ERROR "The flags in `${AX_FLAG_NAMES}` are mutually exclusive.")
            endif()

            set(TRUE_FLAG "${FLAG_NAME}")
        endif()
    endforeach()

    # If none was true, set the default if given as argument.
    if(NOT TRUE_FLAG)
        if (NOT AX_FLAG_DEFAULT_VALUE)
            message(FATAL_ERROR "One of the flags in `${AX_FLAG_NAMES}` must be set.")
        endif()

        set(TRUE_FLAG "${AX_FLAG_DEFAULT_VALUE}")
    endif()

    # Set the name of the true flag to the supplied output variable.
    set("${FLAG_NAME_VAR}" "${TRUE_FLAG}" PARENT_SCOPE)
endfunction()


# TODO: Remove.
function(remove_empty_elements DUMMY)
    message(WARNING "Stub as remove_empty_elements is still used in VisionX. This function will be removed after migrating to modern CMake projects.")
endfunction()

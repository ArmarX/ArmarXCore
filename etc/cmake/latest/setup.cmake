include(CMakeParseArguments)

if(NOT DEFINED CMAKE_SUPPRESS_DEVELOPER_WARNINGS)
     set(CMAKE_SUPPRESS_DEVELOPER_WARNINGS 1 CACHE INTERNAL "No dev warnings")
endif()

set(ARMARX_GNU_COMPILER_MIN_VERSION 8.2)
if(CMAKE_COMPILER_IS_GNUCXX AND CMAKE_CXX_COMPILER_VERSION VERSION_LESS ARMARX_GNU_COMPILER_MIN_VERSION)
    message(FATAL_ERROR "Minimum compiler version of ${ARMARX_GNU_COMPILER_MIN_VERSION} (for gcc) is required for ArmarX! You are using: ${CMAKE_CXX_COMPILER_VERSION}")
endif()

# Default project name is ArmarXCore
if (NOT ARMARX_PROJECT_NAME)
    set(ARMARX_PROJECT_NAME "ArmarXCore")
endif()

# Utility, basic setup.
include(${ArmarXCore_CMAKE_SCRIPTS_DIR}/cmake_util.cmake)
include(${ArmarXCore_CMAKE_SCRIPTS_DIR}/setup_os.cmake)
include(${ArmarXCore_CMAKE_SCRIPTS_DIR}/setup_dirs.cmake)

include(${${PROJECT_NAME}_CMAKE_DIR}/ArmarXPackageVersion.cmake)

# pthread
find_package(Threads REQUIRED)

# Fix for the Qt4 FindPackage script which determines
# the path to Qt4 according to the variable QT_QMAKE_EXECUTABLE
# which should be set to the path of the qmake executable

if(NOT "$ENV{QT_QMAKE_EXECUTABLE}" STREQUAL "")
    message(STATUS "USING QT-PATH from environment variable QT_QMAKE_EXECUTABLE: $ENV{QT_QMAKE_EXECUTABLE}")
    file(TO_CMAKE_PATH "$ENV{QT_QMAKE_EXECUTABLE}" QT_QMAKE_EXECUTABLE)
endif()

add_custom_target(all_generate)

# Internal CMake functions and configurations.
include(${ArmarXCore_CMAKE_SCRIPTS_DIR}/compile_options.cmake)

# Load addons.
file(GLOB ADDONS "${ArmarXCore_CMAKE_SCRIPTS_DIR}/addons/*.cmake")
foreach(ADDON_PATH ${ADDONS})
    get_filename_component(ADDON_NAME "${ADDON_PATH}" NAME_WE)
    message(STATUS "\n== Loading addon `${ADDON_NAME}` ...")
    include(${ADDON_PATH})
endforeach()
message(STATUS "")

# Public CMake API.
include(${ArmarXCore_CMAKE_SCRIPTS_DIR}/find_package.cmake)
include(${ArmarXCore_CMAKE_SCRIPTS_DIR}/add_targets.cmake)  # add_library, add_executable.
include(${ArmarXCore_CMAKE_SCRIPTS_DIR}/add_aron_library.cmake)
include(${ArmarXCore_CMAKE_SCRIPTS_DIR}/add_ice_library.cmake)
include(${ArmarXCore_CMAKE_SCRIPTS_DIR}/add_component.cmake)
include(${ArmarXCore_CMAKE_SCRIPTS_DIR}/add_statechart.cmake)
include(${ArmarXCore_CMAKE_SCRIPTS_DIR}/add_test.cmake)
include(${ArmarXCore_CMAKE_SCRIPTS_DIR}/install_project.cmake)
#include(${ArmarXCore_CMAKE_SCRIPTS_DIR}/generate_scenario.cmake)  # TODO: In use? Delete if not.

# Boost
# look for boost after including testing.cmake because it checks whether the test framework of boost is available and overwrites the boost variables
set(Boost_USE_MULTITHREADED ON)
set(Boost_ADDITIONAL_VERSIONS "1.48.0" "1.48")
#set(Boost_NO_SYSTEM_PATHS ON) # prevent mixing different Boost installations
find_package(Boost ${ArmarX_BOOST_VERSION} EXACT REQUIRED COMPONENTS thread regex system program_options)
#include_directories(${Boost_INCLUDE_DIR})

function(armarx_add_ice_library TARGET)
    # Parse arguments.
    set(multi_param SLICE_FILES DEPENDENCIES)
    set(single_param)
    set(flag_param)
    cmake_parse_arguments(PARSE_ARGV 1 AX "${flag_param}" "${single_param}" "${multi_param}")
    if(DEFINED AX_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "${TARGET}: Unknown arguments `${AX_UNPARSED_ARGUMENTS}`.")
    endif()

    if(NOT DEFINED Ice_SLICE2CPP_EXECUTABLE)
        message(FATAL_ERROR "Ice_SLICE2CPP_EXECUTABLE is undefined!")
    endif()

    # General flags for slice2cpp.
    set(SLICE_COMMON_FLAGS
        "-I${Ice_SLICE_DIR}"
        "-I${PROJECT_SOURCECODE_DIR}"
        --underscore)

    set(SLICE2CPP_INPUT_DIR "${CMAKE_CURRENT_SOURCE_DIR}")
    set(SLICE2CPP_OUTPUT_DIR "${CMAKE_CURRENT_BINARY_DIR}")

    # The uppercase string is needed for the --dll-export slice flag.
    string(TOUPPER ${TARGET} TARGET_UPPERCASE)
    string(REPLACE "-" "_" TARGET_UPPERCASE "${TARGET_UPPERCASE}")
    set(SLICE2CPP_FLAGS --dll-export ${TARGET_UPPERCASE}_IMPORT_EXPORT)

    # Project interface dependency paths go through all dependent projects.
    set(SLICE2CPP_INCLUDE_DIRECTORIES "")
    foreach(DEP ${AX_DEPENDENCIES})
        get_target_property(DEP_INCLUDES "${DEP}" SLICE2CPP_INCLUDE_DIRECTORIES)

        if(DEP_INCLUDES)
            set(SLICE2CPP_INCLUDE_DIRECTORIES ${DEP_INCLUDES})
        # LEGACY MODE - REMOVE ELSE()-PART AFTER MIGRATING ARMARXCORE.
        else()
            string(REPLACE "Interfaces" "" DEP "${DEP}")
            # append all directories found in <DEP>_INTERFACE_DIRS
            # as include directories for the current compilation process
            foreach(DEPENDEND_INTERFACE_DIR ${${DEP}_INTERFACE_DIRS})
                list(APPEND SLICE2CPP_INCLUDE_DIRECTORIES "${DEPENDEND_INTERFACE_DIR}")
            endforeach()

            #include_directories(${${DEP}_INCLUDE_DIRS})
            # append all dependent interface libraries to the DEPENDEND_INTERFACE_LIBRARIES
            # CMake variable which is used further down in target_link_libraries()
            list(APPEND DEPENDEND_INTERFACE_LIBRARIES "${${DEP}_INTERFACE_LIBRARY}")
        endif()
        # /LEGACY MODE
    endforeach()
    foreach(SLICE2CPP_INCLUDE_DIR ${SLICE2CPP_INCLUDE_DIRECTORIES})
        list(APPEND SLICE_COMMON_FLAGS "-I${SLICE2CPP_INCLUDE_DIR}")
    endforeach()

    # setup C++ files in the following loop
    set(LIB_FILES "")
    set(LIB_HEADER_FILES "")

    # Run slice2cpp on each Ice file.
    foreach(CURRENT_SLICE_FILE ${AX_SLICE_FILES})
        # Full path without extension ("path/file.ext" => "path/file").
        get_filename_component(CURRENT_SLICE_FILE_DIR "${CURRENT_SLICE_FILE}" DIRECTORY)
        get_filename_component(CURRENT_SLICE_FILE_NAME_WE "${CURRENT_SLICE_FILE}" NAME_WE)
        if ("${CURRENT_SLICE_FILE_DIR}" STREQUAL "")
            set(STRIPPED_SLICE_FILE "${CURRENT_SLICE_FILE_NAME_WE}")
        else()
            set(STRIPPED_SLICE_FILE "${CURRENT_SLICE_FILE_DIR}/${CURRENT_SLICE_FILE_NAME_WE}")
        endif()

        # Make sure the directory for generated files exists.
        file(MAKE_DIRECTORY ${SLICE2CPP_OUTPUT_DIR}/${CURRENT_SLICE_FILE_DIR})

        # put generated files in a directory with the same hierarchy and names as the .ice file
        set(SLICE_CURRENT_CPP_FLAGS ${SLICE_COMMON_FLAGS} --output-dir ${SLICE2CPP_OUTPUT_DIR}/${CURRENT_SLICE_FILE_DIR})

        # Always add a prefix to the #include directives of generated files
        # Since only #include <> directives are generated the local directory is not
        # regarded when searching for matching header files which would require
        # adding additional include_directories.
        string(LENGTH "${PROJECT_SOURCE_DIR}/source/" PROJECT_SOURCE_DIR_LEN)
        string(SUBSTRING "${CMAKE_CURRENT_SOURCE_DIR}" "${PROJECT_SOURCE_DIR_LEN}" "-1" SLICE_FILE_INCLUDE_DIR)
        #set(SLICE_FILE_INCLUDE_DIR "")
        if("${CURRENT_SLICE_FILE_DIR}" STREQUAL "")
            list(APPEND SLICE_CURRENT_CPP_FLAGS --include-dir "${SLICE_FILE_INCLUDE_DIR}")
        else()
            list(APPEND SLICE_CURRENT_CPP_FLAGS --include-dir "${SLICE_FILE_INCLUDE_DIR}/${CURRENT_SLICE_FILE_DIR}")
        endif()

        set(SLICE2CPP_INPUT_FILE "${SLICE2CPP_INPUT_DIR}/${CURRENT_SLICE_FILE}")
        set(SLICE2CPP_OUTPUT_FILE "${SLICE2CPP_OUTPUT_DIR}/${STRIPPED_SLICE_FILE}")

        # Get the dependencies of the current slice file required for the slice2cpp target.
        _armarx_get_slice2cpp_command_depends(SLICE2CPP_DEPENDS
            SLICE2CPP_ARGS ${SLICE_CURRENT_CPP_FLAGS}
            SLICE_FILE "${SLICE2CPP_INPUT_FILE}"
        )

        # Generate the header and implementation files from the slice file.
        add_custom_command(
           OUTPUT "${SLICE2CPP_OUTPUT_FILE}.h"
                  "${SLICE2CPP_OUTPUT_FILE}.cpp"
           COMMAND "${Ice_SLICE2CPP_EXECUTABLE}"
           ARGS ${SLICE_CURRENT_CPP_FLAGS} ${SLICE2CPP_FLAGS} "${SLICE2CPP_INPUT_FILE}"
           DEPENDS "${Ice_SLICE2CPP_EXECUTABLE}" ${SLICE2CPP_DEPENDS}
           MAIN_DEPENDENCY "${SLICE2CPP_INPUT_FILE}"
           COMMENT "Generating `${SLICE2CPP_OUTPUT_DIR}/${STRIPPED_SLICE_FILE}.{h|cpp}` from `${SLICE2CPP_INPUT_FILE}`."
        )

        list(APPEND LIB_FILES ${SLICE2CPP_OUTPUT_FILE}.cpp)
        list(APPEND LIB_HEADER_FILES ${SLICE2CPP_OUTPUT_FILE}.h)
        list(APPEND LIB_HEADER_FILES ${SLICE2CPP_INPUT_FILE})
    endforeach()

    # TODO: Remove? This is used in RobotAPI.
    if(SLICE_FILES_ADDITIONAL_HEADERS)
        list(APPEND LIB_HEADER_FILES ${SLICE_FILES_ADDITIONAL_HEADERS})
    endif()
    if(SLICE_FILES_ADDITIONAL_SOURCES)
        list(APPEND LIB_FILES ${SLICE_FILES_ADDITIONAL_SOURCES})
    endif()

    # BUILD_INTERFACE_LIBRARY evaluates to FALSE if length is 0
    list(LENGTH LIB_FILES BUILD_INTERFACE_LIBRARY)

    # Only build the library if all files have been generated.
    if(${BUILD_INTERFACE_LIBRARY})
        list(APPEND DEPENDEND_INTERFACE_LIBRARIES ${CMAKE_THREAD_LIBS_INIT})
        
        armarx_add_library(${TARGET}
            SOURCES ${LIB_FILES}
            HEADERS ${LIB_HEADER_FILES}
            DEPENDENCIES ${AX_DEPENDENCIES} Ice::Ice
            SHARED
        )

        # for dependencies, the source directory of this project should also be available
        list(APPEND SLICE2CPP_INCLUDE_DIRECTORIES "${PROJECT_SOURCECODE_DIR}")

        # Set include directories for slice2cpp as property to target.
        set_target_properties(${TARGET}
            PROPERTIES
                SLICE2CPP_INCLUDE_DIRECTORIES "${SLICE2CPP_INCLUDE_DIRECTORIES}"
        )

        # ensure that the property SLICE2CPP_INCLUDE_DIRECTORIES is exported correctly to XYTargets.cmake
        set_property(
            TARGET ${TARGET} 
            APPEND
            PROPERTY EXPORT_PROPERTIES "SLICE2CPP_INCLUDE_DIRECTORIES"
        )

        target_include_directories(${TARGET} INTERFACE "$<BUILD_INTERFACE:${CMAKE_BINARY_DIR}/source>")

        if(COMPILER_SUPPORTS_suggest_override)
            target_compile_options(${TARGET} PRIVATE -Wno-suggest-override)
        endif()
    endif()

    set(SLICE2HTML_INPUT_FILES "")
    foreach(CURRENT_SLICE_FILE ${SLICE_FILES})
        list(APPEND SLICE2HTML_INPUT_FILES "${SLICE_INPUT_DIR}/${CURRENT_SLICE_FILE}")
    endforeach()

    _armarx_generate_ice_library_export(
        EXPORT_PATH ${CMAKE_CURRENT_BINARY_DIR}
        INCLUDE_DIRECTORIES ${SLICE2CPP_INCLUDE_DIRECTORIES}
    )
endfunction()


function(_armarx_get_slice2cpp_command_depends DEPENDS)
    # Parse arguments.
    set(multi_param SLICE2CPP_ARGS)
    set(single_param SLICE_FILE)
    set(flag_param)
    cmake_parse_arguments(PARSE_ARGV 1 AX "${flag_param}" "${single_param}" "${multi_param}")
    if(DEFINED AX_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "${TARGET}: Unknown arguments `${AX_UNPARSED_ARGUMENTS}`.")
    endif()

    set(CPP_DEPEND_FILE "${CMAKE_CURRENT_BINARY_DIR}/slice_cpp_depend.txt")

    if(NOT DEFINED Ice_SLICE2CPP_EXECUTABLE)
        message(FATAL_ERROR "Ice_SLICE2CPP_EXECUTABLE is undefined!")
    endif()

    execute_process(COMMAND ${Ice_SLICE2CPP_EXECUTABLE} ${AX_SLICE2CPP_ARGS} "${AX_SLICE_FILE}" --depend
                    OUTPUT_FILE ${CPP_DEPEND_FILE}
                    ERROR_VARIABLE SLICE2CPP_STDERR
                    RESULT_VARIABLE SLICE2CPP_RESULT)

    # If there was an error (result != 0), print it to stderr via CMake warning.
    if (${SLICE2CPP_RESULT})
        message(SEND_ERROR "slice2cpp error occurred for file `${AX_SLICE_FILE}`.\n Command: ${Ice_SLICE2CPP_EXECUTABLE} ${AX_SLICE2CPP_ARGS}  \n  Message: `${SLICE2CPP_STDERR}`.")
    # If there was no error, the output in stderr can be considered a warning.  Write to log.
    elseif (NOT "${SLICE2CPP_STDERR}" STREQUAL "")
        string(REPLACE ";" " " SLICE_COMMAND_FLAGS "${AX_SLICE2CPP_ARGS}")
        set(SLICE_COMMAND "${Ice_SLICE2CPP_EXECUTABLE} ${SLICE_COMMAND_FLAGS} \"${AX_SLICE_FILE}\"")
        file(APPEND "${CMAKE_BINARY_DIR}/slice_cpp_warnings.log" "WARNINGS EXECUTING THE FOLLOWING COMMAND:\n\n${SLICE_COMMAND}\n\n")
        file(APPEND "${CMAKE_BINARY_DIR}/slice_cpp_warnings.log" "${SLICE2CPP_STDERR}")
        file(APPEND "${CMAKE_BINARY_DIR}/slice_cpp_warnings.log" "\n----------\n\n")
    endif()

    # Parse the file into a list of strings.
    file(STRINGS ${CPP_DEPEND_FILE} DEPENDS_DIRTY)
    # IMPORTANT: This magic line is required.  Without it, 'slice_depends' is not a list, has only one element.
    set(DEPENDS_DIRTY ${DEPENDS_DIRTY})
    list(LENGTH DEPENDS_DIRTY listlen)
    if (${listlen} GREATER 0)
        # The first element is the generated file, we remove it.
        list(REMOVE_AT DEPENDS_DIRTY 0)
    else()
        message(SEND_ERROR "Empty dependency file created with SLICE_FILE: ${SLICE_FILE}`.")
    endif()

    set(DEPENDS_CONTENT "")

    # The rest are the .ice file dependencies. there's at least one: the actual
    # source .ice file. all dependencies end up having a leading "\ "
    # if we don't remove them, add_custom_command() will interpret them as relative commands
    foreach(SLICE_CPP_DEPEND ${DEPENDS_DIRTY})
        string(LENGTH ${SLICE_CPP_DEPEND} SLICE_CPP_DEPEND_LENGTH)
        math(EXPR SLICE_CPP_DEPEND_LENGTH "${SLICE_CPP_DEPEND_LENGTH}-1")
        string(SUBSTRING ${SLICE_CPP_DEPEND} 1 ${SLICE_CPP_DEPEND_LENGTH} SLICE_CPP_DEPEND)
        string(STRIP ${SLICE_CPP_DEPEND} SLICE_CPP_DEPEND)
        list(APPEND DEPENDS_CONTENT ${SLICE_CPP_DEPEND})
    endforeach()

    set("${DEPENDS}" ${DEPENDS_CONTENT} PARENT_SCOPE)
endfunction()


function(_armarx_generate_ice_library_export)
    # Parse arguments.
    set(multi_param INCLUDE_DIRECTORIES)
    set(single_param EXPORT_PATH)
    set(flag_param)
    cmake_parse_arguments(PARSE_ARGV 0 AX "${flag_param}" "${single_param}" "${multi_param}")
    if(DEFINED AX_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "${TARGET}: Unknown arguments `${AX_UNPARSED_ARGUMENTS}`.")
    endif()

    set(FILE "${AX_EXPORT_PATH}/ice_include_directories.txt")

    file(WRITE "${FILE}" "")
    foreach(INCLUDE_DIRECTORY ${AX_INCLUDE_DIRECTORIES})
        file(APPEND "${FILE}" "${INCLUDE_DIRECTORY}\n")
    endforeach()
endfunction()

function(armarx_add_aron_library TARGET)
    # Parse arguments.
    set(multi_param ARON_FILES DEPENDENCIES)
    set(single_param)
    set(flag_param ENABLE_DEBUG_INFO)
    cmake_parse_arguments(PARSE_ARGV 1 AX "${flag_param}" "${single_param}" "${multi_param}")
    if(DEFINED AX_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "${TARGET}: Unknown arguments `${AX_UNPARSED_ARGUMENTS}`.")
    endif()

    set(ARON_ADDITIONAL_ARGUMENTS "")
    if(${AX_ENABLE_DEBUG_INFO})
        list(APPEND ARON_ADDITIONAL_ARGUMENTS "-v")
    endif()

    # Each dependency (should) provide(s) the include paths to discover the xml and generated header files.
    set(ARON_INCLUDE_DIRS "")
    foreach(DEPENDENCY ${AX_DEPENDENCIES})
        if(TARGET ${DEPENDENCY})
            get_target_property(INCLUDE_DIRS ${DEPENDENCY} INTERFACE_INCLUDE_DIRECTORIES)

            foreach(INCLUDE_DIR ${INCLUDE_DIRS})
                list(APPEND ARON_INCLUDE_DIRS -I "${INCLUDE_DIR}")
            endforeach()
        endif()
    endforeach()

    # include dirs of this target
    list(APPEND ARON_INCLUDE_DIRS -I "${CMAKE_SOURCE_DIR}/source")

    set(LIB_FILES)

    foreach(CURRENT_ARON_XML_FILE ${AX_ARON_FILES})
        get_filename_component(ARON_INPUT_FILENAME "${CURRENT_ARON_XML_FILE}" NAME)
        get_filename_component(ARON_INPUT_REL_DIR "${CURRENT_ARON_XML_FILE}" DIRECTORY)

        set(ARON_INPUT_FILE "${CMAKE_CURRENT_SOURCE_DIR}/${CURRENT_ARON_XML_FILE}")

        set(ARON_OUTPUT_FOLDER "${CMAKE_CURRENT_BINARY_DIR}/${ARON_INPUT_REL_DIR}")
        string(REPLACE ".xml" ".aron.generated.h" ARON_OUTPUT_FILENAME "${ARON_INPUT_FILENAME}")
        set(ARON_OUTPUT_FILE "${ARON_OUTPUT_FOLDER}/${ARON_OUTPUT_FILENAME}")

        # message("CURRENT_ARON_XML_FILE: ${CURRENT_ARON_XML_FILE}")
        # message("ARON_INPUT_FILE: ${ARON_INPUT_FILE}")
        # message("ARON_OUTPUT_FOLDER: ${ARON_OUTPUT_FOLDER}")

        file(MAKE_DIRECTORY ${ARON_OUTPUT_FOLDER})
        set(ARON_GEN_ARGS
            -i "${ARON_INPUT_FILE}"
            -o "${ARON_OUTPUT_FOLDER}"
            ${ARON_INCLUDE_DIRS}
            ${ARON_ADDITIONAL_ARGUMENTS}
        )

        _armarx_get_aron_gen_command_depends(ARON_DEPENDS
            ARON_GEN_ARGS ${ARON_GEN_ARGS}
            ARON_FILE
        )

        # TODO: Variable for command should be exported in corresponding project.
        set(ARON_GEN_COMMAND "${RobotAPI_BINARY_DIR}/AronCodeGeneratorAppRun")
        add_custom_command(
            OUTPUT "${ARON_OUTPUT_FILE}"
            COMMAND "${ARON_GEN_COMMAND}"
            ARGS ${ARON_GEN_ARGS}
            DEPENDS "${ARON_GEN_COMMAND}" ${ARON_DEPENDS}
            MAIN_DEPENDENCY "${ARON_INPUT_FILE}"
            COMMENT "Generating `${ARON_OUTPUT_FILE}` from `${ARON_INPUT_FILE}`."
        )

        list(APPEND LIB_FILES "${ARON_INPUT_FILE}")
        list(APPEND LIB_FILES "${ARON_OUTPUT_FILE}")
    endforeach()

    # As long as only header files are generated, an INTERFACE library is enough.
    # If this changes in the future, change this to SHARED instead of INTERFACE.
    armarx_add_library(${TARGET} INTERFACE
        HEADERS ${LIB_FILES}
        DEPENDENCIES ${AX_DEPENDENCIES}
    )

    target_include_directories(${TARGET} INTERFACE $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/source>)
    # target_include_directories(${TARGET} INTERFACE $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/source>)
    target_include_directories(${TARGET} INTERFACE $<BUILD_INTERFACE:${CMAKE_BINARY_DIR}/source>)
    # target_include_directories(${TARGET} INTERFACE $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/source>)
    target_include_directories(${TARGET} INTERFACE $<INSTALL_INTERFACE:include>)
endfunction()


function(_armarx_get_aron_gen_command_depends DEPENDS)
    # Parse arguments.
    set(multi_param ARON_GEN_ARGS)
    set(single_param ARON_FILE)
    set(flag_param)
    cmake_parse_arguments(PARSE_ARGV 1 AX "${flag_param}" "${single_param}" "${multi_param}")
    if(DEFINED AX_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "${TARGET}: Unknown arguments `${AX_UNPARSED_ARGUMENTS}`.")
    endif()

    # TODO: Implement.

    set("${DEPENDS}" ${DEPENDS_CONTENT} PARENT_SCOPE)
endfunction()

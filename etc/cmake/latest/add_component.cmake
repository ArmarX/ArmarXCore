function(armarx_add_component TARGET)
    # Parse arguments.
    set(multi_param ICE_FILES ICE_DEPENDENCIES ARON_FILES HEADERS SOURCES DEPENDENCIES
        DEPENDENCIES_LEGACY EXECUTABLE_FILES
        # Component are leaf targets (quasi-executables) and should not be depended on => no need for visibilities
        # => all default to PUBLIC.
        DEPENDENCIES_PUBLIC DEPENDENCIES_PRIVATE DEPENDENCIES_INTERFACE
        DEPENDENCIES_LEGACY_PUBLIC DEPENDENCIES_LEGACY_PRIVATE DEPENDENCIES_LEGACY_INTERFACE)
    set(single_param)
    set(flag_param MANUAL_EXECUTABLE SHARED_COMPONENT_LIBRARY)
    cmake_parse_arguments(PARSE_ARGV 1 AX "${flag_param}" "${single_param}" "${multi_param}")
    if(DEFINED AX_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "${TARGET}: Unknown arguments `${AX_UNPARSED_ARGUMENTS}`.")
    endif()

    # TODO: Ensure that no visibility flags are used in components => default to public if so with deprecation warning
    # for compatibility.
    if(AX_DEPENDENCIES_PUBLIC OR AX_DEPENDENCIES_PRIVATE OR AX_DEPENDENCIES_INTERFACE)
        message(WARNING "armarx_add_component(...) for target '${TARGET}' does not support dependencies with "
            "visibilities ('DEPENDENCIES_PUBLIC', 'DEPENDENCIES_PRIVATE' or 'DEPENDENCIES_INTERFACE').  Use "
            "'DEPENDENCIES' instead.")
        list(APPEND AX_DEPENDENCIES ${AX_DEPENDENCIES_PUBLIC} ${AX_DEPENDENCIES_PRIVATE} ${AX_DEPENDENCIES_INTERFACE})
    endif()
    if(AX_DEPENDENCIES_LEGACY_PUBLIC OR AX_DEPENDENCIES_LEGACY_PRIVATE OR AX_DEPENDENCIES_LEGACY_INTERFACE)
        message(WARNING "armarx_add_component(...) for target '${TARGET}' does not support legacy dependencies with "
            "visibilities ('DEPENDENCIES_LEGACY_PUBLIC', 'DEPENDENCIES_LEGACY_PRIVATE' or "
            "'DEPENDENCIES_LEGACY_INTERFACE').  Use 'DEPENDENCIES_LEGACY' instead.")
        list(APPEND AX_DEPENDENCIES_LEGACY ${AX_DEPENDENCIES_LEGACY_PUBLIC} ${AX_DEPENDENCIES_LEGACY_PRIVATE}
            ${AX_DEPENDENCIES_LEGACY_INTERFACE})
    endif()
    # /Ensure visibility flags.

    # Target names and component library type.
    set(TARGET_SHARED "${TARGET}")
    set(TARGET_OBJ "${TARGET}_cmp")
    set(TARGET_ICE "${TARGET}_ice")
    set(TARGET_ARON "${TARGET}_aron")
    set(TARGET_RUN "${TARGET}_run")

    # if(NOT AX_SOURCES)
    #     message(FATAL_ERROR "No sources provided! Library cannot be built. Check and add SOURCES.")
    # endif()

    # Define component library target.
    armarx_add_library(${TARGET_OBJ}
        HEADERS ${AX_HEADERS}
        SOURCES ${AX_SOURCES}
        DEPENDENCIES ${AX_DEPENDENCIES}
        DEPENDENCIES_LEGACY ${AX_DEPENDENCIES_LEGACY}
        OBJECT
    )

    # Define Ice library target if applicable.
    if(AX_ICE_FILES)
        armarx_add_ice_library(${TARGET_ICE}
            SLICE_FILES ${AX_ICE_FILES}
            DEPENDENCIES ${AX_ICE_DEPENDENCIES}
        )
        target_link_libraries(${TARGET_OBJ} PUBLIC "${ARMARX_PROJECT_NAME}::${TARGET_ICE}")
    endif()

    # Define ARON library target if applicable.
    if(AX_ARON_FILES)
        armarx_add_aron_library(${TARGET_ARON}
            ARON_FILES ${AX_ARON_FILES}
        )
        target_link_libraries(${TARGET_OBJ} PUBLIC "${ARMARX_PROJECT_NAME}::${TARGET_ARON}")
    endif()

    # Also create shared component library if desired.
    if(${AX_SHARED_COMPONENT_LIBRARY})
        armarx_add_library(${TARGET_SHARED}
            DEPENDENCIES ${ARMARX_PROJECT_NAME}::${TARGET_OBJ}
            SHARED
        )

        # A shared library must contain source files (or generated object files). However, if the
        # object library (TARGET_OBJ) does not get built, no object library will be generated and
        # thus, the TARGET_SHARED library would not contain any sources => cmake generator error.
        # Therefore, we conditionally add a dummy source file `prevent_empty_sources.cpp`.
        if (NOT EXISTS "${CMAKE_CURRENT_BINARY_DIR}/prevent_empty_sources.cpp")
            file(TOUCH "${CMAKE_CURRENT_BINARY_DIR}/prevent_empty_sources.cpp")
        endif()
        target_sources(${TARGET_SHARED} PRIVATE $<IF:$<BOOL:$<TARGET_PROPERTY:${TARGET_OBJ},ENABLED>>, ,${CMAKE_CURRENT_BINARY_DIR}/prevent_empty_sources.cpp>)
    endif()

    # Define executable target if applicable.
    if(NOT ${AX_MANUAL_EXECUTABLE})
        # A decoupled main is used when no executable files were supplied but the
        # user didn't chose the MANUAL_EXECUTABLE option.
        set(USE_DECOUPLED_MAIN)
        if(NOT AX_EXECUTABLE_FILES)
            set(USE_DECOUPLED_MAIN "USE_DECOUPLED_MAIN")
        endif()

        armarx_add_component_executable(${TARGET_RUN}
            SOURCES ${AX_EXECUTABLE_FILES}
            DEPENDENCIES "${ARMARX_PROJECT_NAME}::${TARGET_OBJ}"
            ${USE_DECOUPLED_MAIN}
        )
    endif()
endfunction()


function(armarx_add_component_executable TARGET)
    # Parse arguments.
    set(multi_param SOURCES DEPENDENCIES)
    set(single_param)
    set(flag_param USE_DECOUPLED_MAIN)
    cmake_parse_arguments(PARSE_ARGV 1 AX "${flag_param}" "${single_param}" "${multi_param}")
    if(DEFINED AX_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "${TARGET}: Unknown arguments `${AX_UNPARSED_ARGUMENTS}`.")
    endif()

    # Variables being modified in this context. 
    set(SOURCES ${AX_SOURCES})
    set(DEPENDENCIES ${AX_DEPENDENCIES})

    # TODO: Replace by static lib providing the main in ArmarXCore and just link target.
    # Add decoupled main if no user defined executable files are passed.
    if(${AX_USE_DECOUPLED_MAIN})
        list(APPEND SOURCES "${ArmarXCore_TEMPLATES_DIR}/decoupled_main.cpp")
        list(APPEND DEPENDENCIES DecoupledSingleComponent)
    endif()

    # Define target.
    armarx_add_executable(${TARGET}
        SOURCES ${SOURCES}
        DEPENDENCIES ${DEPENDENCIES}
    )

    # The ARMARX_COMPONENT_EXECUTABLE property is required to flag the executable as
    # component executable so that it can properly be detected by ArmarXGui etc.
    set_property(
        TARGET ${TARGET}
        PROPERTY ARMARX_COMPONENT_EXECUTABLE TRUE
    )

    # ensure that the property is exported correctly to XYTargets.cmake
    set_property(
        TARGET ${TARGET} 
        APPEND
        PROPERTY EXPORT_PROPERTIES "ARMARX_COMPONENT_EXECUTABLE"
    )

    # A test that launches the component executable to expose its properties.
    # In case of USE_DECOUPLED_MAIN, the test will reveal missing usage of the 
    # macro ARMARX_REGISTER_COMPONENT_EXECUTABLE.
    if(${AX_USE_DECOUPLED_MAIN})
        if(${ARMARX_BUILD_TESTS})
            get_target_property(ENABLE_TEST ${TARGET} ARMARX_ENABLED)

            if(${ENABLE_TEST})
                message(STATUS "adding test ${TARGET}__register_component_executable__test")

                add_test(
                    NAME ${TARGET}__register_component_executable__test 
                    COMMAND ${TARGET} -p
                )
            endif()
        endif()
    endif()

endfunction()

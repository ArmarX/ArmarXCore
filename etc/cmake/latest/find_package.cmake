macro(armarx_find_package SCOPE NAMESPACED_DEPENDENCY)
    if(NOT "${SCOPE}" STREQUAL "PUBLIC" AND NOT "${SCOPE}" STREQUAL "PRIVATE" AND NOT "${SCOPE}" STREQUAL "INTERFACE")
        message(FATAL_ERROR "${NAMESPACED_DEPENDENCY}: armarx_find_package expects the first parameter to be any of PUBLIC, PRIVATE, or INTERFACE.")
    endif()

    message(STATUS "Finding package `${NAMESPACED_DEPENDENCY}`.")

    string(REPLACE "::" "_" DEPENDENCY "${NAMESPACED_DEPENDENCY}")

    set(FIND_PACKAGE_CALL ${DEPENDENCY})
    list(APPEND FIND_PACKAGE_CALL ${ARGN})

    # Only actually run find_package if scope is PUBLIC or PRIVATE.
    if("${SCOPE}" STREQUAL "PUBLIC" OR "${SCOPE}" STREQUAL "PRIVATE")
        find_package(${FIND_PACKAGE_CALL})
    endif()

    # Only add as dependency if scope is PUBLIC or INTERFACE.
    if(NOT "${SCOPE}" STREQUAL "PRIVATE")
        # Create a new unique global property for this find_package call.
        set(PROPERTY_NAME "ARMARX_FIND_PACKAGE_${DEPENDENCY}")
        set_property(GLOBAL APPEND PROPERTY ${PROPERTY_NAME} ${FIND_PACKAGE_CALL})
        set_property(GLOBAL APPEND PROPERTY ARMARX_DEPENDENCIES ${DEPENDENCY})
    endif()

    # TODO: Remove after full migration.
    if(DEFINED ${DEPENDENCY}_SCENARIOS_DIR)  # Hack: "if this is an ArmarX project"
        if(EXISTS "${${DEPENDENCY}_USE_FILE}")
            message(WARNING "Legacy mode: Including Use-file for ${DEPENDENCY}.")
            include("${${DEPENDENCY}_USE_FILE}")
            include_directories(${${DEPENDENCY}_INCLUDE_DIRS})
        endif()

        set_property(GLOBAL APPEND PROPERTY ARMARX_DEPENDENT_PACKAGES "${DEPENDENCY}")
        set_property(GLOBAL APPEND PROPERTY ARMARX_PACKAGE_BUILD_DIRS "${DEPENDENCY}:${${DEPENDENCY}_BUILD_DIR}")
    endif()
endmacro()

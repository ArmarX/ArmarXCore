option(ARMARX_USE_GOLD_LINKER "Use the faster Gold Linker" FALSE)
if(ARMARX_USE_GOLD_LINKER)
    set (LDFLAGS "${LDFLAGS} -Wl,-fuse-ld=gold ")
endif()

#we want Release and RelWithDebInfo to use the same optimization level
string(REGEX MATCH "([ \t]|^)-O[^ \t]+([ \t]|^)" RELEASE_O_LEVEL ${CMAKE_CXX_FLAGS_RELEASE})
string(REGEX MATCH "([ \t]|^)-O[^ \t]+([ \t]|^)" RELWITHDEBINFO_O_LEVEL ${CMAKE_CXX_FLAGS_RELWITHDEBINFO})
if(NOT RELEASE_O_LEVEL STREQUAL RELWITHDEBINFO_O_LEVEL)
    string(REPLACE "${RELWITHDEBINFO_O_LEVEL}" "${RELEASE_O_LEVEL}" CMAKE_CXX_FLAGS_RELWITHDEBINFO ${CMAKE_CXX_FLAGS_RELWITHDEBINFO})
    #message(STATUS "Changing o level of RelWithDebInfo to match o level of Release (from '${RELWITHDEBINFO_O_LEVEL}' to '${RELEASE_O_LEVEL}')")
    #message(STATUS "CMAKE_CXX_FLAGS_RELWITHDEBINFO = ${CMAKE_CXX_FLAGS_RELWITHDEBINFO}")
endif()

# Various compiler settings for common targets
include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++2a" COMPILER_SUPPORTS_CXX2A)
CHECK_CXX_COMPILER_FLAG("-stdlib=libstdc++" COMPILER_SUPPORTS_libstdcpp)
#gcc silently accepts unknown -Wno- warnings if no other error occurs
#-> test for positive version
#https://stackoverflow.com/a/38786117

include(CheckCCompilerFlag)
CHECK_C_COMPILER_FLAG("-std=c99" COMPILER_SUPPORTS_C99)

set(ARMARX_GENERAL_COMPILE_FLAGS "-Wall -Wextra -Wpedantic -Wnon-virtual-dtor -Wno-long-long -Wno-unused-parameter -Werror=return-type")
#https://developers.redhat.com/blog/2018/03/21/compiler-and-linker-flags-gcc/
#https://wiki.debian.org/Hardening#gcc_-Wformat_-Wformat-security
set(ARMARX_GENERAL_SECURITY_COMPILE_FLAGS)
set(ARMARX_GENERAL_SECURITY_COMPILE_FLAGS "${ARMARX_GENERAL_SECURITY_COMPILE_FLAGS} -D_FORTIFY_SOURCE=2")           #Run-time buffer overflow detection
set(ARMARX_GENERAL_SECURITY_COMPILE_FLAGS "${ARMARX_GENERAL_SECURITY_COMPILE_FLAGS} -Wformat")                      #call out simple printf format string vulnerabilities
set(ARMARX_GENERAL_SECURITY_COMPILE_FLAGS "${ARMARX_GENERAL_SECURITY_COMPILE_FLAGS} -Wformat-security")             #Reject potentially unsafe format string arguents
set(ARMARX_GENERAL_SECURITY_COMPILE_FLAGS "${ARMARX_GENERAL_SECURITY_COMPILE_FLAGS} -D_GLIBCXX_ASSERTIONS")         #Run-time bounds checking for C++ strings and containers
set(ARMARX_GENERAL_SECURITY_COMPILE_FLAGS "${ARMARX_GENERAL_SECURITY_COMPILE_FLAGS} -fasynchronous-unwind-tables")  #Increased reliability of backtraces
set(ARMARX_GENERAL_SECURITY_COMPILE_FLAGS "${ARMARX_GENERAL_SECURITY_COMPILE_FLAGS} -fexceptions")                  #Enable table-based thread cancellation
set(ARMARX_GENERAL_SECURITY_COMPILE_FLAGS "${ARMARX_GENERAL_SECURITY_COMPILE_FLAGS} -fstack-clash-protection")      #Increased reliability of stack overflow detection
set(ARMARX_GENERAL_SECURITY_COMPILE_FLAGS "${ARMARX_GENERAL_SECURITY_COMPILE_FLAGS} -fstack-protector-strong")      #Stack smashing protector
set(ARMARX_GENERAL_SECURITY_COMPILE_FLAGS "${ARMARX_GENERAL_SECURITY_COMPILE_FLAGS} -fcf-protection")               #Control flow integrity protection
set(ARMARX_GENERAL_SECURITY_COMPILE_FLAGS "${ARMARX_GENERAL_SECURITY_COMPILE_FLAGS} -Wl,-z,relro")                  #Read-only segments after relocation
set(ARMARX_GENERAL_SECURITY_COMPILE_FLAGS "${ARMARX_GENERAL_SECURITY_COMPILE_FLAGS} -Wl,-z,now")                    #Disable lazy binding
set(ARMARX_GENERAL_SECURITY_COMPILE_FLAGS "${ARMARX_GENERAL_SECURITY_COMPILE_FLAGS} -Wl,-z,defs")                   #Detect and reject underlinking
## needed for compability between gcc and clang
if(COMPILER_SUPPORTS_libstdcpp)
    #message(STATUS "Using libstdc++")
    set(ARMARX_GENERAL_COMPILE_FLAGS "${ARMARX_GENERAL_COMPILE_FLAGS} -stdlib=libstdc++")
    # needed to make clang work
    set(LDFLAGS "${LDFLAGS} -lstdc++ -lm ")
endif()

#fix for old graphviz version (similar issue, see: https://github.com/KDAB/GammaRay/issues/70)
# /usr/include/graphviz/cdt.h:27:20: error: declaration of 'int memcmp(const void*, const void*, size_t)' has a different exception specifier
set(ARMARX_GENERAL_COMPILE_FLAGS "${ARMARX_GENERAL_COMPILE_FLAGS} -DHAVE_STRING_H")

# TODO: Use target_add_definitions instead.
add_definitions(-DBOOST_ENABLE_ASSERT_HANDLER -DARMARX_VERSION=${ARMARX_PACKAGE_LIBRARY_VERSION})

set(CMAKE_C_FLAGS "${ARMARX_GENERAL_COMPILE_FLAGS} ${ARMARX_GENERAL_SECURITY_COMPILE_FLAGS} -Wimplicit-function-declaration")

set(CMAKE_CXX_FLAGS "-fconcepts ${ARMARX_GENERAL_COMPILE_FLAGS} ${ARMARX_GENERAL_SECURITY_COMPILE_FLAGS}")

if(COMPILER_SUPPORTS_CXX2A)
    # cmake 3.10 does not understand c++2a, so we tell it we will handle the standard flag
    set(CMAKE_CXX_STANDARD_DEFAULT)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++2a ")
else()
    message(FATAL_ERROR "The compiler ${CMAKE_CXX_COMPILER} has no C++2a support. Please use a different C++ compiler.")
endif()

if(COMPILER_SUPPORTS_C99)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c99 ")
else()
    message(FATAL_ERROR "The compiler ${CMAKE_C_COMPILER} has no C99 support. Please use a different C compiler.")
endif()

set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -g ")

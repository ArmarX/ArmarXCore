function(armarx_install_project)
    message(STATUS "\n== Installing project `${ARMARX_PROJECT_NAME_PRETTY}` ...")

    # Add "etc", "scenarios", and "data" as custom target for IDE support.
    _armarx_add_misc_files()

    # Get all targets and sort them by type.
    get_property(TARGETS GLOBAL PROPERTY ARMARX_TARGETS)
    set(PROJECT_EXES)
    set(PROJECT_SHARED_LIBS)
    set(PROJECT_STATIC_LIBS)
    set(PROJECT_INTERFACE_LIBS)
    foreach(TARGET ${TARGETS})
        get_target_property(TARGET_TYPE ${TARGET} TYPE)
        if("${TARGET_TYPE}" STREQUAL "EXECUTABLE")
            list(APPEND PROJECT_EXES "${TARGET}")
        elseif("${TARGET_TYPE}" STREQUAL "SHARED_LIBRARY")
            list(APPEND PROJECT_SHARED_LIBS "${TARGET}")
        elseif("${TARGET_TYPE}" STREQUAL "STATIC_LIBRARY")
            list(APPEND PROJECT_STATIC_LIBS "${TARGET}")
        elseif("${TARGET_TYPE}" STREQUAL "INTERFACE_LIBRARY")
            list(APPEND PROJECT_INTERFACE_LIBS "${TARGET}")
        endif()
    endforeach()

    # Generate component executables report file.
    _armarx_generate_component_executables_report(TARGETS ${PROJECT_EXES})

    # Generate the needed Ice configs.
    #generate_datapath_config()  # TODO: remove?

    set(SHARE_CMAKE_DIR "${CMAKE_BINARY_DIR}/share/cmake/${PROJECT_NAME}")
    set(CONFIG_VERSION_NAME "${ARMARX_PROJECT_NAME}ConfigVersion.cmake")
    set(CONFIG_NAME "${ARMARX_PROJECT_NAME}Config.cmake")
    set(TARGETS_NAME "${ARMARX_PROJECT_NAME}Targets.cmake")

    _armarx_generate_project_config_version(
        SHARE_CMAKE_DIR ${SHARE_CMAKE_DIR}
        CONFIG_VERSION_NAME ${CONFIG_VERSION_NAME}
    )

    _armarx_generate_project_config(
        SHARE_CMAKE_DIR ${SHARE_CMAKE_DIR}
        CONFIG_NAME ${CONFIG_NAME}
        TARGETS_NAME ${TARGETS_NAME}
    )

    _armarx_generate_and_install_targets(
        SHARE_CMAKE_DIR ${SHARE_CMAKE_DIR}
        TARGETS_NAME ${TARGETS_NAME}
    )

    _armarx_install_share_cmake(${SHARE_CMAKE_DIR})

    # Install project's data directory.
    if(EXISTS "${PROJECT_DATA_DIR}")
        install(
            DIRECTORY "${PROJECT_DATA_DIR}"
            DESTINATION "share/${ARMARX_PROJECT_NAME}"
            USE_SOURCE_PERMISSIONS
            OPTIONAL
            COMPONENT data
            PATTERN ".gitkeep" EXCLUDE
        )
    endif()

    # Install templates.
    if (EXISTS "${${ARMARX_PROJECT_NAME}_TEMPLATES_DIR}")
        install(
            DIRECTORY "${${ARMARX_PROJECT_NAME}_TEMPLATES_DIR}"
            DESTINATION share/${ARMARX_PROJECT_NAME}
            COMPONENT templates
        )
    endif()

    # Install generated config files.
    if (EXISTS "${PACKAGE_CONFIG_DIR}")
        install(
            DIRECTORY "${PACKAGE_CONFIG_DIR}"
            DESTINATION share/${ARMARX_PROJECT_NAME}
            COMPONENT config
        )
    endif()

    # Install Ice interface files.
    # TODO: Search for ice files in whole source dir.
    install(
        DIRECTORY "${PROJECT_SOURCECODE_DIR}/${ARMARX_PROJECT_INCLUDE_PATH}/interface"
        DESTINATION "share/${ARMARX_PROJECT_NAME}/slice/${ARMARX_PROJECT_NAME}"
        COMPONENT interfaces
        FILES_MATCHING PATTERN "*.ice"
    )

    # Install scenario configuration files.
    if(EXISTS "${CMAKE_SOURCE_DIR}/scenarios")
        install(
            DIRECTORY "${CMAKE_SOURCE_DIR}/scenarios"
            DESTINATION "share/${ARMARX_PROJECT_NAME}"
            COMPONENT scenarios
            FILES_MATCHING
                PATTERN "*.scx"
                PATTERN "*.cfg"
                PATTERN "*.icegrid.xml"
                PATTERN "*.xml"
        )
    endif()

    # Print feature summary here since this is the macro which needs to be executed
    # as the last statement in the toplevel CMakeLists.txt file

    # Post-config addon hooks.
    armarx_addon_cpack()
    armarx_addon_feature_summary()
    armarx_addon_target_info()
    armarx_addon_header_check()

    _armarx_export_project()

    message(VERBOSE "Installation complete")
    message(STATUS "")
endfunction()


# Adds files inside "etc", "scenarios", and "data" as sources to a custom target for IDE support.
function(_armarx_add_misc_files)
    foreach(SUBDIR etc scenarios data)
        set(PATH "${CMAKE_CURRENT_LIST_DIR}/${SUBDIR}")
        if(EXISTS "${PATH}")
            file(GLOB_RECURSE FILES "${PATH}/*")
            if(NOT "${FILES}" STREQUAL "")
                add_custom_target(${SUBDIR}_files SOURCES ${FILES})
            endif()
        endif()
    endforeach()
endfunction()


# Generates a report file of all component executables so they can be distinguished from other
# executables for example in the ScenarioManager.
function(_armarx_generate_component_executables_report)
    # Parse arguments.
    set(multi_param TARGETS)
    set(single_param)
    set(flag_param)
    cmake_parse_arguments(PARSE_ARGV 0 AX "${flag_param}" "${single_param}" "${multi_param}")
    if(DEFINED AX_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "Unknown arguments `${AX_UNPARSED_ARGUMENTS}`.")
    endif()

    set(FILENAME "${CMAKE_BINARY_DIR}/component_executables_report.txt")

    file(WRITE ${FILENAME} "")

    foreach(TARGET ${TARGETS})
        get_target_property(IS_COMP ${TARGET} ARMARX_COMPONENT_EXECUTABLE)
        if(${IS_COMP})
            file(APPEND ${FILENAME} "${TARGET}\n")
        endif()
    endforeach()
endfunction()


function(_armarx_generate_project_config_version)
    # Parse arguments.
    set(multi_param)
    set(single_param SHARE_CMAKE_DIR CONFIG_VERSION_NAME)
    set(flag_param)
    cmake_parse_arguments(PARSE_ARGV 0 AX "${flag_param}" "${single_param}" "${multi_param}")
    if(DEFINED AX_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "Unknown arguments `${AX_UNPARSED_ARGUMENTS}`.")
    endif()

    # Variables used within template.
    set(PROJECT_VERSION "${ARMARX_PACKAGE_LIBRARY_VERSION}")

    configure_file(
        ${ArmarXCore_TEMPLATES_DIR}/cmake/ProjectConfigVersion.cmake.in
        "${AX_SHARE_CMAKE_DIR}/${AX_CONFIG_VERSION_NAME}"
        @ONLY
    )

    # This file is also needed in the build directory root, so symlink it.
    execute_process(
        COMMAND ${CMAKE_COMMAND} -E create_symlink "${AX_SHARE_CMAKE_DIR}/${AX_CONFIG_VERSION_NAME}" "${CMAKE_BINARY_DIR}/${AX_CONFIG_VERSION_NAME}"
    )
endfunction()


function(_armarx_generate_project_config)
    # Parse arguments.
    set(multi_param)
    set(single_param SHARE_CMAKE_DIR CONFIG_NAME TARGETS_NAME)
    set(flag_param)
    cmake_parse_arguments(PARSE_ARGV 0 AX "${flag_param}" "${single_param}" "${multi_param}")
    if(DEFINED AX_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "Unknown arguments `${AX_UNPARSED_ARGUMENTS}`.")
    endif()

    # Variables used within template.
    set(PROJECT_CONFIG_PROJECT_NAME "${ARMARX_PROJECT_NAME}")
    set(PROJECT_CONFIG_TARGETS_FILE "${AX_TARGETS_NAME}")
    set(PROJECT_CONFIG_USE_FILE "Use${ARMARX_PROJECT_NAME}.cmake")
    get_property(PROJECT_CONFIG_DEPENDENCIES GLOBAL PROPERTY ARMARX_DEPENDENCIES)
    set(PROJECT_CONFIG_FIND_PACKAGE_DEFS "")
    foreach(DEPENDENCY ${PROJECT_CONFIG_DEPENDENCIES})
        get_property(FIND_PACKAGE_CALL GLOBAL PROPERTY "ARMARX_FIND_PACKAGE_${DEPENDENCY}")
        string(REPLACE ";" " " FIND_PACKAGE_CALL "${FIND_PACKAGE_CALL}")
        string(APPEND PROJECT_CONFIG_FIND_PACKAGE_DEFS "set(${ARMARX_PROJECT_NAME}_FIND_PACKAGE_${DEPENDENCY} ${FIND_PACKAGE_CALL})\n")
    endforeach()
    string(REGEX REPLACE "\n$" "" PROJECT_CONFIG_FIND_PACKAGE_DEFS "${PROJECT_CONFIG_FIND_PACKAGE_DEFS}")  # Remove last new line.

    # ProjectConfig.cmake (for installation).
    configure_file(
        ${ArmarXCore_TEMPLATES_DIR}/cmake/ProjectConfig_install.cmake.in
        "${AX_SHARE_CMAKE_DIR}/${AX_CONFIG_NAME}"
        @ONLY
    )

    # Variables used within template for build-tree variant.
    set(PROJECT_CONFIG_PROJECT_CMAKE_DIR "${${ARMARX_PROJECT_NAME}_CMAKE_DIR}")

    # TODO: Legacy variables used within the template. Do not depend on them.
    set(PROJECT_CONFIG_INCLUDE_DIRS "${PROJECT_INCLUDE_DIRECTORIES}")
    set(PROJECT_CONFIG_LIB_DIR "${ARMARX_LIB_DIR}")
    set(PROJECT_CONFIG_BIN_DIR "${ARMARX_BIN_DIR}")
    set(PROJECT_CONFIG_CMAKE_DIR "${${ARMARX_PROJECT_NAME}_CMAKE_DIR}")
    set(PROJECT_CONFIG_INTERFACE_DIRS "${PROJECT_SOURCECODE_DIR}")
    set(PROJECT_CONFIG_INTERFACE_LIBRARY "${ARMARX_PROJECT_INTERFACE_LIBRARY}")
    set(PROJECT_CONFIG_SCENARIOS_DIR "${PROJECT_SCENARIOS_DIR}")
    set(PROJECT_CONFIG_STATECHARTS_DIR "${PROJECT_STATECHARTS_DIR}")
    set(PROJECT_CONFIG_DATA_DIR "${PROJECT_DATA_DIR}")
    set(PROJECT_CONFIG_TEMPLATES_DIR "${PROJECT_ETC_DIR}/templates")
    set(PROJECT_CONFIG_JAR_DIR "${PROJECT_BINARY_DIR}/lib")
    set(PROJECT_CONFIG_DOXYGEN_TAG_FILE "${DOXYGEN_DOCUMENTATION_DIR}/${DOXYGEN_DOCUMENTATION_HTML_OUTPUT_DIR_REL}/${PROJECT_NAME}.tag")
    set(PROJECT_CONFIG_ETC_DIR "${PROJECT_ETC_DIR}")
    set(PROJECT_CONFIG_SOURCE_DIR "${PROJECT_SOURCE_DIR}")
    set(PROJECT_CONFIG_DOXYGEN_DOCUMENTATION_INPUT_DIR "${DOXYGEN_DOCUMENTATION_INPUT_DIR}")
    set(PROJECT_CONFIG_DOXYGEN_DOCUMENTATION_SNIPPET_DIR "${DOXYGEN_DOCUMENTATION_SNIPPET_DIR}")
    set(PROJECT_CONFIG_DOXYGEN_DOCUMENTATION_IMAGE_DIR "${DOXYGEN_DOCUMENTATION_IMAGE_DIR}")
    set(PROJECT_CONFIG_CMAKE_BINARY_DIR "${CMAKE_BINARY_DIR}")
    set(PROJECT_CONFIG_CONFIG_DIR "${PACKAGE_CONFIG_DIR}")
    set(PROJECT_CONFIG_ICEGRID_DIR "${PACKAGE_ICEGRID_DIR}")
    get_property(PROJECT_CONFIG_SOURCE_PACKAGE_DEPENDENCIES GLOBAL PROPERTY ARMARX_DEPENDENT_PACKAGES)
    get_property(PROJECT_CONFIG_PACKAGE_DEPENDENCY_PATHS GLOBAL PROPERTY ARMARX_PACKAGE_BUILD_DIRS)

    # ProjectConfig.cmake (for use in build-tree).
    configure_file(
        ${ArmarXCore_TEMPLATES_DIR}/cmake/ProjectConfig.cmake.in
        "${CMAKE_BINARY_DIR}/${AX_CONFIG_NAME}"
        @ONLY
    )
endfunction()


function(_armarx_generate_and_install_targets)
    # Parse arguments.
    set(multi_param)
    set(single_param SHARE_CMAKE_DIR TARGETS_NAME)
    set(flag_param)
    cmake_parse_arguments(PARSE_ARGV 0 AX "${flag_param}" "${single_param}" "${multi_param}")
    if(DEFINED AX_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "Unknown arguments `${AX_UNPARSED_ARGUMENTS}`.")
    endif()

    # Install the export set for use with the install-tree (belongs to the files generated above)
    if(TARGETS)
        export(
            TARGETS ${TARGETS}
            FILE "${AX_SHARE_CMAKE_DIR}/${AX_TARGETS_NAME}"
            NAMESPACE ${ARMARX_PROJECT_NAME}::
        )

        install(
            TARGETS ${TARGETS}
            EXPORT ${ARMARX_PROJECT_NAME}Targets
        )

        install(
            EXPORT ${ARMARX_PROJECT_NAME}Targets
            FILE "${AX_TARGETS_NAME}"
            DESTINATION "share/cmake/${ARMARX_PROJECT_NAME}"
            NAMESPACE ${ARMARX_PROJECT_NAME}::
        )

        # The targets-file is also needed in the build directory root, so symlink it.
        execute_process(
            COMMAND ${CMAKE_COMMAND} -E create_symlink "${AX_SHARE_CMAKE_DIR}/${AX_TARGETS_NAME}" "${CMAKE_BINARY_DIR}/${AX_TARGETS_NAME}"
        )
    endif()
endfunction()


# Installs the directory ${CMAKE_PREFIX_DIR}/share/cmake/${PROJECT_NAME} as well as
# the project's CMake dir containing Find*.cmake files etc.
function(_armarx_install_share_cmake SHARE_CMAKE_DIR)
    if(EXISTS "${${ARMARX_PROJECT_NAME}_CMAKE_DIR}")
        install(
            DIRECTORY "${${ARMARX_PROJECT_NAME}_CMAKE_DIR}/"
            DESTINATION share/cmake/${ARMARX_PROJECT_NAME}
            COMPONENT cmake
        )
    endif()

    install(
        DIRECTORY "${SHARE_CMAKE_DIR}/"
        DESTINATION share/cmake/${ARMARX_PROJECT_NAME}
        COMPONENT cmake
    )
endfunction()


macro(_armarx_export_project)
    # Export the package for use from the build-tree (this registers the build-tree with a global
    # CMake-registry).
    export(PACKAGE ${ARMARX_PROJECT_NAME})
endmacro()

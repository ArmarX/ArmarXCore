find_package(Lcov)


# Option to en-/disable unittests
option(ARMARX_BUILD_TESTS "Build unit tests." ON)


# Option to en-/disable generation of gcov reports
option(ARMARX_ENABLE_COVERAGE "Enable the creation of gcov coverage reports" OFF)
if("${ARMARX_ENABLE_COVERAGE}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --coverage")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} --coverage")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} --coverage")
    set(ARMARX_BUILD_TESTS ON)
endif()


# This macro creates the Test.h file which is required to collect test results on Jenkins.  The
# location for the file is testing/<ProjectInstallName> inside the build directory
macro(_armarx_test_generate_test_header)
    set(TESTING_INCLUDE_PATH "${ARMARX_PROJECT_NAME}")
    if(NOT "${ARMARX_PROJECT_NAMESPACE}" STREQUAL "")
        set(TESTING_INCLUDE_PATH "${ARMARX_PROJECT_NAMESPACE}/${ARMARX_PROJECT_NAME_RAW}")
    endif()

    set(ARMARX_TEST_H_DIRECTORY "${PROJECT_BINARY_DIR}/testing/${TESTING_INCLUDE_PATH}")

    # if the required build/testing/<ProjectInstallName> directory is not
    if (NOT EXISTS "${ARMARX_TEST_H_DIRECTORY}")
        file(MAKE_DIRECTORY "${ARMARX_TEST_H_DIRECTORY}")
    endif()

    # include the directory
    # include_directories("${PROJECT_BINARY_DIR}/testing/")

    # export all CMake variables into VARIABLE_LIST_CODE which is then exported into
    # the Test.h file
    # This makes it possible to access all CMake variables like inside Tests
    get_cmake_property(_variableNames VARIABLES)
    set(VARIABLE_LIST_CODE "")
    foreach (_variableName ${_variableNames})
        STRING(REGEX REPLACE "\\\\" "\\\\\\\\" _variableValue "${${_variableName}}")
        STRING(REGEX REPLACE "\n" "\\\\n" _variableValue "${_variableValue}")
        STRING(REGEX REPLACE "\"" "\\\\\"" _variableValue "${_variableValue}")
        set(VARIABLE_LIST_CODE "${VARIABLE_LIST_CODE}${_variableName}=${_variableValue}\n")
    endforeach()
    file(WRITE "${ARMARX_TEST_H_DIRECTORY}/cmakevars.cfg" "${VARIABLE_LIST_CODE}")
    # create Test.h file which must be included by all testbench files
    configure_file("${ArmarXCore_TEMPLATES_DIR}/ComponentTemplate/Test.h.in" "${ARMARX_TEST_H_DIRECTORY}/Test.h")
endmacro()


if(${ARMARX_BUILD_TESTS})
    if(NOT Boost_UNIT_TEST_FRAMEWORK_LIBRARY)
        message(STATUS "Searching boost test")
        find_package(Boost ${ArmarX_BOOST_VERSION} EXACT REQUIRED COMPONENTS unit_test_framework)
    endif()

    if(NOT Boost_UNIT_TEST_FRAMEWORK_LIBRARY)
        message(SEND_ERROR "Boost::Test was not found. Can not build unit tests.")
    endif()

    enable_testing()

    _armarx_test_generate_test_header()

    # add the "coverage" custom target which produces an html coverage report via genhtml
    if(Lcov_FOUND AND ${ARMARX_ENABLE_COVERAGE})
        set(COVERAGE_OUTPUT_DIR "${CMAKE_BINARY_DIR}/coverage")
        set(COVERAGE_OUTPUT_FILE "${CMAKE_BINARY_DIR}/coverage.lcov.info")

        # Hack: execute "CMAKE_BUILD_TOOL test" manually
        # it is not possible to add a dependency to the test target since it is reported as not existing
        add_custom_target(coverage-reset
            COMMAND "${Lcov_EXECUTABLE}" --directory "${CMAKE_BINARY_DIR}" --zerocounters
            COMMAND "${CMAKE_BUILD_TOOL}" test
            WORKING_DIRECTORY "${CMAKE_BINARY_DIR}"
            COMMENT "Reset Code Coverage counters for ${PROJECT_NAME} and run tests.")

        add_custom_target(coverage
              COMMAND "${Lcov_EXECUTABLE}" --capture --base-directory "${CMAKE_BINARY_DIR}" --directory "${CMAKE_BINARY_DIR}" --output-file "${COVERAGE_OUTPUT_FILE}"
              COMMAND "${Lcov_GenHtml_EXECUTABLE}"  --show-details --output-directory "${COVERAGE_OUTPUT_DIR}" "${COVERAGE_OUTPUT_FILE}"
              WORKING_DIRECTORY "${CMAKE_BINARY_DIR}"
              COMMENT "Generating Code Coverage report for ${PROJECT_NAME}")
        add_dependencies(coverage coverage-reset)
    else()
        add_custom_target(coverage-reset
            COMMENT "Lcov has not been found.")

        add_custom_target(coverage
            COMMENT "Lcov has not been found.")
    endif()
endif()

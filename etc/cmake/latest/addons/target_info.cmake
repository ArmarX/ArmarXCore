option(ARMARX_CMAKE_ADDON_TARGET_INFO "For each target, generate a json file that provides information e.g. about associated headers." OFF)

function(_armarx_generate_target_info TARGET)

  get_target_property(AX_TARGET_CPP_HEADERS ${TARGET} CPP_HEADERS)

  # create list of header files with abs paths => ${TARGET}_HEADERS
  foreach(HEADER_FILE ${AX_TARGET_CPP_HEADERS})
    get_filename_component(HEADER_ABS_PATH
                    "${HEADER_FILE}"
                    ABSOLUTE)

    list(APPEND ${TARGET}_HEADERS ${HEADER_ABS_PATH})
  endforeach()

  # include directories with abs paths => ${TARGET}_INCLUDE_DIRS
  get_target_property(INCLUDE_DIRS ${TARGET} INCLUDE_DIRECTORIES)
  foreach(INCLUDE_DIR ${INCLUDE_DIRS})
    if(NOT "${INCLUDE_DIR}" STREQUAL "")
      get_filename_component(INCLUDE_DIR_ABS
                    "${INCLUDE_DIR}"
                    ABSOLUTE)
      list(APPEND ${TARGET}_INCLUDE_DIRS "${INCLUDE_DIR_ABS}")
    endif()
  endforeach()

  # Generate the json file
  file(GENERATE
    OUTPUT "${CMAKE_BINARY_DIR}/target_info/${PROJECT_NAME}::${TARGET}.json"
    CONTENT 
    "{
      \"name\": \"${PROJECT_NAME}::${TARGET}\",
      \"headers\": \"${${TARGET}_HEADERS}\",
      \"include_directories\": \"${${TARGET}_INCLUDE_DIRS}\",
      \"library\": \"${CMAKE_BINARY_DIR}/lib/lib${PROJECT_NAME}_${TARGET}.so\"
    }" 
  )

endfunction()

function(armarx_addon_target_info)
  if(${ARMARX_CMAKE_ADDON_TARGET_INFO})

    message(STATUS "\n== Generating target info ...")

    get_property(AX_TARGETS 
      GLOBAL 
      PROPERTY ARMARX_TARGETS
    )

    foreach(TARGET ${AX_TARGETS})
      # only generate target info for shared libraries
      get_target_property(AX_TARGET_TYPE ${TARGET} TYPE)
      if(${AX_TARGET_TYPE} STREQUAL "SHARED_LIBRARY")
        message(VERBOSE "${PROJECT_NAME}::${TARGET}")
        _armarx_generate_target_info(${TARGET})
      endif()

    endforeach()
  endif()
endfunction()

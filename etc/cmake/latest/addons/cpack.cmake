option(ARMARX_CMAKE_ADDON_ADD_CPACK "Include CPack for the installation steps." OFF)

if(${ARMARX_CMAKE_ADDON_ADD_CPACK})
    find_package(Git QUIET)

    if(CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64")
        set(CPACK_DEBIAN_ARCHITECTURE "amd64")
    else()
        set(CPACK_DEBIAN_ARCHITECTURE "i386")
    endif()

    include(InstallRequiredSystemLibraries)

    set(CPACK_SET_DESTDIR "on")
    set(CPACK_PACKAGING_INSTALL_PREFIX "/tmp")

    set(CPACK_RESOURCE_FILE_LICENSE "${PROJECT_SOURCE_DIR}/LICENSE.md")
    set(CPACK_DEBIAN_PACKAGE_LICENSE "gpl")

    set(CPACK_PACKAGE_DESCRIPTION "${ARMARX_PROJECT_NAME}")
    set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "${ARMARX_PROJECT_NAME}")
    set(CPACK_PACKAGE_VENDOR "KIT")
    set(CPACK_PACKAGE_CONTACT "Nikolaus Vahrenkamp <vahrenkamp@kit.edu>")

    set(CPACK_GENERATOR "DEB")
    set(CPACK_DEBIAN_PACKAGE_NAME "armarx-${ARMARX_PROJECT_NAME}")
    #set(CPACK_DEBIAN_PACKAGE_RECOMMENDS "ArmarXCore" "ArmarXGui" "RobotAPI" "MemoryX" "VisionX" "SimulationX" "RobotSkills" "Armar3")
    set(CPACK_DEBIAN_PACKAGE_SHLIBDEPS ON)

    set(CPACK_PACKAGE_VERSION_MAJOR "${ARMARX_PACKAGE_LIBRARY_VERSION_MAJOR}")
    set(CPACK_PACKAGE_VERSION_MINOR "${ARMARX_PACKAGE_LIBRARY_VERSION_MINOR}")
    set(CPACK_PACKAGE_VERSION_PATCH "${ARMARX_PACKAGE_LIBRARY_VERSION_PATCH}")

    set(ARMARX_PACKAGE_VERSION "v${ARMARX_PACKAGE_LIBRARY_VERSION_MAJOR}.${ARMARX_PACKAGE_LIBRARY_VERSION_MINOR}.${ARMARX_PACKAGE_LIBRARY_VERSION_PATCH}")

    if(GIT_FOUND)
        # output format <tag>-<commit-id>-dirty or <tag>-<commit-id> or <commit-id>
        execute_process(COMMAND ${GIT_EXECUTABLE} describe --long --tags --dirty --always
            WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
            RESULT_VARIABLE  GIT_EXIT_STATUS
            OUTPUT_VARIABLE ARMARX_PACKAGE_GIT_VERSION
            OUTPUT_STRIP_TRAILING_WHITESPACE)

        if(NOT GIT_EXIT_STATUS EQUAL 0)
            message(STATUS "no git repository or unable to run git describe")
        else()
            # string(REGEX REPLACE "\n" "" ARMARX_PACKAGE_GIT_VERSION ${ARMARX_PACKAGE_GIT_VERSION})
            message(STATUS "armarx git version: ${ARMARX_PACKAGE_GIT_VERSION}")
            set(ARMARX_PACKAGE_VERSION ${ARMARX_PACKAGE_GIT_VERSION})

            # test if git describe finds a tag.  the commit id is separated by a dash.
            string(REGEX MATCH "^.*-.*(-dirty)?$" GIT_TAG_FOUND ${ARMARX_PACKAGE_GIT_VERSION})
            if(NOT GIT_TAG_FOUND)
                # append the commit id to the version
                message(STATUS "git tag not found.")
                set(ARMARX_PACKAGE_VERSION "v${ARMARX_PACKAGE_LIBRARY_VERSION_MAJOR}.${ARMARX_PACKAGE_LIBRARY_VERSION_MINOR}.${ARMARX_PACKAGE_LIBRARY_VERSION_PATCH}-${ARMARX_PACKAGE_GIT_VERSION}")
            elseif(NOT ${ARMARX_PACKAGE_GIT_VERSION} MATCHES "^v${ARMARX_PACKAGE_LIBRARY_VERSION_MAJOR}.${ARMARX_PACKAGE_LIBRARY_VERSION_MINOR}.${ARMARX_PACKAGE_LIBRARY_VERSION_PATCH}-.*")
                message(STATUS "unable to determine current version. disabling package target.")
                message(STATUS "git tag does not match armarx version file! maybe you forgot to run git fetch --tags")
                set(CPACK_GENERATOR "INVALID")
            elseif(${ARMARX_PACKAGE_GIT_VERSION} MATCHES ".*dirty.*")
                message(STATUS "git working tree is dirty")
            endif()
        endif()
    else()
        message(STATUS "git command not found. unable to determine current git tag.")
    endif()

    message(STATUS "armarx deb package version: ${ARMARX_PACKAGE_VERSION}")

    set(CPACK_PACKAGE_FILE_NAME "${ARMARX_PROJECT_NAME}_${ARMARX_PACKAGE_VERSION}_${CPACK_DEBIAN_ARCHITECTURE}")
    set(CPACK_SOURCE_PACKAGE_FILE_NAME "${ARMARX_PROJECT_NAME}_${ARMARX_PACKAGE_VERSION}")

    set(CMAKE_INSTALL_RPATH "${ARMARX_LIB_DIR}")
    set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

    #get_property(result GLOBAL ENABLED_FEATURES)
    #message(" enabled features: ${result}")

    set(CPACK_COMPONENT_APPLICATIONS_DISPLAY_NAME "${ARMARX_PROJECT_NAME} applications")
    set(CPACK_COMPONENT_APPLICATIONDATA_DISPLAY_NAME "${ARMARX_PROJECT_NAME} Data")
    set(CPACK_COMPONENT_LIBRARIES_DISPLAY_NAME "${ARMARX_PROJECT_NAME} Libraries")
    set(CPACK_COMPONENT_HEADERS_DISPLAY_NAME "${ARMARX_PROJECT_NAME} C++ Headers")

    set(CPACK_COMPONENTS_ALL Applications Headers Libraries ApplicationData)
endif()

function(armarx_addon_cpack)
    if(${ARMARX_CMAKE_ADDON_ADD_CPACK})
        # Parse arguments.
        set(multi_param DEPENDENCIES)
        set(single_param)
        set(flag_param)
        cmake_parse_arguments(PARSE_ARGV 0 AX "${flag_param}" "${single_param}" "${multi_param}")
        if(DEFINED AX_UNPARSED_ARGUMENTS)
            message(FATAL_ERROR "${TARGET}: Unknown arguments `${AX_UNPARSED_ARGUMENTS}`.")
        endif()

        foreach(DEPENDENCY ${AX_DEPENDENCIES})
            string(TOLOWER ${DEPENDENCY} DEPENDENCY)
            string(REPLACE "_" "-" DEPENDENCY "${DEPENDENCY}")
            list(APPEND CPACK_DEBIAN_PACKAGE_RECOMMENDS "${DEPENDENCY}")
        endforeach()

        if("${ARMARX_PROJECT_NAME}" STREQUAL "ArmarXCore")
            list(REMOVE_ITEM CPACK_DEBIAN_PACKAGE_RECOMMENDS  "armarx-armarxcore")
        endif()

        string(REPLACE ";" ", " CPACK_DEBIAN_PACKAGE_RECOMMENDS "${CPACK_DEBIAN_PACKAGE_RECOMMENDS}")
        string(REPLACE ";" ", " CPACK_DEBIAN_PACKAGE_DEPENDS "${CPACK_DEBIAN_PACKAGE_DEPENDS}")

        include(CPack)
    endif()
endfunction()

# This CMake script checks wheter anaconda is currently used.
# If so, the the conda directories need to be excluded from CMake search paths as some libraries might exist there as well.
#
# Author: Fabian Reister


if(DEFINED ENV{CONDA_PREFIX})
    message(STATUS "Conda environment in $ENV{CONDA_PREFIX} found. Ignoring libraries within this directory.")
    set(CMAKE_IGNORE_PATH "$ENV{CONDA_PREFIX}" CACHE STRING "" FORCE)
endif()

option(ARMARX_CMAKE_ADDON_HEADER_CHECK "Checks whether headers exist that are not associated with any target." OFF)


function(armarx_addon_header_check)
  if(${ARMARX_CMAKE_ADDON_HEADER_CHECK})

    message(STATUS "\n== Checking for orphaned headers ...")

    get_property(AX_TARGETS
      GLOBAL
      PROPERTY ARMARX_TARGETS
    )

    # all headers in the source directory
    file(GLOB_RECURSE ALL_HEADERS "${CMAKE_SOURCE_DIR}/source/*.h")

    # get list of known headers (those that are associated with the targets)
    foreach(TARGET ${AX_TARGETS})
      get_target_property(AX_TARGET_CPP_HEADERS ${TARGET} CPP_HEADERS)

      if(AX_TARGET_CPP_HEADERS)
        # get headers associated with this target
        foreach(HEADER_FILE ${AX_TARGET_CPP_HEADERS})
          get_filename_component(HEADER_ABS_PATH
                          "${HEADER_FILE}"
                          ABSOLUTE)
          list(APPEND KNOWN_HEADERS ${HEADER_ABS_PATH})
        endforeach()
      endif()
    endforeach()

    # check if the header file is associated with any target. Otherwise, print it
    foreach(HEADER_FILE ${ALL_HEADERS})
      if(${HEADER_FILE} IN_LIST KNOWN_HEADERS)
        # message(STATUS "known: ${HEADER_FILE}")
      else()
        message(STATUS "${HEADER_FILE}")

        list(APPEND UNKNOWN_HEADERS ${HEADER_FILE})
      endif()
    endforeach()

  endif()
endfunction()

option(ARMARX_CMAKE_ADDON_FEATURE_SUMMARY "Print a cumulative feature summary after configuration." OFF)

if(${ARMARX_CMAKE_ADDON_FEATURE_SUMMARY})
    include(FeatureSummary)
endif()

function(armarx_addon_feature_summary)
    if(${ARMARX_CMAKE_ADDON_FEATURE_SUMMARY})
        message(STATUS "\n== Feature summary:")
        feature_summary(WHAT ALL INCLUDE_QUIET_PACKAGES)
    endif()
endfunction()

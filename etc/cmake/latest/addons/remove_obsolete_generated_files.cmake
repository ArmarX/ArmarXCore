option(ARMARX_CMAKE_ADDON_REMOVE_OBSOLETE_GENERATED_FILES "Delete obsolete generated files to prevent errors." ON)

function(armarx_remove_obsolete_generated_files)
    message(STATUS "Checking generated files and removing them if necessary ...")

    execute_process(
        COMMAND "${ArmarXCore_BINARY_DIR}/armarx-dev" "checkGeneratedFiles" "${PROJECT_NAME}"
        RESULT_VARIABLE CMD_RESULT
        OUTPUT_VARIABLE CMD_OUTPUT
    )

    string(REGEX REPLACE "\n$" "" CMD_OUTPUT "${CMD_OUTPUT}")
    string(REPLACE "\n" ";" CMD_OUTPUT_LIST "${CMD_OUTPUT}")

    foreach(CMD_OUTPUT_LINE ${CMD_OUTPUT_LIST})
        message(STATUS ${CMD_OUTPUT_LINE})
    endforeach()
endfunction()

if(${ARMARX_CMAKE_ADDON_REMOVE_OBSOLETE_GENERATED_FILES})
    armarx_remove_obsolete_generated_files()
endif()

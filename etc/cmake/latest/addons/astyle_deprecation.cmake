if(DEFINED ARMARX_ENABLE_AUTO_CODE_FORMATTING)
    message(WARNING "Usage of `astyle` was deprecated in favor of `clang-format`. You can safely remove the definition of the variable `ARMARX_ENABLE_AUTO_CODE_FORMATTING` as it has no effect anymore (most likely defined in your top-level CMakeLists.txt).")
endif()

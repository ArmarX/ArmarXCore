# Some IDE's and ninja might not produce colored build logs by default.
# This CMake script tells the compiler to generate the colored log anyways.

option(ARMARX_CMAKE_ADDON_FORCE_COLORED_OUTPUT "Always produce ANSI-colored output (GNU/Clang only)." OFF)

if (${ARMARX_CMAKE_ADDON_FORCE_COLORED_OUTPUT})
    add_compile_options($<$<COMPILE_LANG_AND_ID:CXX,GNU>:-fdiagnostics-color=always>
            $<$<COMPILE_LANG_AND_ID:CXX,Clang>:-fcolor-diagnostics>)
else ()
    message(STATUS "${ADDON_NAME} is disabled per user choice.")
endif ()

# Macros for ArmarX Statechart Libraries

add_custom_target(GENERATE_ALL_STATECHARTS)
add_dependencies(all_generate GENERATE_ALL_STATECHARTS)


function(armarx_add_statechart)
    message(WARNING "`armarx_add_statechart` has been renamed to `armarx_add_statechart_group`. The alias `armarx_add_statechart` will be removed within the next weeks.")
    armarx_add_statechart_group(${ARGV})
endfunction()

function(armarx_add_statechart_group TARGET)
    
    set(multi_param 
        HEADERS 
        SOURCES 
        DEPENDENCIES
        DEPENDENCIES_LEGACY 
    )
    set(single_param GROUP_FILE)
    set(flag_param )

    cmake_parse_arguments(PARSE_ARGV 1 AX "${flag_param}" "${single_param}" "${multi_param}")
    
    # todo check if HEADERS and SOURCES contain RemoteStateOfferer, otherwise add

    if(DEFINED AX_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "${TARGET}: Unknown arguments `${AX_UNPARSED_ARGUMENTS}`.")
    endif()

    set(GROUP_NAME "${TARGET}")

    # default statechart group filename
    if(NOT AX_GROUP_FILE)
        set(AX_GROUP_FILE "${GROUP_NAME}.scgxml")
    endif()

    set(AX_STATECHART_FILES "${AX_GROUP_FILE}" "${AX_STATE_FILES}")

    #
    #   Define statechart library
    #

    # cmake autogen => generates target '${TARGET}_statechart'
    armarx_generate_and_add_statechart_library(${TARGET}) # calls armarx_add_statechart_library

    #  We add the dependencies to the ${TARGET}_statechart target. Remember, the dependency chain is:
    #       ${TARGET}_generated_headers <- ${TARGET}_statechart <- ${TARGET} .
    #
    #   If the ${TARGET}_statechart target is disabled, the ${TARGET} will automatically be disabled as well.

    get_target_property(STATECHART_LIB_TYPE ${ARMARX_PROJECT_NAME}::${TARGET}_statechart TYPE)

    if(${STATECHART_LIB_TYPE} STREQUAL "INTERFACE_LIBRARY")
        _armarx_target_add_dependencies(${TARGET}_statechart INTERFACE ${AX_DEPENDENCIES})
        _armarx_target_add_legacy_dependencies(${TARGET}_statechart INTERFACE ${AX_DEPENDENCIES_LEGACY})
    else()
        _armarx_target_add_dependencies(${TARGET}_statechart ${AX_DEPENDENCIES})
        _armarx_target_add_legacy_dependencies(${TARGET}_statechart ${AX_DEPENDENCIES_LEGACY})
    endif()

   

    # Set target properties.
    # set_property(GLOBAL APPEND PROPERTY ARMARX_TARGETS ${TARGET}_statechart)
    set_target_properties(${TARGET}_statechart
        PROPERTIES
            ARMARX_ENABLED_BY_USER ${ENABLE_LIB_${TARGET}_statechart}
    )

    _armarx_target_check_enabled_status(${TARGET}_statechart)

    #
    #   Generate state and context headers which are need by the statechart library defined above
    #

    set(TARGET_GENERATED_HEADERS ${TARGET}_generated_headers)
    armarx_add_library(${TARGET_GENERATED_HEADERS} INTERFACE)

    if(${STATECHART_LIB_TYPE} STREQUAL "INTERFACE_LIBRARY")
        target_link_libraries(${TARGET}_statechart INTERFACE ${TARGET_GENERATED_HEADERS})
    else()
        target_link_libraries(${TARGET}_statechart PUBLIC ${TARGET_GENERATED_HEADERS})
    endif()

    armarx_generate_statechart_context(GROUP_NAME "${GROUP_NAME}" TARGET "${TARGET_GENERATED_HEADERS}") 
    # TODO read property STATE_FILES of ${TARGET}_statechart
    get_target_property(STATE_FILES ${TARGET}_statechart ARMARX_STATE_FILES)
    armarx_generate_statechart_state_headers(
        GROUP_FILE ${AX_GROUP_FILE}
        STATE_FILES ${STATE_FILES} 
        TARGET ${TARGET_GENERATED_HEADERS}
    )

    #
    #   Define component library and executable
    #

    armarx_add_library(${TARGET}
        SOURCES ${AX_SOURCES}
        HEADERS ${AX_HEADERS}
        DEPENDENCIES ${ARMARX_PROJECT_NAME}::${TARGET}_statechart
        SHARED
    )


endfunction()


## private

#
#   This function will be called by the autogenerated CMake-file
#
function(armarx_add_statechart_library TARGET)

    message(VERBOSE "Adding statechart library `${TARGET}`")

    # Parse arguments.
    set(multi_param 
        HEADERS 
        SOURCES 
        STATE_FILES
        DEPENDENCIES
        # DEPENDENCIES_LEGACY 
    )

    set(single_param)
    set(flag_param)

    cmake_parse_arguments(PARSE_ARGV 1 AX "${flag_param}" "${single_param}" "${multi_param}")
    if(DEFINED AX_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "${TARGET}: Unknown arguments `${AX_UNPARSED_ARGUMENTS}`.")
    endif()

    armarx_add_library(${TARGET}
        SOURCES ${AX_SOURCES}
        HEADERS ${AX_HEADERS}
        DEPENDENCIES ${AX_DEPENDENCIES}
    )

    set_property(
        TARGET ${TARGET}
        PROPERTY ARMARX_STATE_FILES ${AX_STATE_FILES}
    )

 # TODO 
    # armarx_add_statechart_headers()
    # generate_statechart_headers(${TARGET} "${AX_STATECHART_FILES}")
    #target_link_libraries(${TARGET}_statechart PUBLIC ${TARGET}_statechart_headers)



endfunction()


function(armarx_add_generator_target)
    ###############
    #define params#
    ###############
    set(single_param
        TARGET
    )
    set(multi_param
        COMMAND
        ARGS
        DEPENDS
        COMMENT
        DEPENDENCY_OF
        OUTPUT
    )
    cmake_parse_arguments(PARAM "${flags}" "${single_param}" "${multi_param}" ${ARGN} )
    ##############
    #check params#
    ##############
    if("" STREQUAL "${PARAM_TARGET}")
        message(FATAL_ERROR "Parameter TARGET for generator target was not set! ${PARAM_TARGET}")
    endif()
    if("" STREQUAL "${PARAM_COMMAND}")
        message(FATAL_ERROR "Parameter COMMAND for generator target '${PARAM_TARGET}' was not set! ${PARAM_COMMAND}")
    endif()
    if("" STREQUAL "${PARAM_DEPENDS}")
        message(FATAL_ERROR "Parameter DEPENDS for generator target '${PARAM_TARGET}' was not set! ${PARAM_DEPENDS}")
    endif()
    if("" STREQUAL "${PARAM_OUTPUT}")
        message(FATAL_ERROR "Parameter OUTPUT for generator target '${PARAM_TARGET}' was not set! ${PARAM_OUTPUT}")
    endif()
    #############
    #add targets#
    #############
    #set(touch_file "${CMAKE_CURRENT_BINARY_DIR}/${PARAM_TARGET}.touch")
    add_custom_command(
        OUTPUT   ${PARAM_OUTPUT}
        COMMAND  ${PARAM_COMMAND}   ARGS     ${PARAM_ARGS}
       # COMMAND  touch              ARGS    "${touch_file}"
        DEPENDS  ${PARAM_DEPENDS}
        COMMENT "${PARAM_COMMENT}"
    )
    add_custom_target(${PARAM_TARGET} DEPENDS ${PARAM_OUTPUT})

    foreach(dependency ${PARAM_DEPENDENCY_OF})
        add_dependencies(${dependency} ${PARAM_TARGET})
    endforeach()

    # foreach(output ${PARAM_OUTPUT})
    #     if(NOT EXISTS "${output}")
    #         execute_process(COMMAND touch "${output}")
    #     endif()
    # endforeach()
endfunction()



function(armarx_generate_statechart_context )

    set(single_param
        GROUP_NAME
        TARGET
    )

    set(multi_param)
    cmake_parse_arguments(AX "${flags}" "${single_param}" "${multi_param}" ${ARGN} )

    if(NOT AX_GROUP_NAME)
        message(FATAL_ERROR "Required argument 'GROUP_FILE' not set")
    endif()

    if(NOT AX_TARGET)
        message(FATAL_ERROR "Required argument 'TARGET' not set")
    endif()

    message(VERBOSE "Generating statechart context headers for target ${AX_TARGET}")

    set(GROUP_FILE_ABS "${CMAKE_CURRENT_SOURCE_DIR}/${AX_GROUP_NAME}.scgxml")
    MESSAGE(VERBOSE "Statechart group: ${AX_GROUP_FILE} in ${CMAKE_CURRENT_BINARY_DIR}")

    add_custom_target(${AX_GROUP_NAME}_GENERATE_STATECHARTS)
    add_dependencies(GENERATE_ALL_STATECHARTS ${AX_GROUP_NAME}_GENERATE_STATECHARTS)


    MESSAGE(VERBOSE "Generating statechart context headers:")
    message(VERBOSE " - ${CMAKE_CURRENT_BINARY_DIR}/${AX_GROUP_NAME}StatechartContext.generated.h")
    message(VERBOSE " - ${CMAKE_CURRENT_BINARY_DIR}/${AX_GROUP_NAME}StatechartContextBase.generated.h")

    # TODO use plain cmake stuff, delete armarx_add_generator_target
    armarx_add_generator_target(
        TARGET        ${AX_GROUP_NAME}_GENERATE_STATECHARTS_CONTEXT
        DEPENDENCY_OF ${AX_GROUP_NAME}_GENERATE_STATECHARTS
        DEPENDS       "${ArmarXCore_BINARY_DIR}/StatechartGroupGeneratorAppRun"
                        "${GROUP_FILE_ABS}"
        COMMAND       "${ArmarXCore_BINARY_DIR}/StatechartGroupGeneratorAppRun"
        ARGS          "context" "${GROUP_FILE_ABS}" "${CMAKE_BINARY_DIR}" "${ARMARX_PROJECT_INCLUDE_PATH}"
        COMMENT       "Generating ${AX_GROUP_NAME}StatechartContext.generated.h from ${GROUP_FILE_ABS}"
        OUTPUT        "${CMAKE_CURRENT_BINARY_DIR}/${AX_GROUP_NAME}StatechartContext.generated.h"
                        "${CMAKE_CURRENT_BINARY_DIR}/${AX_GROUP_NAME}StatechartContextBase.generated.h"
    )

    # message(STATUS "${ArmarXCore_BINARY_DIR}/StatechartGroupGeneratorAppRun context ${GROUP_FILE_ABS} ${CMAKE_BINARY_DIR} ${ARMARX_PROJECT_INCLUDE_PATH}")
    # message(STATUS "Including directory `${CMAKE_CURRENT_BINARY_DIR}`")
    
    target_include_directories(${AX_TARGET} INTERFACE $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>)
    target_include_directories(${AX_TARGET} INTERFACE $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/${GROUP_NAME_NO_SUFFIX}>)

    add_dependencies(${AX_TARGET} ${AX_GROUP_NAME}_GENERATE_STATECHARTS)

    # The following could technically replace the add_dependencies command above but doesn't work

    # target_sources(${AX_TARGET}
    #     INTERFACE
    #         $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/${AX_GROUP_NAME}StatechartContext.generated.h>
    #         $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/${AX_GROUP_NAME}StatechartContextBase.generated.h>
    # )

endfunction()


function(armarx_generate_statechart_state_headers)
    set(single_param
        GROUP_FILE
        TARGET
    )

    set(multi_param STATE_FILES)
    cmake_parse_arguments(AX "${flags}" "${single_param}" "${multi_param}" ${ARGN} )


    if(NOT AX_GROUP_FILE)
        message(FATAL_ERROR "Required argument 'GROUP_FILE' not set")
    endif()

    if(NOT AX_STATE_FILES)
        message(FATAL_ERROR "Required argument 'STATE_FILES' not set")
    endif()

    if(NOT AX_TARGET)
        message(FATAL_ERROR "Required argument 'TARGET' not set")
    endif()

    message(VERBOSE "Generating statechart state headers for target ${TARGET}")

    # message(WARNING "STATE FILES: ${AX_STATE_FILES}")

    foreach(STATE_FILE ${AX_STATE_FILES})
        #message(STATUS "State file: ${STATE_FILE}")

        # only public states
        string(REGEX MATCH "^.*/([A-Za-z0-9_]+)\\.xml$" VOID ${STATE_FILE})
        #message(STATUS "match count ${CMAKE_MATCH_COUNT}, ${CMAKE_MATCH_0}, ${CMAKE_MATCH_1}")
        set(STATE_NAME ${CMAKE_MATCH_1})

        if ("${STATE_NAME}" STREQUAL "")
            message(WARNING "This command only accepts XML files for the STATE_FILES argument")
            continue()
        endif()
            
        set(ABS_STATE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/${STATE_FILE}")

        set(GENERATOR_TARGET ${ARMARX_PROJECT_NAME}_${TARGET}_GENERATE_STATECHARTS_${STATE_NAME})
        
        if(TARGET ${GENERATOR_TARGET})
            message(FATAL_ERROR "The target ${GENERATOR_TARGET} is already defined! Check your CMakeLists.txt or ${GROUP_NAME_NO_SUFFIX}.scgxml. It probably contains the files '${STATE_PATH_NO_SUFFIX}.{xml, h, cpp}' multiple times.")
        endif()

        armarx_add_generator_target(
            TARGET        ${GENERATOR_TARGET}
            DEPENDENCY_OF ${TARGET}_GENERATE_STATECHARTS
            DEPENDS       "${ArmarXCore_BINARY_DIR}/StatechartGroupGeneratorAppRun"
                            "${CMAKE_CURRENT_SOURCE_DIR}/${AX_GROUP_FILE}"
                            "${ABS_STATE_PATH}"
                            "${CMAKE_SOURCE_DIR}/data/${ARMARX_PROJECT_NAME}/VariantInfo-${ARMARX_PROJECT_NAME}.xml"
            COMMAND       "${ArmarXCore_BINARY_DIR}/StatechartGroupGeneratorAppRun"
            ARGS          "${CMAKE_CURRENT_SOURCE_DIR}/${AX_GROUP_FILE}" "${ABS_STATE_PATH}" "${CMAKE_BINARY_DIR}" "${ARMARX_PROJECT_INCLUDE_PATH}"
            COMMENT       "Generating ${CMAKE_CURRENT_BINARY_DIR}/${STATE_NAME}.generated.h from ${ABS_STATE_PATH}"
            OUTPUT        "${CMAKE_CURRENT_BINARY_DIR}/${STATE_NAME}.generated.h"
        )

        # message(STATUS "Generated ${CMAKE_CURRENT_BINARY_DIR}/${STATE_NAME}.generated.h")

            # FIXME add
            # list(APPEND HEADERS "${CMAKE_CURRENT_BINARY_DIR}/${STATE_NAME}.generated.h")
        # set_property(
        #     TARGET generated_files
        #     APPEND PROPERTY SOURCES
        #         "${CMAKE_CURRENT_BINARY_DIR}/${STATE_NAME}.generated.h"
        # )
        
        set_property(
            TARGET ${TARGET}_statechart # FIXME HACK
            APPEND PROPERTY SOURCES
                "${CMAKE_CURRENT_BINARY_DIR}/${STATE_NAME}.generated.h"
        )
    endforeach()
endfunction()


function(armarx_generate_and_add_statechart_library TARGET)

    # next line triggers cmake when scgxml was changed
    #configure_file("${CMAKE_CURRENT_SOURCE_DIR}/${ARMARX_COMPONENT_NAME}.scgxml" "${CMAKE_CURRENT_BINARY_DIR}/${ARMARX_COMPONENT_NAME}.scgxml.touch")
    #configure_file("${ArmarXCore_BINARY_DIR}/StatechartGroupGeneratorAppRun" "${CMAKE_CURRENT_BINARY_DIR}/StatechartGroupGeneratorAppRun.touch")

#    message(STATUS "${ARMARX_PROJECT_PACKAGE_DEPENDENCY_PATHS}")
    # For newer cmakes: use this for dependency injection
    #set_property(DIRECTORY APPEND PROPERTY CMAKE_CONFIGURE_DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/${ARMARX_COMPONENT_NAME}.scgxml")

    get_property(STATECHART_CMAKE_FILES_GENERATED GLOBAL PROPERTY ARMARX_STATECHART_CMAKE_FILES_GENERATED)
    if(NOT STATECHART_CMAKE_FILES_GENERATED)
        set_property(GLOBAL PROPERTY ARMARX_STATECHART_CMAKE_FILES_GENERATED TRUE)
        execute_process(COMMAND "${ArmarXCore_BINARY_DIR}/StatechartGroupGeneratorAppRun" "package-cmake" "${PROJECT_NAME}"
            "${PROJECT_SOURCECODE_DIR}/${PROJECT_NAME}/statecharts" "${PROJECT_BINARY_DIR}" "${PROJECT_DATA_DIR}"
            "${ARMARX_PROJECT_PACKAGE_DEPENDENCY_PATHS}" "${ARMARX_PROJECT_INCLUDE_PATH}" RESULT_VARIABLE CMD_RESULT)
    endif()

    set(INCLUDE_FILE_PATH "${CMAKE_CURRENT_BINARY_DIR}/${TARGET}Files.generated.cmake")
    message(VERBOSE "Using statechart CMake file ${INCLUDE_FILE_PATH} to provide target `${TARGET}_statechart_generated`")

    # This will provide the CMake target ${TARGET}_statechart_generated
    if(EXISTS "${INCLUDE_FILE_PATH}")
        include("${INCLUDE_FILE_PATH}")
    else()
        message(FATAL_ERROR "${INCLUDE_FILE_PATH} was not found - this statechart group won't compile correctly - you need to compile ArmarXCore first")
    endif()


endfunction()

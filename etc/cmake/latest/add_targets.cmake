# Defines a library target.
function(armarx_add_library TARGET)
    message(STATUS "\n== Configuring library `${ARMARX_PROJECT_NAME}::${TARGET}` ...")

    # Options.
    option(ENABLE_LIB_${TARGET} "Enable library `${TARGET}`." YES)

    # Parse arguments.
    set(multi_param
        SOURCES
        HEADERS
        DEPENDENCIES  # Defaults to PUBLIC.
        DEPENDENCIES_PUBLIC
        DEPENDENCIES_PRIVATE
        DEPENDENCIES_INTERFACE
        DEPENDENCIES_LEGACY  # Defaults to PUBLIC.
        DEPENDENCIES_LEGACY_PUBLIC
        DEPENDENCIES_LEGACY_PRIVATE
        DEPENDENCIES_LEGACY_INTERFACE
    )
    set(single_param)
    set(flag_param STATIC SHARED INTERFACE OBJECT)
    cmake_parse_arguments(PARSE_ARGV 1 AX "${flag_param}" "${single_param}" "${multi_param}")
    if(DEFINED AX_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "${TARGET}: Unknown arguments `${AX_UNPARSED_ARGUMENTS}`.")
    endif()

    # TODO: Ensure that no visibility flags using DEPENDENCIES PRIVATE|PUBLIC are used => USE DEPENDENCIES_PRIVATE etc.
    # instead.
    cmake_parse_arguments(AXDEP "" "" "PUBLIC;PRIVATE" "${AX_DEPENDENCIES}")
    if(AXDEP_PUBLIC OR AXDEP_PRIVATE)
        message(WARNING "armarx_add_library(...) for target '${TARGET}' does not support hierarchical dependency "
            "visibilities ('DEPENDENCIES PUBLIC', 'DEPENDENCIES PRIVATE' or 'DEPENDENCIES INTERFACE').  Use "
            "'DEPENDENCIES_PUBLIC', 'DEPENDENCIES_PRIVATE' or 'DEPENDENCIES_INTERFACE' instead.")
        list(APPEND AX_DEPENDENCIES ${AXDEP_PUBLIC} ${AXDEP_PRIVATE})
    endif()
    cmake_parse_arguments(AXDEPLEG "" "" "PUBLIC;PRIVATE" "${AX_DEPENDENCIES_LEGACY}")
    if(AXDEPLEG_PUBLIC OR AXDEPLEG_PRIVATE)
        message(WARNING "armarx_add_library(...) for target '${TARGET}' does not support hierarchical legacy "
            "dependency visibilities ('DEPENDENCIES_LEGACY PUBLIC', 'DEPENDENCIES_LEGACY PRIVATE' or "
            "'DEPENDENCIES_LEGACY INTERFACE').  Use 'DEPENDENCIES_LEGACY_PUBLIC', 'DEPENDENCIES_LEGACY_PRIVATE' or "
            "'DEPENDENCIES_LEGACY_INTERFACE' instead.")
        list(APPEND AX_DEPENDENCIES_LEGACY ${AXDEPLEG_PUBLIC} ${AXDEPLEG_PRIVATE})
    endif()
    # /Ensure visibility flags.

    # Determine if static, shared, interface, or object library.
    armarx_eval_mutex_flag_group(LIBRARY_TYPE
        FLAG_NAMES STATIC SHARED INTERFACE OBJECT
        FLAG_PREFIX AX_
        FLAG_DEFAULT_VALUE SHARED
    )

    # Define target.
    add_library(${TARGET} ${LIBRARY_TYPE} ${AX_SOURCES} ${AX_HEADERS})
    add_library(${ARMARX_PROJECT_NAME}::${TARGET} ALIAS ${TARGET})
    _armarx_library_compile_options(${TARGET})

    # Set current source directory as private include path for non-interface libraries.
    if(NOT ${LIBRARY_TYPE} STREQUAL "INTERFACE")
        target_include_directories(${TARGET} PUBLIC $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/source>)
        target_include_directories(${TARGET} PUBLIC $<INSTALL_INTERFACE:include>)
    endif()

    # Set target properties.
    # - add target to list if ArmarX targets
    set_property(GLOBAL APPEND PROPERTY ARMARX_TARGETS ${TARGET})

    # - c++ headers associated with this target => CPP_HEADERS property
    foreach(HEADER_FILE ${AX_HEADERS})
    if(HEADER_FILE MATCHES ".*\.h$")
        get_filename_component(HEADER_ABS_PATH "${HEADER_FILE}" ABSOLUTE)
        list(APPEND CPP_HEADERS ${HEADER_ABS_PATH})
    endif()
    endforeach()

    set_target_properties(${TARGET}
        PROPERTIES
            ARMARX_ENABLED_BY_USER ${ENABLE_LIB_${TARGET}}
            HEADERS "${AX_HEADERS}"
            CPP_HEADERS "${CPP_HEADERS}"
    )

    # Handle dependencies.
    if(${LIBRARY_TYPE} STREQUAL "INTERFACE")
        _armarx_target_add_dependencies(${TARGET}
            # TODO check if AX_DEPENDENCIES_PUBLIC and AX_DEPENDENCIES_PRIVATE exists
            # if so => warning
            # PUBLIC  ${AX_DEPENDENCIES_PUBLIC}
            # PRIVATE ${AX_DEPENDENCIES_PRIVATE}
            INTERFACE ${AX_DEPENDENCIES_INTERFACE} ${AX_DEPENDENCIES}
        )
        _armarx_target_add_legacy_dependencies(${TARGET}
            # TODO check if AX_DEPENDENCIES_PUBLIC and AX_DEPENDENCIES_PRIVATE exists
            # if so => warning
            # PUBLIC  ${AX_DEPENDENCIES_LEGACY_PUBLIC}
            # PRIVATE ${AX_DEPENDENCIES_LEGACY_PRIVATE}
            INTERFACE ${AX_DEPENDENCIES_LEGACY_INTERFACE} ${AX_DEPENDENCIES_LEGACY}
        )
    else()
        _armarx_target_add_dependencies(${TARGET}
            PUBLIC ${AX_DEPENDENCIES} ${AX_DEPENDENCIES_PUBLIC}
            PRIVATE ${AX_DEPENDENCIES_PRIVATE}
            INTERFACE ${AX_DEPENDENCIES_INTERFACE}
        )
        _armarx_target_add_legacy_dependencies(${TARGET}
            PUBLIC ${AX_DEPENDENCIES_LEGACY} ${AX_DEPENDENCIES_LEGACY_PUBLIC}
            PRIVATE ${AX_DEPENDENCIES_LEGACY_PRIVATE}
            INTERFACE ${AX_DEPENDENCIES_LEGACY_INTERFACE}
        )
    endif()
    

    # Disable target if dependencies are unmet or user disabled it.
    _armarx_target_check_enabled_status(${TARGET})

    # Install target.
    _armarx_install_library(${TARGET}
        HEADERS ${AX_HEADERS}
    )

    message(VERBOSE "Library `${TARGET}` configured.")
endfunction()


# Defines an executable target.
function(armarx_add_executable TARGET)
    message(STATUS "\n== Configuring executable `${ARMARX_PROJECT_NAME}::${TARGET}` ...")

    # Options.
    option(ENABLE_EXEC_${TARGET} "Enable library `${TARGET}`." YES)

    # Parse arguments.
    set(multi_param SOURCES DEPENDENCIES DEPENDENCIES_LEGACY)
    set(single_param)
    set(flag_param)
    cmake_parse_arguments(PARSE_ARGV 1 AX "${flag_param}" "${single_param}" "${multi_param}")
    if(DEFINED AX_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "${TARGET}: Unknown arguments `${AX_UNPARSED_ARGUMENTS}`.")
    endif()

    # Define target.
    add_executable(${TARGET} ${AX_SOURCES})
    _armarx_executable_compile_options(${TARGET})

    # Set target properties.
    set_property(GLOBAL APPEND PROPERTY ARMARX_TARGETS ${TARGET})
    set_target_properties(${TARGET}
        PROPERTIES
            ARMARX_ENABLED_BY_USER ${ENABLE_EXEC_${TARGET}})

    # Handle dependencies.
    _armarx_target_add_dependencies(${TARGET} ${AX_DEPENDENCIES})
    _armarx_target_add_legacy_dependencies(${TARGET} ${AX_DEPENDENCIES_LEGACY})

    # Disable target if dependencies are unmet or user disabled it.
    _armarx_target_check_enabled_status(${TARGET})

    # Install target.
    _armarx_install_executable(${TARGET})

    message(VERBOSE "Executable `${TARGET}` configured.")
endfunction()


# Defines an external executable target (e.g., a script).
function(armarx_add_external_executable EXECTUABLE_PATH)
    get_filename_component(EXECTUABLE_NAME "${EXECTUABLE_PATH}" NAME)
    get_filename_component(EXECTUABLE_DIR "${EXECTUABLE_PATH}" DIRECTORY)
    file(COPY "${EXECTUABLE_PATH}" DESTINATION "${ARMARX_BIN_DIR}/${EXECTUABLE_DIR}" USE_SOURCE_PERMISSIONS)
    _armarx_install_external_executable("${EXECTUABLE_DIR}/${EXECTUABLE_NAME}")
endfunction()


# Defines all files in a directory as executable targets.
function(armarx_add_external_executable_directory EXECTUABLE_DIRECTORY_PATH)
    file(GLOB_RECURSE fileList ${EXECTUABLE_DIRECTORY_PATH})
    message(STATUS "Copying files from ${EXECTUABLE_DIRECTORY_PATH} to build dir")
    foreach(filePath ${fileList})
        file(RELATIVE_PATH relativePath ${CMAKE_CURRENT_SOURCE_DIR} ${filePath})
        get_filename_component(EXECTUABLE_NAME "${relativePath}" NAME)
        get_filename_component(EXECTUABLE_DIR "${relativePath}" DIRECTORY)
        file(COPY "${filePath}" DESTINATION "${ARMARX_BIN_DIR}/${EXECTUABLE_DIR}" USE_SOURCE_PERMISSIONS)
        _armarx_install_external_executable("${EXECTUABLE_DIR}/${EXECTUABLE_NAME}")
    endforeach()
endfunction()


# Sets common compile options for a library.
function(_armarx_library_compile_options TARGET)
    if(NOT LIB_ALLOW_UNDEFINED_SYMBOLS)
        set(LDFLAGS "${LDFLAGS} -Wl,-z,defs")
    endif()

    set(SCOPE "PUBLIC")
    get_target_property(TARGET_TYPE ${TARGET} TYPE)
    if("${TARGET_TYPE}" STREQUAL "INTERFACE_LIBRARY")
        set(SCOPE "INTERFACE")
    endif()

    target_compile_definitions(${TARGET} ${SCOPE} -D_REENTRANT)  # Enable thread-safe versions of C standard library.
    target_compile_options(${TARGET} ${SCOPE} ${CXX_FLAGS} ${C_FLAGS})
    set_target_properties(${TARGET} PROPERTIES
        VERSION ${ARMARX_PACKAGE_LIBRARY_VERSION}
        SOVERSION ${ARMARX_PACKAGE_LIBRARY_SOVERSION}
        LINK_FLAGS "${LDFLAGS}"
        LIBRARY_OUTPUT_DIRECTORY "${ARMARX_LIB_DIR}"
        LIBRARY_OUTPUT_NAME ${ARMARX_PROJECT_NAME}_${TARGET}  # Ensure unique lib name.
        ARCHIVE_OUTPUT_DIRECTORY "${ARMARX_ARCHIVE_DIR}"
        RUNTIME_OUTPUT_DIRECTORY "${ARMARX_BIN_DIR}"
        LINKER_LANGUAGE CXX
    )

    if (NOT "${TARGET_TYPE}" STREQUAL "OBJECT_LIBRARY" AND NOT "${TARGET_TYPE}" STREQUAL "INTERFACE_LIBRARY"  AND "${CMAKE_BUILD_TYPE}" STREQUAL "Release")
        message(STATUS "Release mode, stripping binaries.")
        add_custom_command(
            TARGET ${TARGET}
            POST_BUILD
            COMMAND strip
            ARGS --strip-unneeded $<TARGET_FILE:${TARGET}>
        )
    endif()
endfunction()


# Sets common compile options for an executable.
function(_armarx_executable_compile_options TARGET)
    target_compile_options(${TARGET} PUBLIC ${CXX_FLAGS} ${C_FLAGS})
    set_target_properties(${TARGET} PROPERTIES
        LINK_FLAGS "${LDFLAGS}"
        LIBRARY_OUTPUT_DIRECTORY "${ARMARX_LIB_DIR}"
        ARCHIVE_OUTPUT_DIRECTORY "${ARMARX_ARCHIVE_DIR}"
        RUNTIME_OUTPUT_DIRECTORY "${ARMARX_BIN_DIR}"
        LINKER_LANGUAGE CXX
    )

    if (${CMAKE_BUILD_TYPE} MATCHES "Release")
        message(STATUS "Release mode, stripping binaries")
        add_custom_command(TARGET ${TARGET} POST_BUILD
                           COMMAND strip ARGS --strip-unneeded $<TARGET_FILE:${TARGET}>)
    endif()
endfunction()


# Decides whether a target is enabled or not given the information available from the properties
# ARMARX_ENABLED_BY_USER, ARMARX_DISABLED_DEPENDENCIES, and ARMARX_UNKNOWN_DEPENDENCIES.
function(_armarx_target_check_enabled_status TARGET)
    get_target_property(TARGET_ENABLED ${TARGET} ARMARX_ENABLED_BY_USER)
    if(NOT ${TARGET_ENABLED})
        message(STATUS "Target `${TARGET}` disabled as per user choice.")
    endif()

    get_target_property(TARGET_DISABLED_DEPENDENCIES ${TARGET} ARMARX_DISABLED_DEPENDENCIES)
    if (TARGET_DISABLED_DEPENDENCIES)
        message(STATUS "Target `${TARGET}` disabled because the following dependencies were disabled:")
        foreach(DEP ${TARGET_DISABLED_DEPENDENCIES})
            set(TARGET_ENABLED NO)
            message(STATUS "  `${DEP}`")
        endforeach()
    endif()

    get_target_property(TARGET_UNKNOWN_DEPENDENCIES ${TARGET} ARMARX_UNKNOWN_DEPENDENCIES)
    if (TARGET_UNKNOWN_DEPENDENCIES)
        message(STATUS "Target `${TARGET}` disabled because the following dependencies were not found:")
        foreach(DEP ${TARGET_UNKNOWN_DEPENDENCIES})
            set(TARGET_ENABLED NO)
            message(STATUS "  `${DEP}`")
        endforeach()
    endif()

    set_property(TARGET ${TARGET}
        PROPERTY
            ARMARX_ENABLED ${TARGET_ENABLED})

    # ensure that the property is exported correctly to XYTargets.cmake
    set_property(
        TARGET ${TARGET} 
        APPEND
        PROPERTY EXPORT_PROPERTIES "ARMARX_ENABLED"
    )

    if (${TARGET_ENABLED})
        set_target_properties(${TARGET}
            PROPERTIES
                EXCLUDE_FROM_ALL NO
                EXCLUDE_FROM_DEFAULT_BUILD NO
        )

        if(${ARMARX_CMAKE_ADDON_FEATURE_SUMMARY})
            add_feature_info(${TARGET} TRUE "")
        endif()

        # TODO: LEGACY: REMOVE AFTER FULL MIGRATION:
        # the item needs to be removed in a temporary variable since the REMOVE_ITEM operation is not possible on a CACHE variable
        set(TEMP_DEPENDENCY_LIST ${ARMARX_PROJECT_DISABLED_TARGETS})
        list(LENGTH TEMP_DEPENDENCY_LIST DEPENDENCIES_AVAILABLE)
        if (DEPENDENCIES_AVAILABLE)
            list(REMOVE_ITEM TEMP_DEPENDENCY_LIST ${TARGET})
            set(ARMARX_PROJECT_DISABLED_TARGETS "${TEMP_DEPENDENCY_LIST}" CACHE INTERNAL "Disabled Targets from dependent ArmarX Packages")
        endif()
    else()
        set_target_properties(${TARGET}
            PROPERTIES
                EXCLUDE_FROM_ALL YES
                EXCLUDE_FROM_DEFAULT_BUILD YES
        )

        if(${ARMARX_CMAKE_ADDON_FEATURE_SUMMARY})
            add_feature_info(${TARGET} FALSE "")
        endif()

        # TODO: LEGACY: REMOVE AFTER FULL MIGRATION:
        set(ARMARX_PROJECT_DISABLED_TARGETS "${ARMARX_PROJECT_DISABLED_TARGETS}" "${TARGET}" CACHE INTERNAL "Disabled Targets from dependent ArmarX Packages")
    endif()
endfunction()


# Adds dependencies to a target, evaluating whether they are disabled or unavailable.
function(_armarx_target_add_dependencies TARGET)
    # Parse arguments.
    set(multi_param PUBLIC PRIVATE INTERFACE)
    set(single_param)
    set(flag_param)
    cmake_parse_arguments(PARSE_ARGV 1 AX "${flag_param}" "${single_param}" "${multi_param}")

    # Unparsed arguments are PUBLIC by default.
    list(APPEND AX_PUBLIC ${AX_UNPARSED_ARGUMENTS})

    # Disable building library if any of the dependent libraries does not get built.
    foreach(LINKAGE_TYPE PUBLIC PRIVATE INTERFACE)
        foreach(DEPENDENCY ${AX_${LINKAGE_TYPE}})
            # Check if DEPENDENCY is a target.
            if(TARGET ${DEPENDENCY})
                get_target_property(ENABLED ${DEPENDENCY} ARMARX_ENABLED)

                # TODO: Workaround until fully migrated.
                if ("${ENABLED}" MATCHES "-NOTFOUND$")
                    get_target_property(EXCLUDE_FROM_ALL ${DEPENDENCY} EXCLUDE_FROM_ALL)
                    if (NOT "${EXCLUDE_FROM_ALL}" MATCHES "-NOTFOUND$" AND ${EXCLUDE_FROM_ALL})
                        message(STATUS "[Legacy mode] Could not find property ARMARX_ENABLED for `${DEPENDENCY}` but EXCLUDE_FROM_ALL is false. Assuming disabled for legacy ArmarX targets.")
                        set(ENABLED FALSE)
                    else()
                        set(ENABLED TRUE) # Regular target from somewhere.
                    endif()
                endif()
                # /TODO: Workaround until fully migrated.

                # If ENABLED is not defined (i.e., the target does not have this property), it is
                # not an ArmarX target and we can just link it.  Otherwise we need to check if
                # ENABLED is true to link.
                if(${ENABLED})
                    target_link_libraries(${TARGET} ${LINKAGE_TYPE} ${DEPENDENCY})
                else()
                    set_property(
                        TARGET ${TARGET}
                        APPEND
                        PROPERTY ARMARX_DISABLED_DEPENDENCIES "${DEPENDENCY}"
                    )
                endif()
            # If not, assume that the target is missing and thus unknown.
            else()
                set_property(
                    TARGET ${TARGET}
                    APPEND
                    PROPERTY ARMARX_UNKNOWN_DEPENDENCIES "${DEPENDENCY}"
                )
            endif()
        endforeach()
    endforeach()
endfunction()


# Adds legacy dependencies to a target, evaluating whether they are disabled or unavailable.
# These are dependencies which don't export targets but ${PROJECT_NAME}_LIBS,
# ${PROJECT_NAME}_INCLUDE_DIRS, etc... variables.
function(_armarx_target_add_legacy_dependencies TARGET)
    # Parse arguments.
    set(multi_param PUBLIC PRIVATE INTERFACE)
    set(single_param)
    set(flag_param)
    cmake_parse_arguments(PARSE_ARGV 1 AX "${flag_param}" "${single_param}" "${multi_param}")

    # Unparsed arguments are PUBLIC by default.
    list(APPEND AX_PUBLIC ${AX_UNPARSED_ARGUMENTS})

    # Find include directories for legacy dependencies.
    foreach(LINKAGE_TYPE PUBLIC PRIVATE INTERFACE)
        foreach(DEPENDENCY ${AX_${LINKAGE_TYPE}})
            if (${${DEPENDENCY}_FOUND})
                if(DEFINED "${DEPENDENCY}_INCLUDE_DIR")
                    target_include_directories(${TARGET} SYSTEM ${LINKAGE_TYPE} ${${DEPENDENCY}_INCLUDE_DIR})
                endif()

                if(DEFINED "${DEPENDENCY}_INCLUDE_DIRS")
                    target_include_directories(${TARGET} SYSTEM ${LINKAGE_TYPE} ${${DEPENDENCY}_INCLUDE_DIRS})
                endif()

                if(DEFINED "${DEPENDENCY}_INCLUDE_DIRECTORIES}")
                    target_include_directories(${TARGET} SYSTEM ${LINKAGE_TYPE} ${${DEPENDENCY}_INCLUDE_DIRECTORIES})
                endif()

                target_link_libraries(${TARGET} ${LINKAGE_TYPE} ${${DEPENDENCY}_LIBRARIES})
                target_link_libraries(${TARGET} ${LINKAGE_TYPE} ${${DEPENDENCY}_LIBRARY})

                if(DEFINED "${DEPENDENCY}_DEFINITIONS")
                    # This is only needed because PCL exports their definitions as "-DX;...;-DY; "
                    # instead of "-DX;...;-DY".  This results in "-D " being added as compiler
                    # definition, which in turn is rejected by the compiler as being invalid.
                    # Instead of the foreach-loop it should just read:
                    # target_compile_definitions(${TARGET} ${LINKAGE_TYPE} ${${DEPENDENCY}_DEFINITIONS})
                    # TODO: Fix after PCL is fixed.
                    foreach(DEF "${DEPENDENCY}_DEFINITIONS")
                        string(STRIP ${DEF} DEF)
                        if (NOT "${DEF}" STREQUAL "")
                            target_compile_definitions(${TARGET} ${LINKAGE_TYPE} "${DEF}")
                        endif()
                    endforeach()
                endif()
            else()
                set_property(
                    TARGET ${TARGET}
                    APPEND
                    PROPERTY ARMARX_UNKNOWN_DEPENDENCIES "${DEPENDENCY}"
                )
            endif()
        endforeach()
    endforeach()
endfunction()


# Installs a library.
function(_armarx_install_library TARGET)
    # Parse arguments.
    set(multi_param HEADERS)
    set(single_param)
    set(flag_param)
    cmake_parse_arguments(PARSE_ARGV 1 AX "${flag_param}" "${single_param}" "${multi_param}")

    get_target_property(TARGET_ENABLED ${TARGET} ARMARX_ENABLED)

    if(${TARGET_ENABLED})
        string(REPLACE "${PROJECT_SOURCECODE_DIR}/" "" HEADER_DIR "${CMAKE_CURRENT_SOURCE_DIR}")

        foreach(HEADER ${AX_HEADERS})
            # strip HEADER_DIR, so that only subdirectory + filename remain
            string(REPLACE "${PROJECT_SOURCECODE_DIR}/${HEADER_DIR}" "" STRIP_HEADER "${HEADER}")
            # strip the binary path if present
            # required, so generated slice files to install into the correct location
            string(REPLACE "${CMAKE_BINARY_DIR}/source/${HEADER_DIR}" "" STRIP_HEADER "${HEADER}")
            # required for files from subdirectories
            get_filename_component(EXTRA_DIR "${STRIP_HEADER}" PATH)
            # install files into respective subdirectory in $PREFIX/include/
            install(
                FILES ${HEADER}
                COMPONENT headers
                DESTINATION "include/${HEADER_DIR}/${EXTRA_DIR}"
            )
        endforeach()

        install(
            TARGETS ${TARGET}
            EXPORT ${ARMARX_PROJECT_NAME}LibraryDepends
            COMPONENT libraries
            RUNTIME DESTINATION bin
            LIBRARY DESTINATION lib
            ARCHIVE DESTINATION lib
            PUBLIC_HEADER DESTINATION include/${ARMARX_PROJECT_NAME}/${TARGET}
        )
    endif()
endfunction()


# Installs an executable.
function(_armarx_install_executable TARGET)
    get_target_property(TARGET_ENABLED ${TARGET} ARMARX_ENABLED)

    if(${TARGET_ENABLED})
        install(
            TARGETS ${TARGET}
            EXPORT "${ARMARX_PROJECT_NAME}LibraryDepends"
            COMPONENT binaries
            RUNTIME DESTINATION bin
        )
    endif()
endfunction()


# Installs an external executable.
function(_armarx_install_external_executable EXECTUABLE_NAME)
    install(
        PROGRAMS "${ARMARX_BIN_DIR}/${EXECTUABLE_NAME}"
        COMPONENT binaries
        DESTINATION bin
    )
endfunction()

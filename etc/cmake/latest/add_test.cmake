function(armarx_add_test TARGET)
    # Parse arguments.
    set(multi_param TEST_FILES DEPENDENCIES DEPENDENCIES_LEGACY)
    set(single_param)
    set(flag_param)
    cmake_parse_arguments(PARSE_ARGV 1 AX "${flag_param}" "${single_param}" "${multi_param}")
    if(DEFINED AX_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "${TARGET}: Unknown arguments `${AX_UNPARSED_ARGUMENTS}`.")
    endif()

    if(${ARMARX_BUILD_TESTS})
        # TODO: Remove.
        if(NOT Boost_UNIT_TEST_FRAMEWORK_LIBRARY)
            find_package(Boost ${ArmarX_BOOST_VERSION} EXACT REQUIRED COMPONENTS unit_test_framework)
        endif()

        armarx_add_executable(${TARGET}
            SOURCES ${AX_TEST_FILES}
            DEPENDENCIES
                Boost::unit_test_framework
                ArmarXCore # for boost::assertion_failed
                ${AX_DEPENDENCIES}
            DEPENDENCIES_LEGACY
                ${AX_DEPENDENCIES_LEGACY}
        )

        target_include_directories(${TARGET} PRIVATE "${PROJECT_BINARY_DIR}/testing/")

        get_target_property(ENABLE_TEST ${TARGET} ARMARX_ENABLED)
        if(${ENABLE_TEST})
            add_test(
                NAME ${TARGET}
                COMMAND "${ARMARX_BIN_DIR}/${TARGET}" --output_format=XML --log_level=all
                        --report_level=detailed
            )
        endif()
    endif()
endfunction()

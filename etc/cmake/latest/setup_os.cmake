#
# OS identification based on gearbox.
#

# CMake does not distinguish Linux from other Unices.
string(REGEX MATCH Linux ARMARX_OS_LINUX ${CMAKE_SYSTEM_NAME})

# From now on, use our own OS flags
if (NOT ARMARX_OS_LINUX)
    message(FATAL_ERROR "You have to build ArmarX on Linux")
endif()

if(APPLE OR NOT UNIX)
    message(FATAL_ERROR "You have to build ArmarX on Linux")
endif()


# 32 or 64 bit Linux
# Set the library directory suffix accordingly
if (${CMAKE_SYSTEM_PROCESSOR} STREQUAL "x86_64")
    set(ARMARX_PROC_64BIT TRUE BOOL INTERNAL)
    message(STATUS "Linux x86_64 target detected")
elseif (${CMAKE_SYSTEM_PROCESSOR} STREQUAL "ppc64")
    message(STATUS "Linux ppc64 target detected")
    set(ARMARX_PROC_64BIT TRUE BOOL INTERNAL)
endif()


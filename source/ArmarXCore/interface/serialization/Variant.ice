#pragma once

[["cpp:include:ArmarXCore/interface/serialization/Variant/StreamableTraits.h"]]
[["cpp:include:ArmarXCore/interface/serialization/Variant/StreamReader.h"]]
[["cpp:include:ArmarXCore/interface/serialization/Variant/StreamWriter.h"]]

#define ARMARX_MAKE_VARIANT_QUOTE(...) #__VA_ARGS__
#define ARMARX_MAKE_VARIANT_ICE(Name, ...) [ ARMARX_MAKE_VARIANT_QUOTE(cpp:type:__VA_ARGS__) ] sequence<byte> Name

#pragma once

#include <ArmarXCore/interface/serialization/Eigen.ice>
#include <ArmarXCore/interface/serialization/Variant.ice>

[["cpp:include:ArmarXCore/interface/serialization/Shapes/XYConstrainedOrientedBox.h"]]
[["cpp:include:ArmarXCore/interface/serialization/Shapes/OrientedBox.h"]]
[["cpp:include:ArmarXCore/interface/serialization/Shapes/AlignedBox.h"]]

//boxes
module simox
{
    [ "cpp:type:::Eigen::AlignedBox<float, 3 >" ] sequence<float > AlignedBoxf;
    [ "cpp:type:::Eigen::AlignedBox<double, 3>" ] sequence<double> AlignedBoxd;
    sequence<AlignedBoxf> AlignedBoxfSeq;
    sequence<AlignedBoxd> AlignedBoxdSeq;
    
    [ "cpp:type:::simox::OrientedBox<float>"  ] sequence<float > OrientedBoxf;
    [ "cpp:type:::simox::OrientedBox<double>" ] sequence<double> OrientedBoxd;
    sequence<OrientedBoxf> OrientedBoxfSeq;
    sequence<OrientedBoxd> OrientedBoxdSeq;
    
    [ "cpp:type:::simox::XYConstrainedOrientedBox<float>"  ] sequence<float > XYConstrainedOrientedBoxf;
    [ "cpp:type:::simox::XYConstrainedOrientedBox<double>" ] sequence<double> XYConstrainedOrientedBoxd;
    sequence<XYConstrainedOrientedBoxf> XYConstrainedOrientedBoxfSeq;
    sequence<XYConstrainedOrientedBoxd> XYConstrainedOrientedBoxdSeq;
        
    ARMARX_MAKE_VARIANT_ICE(BoxfVariant, ::boost::variant<AlignedBoxf, OrientedBoxf, XYConstrainedOrientedBoxf>);
    ARMARX_MAKE_VARIANT_ICE(BoxdVariant, ::boost::variant<AlignedBoxd, OrientedBoxd, XYConstrainedOrientedBoxd>);
    sequence<BoxfVariant> BoxfVariantSeq;
    sequence<BoxdVariant> BoxdVariantSeq;
};

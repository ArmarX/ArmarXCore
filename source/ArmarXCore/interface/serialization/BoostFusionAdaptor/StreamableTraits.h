#pragma once

#include "Meta.h"

#define ARMARX_MAKE_STRUCT_ADAPTOR_StreamableTraits(Type)                                              \
    namespace Ice                                                                                      \
    {                                                                                                  \
        template<>                                                                                     \
        struct StreamableTraits<Type>                                                                  \
        {                                                                                              \
            using Helper = ::armarx::IceSerialization::fusion_struct_size<Type>;                       \
            static_assert (Helper::KnownCategory::value, "Not all members of '" #Type "' are known");  \
            static constexpr StreamHelperCategory helper = StreamHelperCategoryStruct;                 \
            static constexpr int minWireSize = Helper::MinWireSize::value;                             \
            static constexpr bool fixedLength = Helper::FixedLength::value;                            \
        };                                                                                             \
    }

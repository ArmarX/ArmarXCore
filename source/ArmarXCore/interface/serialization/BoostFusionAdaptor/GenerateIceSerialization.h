#pragma once

#include "StreamReader.h"
#include "StreamWriter.h"
#include "StreamableTraits.h"

#define ARMARX_MAKE_STRUCT_ADAPTOR_CPP(Type)                    \
    ARMARX_MAKE_STRUCT_ADAPTOR_StreamReader(Type)               \
    ARMARX_MAKE_STRUCT_ADAPTOR_StreamWriter(Type)               \
    ARMARX_MAKE_STRUCT_ADAPTOR_StreamableTraits(Type)

#define ARMARX_MAKE_STRUCT_AND_FUSION_ADAPTOR_CPP(Type, ...)    \
    BOOST_FUSION_ADAPT_STRUCT(Type, __VA_ARGS__)                \
    ARMARX_MAKE_STRUCT_ADAPTOR_CPP(Type)

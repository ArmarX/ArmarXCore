#pragma once

#include "Meta.h"

#define ARMARX_MAKE_STRUCT_ADAPTOR_StreamReader(Type)                                                  \
    namespace Ice                                                                                      \
    {                                                                                                  \
        template<class S>                                                                              \
        struct StreamReader<Type, S>                                                                   \
        {                                                                                              \
            using Helper = ::armarx::IceSerialization::fusion_struct_size<Type>;                       \
            static_assert (Helper::KnownCategory::value, "Not all members of '" #Type "' are known");  \
            struct Read                                                                                \
            {                                                                                          \
                S* s;                                                                                  \
                template<typename T>                                                                   \
                void operator()(T& t) const                                                            \
                {                                                                                      \
                    s->read(t);                                                                        \
                }                                                                                      \
            };                                                                                         \
            static void read(S* s, Type& v)                                                            \
            {                                                                                          \
                boost::fusion::for_each(v, Read{s});                                                   \
            }                                                                                          \
        };                                                                                             \
    }

#pragma once

[["cpp:include:ArmarXCore/interface/serialization/BoostFusionAdaptor/GenerateIceSerialization.h"]]

#define ARMARX_MAKE_STRUCT_ADAPTOR_QUOTE(...) #__VA_ARGS__
#define ARMARX_MAKE_STRUCT_ADAPTOR_ICE(Name, ...) [ ARMARX_MAKE_STRUCT_ADAPTOR_QUOTE(cpp:type:__VA_ARGS__) ] sequence<byte> Name

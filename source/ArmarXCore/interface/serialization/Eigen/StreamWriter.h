#pragma once

#include <Ice/StreamHelpers.h>

#include <Eigen/Geometry>

namespace Ice
{
    template<class Scalar, int Rows, int Cols, class S>
    struct StreamWriter<::Eigen::Matrix<Scalar, Rows, Cols>, S>
    {
        static void write(S* str, const ::Eigen::Matrix<Scalar, Rows, Cols>& m)
        {
            for (auto i = 0; i < m.size(); ++i)
            {
                str->write(m.data()[i]);
            }
        }
    };
    template<class Scalar, int Rows, class S>
    struct StreamWriter<::Eigen::Matrix<Scalar, Rows, 1   >, S>
    {
        static void write(S* str, const ::Eigen::Matrix<Scalar, Rows, 1>& m)
        {
            for (auto i = 0; i < m.size(); ++i)
            {
                str->write(m.data()[i]);
            }
        }
    };
    template<class Scalar, int Cols, class S>
    struct StreamWriter<::Eigen::Matrix<Scalar, 1, Cols>, S>
    {
        static void write(S* str, const ::Eigen::Matrix<Scalar, 1, Cols>& m)
        {
            for (auto i = 0; i < m.size(); ++i)
            {
                str->write(m.data()[i]);
            }
        }
    };

    template<class Scalar, int Rows, class S>
    struct StreamWriter <::Eigen::Matrix < Scalar, Rows, -1  >, S >
    {
        static void write(S* str, const ::Eigen::Matrix < Scalar, Rows, -1 > & m)
        {
            const Ice::Int cols = m.cols();
            str->write(cols);
            for (auto i = 0; i < m.size(); ++i)
            {
                str->write(m.data()[i]);
            }
        }
    };
    template<class Scalar, int Cols, class S>
    struct StreamWriter <::Eigen::Matrix < Scalar, -1, Cols >, S >
    {
        static void write(S* str, const ::Eigen::Matrix < Scalar, -1, Cols > & m)
        {
            const Ice::Int rows = m.rows();
            str->write(rows);
            for (auto i = 0; i < m.size(); ++i)
            {
                str->write(m.data()[i]);
            }
        }
    };
    template<class Scalar, class S>
    struct StreamWriter <::Eigen::Matrix < Scalar, -1, -1  >, S >
    {
        static void write(S* str, const ::Eigen::Matrix < Scalar, -1, -1 > & m)
        {
            const Ice::Int rows = m.rows();
            const Ice::Int cols = m.cols();
            str->write(rows);
            str->write(cols);
            for (auto i = 0; i < m.size(); ++i)
            {
                str->write(m.data()[i]);
            }
        }
    };

    template<class Scalar, class S>
    struct StreamWriter <::Eigen::Matrix < Scalar, 1, -1  >, S >
    {
        static void write(S* str, const ::Eigen::Matrix < Scalar, 1, -1 > & m)
        {
            const Ice::Int cols = m.cols();
            str->write(cols);
            for (auto i = 0; i < m.size(); ++i)
            {
                str->write(m.data()[i]);
            }
        }
    };
    template<class Scalar, class S>
    struct StreamWriter <::Eigen::Matrix < Scalar, -1, 1   >, S >
    {
        static void write(S* str, const ::Eigen::Matrix < Scalar, -1, 1 > & m)
        {
            const Ice::Int rows = m.rows();
            str->write(rows);
            for (auto i = 0; i < m.size(); ++i)
            {
                str->write(m.data()[i]);
            }
        }
    };

    template<class Scalar, class S>
    struct StreamWriter <::Eigen::Quaternion<Scalar>, S >
    {
        static void write(S* str, const ::Eigen::Quaternion<Scalar>& q)
        {
            str->write(q.w());
            str->write(q.x());
            str->write(q.y());
            str->write(q.z());
        }
    };
}

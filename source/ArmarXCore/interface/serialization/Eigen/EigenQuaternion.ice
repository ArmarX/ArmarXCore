#pragma once

[["cpp:include:ArmarXCore/interface/serialization/Eigen/StreamableTraits.h"]]
[["cpp:include:ArmarXCore/interface/serialization/Eigen/StreamReader.h"]]
[["cpp:include:ArmarXCore/interface/serialization/Eigen/StreamWriter.h"]]

module Eigen
{
    [ "cpp:type:::Eigen::Quaternion<float>" ] sequence<float> Quaternionf;
    [ "cpp:type:::Eigen::Quaternion<double>" ] sequence<double> Quaterniond;
};

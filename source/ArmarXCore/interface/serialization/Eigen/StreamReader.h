#pragma once

#include <Ice/StreamHelpers.h>

#include <Eigen/Geometry>

namespace Ice
{
    template<class Scalar, int Rows, int Cols, class S>
    struct StreamReader<::Eigen::Matrix<Scalar, Rows, Cols>, S>
    {
        static void read(S* str, ::Eigen::Matrix<Scalar, Rows, Cols>& m)
        {
            for (auto i = 0; i < m.size(); ++i)
            {
                str->read(m.data()[i]);
            }
        }
    };
    template<class Scalar, int Rows, class S>
    struct StreamReader<::Eigen::Matrix<Scalar, Rows, 1   >, S>
    {
        static void read(S* str, ::Eigen::Matrix<Scalar, Rows, 1>& m)
        {
            for (auto i = 0; i < m.size(); ++i)
            {
                str->read(m.data()[i]);
            }
        }
    };
    template<class Scalar, int Cols, class S>
    struct StreamReader<::Eigen::Matrix<Scalar, 1, Cols>, S>
    {
        static void read(S* str, ::Eigen::Matrix<Scalar, 1, Cols>& m)
        {
            for (auto i = 0; i < m.size(); ++i)
            {
                str->read(m.data()[i]);
            }
        }
    };

    template<class Scalar, int Rows, class S>
    struct StreamReader <::Eigen::Matrix < Scalar, Rows, -1  >, S >
    {
        static void read(S* str, ::Eigen::Matrix < Scalar, Rows, -1 > & m)
        {
            Ice::Int cols;
            str->read(cols);
            m.resize(Rows, cols);
            for (auto i = 0; i < m.size(); ++i)
            {
                str->read(m.data()[i]);
            }
        }
    };
    template<class Scalar, int Cols, class S>
    struct StreamReader <::Eigen::Matrix < Scalar, -1, Cols >, S >
    {
        static void read(S* str, ::Eigen::Matrix < Scalar, -1, Cols > & m)
        {
            Ice::Int rows;
            str->read(rows);
            m.resize(rows, Cols);
            for (auto i = 0; i < m.size(); ++i)
            {
                str->read(m.data()[i]);
            }
        }
    };
    template<class Scalar, class S>
    struct StreamReader <::Eigen::Matrix < Scalar, -1, -1  >, S >
    {
        static void read(S* str, ::Eigen::Matrix < Scalar, -1, -1 > & m)
        {
            Ice::Int rows;
            Ice::Int cols;
            str->read(rows);
            str->read(cols);
            m.resize(rows, cols);
            for (auto i = 0; i < m.size(); ++i)
            {
                str->read(m.data()[i]);
            }
        }
    };

    template<class Scalar, class S>
    struct StreamReader <::Eigen::Matrix < Scalar, 1, -1  >, S >
    {
        static void read(S* str, ::Eigen::Matrix < Scalar, 1, -1 > & m)
        {
            Ice::Int cols;
            str->read(cols);
            m.resize(cols);
            for (auto i = 0; i < m.size(); ++i)
            {
                str->read(m.data()[i]);
            }
        }
    };
    template<class Scalar, class S>
    struct StreamReader <::Eigen::Matrix < Scalar, -1, 1   >, S >
    {
        static void read(S* str, ::Eigen::Matrix < Scalar, -1, 1 > & m)
        {
            Ice::Int rows;
            str->read(rows);
            m.resize(rows);
            for (auto i = 0; i < m.size(); ++i)
            {
                str->read(m.data()[i]);
            }
        }
    };

    template<class Scalar, class S>
    struct StreamReader <::Eigen::Quaternion<Scalar>, S >
    {
        static void read(S* str, ::Eigen::Quaternion<Scalar>& q)
        {
            str->read(q.w());
            str->read(q.x());
            str->read(q.y());
            str->read(q.z());
        }
    };
}

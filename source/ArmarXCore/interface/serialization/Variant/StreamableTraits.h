#pragma once

#include <Ice/StreamHelpers.h>

#include <ArmarXCore/util/CPPUtility/variant.h>

namespace Ice
{
    template<>
    struct StreamableTraits<boost::blank>
    {
        static constexpr StreamHelperCategory helper = StreamHelperCategoryStruct;
        static constexpr int minWireSize = 1;
        static constexpr bool fixedLength = true;
    };

    template<class...Ts>
    struct StreamableTraits<boost::variant<Ts...>>
    {
        static constexpr StreamHelperCategory helper = StreamHelperCategoryStruct;
        static constexpr int minWireSize = 1;
        static constexpr bool fixedLength = false;
    };
}

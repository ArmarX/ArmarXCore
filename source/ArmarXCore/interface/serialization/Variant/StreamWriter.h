#pragma once

#include <Ice/StreamHelpers.h>

#include <ArmarXCore/util/CPPUtility/variant.h>

namespace Ice
{
    template<class S>
    struct StreamWriter<boost::blank, S>
    {
        static void write(S*, const boost::blank&) {}
    };

    template<class S, class...Ts>
    struct StreamWriter<boost::variant<Ts...>, S>
    {
    private:
        template<int I, class...Params>
        struct Writer
        {
            static void write(S*, const boost::variant<Ts...>&)
            {
                throw std::logic_error{"should not be possible to reach this"};
            }
        };
        template<int I, class T0, class...Tail>
        struct Writer<I, T0, Tail...>
        {
            static void write(S* str, const boost::variant<Ts...>& v)
            {
                const T0* p = boost::get<const T0>(&v);
                if (p)
                {
                    str->write(I);
                    str->write(*p);
                }
                else
                {
                    Writer < I + 1, Tail... >::write(str, v);
                }
            }
        };
    public:
        static void write(S* str, const boost::variant<Ts...>& v)
        {
            Writer<0, Ts...>::write(str, v);
        }
    };

    //    template<class S>
    //    struct StreamWriter<boost::variant<>, S>
    //    {
    //        static void write(S* str, boost::variant<>& v)
    //        {
    //        }
    //    };
}

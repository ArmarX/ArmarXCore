#pragma once

#include <Ice/StreamHelpers.h>

#include <SimoxUtility/shapes/OrientedBox.h>

#include <ArmarXCore/interface/serialization/Eigen.h>

namespace Ice
{
    template<class FloatT>
    struct StreamableTraits<::simox::OrientedBox<FloatT>>
    {
        static constexpr StreamHelperCategory helper = StreamHelperCategoryStruct;
        static constexpr int minWireSize = sizeof(FloatT) * 12;
        static constexpr bool fixedLength = true;
    };

    template<class FloatT, class S>
    struct StreamWriter<::simox::OrientedBox<FloatT>, S>
    {
        static void write(S* str, const ::simox::OrientedBox<FloatT>& m)
        {
            str->write(Eigen::Matrix<FloatT, 3, 1> {m.translation()});
            str->write(Eigen::Matrix<FloatT, 3, 1> {m.axis_x()* m.dimension(0)});
            str->write(Eigen::Matrix<FloatT, 3, 1> {m.axis_y() * m.dimension(1)});
            str->write(Eigen::Matrix<FloatT, 3, 1> {m.axis_z() * m.dimension(2)});
        }
    };

    template<class FloatT, class S>
    struct StreamReader<::simox::OrientedBox<FloatT>, S>
    {
        static void read(S* str, ::simox::OrientedBox<FloatT>& m)
        {
            Eigen::Matrix<FloatT, 3, 1> corner;
            Eigen::Matrix<FloatT, 3, 1> ex0;
            Eigen::Matrix<FloatT, 3, 1> ex1;
            Eigen::Matrix<FloatT, 3, 1> ex2;

            str->read(corner);
            str->read(ex0);
            str->read(ex1);
            str->read(ex2);
            m = ::simox::OrientedBox<FloatT> {corner, ex0, ex1, ex2};
        }
    };
}

#pragma once

#include <Ice/StreamHelpers.h>

#include <Eigen/Dense>

#include <ArmarXCore/interface/serialization/Eigen.h>

namespace Ice
{
    template<class FloatT, int Dim>
    struct StreamableTraits<::Eigen::AlignedBox<FloatT, Dim>>
    {
        static_assert(Dim > 0);
        static constexpr StreamHelperCategory helper = StreamHelperCategoryStruct;
        static constexpr int minWireSize = sizeof(FloatT) * Dim * 2;
        static constexpr bool fixedLength = true;
    };

    template<class FloatT, int Dim, class S>
    struct StreamWriter<::Eigen::AlignedBox<FloatT, Dim>, S>
    {
        static_assert(Dim > 0);
        static void write(S* str, const ::Eigen::AlignedBox<FloatT, Dim>& m)
        {
            str->write(m.min());
            str->write(m.max());
        }
    };

    template<class FloatT, int Dim, class S>
    struct StreamReader<::Eigen::AlignedBox<FloatT, Dim>, S>
    {
        static_assert(Dim > 0);
        static void read(S* str, ::Eigen::AlignedBox<FloatT, Dim>& m)
        {
            Eigen::Matrix<FloatT, Dim, 1> min;
            Eigen::Matrix<FloatT, Dim, 1> max;

            str->read(min);
            str->read(max);
            m = ::Eigen::AlignedBox<FloatT, Dim> {min, max};
        }
    };
}

#pragma once

#include <Ice/StreamHelpers.h>

#include <SimoxUtility/shapes/XYConstrainedOrientedBox.h>

#include <ArmarXCore/interface/serialization/Eigen.h>

namespace Ice
{
    template<class FloatT>
    struct StreamableTraits<::simox::XYConstrainedOrientedBox<FloatT>>
    {
        static constexpr StreamHelperCategory helper = StreamHelperCategoryStruct;
        static constexpr int minWireSize = sizeof(FloatT) * 7;
        static constexpr bool fixedLength = true;
    };

    template<class FloatT, class S>
    struct StreamWriter<::simox::XYConstrainedOrientedBox<FloatT>, S>
    {
        static void write(S* str, const ::simox::XYConstrainedOrientedBox<FloatT>& m)
        {
            str->write(Eigen::Matrix<FloatT, 3, 1> {m.translation()});
            str->write(m.yaw());
            str->write(Eigen::Matrix<FloatT, 3, 1> {m.dimensions()});
        }
    };

    template<class FloatT, class S>
    struct StreamReader<::simox::XYConstrainedOrientedBox<FloatT>, S>
    {
        static void read(S* str, ::simox::XYConstrainedOrientedBox<FloatT>& m)
        {
            Eigen::Matrix<FloatT, 3, 1> corner;
            FloatT yaw;
            Eigen::Matrix<FloatT, 3, 1> dimensions;
            str->read(corner);
            str->read(yaw);
            str->read(dimensions);
            m = ::simox::XYConstrainedOrientedBox<FloatT> {corner, yaw, dimensions};
        }
    };
}

#pragma once

module armarx { module core { module time { module dto
{

    module ClockType
    {
        enum ClockTypeEnum
        {
            Realtime,
            Monotonic,
            Virtual,
            Unknown
        };
    };

    struct Duration
    {
        long microSeconds = -9223372036854775808;  // std::numeric_limits<long>::min()
    };

    struct Frequency
    {
        Duration cycleDuration;
    }

    struct DateTime
    {
        Duration timeSinceEpoch;
        ClockType::ClockTypeEnum clockType;
        string hostname;
    };

};};};};

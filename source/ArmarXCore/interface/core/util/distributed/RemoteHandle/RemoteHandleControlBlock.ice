/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#pragma once
[["suppress-warning:deprecated"]]
module armarx
{
    class ClientSideRemoteHandleControlBlockBase;

    /**
     * @brief The reference counter for a handled object (can be in/decremented by a remote client)
     */
    interface RemoteHandleControlBlockInterface
    {
        /**
         * @brief increments the use count
         */
        void incrementUseCount();
        /**
         * @brief decrements the use count
         */
        void decrementUseCount();

        /**
         * @return An object used by the client to keep the object in memory.
         */
        ClientSideRemoteHandleControlBlockBase getClientSideRemoteHandleControlBlock();
        /**
         * @brief The managed object proxy.
         */
        ["cpp:const"] idempotent Object* getManagedObjectProxy();
        /**
         * @return The objects's use count.
         */
        ["cpp:const"] idempotent long  getUseCount();
    };

    /**
     * @brief Used to notice the remote object when the reference count is in/decreased (per ctor/dtor)
     */
    class ClientSideRemoteHandleControlBlockBase
    {
        /**
         * @return The managed object proxy.
         */
        Object* getManagedObjectProxy();

        /**
         * @brief The remot object reference counter.
         */
        ["protected"] RemoteHandleControlBlockInterface* remoteHandleControlBlockProxy;
        /**
         * @brief The managed object proxy.
         */
        ["protected"] Object* managedObjectProxy;
    };
};

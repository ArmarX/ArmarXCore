/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::ArmarXObjects::JsonStorage
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

module armarx
{
    struct JsonStoreValue
    {
        string key;
        string value;

        string provider;
    };

    struct JsonRetrieveValue
    {
        string key;
        string value;

        string provider;
        long revision = 0;
        long timestampInMicroSeconds = 0;
    };

    interface JsonStorageInterface
    {
        void storeJsonValue(JsonStoreValue value);

        JsonRetrieveValue retrieveValue(string key);
    };
};


#pragma once

module armarx
{
    interface ExternalApplicationManagerInterface
    {
        void restartApplication();
        void terminateApplication();
        string getPathToApplication();
        bool isApplicationRunning();
    };
};


/*
* This file is part of ArmarX.
*
* Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore
* @author     Stefan Reither ( stefan dot reither at kit dot edu )
* @date       2020
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/statechart/StatechartIce.ice>

module armarx
{
    enum StatechartExecutionResult
    {
        eSuccess,
        eFailure,
        eAborted
    }

    sequence<string> StringList;

    interface SimpleStatechartExecutorInterface
    {
        void ensureVariantLibrariesAreLoaded(StateParameterMap inputArguments);
        bool startStatechart(string proxyName, string stateName, StateParameterMap inputArguments);
        void stopImmediatly();
        bool hasExecutionFinished();
        StatechartExecutionResult waitUntilStatechartExecutionIsFinished();
        StringVariantContainerBaseMap getSetOutputParameters();
        StateParameterMap getOutputParameters();
        void preloadLibrariesFromHumanNames(StringList typeNames);
    };
};


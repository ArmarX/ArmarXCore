#pragma once

#include <ArmarXCore/interface/components/ExternalApplicationManagerInterface.ice>


module armarx
{
    interface PythonApplicationManagerInterface extends ExternalApplicationManagerInterface
    {
    };
};


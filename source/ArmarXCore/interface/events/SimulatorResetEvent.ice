#pragma once

module armarx
{
    interface SimulatorResetEvent
    {
        void simulatorWasReset();
    };
};

/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ObserverObjectFactories.h"
#include <ArmarXCore/observers/condition/LiteralImpl.h>
#include <ArmarXCore/observers/condition/Operations.h>
#include <ArmarXCore/observers/condition/ConditionRoot.h>
#include <ArmarXCore/observers/ConditionCheck.h>
#include <ArmarXCore/observers/Event.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/variant/StringValueMap.h>
#include <ArmarXCore/observers/variant/SingleTypeVariantList.h>
#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
#include <ArmarXCore/observers/variant/ChannelRef.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <ArmarXCore/observers/variant/TimestampVariant.h>
#include <ArmarXCore/observers/variant/VariantContainer.h>
#include <ArmarXCore/observers/variant/TimedVariant.h>
#include <ArmarXCore/observers/parameter/VariantParameter.h>
#include <ArmarXCore/observers/parameter/VariantListParameter.h>
#include <ArmarXCore/interface/observers/VariantBase.h>
#include <ArmarXCore/interface/observers/DataFieldIdentifierBase.h>
#include <ArmarXCore/interface/observers/Event.h>
#include <ArmarXCore/interface/observers/TermImplBase.h>
#include <ArmarXCore/interface/observers/ConditionCheckBase.h>
#include <ArmarXCore/interface/observers/ParameterBase.h>

#include <ArmarXCore/observers/filters/AverageFilter.h>
#include <ArmarXCore/observers/filters/ButterworthFilter.h>
#include <ArmarXCore/observers/filters/GaussianFilter.h>
#include <ArmarXCore/observers/filters/MedianFilter.h>
#include <ArmarXCore/observers/filters/DerivationFilter.h>
armarx::ObjectFactoryMap armarx::ObjectFactories::ObserverObjectFactories::getFactories()
{
    ObjectFactoryMap map;

    add<VariantBase, Variant>(map);
    add<TimedVariantBase, TimedVariant>(map);
    add<VariantParameterBase, VariantParameter>(map);
    add<VariantListParameterBase, VariantListParameter>(map);
    add<DataFieldIdentifierBase, DataFieldIdentifier>(map);
    add<ChannelRefBase, ChannelRef>(map);
    add<DatafieldRefBase, DatafieldRef>(map);
    add<TimestampBase, TimestampVariant>(map);
    add<VariantContainerBase, ContainerDummy>(map);
    add<SingleVariantBase, SingleVariant>(map);
    add<SingleTypeVariantListBase, SingleTypeVariantList>(map);
    add<StringValueMapBase, StringValueMap>(map);
    add<ContainerType, ContainerTypeI>(map);


    // condition
    add<LiteralImplBase, LiteralImpl>(map);
    add<OperationAndBase, OperationAnd>(map);
    add<OperationOrBase, OperationOr>(map);
    add<OperationNotBase, OperationNot>(map);
    add<ConditionRootBase, ConditionRoot>(map);

    add<ConditionCheckBase, ConditionCheck>(map);
    add<EventBase, Event>(map);
    add<MedianFilterBase, filters::MedianFilter>(map);
    add<AverageFilterBase, filters::AverageFilter>(map);
    add<GaussianFilterBase, filters::GaussianFilter>(map);
    add<ButterworthFilterBase, filters::ButterworthFilter>(map);
    add<DerivationFilterBase, filters::DerivationFilter>(map);

    return map;
}

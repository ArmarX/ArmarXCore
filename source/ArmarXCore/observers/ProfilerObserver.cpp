/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Core::observers::ProfilerObserver
 * @author     Manfred Kroehnert (manfred dot kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ProfilerObserver.h"

#include "checks/ConditionCheckEquals.h"
#include "checks/ConditionCheckUpdated.h"
#include "checks/ConditionCheckChanged.h"
#include "checks/ConditionCheckValid.h"

#include "../observers/variant/TimestampVariant.h"

#include <set>
#include <algorithm>
#include <ArmarXCore/core/services/profiler/Profiler.h>  // for ProfilerPtr



armarx::ProfilerObserver::~ProfilerObserver()
{
}

std::string armarx::ProfilerObserver::getDefaultName() const
{
    return "ProfilerObserver";
}


void armarx::ProfilerObserver::onInitObserver()
{
    usingTopic(armarx::Profiler::PROFILER_TOPIC_NAME);
}


void armarx::ProfilerObserver::onConnectObserver()
{
    offerChannel("activeStateChanged", "Channel reporting the currently active state of statecharts with EnableProfiling property set");
    offerDataField("activeStateChanged", "activeState", VariantType::String, "Name of the currently active state");
    offerConditionCheck(checks::equals, new ConditionCheckEquals());
    offerConditionCheck(checks::updated, new ConditionCheckUpdated());
    offerConditionCheck(checks::changed, new ConditionCheckChanged());
    offerConditionCheck(checks::valid, new ConditionCheckValid());

    setDataField("activeStateChanged", "activeState", "");
}


void armarx::ProfilerObserver::reportNetworkTraffic(const std::string& id, const std::string& protocol, Ice::Int inBytes, Ice::Int outBytes, const Ice::Current&)
{
    try
    {
        if (!existsChannel(id))
        {
            offerChannel(id, "Network traffic");
        }

        offerOrUpdateDataField(id, protocol + "_inBytes", Variant(inBytes), "Incoming network traffic");
        offerOrUpdateDataField(id, protocol + "_outBytes", Variant(outBytes), "Outgoing network traffic");
        updateChannel(id);
    }
    catch (...)
    {
        handleExceptions();
    }
}

void armarx::ProfilerObserver::reportEvent(const armarx::ProfilerEvent& event, const Ice::Current&)
{
    std::string stateIdentifier = event.parentName;
    try
    {
        createStateChannelIfRequired(stateIdentifier);

        int microsecondsModifier = 0;
        if (event.timestampUnit == "ms")
        {
            microsecondsModifier = 1000;
        }
        else if (event.timestampUnit == "us")
        {
            microsecondsModifier = 1;
        }

        TimestampVariant timestamp(event.timestamp * microsecondsModifier);
        if (event.functionName == Profiler::Profiler::GetEventName(Profiler::Profiler::eFunctionStart))
        {
            setDataField(stateIdentifier, "lastEnterTimestamp", timestamp);
        }
        else
        {
            VariantPtr lastActiveState = VariantPtr::dynamicCast(getDataField(new DataFieldIdentifier(getName(), stateIdentifier, "activeSubstate")));
            setDataField(stateIdentifier, "lastExitTimestamp", timestamp);
            setDataField(stateIdentifier, "activeSubstate", Variant(""));
            if (lastActiveState)
            {
                setDataFieldFlatCopy(stateIdentifier, "previousSubstate", lastActiveState);
            }
        }
        updateChannel(stateIdentifier);
        ARMARX_INFO << "Updating function info channel " << stateIdentifier;

    }
    catch (...)
    {
        handleExceptions();
    }
}

void armarx::ProfilerObserver::reportProcessCpuUsage(const ProfilerProcessCpuUsage& process, const Ice::Current&)
{
    std::string id = process.processName + std::to_string(process.processId);

    try
    {
        createResourceChannelIfRequired(id);

        setDataField(id, "cpuUsage", Variant(process.cpuUsage));
        updateChannel(id);
    }
    catch (...)
    {
        handleExceptions();
    }
}

void armarx::ProfilerObserver::reportProcessMemoryUsage(const ProfilerProcessMemoryUsage& memoryUsage, const Ice::Current&)
{
    std::string id = memoryUsage.processName + std::to_string(memoryUsage.processId);

    try
    {
        createResourceChannelIfRequired(id);

        setDataField(id, "memoryUsage", Variant(memoryUsage.memoryUsage));
        updateChannel(id);
    }
    catch (...)
    {
        handleExceptions();
    }
}



void armarx::ProfilerObserver::reportStatechartTransition(const ProfilerStatechartTransition& transition, const Ice::Current&)
{
    try
    {
        Variant activeSubstate(transition.targetStateIdentifier);
        setDataField("activeStateChanged", "activeState", activeSubstate);
        updateChannel("activeStateChanged");

        std::string stateIdentifier = transition.parentStateIdentifier;
        createStateChannelIfRequired(stateIdentifier);

        setDataField(stateIdentifier, "activeSubstate", activeSubstate);
        setDataField(stateIdentifier, "previousSubstate", Variant(transition.sourceStateIdentifier));
        setDataField(stateIdentifier, "lastEventName", Variant(transition.eventName));
        updateChannel(stateIdentifier);
        ARMARX_INFO << "Updating Transition info channel " << stateIdentifier;
    }
    catch (...)
    {
        handleExceptions();
    }
}


void armarx::ProfilerObserver::StateParameterToVariantMap(const armarx::StateParameterMap& parameterMap, armarx::StringValueMap& variantMap)
{
    for (auto& entry : parameterMap)
    {
        variantMap.addElement(entry.first, entry.second->value);
    }
}


void armarx::ProfilerObserver::reportStatechartInputParameters(const ProfilerStatechartParameters& inputParameters, const Ice::Current&)
{
    try
    {
        createStateChannelIfRequired(inputParameters.stateIdentifier);

        StringValueMap parameters(false);
        StateParameterToVariantMap(inputParameters.parameterMap, parameters);
        setDataField(inputParameters.stateIdentifier, "inputParameters", parameters);

        updateChannel(inputParameters.stateIdentifier);
    }
    catch (...)
    {
        handleExceptions();
    }
}


void armarx::ProfilerObserver::reportStatechartLocalParameters(const ProfilerStatechartParameters& localParameters, const Ice::Current&)
{
    try
    {
        createStateChannelIfRequired(localParameters.stateIdentifier);

        StringValueMap parameters(false);
        StateParameterToVariantMap(localParameters.parameterMap, parameters);
        setDataField(localParameters.stateIdentifier, "localParameters", parameters);

        updateChannel(localParameters.stateIdentifier);
    }
    catch (...)
    {
        handleExceptions();
    }
}

void armarx::ProfilerObserver::reportStatechartOutputParameters(const ProfilerStatechartParameters& outputParameters, const Ice::Current&)
{
    try
    {
        createStateChannelIfRequired(outputParameters.stateIdentifier);

        StringValueMap parameters(false);
        StateParameterToVariantMap(outputParameters.parameterMap, parameters);
        setDataField(outputParameters.stateIdentifier, "outputParameters", parameters);
        updateChannel(outputParameters.stateIdentifier);
    }
    catch (...)
    {
        handleExceptions();
    }
}

void armarx::ProfilerObserver::reportStatechartTransitionWithParameters(
    const armarx::ProfilerStatechartTransitionWithParameters& outputParameters, const Ice::Current&)
{
    // ignore this in the observer
}



void armarx::ProfilerObserver::createStateChannelIfRequired(const std::string& channelName)
{
    std::unique_lock lock(channelCheckMutex);
    if (existsChannel(channelName))
    {
        return;
    }

    offerChannel(channelName, "Statechart State Channel");

    offerDataField(channelName, "activeSubstate", VariantType::String, "Currently Active Substate");
    offerDataField(channelName, "previousSubstate", VariantType::String, "Last Active Substate");
    offerDataField(channelName, "lastEventName", VariantType::String, "Last Event/Transition Name");
    offerDataField(channelName, "inputParameters", VariantType::VariantContainer, "State Input Parameters");
    offerDataField(channelName, "localParameters", VariantType::VariantContainer, "State Local Parameters");
    offerDataField(channelName, "outputParameters", VariantType::VariantContainer, "State Output Parameters");
    offerDataField(channelName, "lastEnterTimestamp", VariantType::Timestamp, "Timestamp of when onEnter() of the state was last called");
    offerDataField(channelName, "lastExitTimestamp", VariantType::Timestamp, "Timestamp of when onExit() or onBreak() of the state was last called");
}


void armarx::ProfilerObserver::createResourceChannelIfRequired(const std::string& channelName)
{
    std::unique_lock lock(channelCheckMutex);
    if (existsChannel(channelName))
    {
        return;
    }

    offerChannel(channelName, "Resource Information");

    offerDataField(channelName, "cpuUsage", VariantType::Float, "Cpu Usage of process specified by the channelname");
    offerDataField(channelName, "memoryUsage", VariantType::Int, "Memory Usage of process specified by the channelname");
}





void armarx::ProfilerObserver::reportEventList(const armarx::ProfilerEventList& events, const Ice::Current&)
{
    // ignore these events in the observer
}

void armarx::ProfilerObserver::reportStatechartTransitionList(const armarx::ProfilerStatechartTransitionList& transitions, const Ice::Current&)
{
    armarx::ProfilerStatechartTransition lastTransition = transitions.back();
    setDataField("activeStateChanged", "activeState", Variant(lastTransition.targetStateIdentifier));
    updateChannel("activeStateChanged");
}

void armarx::ProfilerObserver::reportStatechartInputParametersList(const ProfilerStatechartParametersList& inputParametersList, const Ice::Current&)
{
    // remove all but the latest occurence of an entry in the list of profiler entries
    auto compareFunction = [](const ProfilerStatechartParameters & lhs, const ProfilerStatechartParameters & rhs)
    {
        return lhs.stateIdentifier < rhs.stateIdentifier;
    };
    std::set<ProfilerStatechartParameters, decltype(compareFunction)> latestElements(inputParametersList.rbegin(), inputParametersList.rend(), compareFunction);

    for (const auto& element : latestElements)
    {
        reportStatechartInputParameters(element);
    }
}

void armarx::ProfilerObserver::reportStatechartLocalParametersList(const ProfilerStatechartParametersList& localParametersList, const Ice::Current&)
{
    // remove all but the latest occurence of an entry in the list of profiler entries
    auto compareFunction = [](const ProfilerStatechartParameters & lhs, const ProfilerStatechartParameters & rhs)
    {
        return lhs.stateIdentifier < rhs.stateIdentifier;
    };
    std::set<ProfilerStatechartParameters, decltype(compareFunction)> latestElements(localParametersList.rbegin(), localParametersList.rend(), compareFunction);

    for (const auto& element : latestElements)
    {
        reportStatechartLocalParameters(element);
    }
}

void armarx::ProfilerObserver::reportStatechartOutputParametersList(const ProfilerStatechartParametersList& outputParametersList, const Ice::Current&)
{
    // remove all but the latest occurence of an entry in the list of profiler entries
    auto compareFunction = [](const ProfilerStatechartParameters & lhs, const ProfilerStatechartParameters & rhs)
    {
        return lhs.stateIdentifier < rhs.stateIdentifier;
    };
    std::set<ProfilerStatechartParameters, decltype(compareFunction)> latestElements(outputParametersList.rbegin(), outputParametersList.rend(), compareFunction);

    for (const auto& element : latestElements)
    {
        reportStatechartOutputParameters(element);
    }
}

void armarx::ProfilerObserver::reportStatechartTransitionWithParametersList(
    const armarx::ProfilerStatechartTransitionWithParametersList& outputParameters, const Ice::Current&)
{
    // ignore this in the observer
}

void armarx::ProfilerObserver::reportProcessCpuUsageList(const ProfilerProcessCpuUsageList& processes, const Ice::Current&)
{
    // remove all but the latest occurence of an entry in the list of profiler entries
    auto compareFunction = [](const ProfilerProcessCpuUsage & lhs, const ProfilerProcessCpuUsage & rhs)
    {
        return lhs.processId < rhs.processId;
    };
    std::set<ProfilerProcessCpuUsage, decltype(compareFunction)> latestElements(processes.rbegin(), processes.rend(), compareFunction);

    for (const auto& element : latestElements)
    {
        reportProcessCpuUsage(element);
    }
}

void armarx::ProfilerObserver::reportProcessMemoryUsageList(const ProfilerProcessMemoryUsageList& memoryUsages, const Ice::Current&)
{
    // remove all but the latest occurence of an entry in the list of profiler entries
    auto compareFunction = [](const ProfilerProcessMemoryUsage & lhs, const ProfilerProcessMemoryUsage & rhs)
    {
        return lhs.processId < rhs.processId;
    };
    std::set<ProfilerProcessMemoryUsage, decltype(compareFunction)> latestElements(memoryUsages.rbegin(), memoryUsages.rend(), compareFunction);

    for (const auto& element : latestElements)
    {
        reportProcessMemoryUsage(element);
    }
}

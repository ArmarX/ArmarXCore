/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include <chrono>

#include "DebugObserver.h"

#include <ArmarXCore/observers/exceptions/user/InvalidDatafieldException.h>

#include <ArmarXCore/observers/checks/ConditionCheckEquals.h>
#include <ArmarXCore/observers/checks/ConditionCheckUpdated.h>

namespace armarx
{

    DebugObserver::DebugObserver()
    {
    }

    void DebugObserver::onInitObserver()
    {
        usingTopic(getProperty<std::string>("DebugObserverTopicName"));

        offerConditionCheck("updated", new ConditionCheckUpdated());
        offerConditionCheck("equals", new ConditionCheckEquals());
    }

    void DebugObserver::onConnectObserver()
    {
    }

    void DebugObserver::setDebugDatafield_async(
        const AMD_DebugObserverInterface_setDebugDatafieldPtr& amd,
        const std::string& channelName,
        const std::string& datafieldName,
        const VariantBasePtr& value,
        const Ice::Current& c)
    {
        addWorkerJob("DebugObserver::setDebugDatafield", [this, amd, channelName, datafieldName, value]
        {
            callAndPassExceptionToAMD(amd, [&]
            {
                if (!existsChannel(channelName))
                {
                    offerChannel(channelName, "");
                }

                if (!existsDataField(channelName, datafieldName))
                {
                    offerDataFieldWithDefault(channelName, datafieldName, *VariantPtr::dynamicCast(value), "");
                }
                else
                {
                    setDataFieldFlatCopy(channelName, datafieldName, VariantPtr::dynamicCast(value));
                }

                updateChannel(channelName);
                amd->ice_response();
            });
        });
    }

    void DebugObserver::setDebugChannel_async(
        const AMD_DebugObserverInterface_setDebugChannelPtr& amd,
        const std::string& channelName,
        const StringVariantBaseMap& valueMap,
        const Ice::Current& c)
    {
        addWorkerJob("DebugObserver::setDebugChannel", [this, amd, channelName, valueMap]
        {
            callAndPassExceptionToAMD(amd, [&]
            {
                offerOrUpdateDataFieldsFlatCopy(channelName, valueMap);
                amd->ice_response();
            });
        });
    }

    void DebugObserver::removeDebugDatafield_async(
        const AMD_DebugObserverInterface_removeDebugDatafieldPtr& amd,
        const std::string& channelName,
        const std::string& datafieldName,
        const Ice::Current&)
    {
        addWorkerJob("DebugObserver::removeDebugDatafield", [this, amd, channelName, datafieldName]
        {
            callAndPassExceptionToAMD(amd, [&]
            {
                removeDatafield(new DataFieldIdentifier(getName(), channelName, datafieldName));
                amd->ice_response();
            });
        });
    }

    void DebugObserver::removeDebugChannel_async(
        const AMD_DebugObserverInterface_removeDebugChannelPtr& amd,
        const std::string& channelName,
        const Ice::Current&)
    {
        addWorkerJob("DebugObserver::removeDebugChannel", [this, amd, channelName]
        {
            callAndPassExceptionToAMD(amd, [&]
            {
                removeChannel(channelName);
                amd->ice_response();
            });
        });
    }

    void DebugObserver::removeAllChannels_async(
        const AMD_DebugObserverInterface_removeAllChannelsPtr& amd,
        const Ice::Current&)
    {
        addWorkerJob("DebugObserver::removeAllChannels", [this, amd]
        {
            callAndPassExceptionToAMD(amd, [&]
            {
                ChannelRegistry channels = getAvailableChannels(false);
                ChannelRegistry::iterator it = channels.end();

                for (; it != channels.end(); it++)
                {
                    ChannelRegistryEntry& entry = it->second;
                    removeChannel(entry.name);
                }
                amd->ice_response();
            });
        });
    }
}


armarx::PropertyDefinitionsPtr armarx::DebugObserver::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new DebugObserverPropertyDefinitions(
                                      getConfigIdentifier()));
}

/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter (mirko.waechter at kit dot edu)
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "TimeWindowAverageFilter.h"
//#include <ArmarXCore/core/logging/Logging.h>
namespace armarx::rtfilters
{

    TimeWindowAverageFilter::TimeWindowAverageFilter(IceUtil::Time timeWindowSize, double maxFrequency) :
        RTFilterBase(timeWindowSize.toSecondsDouble() * maxFrequency),
        timeWindowSize(timeWindowSize)
    {
    }

    double TimeWindowAverageFilter::calculate()
    {
        if (dataHistory.empty())
        {
            return 0;
        }
        IceUtil::Time newestTimestamp = dataHistory.back().first;
        IceUtil::Time timeBorder = (newestTimestamp - timeWindowSize);
        double sum = 0;
        int count = 0;
        for (auto it = dataHistory.rbegin(); it != dataHistory.rend(); it++)
        {
            IceUtil::Time& t = it->first;
            double val = it->second;
            if (t > timeBorder)
            {
                count++;
                sum += val;
            }
            else
            {
                // assume that timestamps are ordered
                break;
            }
        }
        if (count > 0)
        {
            return sum / count;
        }
        else
        {
            return 0;
        }
    }

    RTFilterBasePtr TimeWindowAverageFilter::clone() const
    {
        return std::make_shared<TimeWindowAverageFilter>(*this);
    }

}

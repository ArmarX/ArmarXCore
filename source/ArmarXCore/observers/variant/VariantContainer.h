/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Kai Welke (welke@kit.edu)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once


#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/interface/observers/VariantContainers.h>
#include <ArmarXCore/interface/observers/VariantBase.h>
#include <ArmarXCore/observers/variant/Variant.h>

#include <IceUtil/Handle.h>

#include <string>
#include <map>


namespace armarx::VariantType
{
    const VariantTypeId VariantContainer = Variant::addTypeName("::armarx::VariantContainerBase");
}

namespace armarx
{
    class VariantContainer;
    using VariantContainerPtr = IceInternal::Handle<VariantContainer>;


    class SingleVariant;
    using SingleVariantPtr = IceInternal::Handle<SingleVariant>;

    /**
     * \class VariantContainer
     * \ingroup VariantsGrp
     * \brief VariantContainer is the base class of all other Variant container classes.
     *
     * Each VariantContainer can contain values of the types
     *
     * \li VariantContainer or subclasses thereof
     * \li SingleVariant
     *
     * Since VariantContainer inherits from VariantDataClass it is possible
     * to put a VariantContainer into a Variant to send it via Ice.
     * \see StringValueMap
     * \see SingleTypeVariantList
     *
     \code
     // VariantContainer can be replaced with any subclass of it
     armarx::VariantContainerPtr variantContainer;
     // create variant from VariantContainer
     armarx::VariantPtr variant = new armarx::Variant(variantContainer);
     // get VariantContainer from Variant
     armarx::VariantContainerPtr variantContainer2 = variant->get<armarx::VariantContainer>();
     if (variantContainer2)
     {
        // work with the container
     }
     else
     {
        // value of the variant was not a VariantContainer
     }
     \endcode
     */
    class ARMARXCORE_IMPORT_EXPORT VariantContainer :
        virtual public VariantContainerBase
    {
    public:
        ContainerTypePtr getContainerType(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setContainerType(const ContainerTypePtr& containerType, const Ice::Current& c = Ice::emptyCurrent) override;

        // VariantDataClass interface
    public:
        VariantDataClassPtr clone(const Ice::Current& c = Ice::emptyCurrent) const override;
        std::string output(const Ice::Current& c = Ice::emptyCurrent) const override;
        Ice::Int getType(const Ice::Current& c = Ice::emptyCurrent) const override;
        bool validate(const Ice::Current& c = Ice::emptyCurrent) override;
    };


    /**
     * \class SingleVariant
     * \ingroup VariantsGrp
     * \brief The SingleVariant class is required to store single Variant instances in VariantContainer subclasses.
     *
     * Without this class the compiler would not be able to compile code which is storing single Variants instead of
     * VariantContainers as values inside a VariantContainer instance .
     */
    class ARMARXCORE_IMPORT_EXPORT SingleVariant :
        virtual public VariantContainer,
        virtual public SingleVariantBase
    {
    public:
        SingleVariant();
        SingleVariant(const Variant& variant);
        SingleVariant(Variant& variant);
        SingleVariant(Variant&& variant);
        SingleVariant(const SingleVariant& source);
        SingleVariant& operator=(const SingleVariant& source);
        VariantContainerBasePtr cloneContainer(const::Ice::Current& = Ice::emptyCurrent) const override;
        Ice::ObjectPtr ice_clone() const override;
        VariantBasePtr getElementBase(const Ice::Current& = Ice::emptyCurrent) const override;
        VariantPtr get() const;
        void setVariant(const VariantPtr& variant);

        int getSize(const::Ice::Current& = Ice::emptyCurrent) const override;
        ContainerTypePtr getContainerType(const Ice::Current& c = Ice::emptyCurrent) const override;
        void setContainerType(const ContainerTypePtr& containerType, const Ice::Current& c = Ice::emptyCurrent) override;
        static VariantTypeId getStaticType(const Ice::Current& c = Ice::emptyCurrent);
        //        int getSubType(const::Ice::Current & = Ice::emptyCurrent) const { return subType;}
        void clear(const::Ice::Current& = Ice::emptyCurrent) override { }
        bool validateElements(const::Ice::Current& = Ice::emptyCurrent) override;

    public: // serialization
        void serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = Ice::emptyCurrent) const override;
        void deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = Ice::emptyCurrent) override;

        template<class T>
        static T fromString(const std::string& s)
        {
            std::istringstream stream(s);
            T t;
            stream >> t;
            return t;
        }

        static std::string getTypePrefix();

        // VariantContainerBase interface
    public:
        std::string toString(const Ice::Current& c) const override;
    };

    class ContainerDummy : virtual public VariantContainer
    {
    public:
        ContainerDummy();
        ContainerDummy(const ContainerDummy& source);
        VariantContainerBasePtr cloneContainer(const Ice::Current& = Ice::emptyCurrent) const override;
        Ice::ObjectPtr ice_clone() const override;
        ContainerTypePtr getContainerType(const Ice::Current&   = Ice::emptyCurrent) const override;
        void setContainerType(const ContainerTypePtr& containerType, const Ice::Current& = Ice::emptyCurrent) override;
        void clear(const Ice::Current& = Ice::emptyCurrent) override {}
        int getSize(const Ice::Current& = Ice::emptyCurrent) const override;
        bool validateElements(const Ice::Current& = Ice::emptyCurrent) override;
    public: // serialization
        void serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = Ice::emptyCurrent) const override {}
        void deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = Ice::emptyCurrent) override {}

        // VariantContainerBase interface
    public:
        std::string toString(const Ice::Current& c) const override;
    };

    class ContainerTypeI : virtual public ContainerType
    {
    public:
        ContainerTypeI() {}
        ContainerTypeI(VariantTypeId variantType);
        ContainerTypePtr clone(const Ice::Current& = Ice::emptyCurrent) const override;
    };
    using ContainerTypeIPtr = IceInternal::Handle<ContainerTypeI>;


    class VariantContainerType
    {
    public:
        VariantContainerType(std::string containerType);
        const ContainerTypeI operator()(VariantTypeId typeId) const;
        const ContainerTypeI operator()(const ContainerType& subType) const;
        static bool compare(const ContainerTypePtr& type1, const ContainerTypePtr& secondType);
        static std::string allTypesToString(const ContainerTypePtr& type);
        static ContainerTypePtr FromString(const std::string& typeStr);
        static std::string GetInnerType(const std::string& typeStr);
        static std::string GetInnerType(ContainerTypePtr type);
    private:
        std::string containerType;
        ContainerTypePtr thisType;
    };
}

namespace armarx::VariantType
{
    const VariantContainerType SingleVariantContainer = VariantContainerType(SingleVariant::getTypePrefix());
}

extern template class ::IceInternal::Handle< ::armarx::SingleVariant>;
extern template class ::IceInternal::Handle< ::armarx::ContainerTypeI>;

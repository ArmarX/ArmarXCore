/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TermImpl.h"

template class ::IceInternal::Handle<::armarx::TermImpl>;

namespace armarx
{
    TermImpl::~TermImpl()
    {
        // assure all childs are removed properly (required because of loop)
        removeChildren();
    }

    void TermImpl::addChild(const TermImplBasePtr& child, const Ice::Current& c)
    {
        child->setParent(this);

        childs.push_back(child);
    }

    TermImplSequence TermImpl::getChilds(const Ice::Current& c)
    {
        return childs;
    }

    TermImplBasePtr TermImpl::getParent(const Ice::Current& c)
    {
        return parent;
    }

    bool TermImpl::getValue(const Ice::Current& c) const
    {
        return value;
    }

    StringVariantBaseMap TermImpl::getDatafields(const Ice::Current&) const
    {
        return datafieldValues;
    }

    TermType TermImpl::getType(const Ice::Current& c) const
    {
        return type;
    }

    void TermImpl::update(const Ice::Current& c)
    {
        if (parent)
        {
            parent->update();
        }
    }

    void TermImpl::updateWithData(const Ice::Current& c)
    {
        if (parent)
        {
            parent->updateWithData();
        }
    }

    void TermImpl::removeChildren()
    {
        __incRef(); // prevent self deletion
        TermImplSequence::iterator iterChilds = childs.begin();

        while (iterChilds != childs.end())
        {
            if (*iterChilds)
            {
                (*iterChilds)->resetParent();
            }

            childs.erase(iterChilds);
            iterChilds = childs.begin();
        }

        __decRef();
        this->childs.clear();
    }

    void TermImpl::setParent(const TermImplBasePtr& parent, const Ice::Current& c)
    {
        this->parent = parent;
    }

    void TermImpl::resetParent(const Ice::Current& c)
    {
        parent = nullptr;
    }

    int TermImpl::atomicDecAndTestValue(volatile int* counter, int value)
    {
        unsigned char c;
        __asm__ __volatile__(
            "lock ; decl %0; sete %1"
            :"=m"(*counter), "=qm"(c)
            :"m"(*counter) : "memory");
        return c != value;
    }

    void TermImpl::__incRef()
    {
        Shared::__incRef();
    }

    void TermImpl::__decRef()
    {
        //            int numberChilds = int(getChilds().size());
        int numberChilds = int(childs.size());
#if defined(_WIN32)
        assert(InterlockedExchangeAdd(&_ref, 0) > 0);

        if (InterlockedDecrement(&_ref) == numberChilds && !_noDelete)
        {
            _noDelete = true;
            delete this;
        }

#elif defined(ICE_HAS_GCC_BUILTINS)
        int c = __sync_fetch_and_sub(&_ref, 1);
        assert(c > 0);

        if ((c == numberChilds + 1) && !_noDelete)
        {
            _noDelete = true;
            delete this;
        }

#elif defined(ICE_HAS_ATOMIC_FUNCTIONS)
        assert(IceUtilInternal::atomicExchangeAdd(&_ref, 0) > 0);

        if (IceUtilInternal::atomicDecAndTestValue(&_ref, numberChilds) && !_noDelete)
        {
            _noDelete = true;
            delete this;
        }
#elif ICE_INT_VERSION >= 30603
        int c = _ref.fetch_sub(1);
        assert(c > 0);

        if ((c == numberChilds + 1) && !__hasFlag(NoDelete))
        {
            __setNoDelete(true);
            delete this;
        }
#else
        _mutex.lock();
        bool doDelete = false;
        assert(_ref > 0);

        if (--_ref == numberChilds)
        {
            doDelete = !_noDelete;
            _noDelete = true;
        }

        _mutex.unlock();

        if (doDelete)
        {
            delete this;
        }
#endif
    }
}

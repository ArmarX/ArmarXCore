/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::ArmarXObjects::SimpleSimpleStatechartExecutor
 * @author     Stefan Reither ( stefan dot reither at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/interface/components/SimpleStatechartExecutorInterface.h>

#include <ArmarXCore/interface/statechart/RemoteStateOffererIce.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/observers/variant/VariantInfo.h>

#include <condition_variable>
#include <mutex>


namespace armarx
{
    /**
     * @class SimpleStatechartExecutorPropertyDefinitions
     * @brief
     */
    class SimpleStatechartExecutorPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        SimpleStatechartExecutorPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("PackagesForVariantLibraries", "ArmarXCore, ArmarXGui, RobotAPI",
                                                "Comma-seperated list of packages that are used for loading nessecary libraries for used variants");
        };
    };



    /**
     * @defgroup Component-SimpleStatechartExecutor SimpleStatechartExecutor
     * @ingroup ArmarXCore-Components
     * This component provides an interface for executing and stopping statecharts without the need for
     * the StatechartEditor. The proxy for the statechart-group containing the statechart to execute must already run.
     *
     * Only one state can run at any time from within this component, but it is possible
     * to start more than one instance of this component.
     *
     * @class SimpleStatechartExecutor
     * @ingroup Component-SimpleStatechartExecutor
     * @brief This component provides interface functions to execute a statechart, stop the currently running
     * statechart and provide the output-parameter of the statechart.
     */
    class SimpleStatechartExecutor :
        virtual public armarx::Component,
        virtual public armarx::SimpleStatechartExecutorInterface
    {


        // SimpleStatechartExecutorInterface interface
    public:
        void ensureVariantLibrariesAreLoaded(const StateParameterMap& inputArguments, const Ice::Current&) override;
        bool startStatechart(const std::string& proxyName, const std::string& stateName, const StateParameterMap& inputArguments, const Ice::Current&) override;
        void stopImmediatly(const Ice::Current&) override;
        bool hasExecutionFinished(const Ice::Current&) override;
        StatechartExecutionResult waitUntilStatechartExecutionIsFinished(const Ice::Current&) override;
        StringVariantContainerBaseMap getSetOutputParameters(const Ice::Current&) override;
        StateParameterMap getOutputParameters(const Ice::Current&) override;
        void preloadLibrariesFromHumanNames(const StringList& typeNames, const Ice::Current&) override;

        std::string getDefaultName() const override
        {
            return "SimpleStatechartExecutor";
        }

    protected:
        void onInitComponent() override;
        void onConnectComponent() override;
        void onExitComponent() override;
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    private:
        void statechartTask();
        ContainerTypePtr checkIfLibraryNeedsToBeLoaded(const StateParameterIceBasePtr parameter) const;
        bool isCurrentStateIdValid() const;

        bool _finished;
        mutable std::mutex _finishedMutex;
        mutable std::condition_variable _finishedCondition;
        bool _aborted;

        StatechartExecutionResult _lastResult;
        StateParameterMap _lastOutputParameters;

        RunningTask<SimpleStatechartExecutor>::pointer_type _runningTask;

        RemoteStateOffererInterfacePrx _prx;
        int _currentStateId;

        Ice::StringSeq _packages;
        VariantInfoPtr variantInfo;
        std::map<std::string, DynamicLibraryPtr> _loadedDynamicLibraries;
    };
}

/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::ArmarXObjects::SimpleStatechartExecutor
 * @author     Stefan Reither ( stefan dot reither at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SimpleStatechartExecutor.h"
#include <ArmarXCore/statechart/StateUtilFunctions.h>
#include <ArmarXCore/statechart/StateBase.h>
#include <ArmarXCore/observers/variant/StringValueMap.h>
#include <ArmarXCore/core/system/DynamicLibrary.h>
#include <ArmarXCore/core/ArmarXManager.h>

namespace armarx
{
    void SimpleStatechartExecutor::ensureVariantLibrariesAreLoaded(const StateParameterMap& inputArguments, const Ice::Current&)
    {
        for (const auto& arg : inputArguments)
        {
            ContainerTypePtr type = checkIfLibraryNeedsToBeLoaded(arg.second);
            if (type)
            {
                ARMARX_DEBUG << "Loading lib for variant: " << type->typeId;
                _loadedDynamicLibraries[type->typeId] = variantInfo->loadLibraryOfVariant(type->typeId);
            }
        }
        ArmarXManager::RegisterKnownObjectFactoriesWithIce(getIceManager()->getCommunicator());
    }

    bool SimpleStatechartExecutor::startStatechart(const std::string& proxyName, const std::string& stateName, const StateParameterMap& inputArguments, const Ice::Current&)
    {
        for (const auto& arg : inputArguments)
        {
            if (checkIfLibraryNeedsToBeLoaded(arg.second))
            {
                ARMARX_ERROR << "DynamicLibrary for argument '" << arg.first << "' is not loaded! Statechart will not be started.";
                return false;
            }
        }

        if (!_finished)
        {
            ARMARX_WARNING << "SimpleStatechartExecutor is not ready. Cannot start statechart";
            return false;
        }

        RemoteStateOffererIceBasePrx prx;
        try
        {
            prx = getProxy<RemoteStateOffererIceBasePrx>(proxyName, false);
        }
        catch (::Ice::NotRegisteredException& e)
        {
            ARMARX_WARNING << "RemoteStateOfferer '" << proxyName << "' not available!";
            return false;
        }

        if (!prx)
        {
            ARMARX_WARNING << "RemoteStateOfferer '" << proxyName << "' not available!";
            return false;
        }

        StateBasePtr stateBasePtr = StateBasePtr::dynamicCast(prx->getStatechart(stateName));
        if (!stateBasePtr)
        {
            ARMARX_WARNING << "RemoteStateOfferer '" << proxyName << "' does not have an available state with name: '" << stateName << "'!";
            return false;
        }

        _prx = RemoteStateOffererInterfacePrx::checkedCast(prx);
        _currentStateId = _prx->createRemoteStateInstance(stateName, nullptr, stateName, stateName + IceUtil::generateUUID());

        StringVariantContainerBaseMap input = StateUtilFunctions::getSetValues(inputArguments);
        _prx->callRemoteState(_currentStateId, input);

        {
            std::unique_lock lock(_finishedMutex);
            _finished = false;
        }
        _aborted = false;

        if (_runningTask)
        {
            _runningTask->stop(false);
        }
        _runningTask = new RunningTask<SimpleStatechartExecutor>(this, &SimpleStatechartExecutor::statechartTask, "StatechartManager");
        _runningTask->start();
        return true;
    }

    void SimpleStatechartExecutor::stopImmediatly(const Ice::Current&)
    {
        std::unique_lock lock(_finishedMutex);
        if (!_finished && _prx && isCurrentStateIdValid())
        {
            _prx->breakActiveSubstateRemotely(_currentStateId, nullptr);
            _prx->breakRemoteState(_currentStateId, nullptr);
            _aborted = true;
            _finishedCondition.wait(lock);
        }
        else
        {
            _finished = true;
            _finishedCondition.notify_all();
        }
        _currentStateId = -1;
    }

    bool SimpleStatechartExecutor::hasExecutionFinished(const Ice::Current&)
    {
        return _finished;
    }

    StatechartExecutionResult SimpleStatechartExecutor::waitUntilStatechartExecutionIsFinished(const Ice::Current&)
    {
        std::unique_lock lock(_finishedMutex);

        while (!_finished)
        {
            _finishedCondition.wait(lock);
            usleep(1000);
        }
        return _lastResult;
    }

    StringVariantContainerBaseMap SimpleStatechartExecutor::getSetOutputParameters(const Ice::Current&)
    {
        std::unique_lock lock(_finishedMutex);
        if (_finished)
        {
            return StateUtilFunctions::getSetValues(_lastOutputParameters);
        }
        else
        {
            return StringVariantContainerBaseMap();
        }
    }

    StateParameterMap SimpleStatechartExecutor::getOutputParameters(const Ice::Current&)
    {
        std::unique_lock lock(_finishedMutex);
        if (_finished)
        {
            return _lastOutputParameters;
        }
        else
        {
            return StateParameterMap();
        }
    }

    void SimpleStatechartExecutor::preloadLibrariesFromHumanNames(const StringList& typeNames, const Ice::Current&)
    {
        for (const auto& typeName : typeNames)
        {
            ARMARX_INFO << "Loading library for type '" << typeName << "'";
            _loadedDynamicLibraries[typeName] = variantInfo->loadLibraryOfVariant(typeName);
        }
        ArmarXManager::RegisterKnownObjectFactoriesWithIce(getIceManager()->getCommunicator());
    }

    void SimpleStatechartExecutor::onInitComponent()
    {
        std::string packagesString = getProperty<std::string>("PackagesForVariantLibraries").getValue();
        _packages = armarx::Split(packagesString, ",", true, true);
        variantInfo = VariantInfo::ReadInfoFiles(_packages, true, false);
    }

    void SimpleStatechartExecutor::onConnectComponent()
    {
        _finished = true;
    }

    void SimpleStatechartExecutor::onExitComponent()
    {
        ARMARX_DEBUG << "Unloading dynamicLibraries";
        for (const auto& it : _loadedDynamicLibraries)
        {
            DynamicLibraryPtr lib = it.second;
            lib->unload();
        }

        if (!_finished)
        {
            stopImmediatly(Ice::emptyCurrent);
            {
                std::unique_lock lock(_finishedMutex);
                _finished = true;
                _finishedCondition.notify_all();
            }
            _runningTask->stop();
        }
    }

    armarx::PropertyDefinitionsPtr SimpleStatechartExecutor::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new SimpleStatechartExecutorPropertyDefinitions(
                getConfigIdentifier()));
    }


    void SimpleStatechartExecutor::statechartTask()
    {
        while (_prx && isCurrentStateIdValid() && !_prx->isRemoteStateFinished(_currentStateId))
        {
            usleep(1000);
        }
        if (_prx)
        {
            _lastResult = (_aborted ? eAborted : eSuccess);
            // TODO find better way to solve the problem of not laoded libraries for outputParameter
            _lastOutputParameters = _prx->getStatechartInstance(_currentStateId)->outputParameters;
            ensureVariantLibrariesAreLoaded(_lastOutputParameters, Ice::emptyCurrent);
            _lastOutputParameters = _prx->getStatechartInstance(_currentStateId)->outputParameters;
        }
        std::unique_lock lock(_finishedMutex);
        _finished = true;
        _finishedCondition.notify_all();

        ARMARX_INFO << "statechartTask finished";
    }

    ContainerTypePtr SimpleStatechartExecutor::checkIfLibraryNeedsToBeLoaded(const StateParameterIceBasePtr parameter) const
    {
        bool loadLib = false;
        ContainerTypePtr containerType = parameter->value->getContainerType();
        if (containerType->typeId == "::armarx::SingleTypeVariantListBase" || containerType->typeId == "::armarx::StringValueMapBase")
        {
            while (containerType->subType)
            {
                containerType = containerType->subType;
            }
            loadLib = _loadedDynamicLibraries.count(containerType->typeId) == 0;
        }
        else
        {
            SingleVariantPtr variant = SingleVariantPtr::dynamicCast(parameter->value);
            if (variant && variant->get()->data->ice_id() == "::armarx::VariantData")
            {
                loadLib = _loadedDynamicLibraries.count(containerType->typeId) == 0;
            }
        }

        return loadLib ? containerType : ContainerTypePtr();
    }

    bool SimpleStatechartExecutor::isCurrentStateIdValid() const
    {
        for (const auto& it : _prx->getAvailableStateInstances())
        {
            if (it.first == _currentStateId)
            {
                return true;
            }
        }
        return false;
    }
}

/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::ArmarXObjects::PythonApplicationManager
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <filesystem>

#include <SimoxUtility/meta/EnumNames.hpp>

#include <ArmarXCore/interface/components/PythonApplicationManagerInterface.h>
#include <ArmarXCore/components/ExternalApplicationManager/ExternalApplicationManager.h>


namespace armarx
{

    /**
     * @defgroup Component-PythonApplicationManager PythonApplicationManager
     * @ingroup ArmarXCore-Components
     *
     * The PythonApplicationManager is designed to run a python script located
     * in an ArmarX package. It is based on the `ExternalApplicationManager`,
     * but provides more suitable properties for such python scripts and some
     * path discovery.
     *
     * @class PythonApplicationManager
     * @ingroup Component-PythonApplicationManager
     * @brief Replaces some parts of the `ExternalApplicationManager` to be
     * more suitable for python scripts.
     *
     */
    class PythonApplicationManager :
        virtual public armarx::ExternalApplicationManager,  // Component
        virtual public armarx::PythonApplicationManagerInterface
    {
    public:

        using Base = armarx::ExternalApplicationManager;

        enum class VenvType
        {
            Auto,
            Dedicated,
            Shared,
        };
        static const simox::meta::EnumNames<VenvType> VenvTypeNames;

    public:



    protected:

        std::string getDefaultName() const override
        {
            return "PythonApplicationManager";
        }

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;


        std::string deriveWorkingDir() const override;
        std::string deriveApplicationPath() const override;
        void addApplicationArguments(Ice::StringSeq& args) override;


    public:

        struct Properties
        {
            std::string armarxPackageName;
            std::string armarxPythonPackagesDir = "python/";



            std::string pythonPackageName;
            std::string pythonScriptPath;
            /// Whitespace separated list of arguments.
            std::string pythonScriptArgumentsString;
            std::vector<std::string> pythonScriptArgumentsVector;
            std::string pythonPathEntriesString;
            std::vector<std::string> pythonPathEntriesVector;

            std::string venvName = "venv";
            VenvType venvType = VenvType::Auto;

            bool pythonPoetry = false;

            std::string workingDir = "";

            void defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix = "");
            void read(PropertyUser& props, const std::string& prefix = "");
        };

        struct Paths
        {
            using path = std::filesystem::path;

            std::string sharedVenvsDir = "shared_venvs";

            // These are absolute paths.

            path armarxPackagePath;
            path pythonPackagePath;
            path pythonScriptPath;
            path venvPath;
            path pythonBinPath;
            path workingDir;

            void derive(const Properties& properties);

            path findArmarXPackagePath(const Properties& properties) const;
            path findPythonPackagePath(const Properties& properties) const;
            path findPythonScriptPath(const Properties& properties) const;
            path findVenvPath(const Properties& properties) const;
            path findPythonBinaryPath(const Properties& properties) const;

            path getDedicatedVenvPath(const Properties& properties) const;
            path getSharedVenvPath(const Properties& properties) const;

            friend std::ostream& operator<<(std::ostream& os, const Paths& rhs);

            static std::optional<path> checkCandidateFiles(const std::vector<path>& candidates);
            static std::optional<path> checkCandidateDirectories(const std::vector<path>& candidates);
            static std::optional<path> checkCandidatePaths(const std::vector<path>& candidates, std::function<bool(path)> existsFn);

        };


    private:

        Properties properties;
        Paths paths;

        bool inputOk = false;


    };
}


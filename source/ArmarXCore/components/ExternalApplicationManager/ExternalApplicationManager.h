/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::ArmarXObjects::ExternalApplicationManager
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/interface/components/ExternalApplicationManagerInterface.h>

#include <ArmarXCore/components/ExternalApplicationManager/ExternalApplicationManagerDependency.h>
#include <ArmarXCore/components/ExternalApplicationManager/ExternalApplicationManagerStarter.h>


#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-result"
#include <boost/process.hpp>
#include <boost/process/child.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/asio.hpp>
#include <boost/iostreams/device/file_descriptor.hpp>
#pragma GCC diagnostic pop

#include <mutex>

#if defined(BOOST_WINDOWS_API)
using pipe_end = boost::asio::windows::stream_handle;
#elif defined(BOOST_POSIX_API)
using pipe_end = boost::asio::posix::stream_descriptor;
#endif


namespace armarx
{
    /**
     * @class ExternalApplicationManagerPropertyDefinitions
     * @brief
     */
    class ExternalApplicationManagerPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        ExternalApplicationManagerPropertyDefinitions(std::string prefix);
    };


    /**
     * @defgroup Component-ExternalApplicationManager ExternalApplicationManager
     * @ingroup ArmarXCore-Components
     * The ExternalApplicationManager can run any executale file. It can be configured to restart the given application and/or to disconnect itself if said application crashes.
     * Applications within an ArmarX-Project can be started by setting its package and its name in the config file.
     * In case of applications outside of ArmarX the whole path or its name if its directory is part of the PATH variable is required.\n
     *
     * Furthermore it provides functionalities over Ice to stop and restart the executed application.
     *
     * @class ExternalApplicationManager
     * @ingroup Component-ExternalApplicationManager
     * @brief Executes a given application and keeps track if it is still running. Provides methods for stopping and restarting said application.
     *
     *
     */
    class ExternalApplicationManager :
        virtual public armarx::Component,
        virtual public armarx::ExternalApplicationManagerInterface
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        virtual std::string getDefaultName() const override
        {
            return "ExternalApplicationManager";
        }


        // ExternalApplicationManagerInterface

        void restartApplication(const Ice::Current&) override;
        void terminateApplication(const Ice::Current&) override;
        std::string getPathToApplication(const Ice::Current&) override;
        bool isApplicationRunning(const Ice::Current&) override;


    protected:

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;
        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;
        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;
        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;


        void addDependencyObject();
        void addStarterObject();

        virtual std::string deriveWorkingDir() const;
        virtual std::string deriveApplicationPath() const;
        virtual void addApplicationArguments(Ice::StringSeq& args);


    private:

        struct StreamMetaData
        {
            StreamMetaData();
            StreamMetaData(const StreamMetaData& data);

            MessageTypeT level;
            boost::asio::io_service io_service;
            boost::process::pipe pipe;
            pipe_end pend;
            boost::asio::streambuf input_buffer;
            boost::iostreams::file_descriptor_sink sink;
            std::function<void(const boost::system::error_code& error, std::size_t size)> read;
        };
        using StreamMetaDataPtr = std::shared_ptr<struct StreamMetaData>;


    private:

        void updateLogSenderComponentName();

        void startApplication();
        void stopApplication();
        void waitForApplication();

        void cleanUp();

        bool waitForProcessToFinish(int pid, int timeoutMS);

        void setupStream(StreamMetaData& meta);


    private:

        /// Either name of application or relative or absolute path
        std::string application;
        Ice::StringSeq args;
        Ice::StringSeq envVars;
        bool restartWhenCrash;
        bool disconnectWhenCrash;
        std::string startUpKeyword;
        std::string workingDir;
        bool redirectToArmarXLog;

        std::unique_ptr<boost::process::child> childProcess;

        bool isAppRunning;
        bool appStoppedOnPurpose;

        RunningTask<ExternalApplicationManager>::pointer_type waitTask;

        boost::iostreams::stream<boost::iostreams::file_descriptor_source> out_stream;
        StreamMetaDataPtr outMetaData;

        boost::iostreams::stream<boost::iostreams::file_descriptor_source> err_stream;
        StreamMetaDataPtr errMetaData;

        boost::iostreams::file_descriptor_source stdout_source;
        boost::iostreams::file_descriptor_source stderr_source;

        ExternalApplicationManagerStarterPtr starter;
        ExternalApplicationManagerDependencyPtr depObj;
        std::string starterUUID, depObjUUID;

        friend struct ExternalApplicationManagerStarter;
        friend struct ExternalApplicationManagerDependency;

    };
}


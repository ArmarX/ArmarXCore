/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::ArmarXObjects::ExternalApplicationManager
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/ManagedIceObject.h>


namespace armarx
{
    using ExternalApplicationManagerPtr = IceInternal::Handle<class ExternalApplicationManager>;

    struct ExternalApplicationManagerStarter : virtual public armarx::ManagedIceObject
    {
        Ice::StringSeq dependencies;
        void setDependencies(const Ice::StringSeq& deps)
        {
            this->dependencies = deps;
        }

        ExternalApplicationManagerPtr parent;
        void setParent(ExternalApplicationManagerPtr parent)
        {
            this->parent = parent;
        }


        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        void onInitComponent() override;
        void onConnectComponent() override;

        void onDisconnectComponent() override;
        void onExitComponent() override;
    };

    using ExternalApplicationManagerStarterPtr = IceInternal::Handle<ExternalApplicationManagerStarter>;

}



# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore ArmarXFileLogger)
 
armarx_add_test(ArmarXFileLoggerTest ArmarXFileLoggerTest.cpp "${LIBS}")
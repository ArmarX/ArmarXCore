/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::ArmarXObjects::JsonStorage
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "JsonStorage.h"

#include <ArmarXCore/core/time/TimeUtil.h>

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>


namespace armarx
{
    ARMARX_DECOUPLED_REGISTER_COMPONENT(JsonStorage);

    class JsonStoragePropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        JsonStoragePropertyDefinitions(std::string prefix)  :
            armarx::ComponentPropertyDefinitions(prefix)
        {
        }
    };


    std::string JsonStorage::getDefaultName() const
    {
        return "JsonStorage";
    }


    void JsonStorage::onInitComponent()
    {

    }


    void JsonStorage::onConnectComponent()
    {

    }


    void JsonStorage::onDisconnectComponent()
    {

    }


    void JsonStorage::onExitComponent()
    {

    }

    armarx::PropertyDefinitionsPtr JsonStorage::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new JsonStoragePropertyDefinitions(
                getConfigIdentifier()));
    }

    void JsonStorage::storeJsonValue(const JsonStoreValue& value, const Ice::Current&)
    {
        long timestampInMicroSeconds = TimeUtil::GetTime().toMicroSeconds();

        std::lock_guard lock(mutex);

        revision += 1;

        JsonRetrieveValue& inserted = data[value.key];
        inserted.key = value.key;
        inserted.value = value.value;
        inserted.provider = value.provider;
        inserted.revision = revision;
        inserted.timestampInMicroSeconds = timestampInMicroSeconds;
    }

    JsonRetrieveValue JsonStorage::retrieveValue(const std::string& key, const Ice::Current&)
    {
        std::lock_guard lock(mutex);

        auto it = data.find(key);
        if (it == data.end())
        {
            return JsonRetrieveValue();
        }
        else
        {
            return it->second;
        }
    }

}


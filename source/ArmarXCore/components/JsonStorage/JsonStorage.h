/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::ArmarXObjects::JsonStorage
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/interface/components/JsonStorageInterface.h>

#include <mutex>
#include <map>

namespace armarx
{

    struct JsonStorage
        : virtual armarx::Component
        , virtual armarx::JsonStorageInterface
    {
        std::string getDefaultName() const override;
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void storeJsonValue(const JsonStoreValue& value, const Ice::Current&) override;
        JsonRetrieveValue retrieveValue(const std::string& key, const Ice::Current&) override;


        std::mutex mutex;
        std::map<std::string, JsonRetrieveValue> data;
        long revision = 0;
    };
}

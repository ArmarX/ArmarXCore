/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::ArmarXObjects::PingLoadTest
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
//#include <RobotAPI/libraries/core/visualization/DebugDrawerTopic.h>
#include <ArmarXCore/interface/components/PingLoadTestInterface.h>

#include <ArmarXCore/core/services/tasks/RunningTask.h>

namespace armarx
{
    /**
     * @class PingLoadTestPropertyDefinitions
     * @brief
     */
    class PingLoadTestPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        PingLoadTestPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-PingLoadTest PingLoadTest
     * @ingroup ArmarXCore-Components
     * A description of the component PingLoadTest.
     *
     * @class PingLoadTest
     * @ingroup Component-PingLoadTest
     * @brief Brief description of class PingLoadTest.
     *
     * Detailed description of class PingLoadTest.
     */
    class PingLoadTest :
        virtual public PingLoadTestInterface,
        virtual public armarx::Component
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        virtual std::string getDefaultName() const override;


    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        virtual void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        virtual void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        virtual void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        virtual void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        // Private methods and member variables go here.

        /// Debug observer. Used to visualize e.g. time series.
        armarx::DebugObserverInterfacePrx debugObserver;
        /// Debug drawer. Used for 3D visualization.
        //armarx::DebugDrawerTopic debugDrawer;
        std::string remotePingComponentName;
        PingLoadTestInterfacePrx remotePingPrx;
        PingLoadTestTopicInterfacePrx pingTopicPrx;
        RunningTask<PingLoadTest>::pointer_type taskProxyPing;
        RunningTask<PingLoadTest>::pointer_type taskTopicPing;
        RunningTask<PingLoadTest>::pointer_type taskTopicSleep;
        int PingProxyDelayMS;
        int PingTopicDelayMS;
        int RemoteTopicSleepDelayMS;
        int SleepDuringTopicPing;
        int topicPingSeqNr = 0;


        // PingLoadTestTopicInterface interface
        void taskProxyPingRun();
        void taskTopicPingRun();
        void taskTopicSleepPingRun();
    public:
        void pingTopic(int seqNr, Ice::Long senderTime, const Ice::Current&) override;
        void remoteTopicSleep(const std::string& targetNameFilter, Ice::Long sleepMS, const Ice::Current&) override;

        // PingLoadTestInterface interface
    public:
        void ping(Ice::Long senderTime, const Ice::Current&) override;
        Ice::Long getRemoteTime(const Ice::Current&) override;
    };
}

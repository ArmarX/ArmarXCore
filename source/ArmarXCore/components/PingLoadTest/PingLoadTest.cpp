/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::ArmarXObjects::PingLoadTest
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PingLoadTest.h"

#include <ArmarXCore/core/time/TimeUtil.h>


namespace armarx
{
    PingLoadTestPropertyDefinitions::PingLoadTestPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
        //defineRequiredProperty<std::string>("PropertyName", "Description");
        //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");

        defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver", "Name of the topic the DebugObserver listens on");

        defineOptionalProperty<std::string>("RemotePingComponentName", "", "Name of the remote ping component");
        defineOptionalProperty<std::string>("PingTopicName", "PingTopic", "Name of the ping topic");

        defineOptionalProperty<int>("PingProxyDelayMS", -1, "Delay between proxy ping messages: < 0 disable ping; = 0 no delay; > 0 enable with delay ");
        defineOptionalProperty<int>("PingTopicDelayMS", -1, "Delay between topic ping messages: < 0 disable ping; = 0 no delay; > 0 enable with delay ");
        defineOptionalProperty<int>("RemoteTopicSleepDelayMS", -1, "Delay between topic sleep calls: < 0 disable; = 0 no delay; > 0 enable with delay ");
        defineOptionalProperty<int>("SleepDuringTopicPing", -1, "Sleep during topic ping: < 0 disable; = 0 no delay; > 0 enable with delay ");
    }


    std::string PingLoadTest::getDefaultName() const
    {
        return "PingLoadTest";
    }


    void PingLoadTest::onInitComponent()
    {
        // Register offered topices and used proxies here.
        offeringTopicFromProperty("DebugObserverName");
        // debugDrawer.offeringTopic(*this);  // Calls this->offeringTopic().
        if (getProperty<std::string>("RemotePingComponentName").getValue() != "")
        {
            usingProxyFromProperty("RemotePingComponentName");
        }
        // Using a proxy will cause the component to wait until the proxy is available.
        // usingProxyFromProperty("MyProxyName");

        offeringTopicFromProperty("PingTopicName");
        usingTopicFromProperty("PingTopicName");

        getProperty(PingProxyDelayMS, "PingProxyDelayMS");
        getProperty(PingTopicDelayMS, "PingTopicDelayMS");
        getProperty(RemoteTopicSleepDelayMS, "RemoteTopicSleepDelayMS");
        getProperty(SleepDuringTopicPing, "SleepDuringTopicPing");

        ARMARX_IMPORTANT << VAROUT(PingProxyDelayMS);
        ARMARX_IMPORTANT << VAROUT(PingTopicDelayMS);
        ARMARX_IMPORTANT << VAROUT(RemoteTopicSleepDelayMS);


    }




    void PingLoadTest::onConnectComponent()
    {
        // Get topics and proxies here. Pass the *InterfacePrx type as template argument.
        debugObserver = getTopicFromProperty<DebugObserverInterfacePrx>("DebugObserverName");
        // debugDrawer.getTopic(*this);  // Calls this->getTopic().

        if (getProperty<std::string>("RemotePingComponentName").getValue() != "")
        {
            getProxyFromProperty(remotePingPrx, "RemotePingComponentName");
        }
        getTopicFromProperty(pingTopicPrx, "PingTopicName");

        // getProxyFromProperty<MyProxyInterfacePrx>("MyProxyName");

        if (PingProxyDelayMS >= 0)
        {
            taskProxyPing = new RunningTask<PingLoadTest>(this, &PingLoadTest::taskProxyPingRun, "taskProxyPing");
            taskProxyPing->start();
        }
        if (PingTopicDelayMS >= 0)
        {
            taskTopicPing = new RunningTask<PingLoadTest>(this, &PingLoadTest::taskTopicPingRun, "taskTopicPing");
            taskTopicPing->start();
        }
        if (RemoteTopicSleepDelayMS >= 0)
        {
            taskTopicSleep = new RunningTask<PingLoadTest>(this, &PingLoadTest::taskTopicSleepPingRun, "taskTopicSleep");
            taskTopicSleep->start();
        }
    }

    void PingLoadTest::taskProxyPingRun()
    {
        while (taskProxyPing->isRunning())
        {
            remotePingPrx->ping(TimeUtil::GetTime().toMicroSeconds());
            TimeUtil::SleepMS(PingProxyDelayMS);
        }
    }
    void PingLoadTest::taskTopicPingRun()
    {
        topicPingSeqNr = 0;
        while (taskTopicPing->isRunning())
        {
            pingTopicPrx->pingTopic(topicPingSeqNr, TimeUtil::GetTime().toMicroSeconds());
            topicPingSeqNr++;
            TimeUtil::SleepMS(PingTopicDelayMS);
        }
    }
    void PingLoadTest::taskTopicSleepPingRun()
    {
        while (taskTopicSleep->isRunning())
        {
            pingTopicPrx->remoteTopicSleep("__any__", 1000);
            TimeUtil::SleepMS(RemoteTopicSleepDelayMS);
        }
    }


    void PingLoadTest::onDisconnectComponent()
    {

    }


    void PingLoadTest::onExitComponent()
    {

    }




    armarx::PropertyDefinitionsPtr PingLoadTest::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new PingLoadTestPropertyDefinitions(
                getConfigIdentifier()));
    }
}


void armarx::PingLoadTest::pingTopic(int seqNr, Ice::Long senderTime, const Ice::Current&)
{
    ARMARX_IMPORTANT << "seqNr: " << seqNr << " " << getName() + ": pingTopic. Time diff: " << (TimeUtil::GetTime().toMicroSeconds() - senderTime) << "us";
    if (SleepDuringTopicPing > 0)
    {
        ARMARX_IMPORTANT << "seqNr: " << seqNr << " sleeping for " << SleepDuringTopicPing << "ms";
        TimeUtil::SleepMS(SleepDuringTopicPing);
        ARMARX_IMPORTANT << "seqNr: " << seqNr << " finshed sleep";
    }
}

void armarx::PingLoadTest::remoteTopicSleep(const std::string& targetNameFilter, Ice::Long sleepMS, const Ice::Current&)
{
    if (targetNameFilter == getName() || targetNameFilter == "" || targetNameFilter == "__any__")
    {
        ARMARX_IMPORTANT << "remoteTopicSleep: sleeping for " << sleepMS << "ms";
        TimeUtil::SleepMS(sleepMS);
    }
}

void armarx::PingLoadTest::ping(Ice::Long senderTime, const Ice::Current&)
{
    ARMARX_IMPORTANT << getName() + ": ping. Time diff: " << (TimeUtil::GetTime().toMicroSeconds() - senderTime) << "us";
}

Ice::Long armarx::PingLoadTest::getRemoteTime(const Ice::Current&)
{
    return TimeUtil::GetTime().toMicroSeconds();
}

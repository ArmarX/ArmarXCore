/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <SimoxUtility/meta/enum/adapt_enum.h>
#include <ArmarXCore/util/CPPUtility/trace.h>
#include <ArmarXCore/core/application/properties/PropertyUser.h>
//bimap
namespace armarx::meta::properties
{
    template<class T>
    struct MapPropertyValuePlugin<boost::bimap<std::string, T>, void> : std::true_type
    {
        static PropertyDefinition<T>&
        Map(armarx::PropertyDefinition<T>& pd, const boost::bimap<std::string, T>& values)
        {
            for (const auto& [name, value] : values)
            {
                pd.map(name, value);
            }
            return pd;
        }
    };

    template<class T>
    struct MapPropertyValuePlugin <
        boost::bimap<T, std::string>,
        std::enable_if_t < !std::is_same_v<T, std::string >>
                > : std::true_type
    {
        static PropertyDefinition<T>&
        Map(armarx::PropertyDefinition<T>& pd, const boost::bimap<T, std::string>& values)
        {
            for (const auto& [value, name] : values)
            {
                pd.map(name, value);
            }
            return pd;
        }
    };
}
//EnumNames
namespace armarx::meta::properties
{
    template<class T>
    struct MapPropertyValuePlugin <simox::meta::EnumNames<T>, void> : std::true_type
    {
        static PropertyDefinition<T>&
        Map(armarx::PropertyDefinition<T>& pd, const simox::meta::EnumNames<T>& values)
        {
            return pd.map(values.map());
        }
    };
}
//GetPropertyPlugin
namespace armarx::meta::properties
{
    template<class T>
    struct GetPropertyPlugin<T, simox::meta::enable_if_enum_adapted_t<T>> : std::true_type
    {
        static void Get(armarx::PropertyUser& pu, T& val, const std::string& name)
        {
            std::cout << "callget\n";
            ARMARX_TRACE;
            val = boost::lexical_cast<T>(pu.getProperty<std::string>(name));
        }
    };
}
//DefinePropertyPlugin
namespace armarx::meta::properties
{
    template<class T>
    struct PDInitHookPlugin<T, simox::meta::enable_if_enum_adapted_t<T>> : std::true_type
    {
        static void Init(PropertyDefinition<T>& pd)
        {
            pd.map(simox::meta::enum_names<T>);
        }
    };
}

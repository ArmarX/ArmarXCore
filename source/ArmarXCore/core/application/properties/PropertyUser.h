/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "Property.h"                   // for Property
#include "PropertyDefinitionContainer.h"

#include <ArmarXCore/util/CPPUtility/emplace.h>
#include <ArmarXCore/core/IceManager.h>

#include <IceUtil/Handle.h>             // for Handle, HandleBase
#include <IceUtil/Shared.h>             // for Shared

#include <vector>
#include <string>
#include <atomic>

namespace armarx
{
    class PropertyUser;
    using PropertyUserPtr = IceUtil::Handle<PropertyUser>;
    class PropertyDefinitionContainer;

    using PropertyDefinitionsPtr = IceUtil::Handle<PropertyDefinitionContainer>;
    /* ====================================================================== */
    /* === PropertyUser Declaration ========================================= */
    /* ====================================================================== */

    /**
     * @class PropertyUser
     * @brief Abstract PropertyUser class
     *
     * PropertyUser provides the interface to create a PropertyDefinition as
     * well as the access to each property which has been defined.
     *
     * Properties can be updated by calling PropertyUser::setIceProperties().
     * A class inheriting from PropertyUser can react to property changes by
     * overriding PropertyUser::propertiesUpdated();
     */
    class PropertyUser:
        public virtual IceUtil::Shared
    {
    public:
        PropertyUser();

        ~PropertyUser() override;

        /**
         * Property creation and retrieval
         *
         * @param name  Requested property name (note: without prefix)
         *
         * @return Property instance
         */
        template <typename PropertyType>
        Property<PropertyType> getProperty(const std::string& name);

        /**
         * Hack to allow using getProperty in `const`-modified methods.
         */
        template <typename PropertyType>
        Property<PropertyType> getProperty(const std::string& name) const
        {
            return const_cast<PropertyUser&>(*this).getProperty<PropertyType>(name);
        }

        template <class T>
        void getProperty(T& val, const std::string& name) const;

        template <class T>
        void getProperty(std::atomic<T>& val, const std::string& name) const;

        void updateProperties();
        void updateProxies(IceManagerPtr);
        std::vector<std::string> getComponentProxyNames();
        std::vector<std::string> getTopicProxyNames();
        std::vector<std::string> getSubscribedTopicNames();

        template <class ContainerT>
        void getPropertyAsCSV(ContainerT& val,
                              const std::string& name,
                              const std::string& splitBy = ",;",
                              bool trimElements          = true,
                              bool removeEmptyElements   = true);

        template <class T>
        std::vector<T> getPropertyAsCSV(const std::string& name,
                                        const std::string& splitBy = ",;",
                                        bool trimElements          = true,
                                        bool removeEmptyElements   = true);

        bool hasProperty(const std::string& name);

        /**
         * Creates the property definition container.
         * @note Implement this factory function to create your own definitions.
         *
         * @return Property definition container pointer
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions() = 0;

        /**
         * This method is called when new Properties are set via setIceProperties().
         * Each class deriving from PropertyUser can overwrite this method in order
         * to react to changed properties.
         */
        virtual void icePropertiesUpdated(const std::set<std::string>& changedProperties);

        /**
         * Returns the component's property definition container
         *
         * @return Property definition container
         */
        PropertyDefinitionsPtr getPropertyDefinitions();

        /**
         * @brief Called after \ref createPropertyDefinitions by Component to
         * inject propertes of \ref ComponentPlugin
         *
         */
        virtual void injectPropertyDefinitions(PropertyDefinitionsPtr&);

        /**
         * Returns the set of Ice properties.
         *
         * @return Pointer to the Ice::Properties set.
         */
        Ice::PropertiesPtr getIceProperties() const;

        /**
         * Sets the Ice properties.
         *
         * @param properties Ice Properties.
         */
        virtual void setIceProperties(Ice::PropertiesPtr properties);

        virtual void updateIceProperties(const std::map<std::string, std::string>& changes);

        bool tryAddProperty(const std::string& propertyName, const std::string& value);

    private:
        /**
         * Component related properties
         */
        Ice::PropertiesPtr properties;

        struct Impl;
        std::unique_ptr<Impl> impl;
    };


    /* ====================================================================== */
    /* === PropertyUser implementation ====================================== */
    /* ====================================================================== */

    template <typename PropertyType>
    Property<PropertyType> PropertyUser::getProperty(const std::string& name)
    {
        return Property<PropertyType>(
                   getPropertyDefinitions()->getDefintion<PropertyType>(name),
                   getPropertyDefinitions()->getPrefix(),
                   properties);
    }

    template <class T>
    inline void PropertyUser::getProperty(T& val, const std::string& name) const
    {
        using plugin = meta::properties::GetPropertyPlugin<T>;
        if constexpr(plugin::value)
        {
            plugin::Get(*this, val, name);
        }
        else
        {
            val = getProperty<T>(name).getValue();
        }
    }

    template <class T>
    inline void PropertyUser::getProperty(std::atomic<T>& val, const std::string& name) const
    {
        val = getProperty<T>(name).getValue();
    }

    template <class ContainerT>
    inline void PropertyUser::getPropertyAsCSV(
        ContainerT& val,
        const std::string& name,
        const std::string& splitBy,
        bool trimElements,
        bool removeEmptyElements)
    {
        using ValueT = typename ContainerT::value_type;
        const std::string csv = getProperty<std::string>(name);
        if constexpr(std::is_same_v<ContainerT, std::vector<std::string>>)
        {
            val =  Split(csv, splitBy, trimElements, removeEmptyElements);
        }
        else
        {
            const auto strvec = Split(csv, splitBy, trimElements, removeEmptyElements);
            val.clear();
            for (const auto& str : strvec)
            {
                if constexpr(std::is_same_v<ValueT, std::string>)
                {
                    armarx::emplace(val, str);
                }
                else
                {
                    armarx::emplace(val, PropertyDefinition_lexicalCastTo<ValueT>(str));
                }
            }
        }
    }
    template <class T>
    inline std::vector<T> PropertyUser::getPropertyAsCSV(
        const std::string& name,
        const std::string& splitBy,
        bool trimElements,
        bool removeEmptyElements)
    {
        std::vector<T> val;
        getPropertyAsCSV(val, name, splitBy, trimElements, removeEmptyElements);
        return val;
    }
    /**
     * PropertyUser smart pointer type
     */
    using PropertyUserPtr = IceUtil::Handle<PropertyUser>;

    /**
     * UserProperty list type
     */
    using PropertyUserList = std::vector<PropertyUserPtr>;
}


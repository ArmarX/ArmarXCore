/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "PropertyDefinition.hpp"

#include <ArmarXCore/core/application/properties/PropertyDefinition.h>

#include <Ice/LocalObject.h>

#include <algorithm>                    // for min, fill
#include <iostream>                     // for basic_ostream, operator<<, etc
#include <limits>
#include <map>
#include <string>                       // for basic_string, string, etc
#include <typeinfo>                     // for bad_cast
#include <vector>                       // for vector

namespace armarx
{
    // Helper functions to reduce includes
    std::string FileSystemPathBuilder_ApplyFormattingAndResolveEnvAndCMakeVars(std::string const& value);


    /* ====================================================================== */
    /* === Property Declaration ============================================= */
    /* ====================================================================== */

    /**
     * @ingroup properties
     *
     * @class Property
     * @brief Provides access to Ice properties with extended capabilities.
     *
     * The Property template class provides access to Ice config properties
     * and command line options. Its main capability is to map a string value on
     * any type that is required and put constrains on the value as well as
     * on the property itself at the same time. For instance you can specify
     * that a certain property is required for the component to proceed or a
     * property may require a specific value syntax or even numeric bounds which
     * may not be exceeded. These features and few more are supported by the
     * Property combined with the PropertyDefinition.
     *
     * \section properties-outline Outline
     *
     * @code
     *  template<typename PropertyType> class Property
     *  {
     *      bool            iseSet();
     *      bool            isRequired();
     *      PropertyType    getValue();
     *  }
     * @endcode
     */
    template <typename PropertyType>
    class Property
    {
    public:
        /**
         * Property value map type definition
         */
        typedef typename PropertyDefinition<PropertyType>::PropertyValuesMap
        PropertyValuesMap;

        /**
         * Property constructor
         *
         * @param definition    Property specific definition
         * @param prefix        Property prefix such as domain or component name
         * @param properties    Ice properties set which contains all property
         *                      strings.
         *
         */
        Property(PropertyDefinition<PropertyType> definition,
                 std::string prefix,
                 Ice::PropertiesPtr properties);

        /**
         * Checks whether the property is set
         *
         * @return true if property is set, otheriwse false
         */
        bool isSet() const;

        /**
         * Checks if this property is required
         *
         * @return true if set required, otherwise false
         */
        bool isRequired() const;

        /**
         * Checks if this property is constant or if it can be changed at runtime.
         *
         * @return true if property is constant, otherwise false
         */
        bool isConstant() const;

        /**
         * Returns the property value set in a config file or passed as a
         * command-line option. If property is not set, the default value is
         * returned unless the property is required. In the latter case an
         * exception is thrown indicating that the required property is not set.
         *
         * @return Property value of the type <b>PropertyType</b>
         *
         * @throw armarx::exceptions::local::ValueRangeExceededException
         * @throw armarx::exceptions::local::InvalidPropertyValueException
         */
        PropertyType getValue();

        /**
         * @brief Convenience overload of getValue().
         * Usage:
         * \code
         * std::string val = getProperty<std::string>("myProp");
         * \endcode
         */
        operator PropertyType()
        {
            return getValue();
        }

        template<class T = PropertyType>
        std::enable_if_t<std::is_same_v<T, std::string>, T>
        getValueAndReplaceAllVars()
        {
            return FileSystemPathBuilder_ApplyFormattingAndResolveEnvAndCMakeVars(getValue());
        }

        template<class T = PropertyType>
        std::enable_if_t<std::is_same_v<T, std::vector<std::string>>, T>
                getValueAndReplaceAllVars()
        {
            auto vals = getValue();
            for (auto& val : vals)
            {
                val = FileSystemPathBuilder_ApplyFormattingAndResolveEnvAndCMakeVars(val);
            }
            return vals;
        }

    private:
        /* = Implementation details ========================================= */

        template <int> struct Qualifier
        {
            Qualifier(int) {}
        };

    private:

        /// Property definition.
        PropertyDefinition<PropertyType> definition;

        /// Property prefix such as domain name.
        std::string prefix;

        /// Ice property container.
        Ice::PropertiesPtr properties;
    };


    /* ====================================================================== */
    /* === Property Implementation ========================================== */
    /* ====================================================================== */

    template <typename PropertyType>
    Property<PropertyType>::Property(
        PropertyDefinition<PropertyType> definition,
        std::string prefix,
        Ice::PropertiesPtr properties) :
        definition(definition),
        prefix(prefix),
        properties(properties)
    {
    }


    template <typename PropertyType>
    PropertyType
    Property<PropertyType>::getValue()
    {
        return definition.getValue(prefix, properties);
    }


    template <typename PropertyType>
    bool
    Property<PropertyType>::isRequired() const
    {
        return definition.isRequired();
    }


    template <typename PropertyType>
    bool
    Property<PropertyType>::isConstant() const
    {
        return definition.isConstant();
    }


    template <typename PropertyType>
    bool
    Property<PropertyType>::isSet() const
    {
        return definition.isSet(prefix, properties);
    }

    extern template class Property <int>;
    extern template class Property <float>;
    extern template class Property <double>;
    extern template class Property <std::string>;
    extern template class Property <bool>;

}



/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <ArmarXCore/core/application/properties/PropertyDefinitionFormatter.h>
#include <ArmarXCore/core/IceManager.h>

#include <Ice/Handle.h>

namespace Ice
{

    class Properties;
    ICE_API ::Ice::LocalObject* upCast(::Ice::Properties*);
    typedef ::IceInternal::Handle< ::Ice::Properties> PropertiesPtr;

}

namespace armarx
{
    /**
     * @ingroup properties
     *
     * @class PropertyDefinitionBase
     * @brief Common interface of any property definition
     *
     * This abstract class is part of the internal implementation
     */
    class PropertyDefinitionBase
    {
    public:
        enum PropertyConstness
        {
            eConstant,
            eModifiable
        };

        PropertyDefinitionBase(bool required = true, PropertyConstness constness = eConstant) :
            required(required),
            constness(constness)
        {}

        virtual ~PropertyDefinitionBase() {}

        /**
         * Converts the property definition into a string using a specified
         * formatter.
         *
         * @param formatter Custom definition formatter
         */
        virtual std::string toString(PropertyDefinitionFormatter& formatter, const std::string& value) = 0;

        bool isRequired() const
        {
            return required;
        }


        bool isConstant() const
        {
            return constness == eConstant;
        }

        virtual std::string getDefaultAsString() = 0;

        virtual
        void
        writeValueToSetter(
            const std::string& prefix,
            Ice::PropertiesPtr) = 0;

        static std::string icePropertyGet(Ice::PropertiesPtr const& iceProperties, std::string const& key);

        bool isSet(std::string const& prefix, std::string const& propertyName, Ice::PropertiesPtr const& iceProperties) const;

    protected:
        /**
         * If set to true, the property must be explicitely set through configuration parameters.
         */
        bool required;

        PropertyConstness constness;

    private:
        friend class PropertyDefinitionContainer;

        /**
         * Sets the typeid name of the property value type
         *
         * @param typeIdName    Value type name
         */
        void setTypeIdName(std::string typeIdName)
        {
            this->typeIdName = typeIdName;
        }

        /**
         * Returns the value type name
         */
        std::string getTypeIdName() const
        {
            return typeIdName;
        }

        /**
         * Value type name
         */
        std::string typeIdName;
    };

}


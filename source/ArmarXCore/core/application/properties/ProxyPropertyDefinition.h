/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <string>

// ArmarXCore
#include <ArmarXCore/core/IceManager.h>
#include <ArmarXCore/util/check.h>


namespace armarx
{


    enum class ProxyType
    {
        component,
        topic
    };


    class ProxyPropertyDefinitionBase
    {

    protected:

        const ProxyType m_proxy_type;

    public:

        ProxyPropertyDefinitionBase(ProxyType proxy_type) :
            m_proxy_type{proxy_type}
        {
            // pass
        }

        virtual
        ~ProxyPropertyDefinitionBase()
        {
            // pass
        }

        virtual
        void
        write_proxy_to_setter(
            IceManagerPtr ice_manager,
            const std::string& proxy_name) = 0;

        ProxyType
        getProxyType()
        {
            return m_proxy_type;
        }

    };


    template <typename PropertyType>
    class ProxyPropertyDefinition : public ProxyPropertyDefinitionBase
    {

    private:

        PropertyType* m_setter_ref;

    public:

        ProxyPropertyDefinition(PropertyType* setter_ref, ProxyType proxy_type) :
            ProxyPropertyDefinitionBase{proxy_type},
            m_setter_ref{setter_ref}
        {
            ARMARX_CHECK(m_setter_ref != nullptr) << "`m_setter_ref` must not be a nullptr";
        }

        virtual
        ~ProxyPropertyDefinition()
        {
            // pass
        }

        virtual
        void
        write_proxy_to_setter(
            IceManagerPtr ice_manager,
            const std::string& proxy_name) override
        {
            switch (m_proxy_type)
            {
                case ProxyType::component:
                    *m_setter_ref = ice_manager->getProxy<PropertyType>(proxy_name);
                    break;
                case ProxyType::topic:
                    *m_setter_ref = ice_manager->getTopic<PropertyType>(proxy_name);
                    break;
            }
        }

    };


}

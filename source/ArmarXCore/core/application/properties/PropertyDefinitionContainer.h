/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <ArmarXCore/core/application/properties/PropertyDefinitionInterface.h>
#include <ArmarXCore/core/application/properties/PropertyDefinition.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionFormatter.h>
#include <ArmarXCore/core/application/properties/ProxyPropertyDefinition.h>
#include <ArmarXCore/core/exceptions/LocalException.h>
#include <ArmarXCore/core/IceManager.h>

#include <Ice/Proxy.h>
#include <Ice/ProxyHandle.h>

#include <typeinfo>
#include <vector>
#include <string>
#include <map>

namespace armarx
{
    class PropertyDefinitionBase;

    /**
     * @ingroup properties
     *
     * @class PropertyDefinitionContainer
     * @brief PropertyDefinitionContainer
     */
    class PropertyDefinitionContainer : public virtual IceUtil::Shared
    {
    public:

        typedef std::map<std::string, PropertyDefinitionBase*>
        DefinitionContainer;

        PropertyDefinitionContainer(const std::string& prefix);

        ~PropertyDefinitionContainer() override;

        Ice::PropertiesPtr getProperties();

        void setProperties(Ice::PropertiesPtr properties);

        void writeValues();
        void writeProxyValues(IceManagerPtr);
        std::map<std::string, ProxyPropertyDefinitionBase*> getProxyDefinitions();
        std::vector<std::string> getSubscribedTopicDefinitions();

        template <typename PropertyType>
        decltype(auto) required(
            PropertyType& setter,
            const std::string& name,
            const std::string& description = "",
            PropertyDefinitionBase::PropertyConstness constness = PropertyDefinitionBase::eConstant)
        {
            return requiredOrOptional(true, setter, name, description, constness);
        }

        template <typename PropertyType>
        decltype(auto) optional(
            PropertyType& setter,
            const std::string& name,
            const std::string& description = "",
            PropertyDefinitionBase::PropertyConstness constness = PropertyDefinitionBase::eConstant)
        {
            return requiredOrOptional(false, setter, name, description, constness);
        }

        template <typename PropertyType>
        decltype(auto) requiredOrOptional(
            bool isRequired,
            PropertyType& setter,
            const std::string& name,
            const std::string& description = "",
            PropertyDefinitionBase::PropertyConstness constness = PropertyDefinitionBase::eConstant);

    public:
        template <typename PropertyType>
        void
        component(
            IceInternal::ProxyHandle<PropertyType>& setter,
            const std::string& default_name = "",
            const std::string& property_name = "",
            const std::string& description = "");

        /**
         * Define a property to set the name of a topic which is subscribed to.
         *
         * This defines property to set the name of a topic of the given type from setter.  Before
         * `Component::onInitComponent()`, the component will subscribe to this topic, and before
         * `Component::onConnectComponent()`, the topic proxy will be written into `setter`.
         */
        template <typename PropertyType>
        void
        topic(
            IceInternal::ProxyHandle<PropertyType>& setter,
            const std::string& default_name = "",
            const std::string& property_name = "",
            const std::string& description = "");

        /**
         * Define a topic which is offered from the component via the template parameter.
         *
         * Sane defaults will be chosen for the property name and the default values depending on
         * the template parameter, but these values can all be overridden with the parameters.
         */
        template <typename PropertyType>
        void
        topic(
            std::string default_name = "",
            std::string property_name = "",
            std::string description = "");

        template <typename PropertyType>
        PropertyDefinition<PropertyType>& defineRequiredProperty(
            const std::string& name,
            const std::string& description = "",
            PropertyDefinitionBase::PropertyConstness constness = PropertyDefinitionBase::eConstant);

        template <typename PropertyType>
        PropertyDefinition<PropertyType>& defineOptionalProperty(
            const std::string& name,
            PropertyType defaultValue,
            const std::string& description = "",
            PropertyDefinitionBase::PropertyConstness constness = PropertyDefinitionBase::eConstant);

        /**
         * @brief Define a required property for an Eigen vector type.
         * The EigenVectorType can be any Eigen::Vector type, (Vector3f, VectorXd, ...).
         *
         * @param delimiter the delimiter between vector coefficients in the string representation
         *
         * Usage example:
         * @code
         * // Definition
         *  // accepts e.g. "1.0 2.0 3.0" and "1.0 -.3 .5"
         *  defineRequiredPropertyVector<Eigen::Vector3f>("Position", "The position.");
         *  // accepts e.g. "640x480" and "1920x1080"
         *  defineRequiredPropertyVector<Eigen::Vector2i>("Resolution", "The resolution.", 'x');
         *  // accepts e.g. "0,1,2,3,4,5,6,7,8,9" and "1"
         *  defineRequiredPropertyVector<Eigen::VectorXd>("Values", "The values.", ',');
         *
         * // Usage:
         *  Eigen::Vector3f position = getProperty<Eigen::Vector3f>("Position");
         *  Eigen::Vector2i resolution = getProperty<Eigen::Vector2i>("Resolution");
         *  Eigen::VectorXd values = getProperty<Eigen::VectorXd>("Values");
         *  // .getValue() can trigger Eigen's implicit conversion
         *  Eigen::VectorXf positionX = getProperty<Eigen::VectorXf>("Position").getValue();
         *
         * // Specification on command line:
         *  ./<binary> --ArmarX.<ComponentName>.Position="1 2 3"
         * @endcode
         */
        template <typename EigenVectorType>
        PropertyDefinition<EigenVectorType>& defineRequiredPropertyVector(
            const std::string& name,
            const std::string& description = "",
            const std::string& delimiter = " ",
            PropertyDefinitionBase::PropertyConstness constness = PropertyDefinitionBase::eConstant);

        // Legacy overloads taking char as delimiter.
        template <typename EigenVectorType>
        PropertyDefinition<EigenVectorType>& defineRequiredPropertyVector(
            const std::string& name, const std::string& description, char delimiter,
            PropertyDefinitionBase::PropertyConstness constness)
        {
            return defineRequiredPropertyVector<EigenVectorType>(name, description, std::string(1, delimiter), constness);
        }
        template <typename EigenVectorType>
        PropertyDefinition<EigenVectorType>& defineRequiredPropertyVector(
            const std::string& name, const std::string& description, char delimiter)
        {
            return defineRequiredPropertyVector<EigenVectorType>(name, description, delimiter, PropertyDefinitionBase::eConstant);
        }

        /**
         * @brief Define a required property for an Eigen vector type.
         * The EigenVectorType can be any Eigen::Vector type, (Vector3f, VectorXd, ...).
         * @param delimiter the delimiter between vector coefficients in the string representation
         *
         * Usage example:
         * @code
         * // Definition
         *  // defaults to "0 0 0", accepts e.g. "1.0 2.0 3.0" and "1.0 -.3 .5"
         *  defineOptionalPropertyVector<Eigen::Vector3f>(
         *      "Position", Eigen::Vector3f::Zero(), "The position.");
         *  // defaults to "640x480", accepts e.g. "1920x1080"
         *  defineOptionalPropertyVector<Eigen::Vector2i>(
         *      "Resolution", Eigen::Vector2f(640, 480), "The resolution.", 'x');
         *  // defaults to "0,1,2,3,4,5,6,7,8,9", accepts e.g. "1" or "1,2,3"
         *  Eigen::VectorXd valuesDefault(10);
         *  valuesDefault << 0, 1, 2, 3, 4, 5, 6, 7, 8, 9;
         *  defineOptionalPropertyVector<Eigen::VectorXd>(
         *      "Values", valuesDefault, "The values.", ',');
         *
         * // Usage:
         *  Eigen::Vector3f position = getProperty<Eigen::Vector3f>("Position");
         *  Eigen::Vector2i resolution = getProperty<Eigen::Vector2i>("Resolution");
         *  Eigen::VectorXd values = getProperty<Eigen::VectorXd>("Values");
         *  // .getValue() can trigger Eigen's implicit conversion
         *  Eigen::VectorXf positionX = getProperty<Eigen::VectorXf>("Position").getValue();
         *
         * // Specification on command line:
         *  ./<binary> --ArmarX.<ComponentName>.Position="1 2 3"
         * @endcode
         */
        template <typename EigenVectorType>
        PropertyDefinition<EigenVectorType>& defineOptionalPropertyVector(
            const std::string& name,
            EigenVectorType defaultValue,
            const std::string& description = "",
            const std::string& delimiter = " ",
            PropertyDefinitionBase::PropertyConstness constness = PropertyDefinitionBase::eConstant);

        // Legacy overloads taking char as delimiter.
        template <typename EigenVectorType>
        PropertyDefinition<EigenVectorType>& defineOptionalPropertyVector(
            const std::string& name, EigenVectorType defaultValue, const std::string& description, char delimiter,
            PropertyDefinitionBase::PropertyConstness constness)
        {
            return defineOptionalPropertyVector<EigenVectorType>(name, defaultValue, description, std::string(1, delimiter), constness);
        }
        template <typename EigenVectorType>
        PropertyDefinition<EigenVectorType>& defineOptionalPropertyVector(
            const std::string& name, EigenVectorType defaultValue, const std::string& description, char delimiter)
        {
            return defineOptionalPropertyVector<EigenVectorType>(name, defaultValue, description, delimiter, PropertyDefinitionBase::eConstant);
        }


        PropertyDefinitionBase* getDefinitionBase(const std::string& name);

        template <typename PropertyType>
        PropertyDefinition<PropertyType>& getDefintion(const std::string& name);
        bool hasDefinition(const std::string& name)
        {
            return (definitions.find(name) != definitions.end());
        }

        std::string toString(PropertyDefinitionFormatter& formatter);

        /**
         * Returns the detailed description of the property user
         *
         * @return detailed description text
         */
        std::string getDescription() const;

        /**
         * Sets the detailed description of the property user
         *
         * @param description detailed description text
         */
        void setDescription(const std::string& description);

        void setPrefix(std::string prefix);

        std::string getPrefix();

        bool isPropertySet(const std::string& name);

        std::string getValue(const std::string& name);

        std::map<std::string, std::string> getPropertyValues(const std::string& prefix = "") const;

        /**
         * @brief Parses `string` to an Eigen vector of type `EigenVectorType`.
         * @param delim The delimiter between coefficients.
         * @throw std::bad_cast If parsing `string` fails.
         */
        template <typename EigenVectorType>
        static EigenVectorType eigenVectorFactoryFunction(std::string string, std::string delim);
        template <typename EigenVectorType>
        static std::string eigenVectorToString(const EigenVectorType& vector, std::string delim);

        static std::string eigenVectorPropertyDescription(
            const std::string& description, long size, std::string delim);

    private:

        template <typename PropertyType>
        std::string
        ice_class_name();

        template <typename PropertyType>
        void
        proxy(
            IceInternal::ProxyHandle<PropertyType>& setter,
            const ProxyType proxy_type,
            std::string default_name,
            std::string property_name,
            std::string description);


    protected:
        /**
         * Property definitions container
         */
        DefinitionContainer definitions;

        std::map<std::string, ProxyPropertyDefinitionBase*> proxies;

        std::vector<std::string> subscribedTopics;

        /**
         * Prefix of the properties such as namespace, domain, component name,
         * etc.
         */
        std::string prefix;

        /**
         * Property User description
         */
        std::string description;

        /**
         * PropertyUser brief description
         */
        std::string briefDescription;

        Ice::PropertiesPtr properties;
    };


    /**
     * PropertyDefinitions smart pointer type
     */
    using PropertyDefinitionsPtr = IceUtil::Handle<PropertyDefinitionContainer>;

    /* ====================================================================== */
    /* === Property Definition Container Implementation ===================== */
    /* ====================================================================== */

    std::string PropertyDefinitionContainer_ice_class_name(std::string const& full_type_name);


    template <typename PropertyType>
    std::string
    PropertyDefinitionContainer::ice_class_name()
    {
        const std::string full_type_name = PropertyType::ice_staticId();
        return PropertyDefinitionContainer_ice_class_name(full_type_name);
    }

    template <typename PropertyType>
    inline decltype(auto)
    PropertyDefinitionContainer::requiredOrOptional(
        bool isRequired,
        PropertyType& setter,
        const std::string& name,
        const std::string& description,
        PropertyDefinitionBase::PropertyConstness constness)
    {
        using plugin = meta::properties::DefinePropertyPlugin<PropertyType>;
        if constexpr(plugin::value)
        {
            plugin::Define(*this, isRequired, setter, name, description, constness);
        }
        else
        {
            PropertyDefinition<PropertyType>* def;
            if (isRequired)
            {
                def = new PropertyDefinition<PropertyType>(&setter, name, description, constness);
            }
            else
            {
                def = new PropertyDefinition<PropertyType>(&setter, name, setter, description, constness);
            }

            def->setTypeIdName(typeid(PropertyType).name());
            definitions[name] = static_cast<PropertyDefinitionBase*>(def);
            return *def;
        }
    }


    template <typename PropertyType>
    void
    PropertyDefinitionContainer::component(
        IceInternal::ProxyHandle<PropertyType>& setter,
        const std::string& default_name,
        const std::string& property_name,
        const std::string& description)
    {
        proxy(setter, ProxyType::component, default_name, property_name, description);
    }


    template <typename PropertyType>
    void
    PropertyDefinitionContainer::topic(
        IceInternal::ProxyHandle<PropertyType>& setter,
        const std::string& default_name,
        const std::string& property_name,
        const std::string& description)
    {
        proxy(setter, ProxyType::topic, default_name, property_name, description);
    }


    template <typename PropertyType>
    void
    PropertyDefinitionContainer::topic(
        std::string default_name,
        std::string property_name,
        std::string description)
    {
        const std::string class_name = ice_class_name<PropertyType>();

        if (default_name.empty())
        {
            default_name = class_name;
        }
        if (property_name.empty())
        {
            property_name = "tpc.sub." + class_name;
        }
        if (description.empty())
        {
            description = "Name of the `" + class_name + "` topic to subscribe to.";
        }

        // Append property definition for the proxy name.
        PropertyDefinition<std::string>* def_name =
            new PropertyDefinition<std::string>(nullptr, property_name, default_name, description,
                                                PropertyDefinitionBase::eConstant);
        def_name->setTypeIdName(typeid(std::string).name());
        definitions[property_name] = static_cast<PropertyDefinitionBase*>(def_name);
        subscribedTopics.push_back(property_name);
    }


    template <typename PropertyType>
    void
    PropertyDefinitionContainer::proxy(
        IceInternal::ProxyHandle<PropertyType>& setter,
        const ProxyType proxy_type,
        std::string default_name,
        std::string property_name,
        std::string description)
    {
        using PropertyTypePrx = IceInternal::ProxyHandle<PropertyType>;

        const std::string class_name = ice_class_name<PropertyType>();

        if (default_name.empty())
        {
            default_name = class_name;
        }
        if (property_name.empty())
        {
            switch (proxy_type)
            {
                case ProxyType::component:
                    property_name = "cmp." + class_name;
                    break;
                case ProxyType::topic:
                    property_name = "tpc.pub." + class_name;
                    break;
            }
        }
        if (description.empty())
        {
            switch (proxy_type)
            {
                case ProxyType::component:
                    description = "Ice object name of the `" + class_name + "` component.";
                    break;
                case ProxyType::topic:
                    description = "Name of the `" + class_name + "` topic to publish data to.";
                    break;
            }
        }

        // Append property definition for the proxy name.
        PropertyDefinition<std::string>* def_name =
            new PropertyDefinition<std::string>(nullptr, property_name, default_name, description,
                                                PropertyDefinitionBase::eConstant);
        def_name->setTypeIdName(typeid(std::string).name());
        definitions[property_name] = static_cast<PropertyDefinitionBase*>(def_name);

        // Append proxy definition.
        ProxyPropertyDefinition<PropertyTypePrx>* def =
            new ProxyPropertyDefinition<PropertyTypePrx>(&setter, proxy_type);
        proxies[property_name] = static_cast<ProxyPropertyDefinitionBase*>(def);
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinitionContainer::defineRequiredProperty(const std::string& name,
            const std::string& description,
            PropertyDefinitionBase::PropertyConstness constness)
    {
        PropertyDefinition<PropertyType>* def =
            new PropertyDefinition<PropertyType>(nullptr, name, description, constness);

        def->setTypeIdName(typeid(PropertyType).name());

        definitions[name] = static_cast<PropertyDefinitionBase*>(def);

        return *def;
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinitionContainer::defineOptionalProperty(
        const std::string& name,
        PropertyType defaultValue,
        const std::string& description,
        PropertyDefinitionBase::PropertyConstness constness)
    {
        PropertyDefinition<PropertyType>* def =
            new PropertyDefinition<PropertyType>(nullptr, name,
                    defaultValue,
                    description,
                    constness);

        def->setTypeIdName(typeid(PropertyType).name());

        definitions[name] = static_cast<PropertyDefinitionBase*>(def);

        return *def;
    }


    template <typename EigenVectorType>
    PropertyDefinition<EigenVectorType>&
    PropertyDefinitionContainer::defineRequiredPropertyVector(
        const std::string& name,
        const std::string& description,
        const std::string& delimiter,
        PropertyDefinitionBase::PropertyConstness constness)
    {
        std::string appendedDescription =
            eigenVectorPropertyDescription(description, EigenVectorType::SizeAtCompileTime,
                                           delimiter);

        PropertyDefinition<EigenVectorType>& def = defineRequiredProperty<EigenVectorType>(
                    name, appendedDescription, constness);

        // set a factory
        def.setFactory([delimiter](std::string string) -> EigenVectorType
        {
            return eigenVectorFactoryFunction<EigenVectorType>(string, delimiter);
        });
        def.setCaseInsensitive(true);

        return def;
    }


    template<typename EigenVectorType>
    PropertyDefinition<EigenVectorType>&
    PropertyDefinitionContainer::defineOptionalPropertyVector(
        const std::string& name,
        EigenVectorType defaultValue,
        const std::string& description,
        const std::string& delimiter,
        PropertyDefinitionBase::PropertyConstness constness)
    {
        std::string appendedDescription =
            eigenVectorPropertyDescription(description, EigenVectorType::SizeAtCompileTime,
                                           delimiter);

        PropertyDefinition<EigenVectorType>& def = defineOptionalProperty<EigenVectorType>(
                    name, defaultValue, appendedDescription, constness);

        // set a factory
        def.setFactory([delimiter](std::string string) -> EigenVectorType
        {
            return eigenVectorFactoryFunction<EigenVectorType>(string, delimiter);
        });
        // map the default value
        def.map(eigenVectorToString<EigenVectorType>(defaultValue, delimiter), defaultValue);
        def.setCaseInsensitive(true);

        return def;
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinitionContainer::getDefintion(const std::string& name)
    {
        // check definition existance
        if (definitions.find(name) == definitions.end())
        {
            std::stringstream ss;
            ss << name + " property is not defined. Defined are ";
            for (typename DefinitionContainer::iterator it = definitions.begin(); it != definitions.end(); ++it)
            {
                ss << "\n" << it->first;
            }
            throw armarx::LocalException(ss.str());
        }

        PropertyDefinitionBase* def = definitions[name];

        // check definition type
        if (def->getTypeIdName().compare(typeid(PropertyType).name()) != 0)
        {
            throw armarx::LocalException(
                std::string("Calling getProperty<T>() for the property '")
                + name
                + "' with wrong property type [note: "
                + typeid(PropertyType).name()
                + " instead of "
                + def->getTypeIdName()
                + "]");
        }

        // retrieve and down cast the requested definition
        return *dynamic_cast< PropertyDefinition<PropertyType>* >(def);
    }


    template<typename EigenVectorType>
    EigenVectorType PropertyDefinitionContainer::eigenVectorFactoryFunction(
        std::string string, std::string delim)
    {
        using Scalar = typename EigenVectorType::Scalar;

        long size = EigenVectorType::SizeAtCompileTime;
        bool isFixedSize = size >= 0; // Eigen::Dynamic is -1.

        const std::vector<std::string> stringScalars = split(string, delim, true);

        std::vector<Scalar> scalars;
        if (isFixedSize)
        {
            if (stringScalars.size() != static_cast<std::size_t>(size))
            {
                throw std::bad_cast();
            }
            scalars.reserve(size);
        }
        for (const auto& scalarStr : stringScalars)
        {
            // May throw boost::bad_lexical_cast (derives from std::bad_cast).
            Scalar value = PropertyDefinition_lexicalCastTo<Scalar>(scalarStr);
            scalars.push_back(value);
        }

        EigenVectorType vector;
        if (!isFixedSize)
        {
            vector.resize(scalars.size());
        }
        // Write values.
        for (std::size_t i = 0; i < scalars.size(); ++i)
        {
            vector(i) = scalars[i];
        }
        return vector;
    }


    template<typename EigenVectorType>
    std::string PropertyDefinitionContainer::eigenVectorToString(
        const EigenVectorType& vector, std::string delim)
    {
        std::stringstream ss;
        long size = vector.size();

        ss << vector(0);
        for (int i = 1; i < size; ++i)
        {
            ss << delim << vector(i);
        }

        return ss.str();
    }


}


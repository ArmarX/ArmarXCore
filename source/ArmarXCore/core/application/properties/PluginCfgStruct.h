/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/util/CPPUtility/ConfigIntrospection.h>
#include <ArmarXCore/util/CPPUtility/trace.h>
#include <ArmarXCore/core/application/properties/PropertyUser.h>

namespace armarx::meta::cfg
{
    template<class T>
    struct properties_definition_enabled
        : std::bool_constant<meta::has_hana_accessor_v<T>> {};

    template<class T>
    static constexpr bool properties_definition_enabled_v =
        properties_definition_enabled<T>::value;
}
//GetPropertyPlugin
namespace armarx::meta::properties
{
    template<class T>
    struct GetPropertyPlugin <
        T,
        std::enable_if_t<meta::cfg::properties_definition_enabled_v<T>>
                > : std::true_type
    {
        static void Get(armarx::PropertyUser& pu, T& val, const std::string& name)
        {
            ARMARX_TRACE;
            namespace hana = boost::hana;
            static constexpr auto accessors = hana::accessors<T>();
            hana::for_each(accessors, [&](auto & e)
            {
                ARMARX_TRACE_LITE;
                using elem_det = decltype(armarx::meta::cfg::to_element_detail(e));
                pu.getProperty(
                    hana::second(e)(val),
                    elem_det::make_property_name(name)
                );
            });
        }
    };
}
//DefinePropertyPlugin
namespace armarx::meta::properties
{
    template<class T>
    struct DefinePropertyPlugin <
        T,
        std::enable_if_t<meta::cfg::properties_definition_enabled_v<T>>
                > : std::true_type
    {
        static void Define(
            armarx::PropertyDefinitionContainer& pc,
            bool required,
            T& setter,
            const std::string& name,
            const std::string& description,
            PropertyDefinitionBase::PropertyConstness constness)
        {
            ARMARX_TRACE;
            namespace hana = boost::hana;
            static constexpr auto accessors = hana::accessors<T>();
            hana::for_each(accessors, [&](auto & e)
            {
                ARMARX_TRACE_LITE;
                using elem_det = decltype(armarx::meta::cfg::to_element_detail(e));
                if constexpr(elem_det::is_no_prop)
                {
                    return;
                }
                else
                {
                    using sub_result_t = std::remove_reference_t<decltype(pc.required(hana::second(e)(setter), "", "",
                                         constness))>;
                    const auto varname = hana::to<char const*>(hana::first(e));
#define call_params                                 \
    required,                                       \
    hana::second(e)(setter),                        \
    elem_det::make_property_name(name),             \
    description + " (parameter " + varname + ") " + \
    elem_det::description_as_string(),              \
    constness

                    static constexpr bool min_set = simox::meta::is_not_undefined_t(elem_det::min);
                    static constexpr bool max_set = simox::meta::is_not_undefined_t(elem_det::max);
                    if constexpr(std::is_same_v<sub_result_t, void> || (!min_set && !max_set))
                    {
                        pc.requiredOrOptional(call_params);
                    }
                    else
                    {
                        auto& subpdef = pc.requiredOrOptional(call_params);
                        if constexpr(min_set)
                        {
                            subpdef.setMin(elem_det::min);
                        }
                        if constexpr(max_set)
                        {
                            subpdef.setMax(elem_det::max);
                        }
                    }
#undef call_params
                }
            });
        }
    };
}


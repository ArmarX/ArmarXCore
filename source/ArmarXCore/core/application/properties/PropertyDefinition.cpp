/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/


#include "PropertyDefinition.hpp"

#include <Ice/Properties.h>

#include <SimoxUtility/algorithm/string/string_tools.h>

#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>

#include <regex>

namespace armarx
{
    template class PropertyDefinition<int>;
    template class PropertyDefinition<float>;
    template class PropertyDefinition<double>;
    template class PropertyDefinition<std::string>;
    template class PropertyDefinition<bool>;

    std::string PropertyDefinition_toLowerCopy(const std::string& input)
    {
        return simox::alg::to_lower(input);
    }

    bool PropertyDefinition_matchRegex(std::string const& regex, std::string const& value)
    {
        boost::regex expr(regex);
        return boost::regex_match(value, expr);
    }

    std::map<std::string, std::string> PropertyDefinition_parseMapStringString(const std::string& input)
    {
        std::map<std::string, std::string> result;
        auto splits = armarx::Split(input, ",");
        for (auto& elem : splits)
        {
            simox::alg::trim(elem);
            auto split = armarx::Split(elem, ":");
            if (split.size() == 2)
            {
                result[simox::alg::trim_copy(split[0])] = simox::alg::trim_copy(split[1]);
            }
        }
        return result;
    }

    std::vector<std::string> PropertyDefinition_parseVectorString(std::string const& input)
    {
        std::vector<std::string> args;

        const boost::regex re(",(?=(?:[^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)");

        boost::match_results<std::string::const_iterator> what;
        boost::match_flag_type flags = boost::match_default;
        std::string::const_iterator s = input.begin();
        std::string::const_iterator e = input.end();
        size_t begin = 0;
        while (boost::regex_search(s, e, what, re, flags))
        {
            int pos = what.position();
            auto arg = input.substr(begin, pos);
            simox::alg::trim(arg);
            if (!arg.empty())
            {
                args.push_back(arg);
            }
            std::string::difference_type l = what.length();
            std::string::difference_type p = what.position();
            begin += l + p;
            s += p + l;
        }
        std::string lastArg = input.substr(begin);
        simox::alg::trim(lastArg);
        if (!lastArg.empty())
        {
            args.push_back(lastArg);
        }
        return args;
    }

    IceUtil::Time PropertyDefinition_parseIceUtilTime(std::string const& input)
    {
        std::regex re_number{"^[0-9]*\\.?[0-9]+"};
        std::regex re_unit{"[a-zµ]+$"};
        std::smatch number_match;
        std::smatch unit_match;

        if (not std::regex_search(input.begin(), input.end(), number_match, re_number))
        {
            using armarx::exceptions::local::InvalidPropertyValueException;
            throw InvalidPropertyValueException("unknown", input)
                    << "Could not parse Time (number invalid).";
        }

        if (not std::regex_search(input.begin(), input.end(), unit_match, re_unit))
        {
            using armarx::exceptions::local::InvalidPropertyValueException;
            throw InvalidPropertyValueException("unknown", input)
                    << "Could not parse Time (unit invalid).";
        }

        const double value = boost::lexical_cast<double>(number_match[0]);

        static constexpr long h2us = 1000l * 1000 * 60 * 60;
        static constexpr long m2us = 1000 * 1000 * 60;
        static constexpr long s2us = 1000 * 1000;
        static constexpr long ms2us = 1000;
        static constexpr long us = 1;

        static std::map<std::string, long> conv_map;
        // Hours.
        conv_map["h"] = h2us;
        conv_map["hrs"] = h2us;
        conv_map["hour"] = h2us;
        conv_map["hours"] = h2us;
        // Minutes.
        conv_map["m"] = m2us;
        conv_map["min"] = m2us;
        conv_map["minute"] = m2us;
        conv_map["minutes"] = m2us;
        // Seconds.
        conv_map["s"] = s2us;
        conv_map["sec"] = s2us;
        conv_map["second"] = s2us;
        conv_map["seconds"] = s2us;
        // Milliseconds.
        conv_map["ms"] = ms2us;
        conv_map["msec"] = ms2us;
        conv_map["millisecond"] = ms2us;
        conv_map["milliseconds"] = ms2us;
        // Microseconds.
        conv_map["µs"] = us;
        conv_map["us"] = us;
        conv_map["µsec"] = us;
        conv_map["usec"] = us;
        conv_map["microsecond"] = us;
        conv_map["microseconds"] = us;

        if (conv_map.find(unit_match[0]) == conv_map.end())
        {
            using armarx::exceptions::local::InvalidPropertyValueException;
            throw InvalidPropertyValueException("unknown", input)
                    << "Invalid/unknown unit `" << unit_match[0] << "`.";
        }

        const long int to_microseconds_mult = conv_map[unit_match[0]];
        return IceUtil::Time::microSeconds(value * to_microseconds_mult);
    }

    std::string PropertyDefinition_lexicalCastToString(float input)
    {
        std::locale::global(std::locale::classic());
        return boost::lexical_cast<std::string>(input);
    }

    std::string PropertyDefinition_lexicalCastToString(double input)
    {
        std::locale::global(std::locale::classic());
        return boost::lexical_cast<std::string>(input);
    }

    std::string PropertyDefinition_lexicalCastToString(long input)
    {
        return boost::lexical_cast<std::string>(input);
    }

    std::string PropertyDefinition_lexicalCastToString(int input)
    {
        return boost::lexical_cast<std::string>(input);
    }

    std::string PropertyDefinition_lexicalCastToString(unsigned long input)
    {
        return boost::lexical_cast<std::string>(input);
    }

    std::string PropertyDefinition_lexicalCastToString(unsigned int input)
    {
        return boost::lexical_cast<std::string>(input);
    }

    std::string PropertyDefinition_lexicalCastToString(std::string const& input)
    {
        return input;
    }

    template <>
    double PropertyDefinition_lexicalCastTo<double>(std::string const& input)
    {
        std::locale::global(std::locale::classic());
        return boost::lexical_cast<double>(input);
    }

    template <>
    float PropertyDefinition_lexicalCastTo<float>(std::string const& input)
    {
        std::locale::global(std::locale::classic());
        return boost::lexical_cast<float>(input);
    }

    template <>
    long PropertyDefinition_lexicalCastTo<long>(std::string const& input)
    {
        return boost::lexical_cast<long>(input);
    }

    template <>
    int PropertyDefinition_lexicalCastTo<int>(std::string const& input)
    {
        return boost::lexical_cast<int>(input);
    }

    template <>
    unsigned long PropertyDefinition_lexicalCastTo<unsigned long>(std::string const& input)
    {
        return boost::lexical_cast<unsigned long>(input);
    }

    template <>
    unsigned int PropertyDefinition_lexicalCastTo<unsigned int>(std::string const& input)
    {
        return boost::lexical_cast<unsigned int>(input);
    }

    template <>
    char PropertyDefinition_lexicalCastTo<char>(std::string const& input)
    {
        return boost::lexical_cast<char>(input);
    }

    template <>
    unsigned char PropertyDefinition_lexicalCastTo<unsigned char>(std::string const& input)
    {
        return boost::lexical_cast<unsigned char>(input);
    }

    template <>
    bool PropertyDefinition_lexicalCastTo<bool>(std::string const& input)
    {
        return boost::lexical_cast<bool>(input);
    }

    std::string PropertyDefinitionBase::icePropertyGet(const Ice::PropertiesPtr& iceProperties, const std::string& key)
    {
        return iceProperties->getProperty(key);
    }

    bool PropertyDefinitionBase::isSet(std::string const& prefix, std::string const& propertyName, Ice::PropertiesPtr const& iceProperties) const
    {
        if (!iceProperties)
        {
            return false;
        }

        return iceProperties->getPropertyWithDefault(prefix + propertyName, "::NOT-SET::")
               .compare("::NOT-SET::") != 0;
    }

}

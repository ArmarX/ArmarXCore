/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <ArmarXCore/core/application/properties/PropertyDefinitionInterface.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionFormatter.h>
#include <ArmarXCore/core/exceptions/local/InvalidPropertyValueException.h>
#include <ArmarXCore/core/exceptions/local/MissingRequiredPropertyException.h>
#include <ArmarXCore/core/exceptions/local/UnmappedValueException.h>
#include <ArmarXCore/core/exceptions/local/ValueRangeExceededException.h>

#include <Ice/Handle.h>

#include <type_traits>
#include <functional>
#include <memory>
#include <string>
#include <map>

namespace Ice
{
    class Properties;
    class LocalObject;
    ICE_API ::Ice::LocalObject* upCast(::Ice::Properties*);
    typedef ::IceInternal::Handle< ::Ice::Properties> PropertiesPtr;
}

namespace armarx::meta::properties
{
    template<class T, class = void>
    struct GetPropertyPlugin : std::false_type {};

    template<class T, class = void>
    struct DefinePropertyPlugin : std::false_type {};

    template<class T, class = void>
    struct MapPropertyValuePlugin : std::false_type {};

    template<class T, class = void>
    struct DefaultAsStringPlugin : std::false_type {};

    template<class T, class = void>
    struct PDInitHookPlugin : std::false_type {};
}

namespace armarx
{
    /**
     * @ingroup properties
     *
     * @class PropertyDefinition
     * @brief PropertyDefinition defines a property that will be available
     *        within the PropertyUser
     *
     * PropertyDefinition provides the definition and description of a
     * property that may be definined in a config file or passed via command
     * line as an option. Depending which constructor is used a property
     * is either required or optional. In the latter case a default value has
     * to be specified.
     *
     * The property specific value type (@em PropertyType) can be arbitrary, yet
     * it is devided into these three groups which are handled internally
     * differently:
     *  - String values
     *  - Arithmetic values (integral & floating point types)
     *  - Custom objects (Classes, Structs, Enums)
     *
     * @section fluentinterface Fluent Interface
     *
     * All setters are self referential and therefor can be chained to
     * simplify the implementation and increase the readability.
     *
     * @subsection fluentinterface_example Example
     * @code
     * PropertyDefinition<float> frameRateDef("MyFrameRate", "Capture frame rate");
     *
     * frameRateDef
     *     .setMatchRegex("\\d+(.\\d*)?")
     *     .setMin(0.0f)
     *     .setMax(60.0f);
     * @endcode
     */
    template<typename PropertyType>
    class PropertyDefinition :
        public PropertyDefinitionBase
    {
    public:
        using PropertyTypePtr = std::shared_ptr<PropertyType>;
        using ValueEntry = std::pair<std::string, PropertyType>;
        using PropertyValuesMap = std::map<std::string, ValueEntry>;
        typedef std::function<PropertyType(std::string)> PropertyFactoryFunction;

        /**
         * Constructs a property definition of a required property
         *
         * @param setterRef     Setter
         * @param propertyName  Name of the property used in config files
         * @param description   Mandatory property description
         */
        PropertyDefinition(
            PropertyType* setterRef,
            const std::string& propertyName,
            const std::string& description,
            PropertyDefinitionBase::PropertyConstness constness = PropertyDefinitionBase::eConstant);


        /**
         * Constructs a property definition of an optional property
         *
         * @param setterRef     Setter
         * @param propertyName  Name of the property used in config files
         * @param defaultValue  Property default value of the type
         *                      @em PropertyType
         * @param description   Mandatory property description
         */
        PropertyDefinition(
            PropertyType* setterRef,
            const std::string& propertyName,
            PropertyType defaultValue,
            const std::string& description,
            PropertyDefinitionBase::PropertyConstness constness = PropertyDefinitionBase::eConstant);

        /**
         * Maps a string value onto a value of the specified template type
         *
         * @param valueString   Property string value key
         * @param value         Property value
         *
         * @return self reference
         */
        PropertyDefinition<PropertyType>& map(const std::string& valueString, PropertyType value);


        PropertyDefinition<PropertyType>& map(const std::map<std::string, PropertyType>& values);

        template<class T>
        std::enable_if_t < std::is_same_v<T, PropertyType>&& !std::is_same_v<T, std::string>, PropertyDefinition<T>& >
        map(const std::map<T, std::string>& values);

        template<class T>
        std::enable_if_t<meta::properties::MapPropertyValuePlugin<T>::value, PropertyDefinition<PropertyType>& >
        map(const T& values)
        {
            return meta::properties::MapPropertyValuePlugin<T>::Map(*this, values);
        }

        /**
         * Sets the factory function that creates the specified template type from the actual string value
         *
         * @param func         the factory function
         *
         * @return self reference
         */
        PropertyDefinition<PropertyType>& setFactory(const PropertyFactoryFunction& func);

        /**
         * Sets whether the property value matching is case insensitive.
         *
         * @param isCaseInsensitive   Case sensitivity state
         *
         * @return self reference
         */
        PropertyDefinition<PropertyType>& setCaseInsensitive(bool isCaseInsensitive);

        /**
         * Sets whether for string values environment varbiale expanding should be considered.
         *
         * @param expand Expand entries like '${ENV_VAR}' to the according values of the
         * environment.
         *
         * @return self reference
         */
        PropertyDefinition<PropertyType>& setExpandEnvironmentVariables(bool expand);

        /**
         * Sets whether for string values leading and trailing quotes should be removed.
         *
         * @param removeQuotes The indicator.
         *
         * @return self reference
         */
        PropertyDefinition<PropertyType>& setRemoveQuotes(bool removeQuotes);

        /**
        * Sets the regular expression which the value has to be matched with.
        *
        * @param expr              Value regular expression
        *
        * @return self reference
        */
        PropertyDefinition<PropertyType>& setMatchRegex(const std::string& expr);

        /**
         * Sets the min allowed numeric value
         *
         * @param min   Min. allowed value
         *
         * @return self reference
         */
        PropertyDefinition<PropertyType>&
        setMin(double min);

        template<class T = PropertyType>
        PropertyDefinition<PropertyType>& setMin(std::array<T, 1> min)
        {
            return setMin(min.at(0));
        }

        /**
         * Sets the max allowed numeric value
         *
         * @param max   Max. allowed value
         *
         * @return self reference
         */
        PropertyDefinition<PropertyType>&
        setMax(double max);

        template<class T = PropertyType>
        PropertyDefinition<PropertyType>& setMax(std::array<T, 1> max)
        {
            return setMax(max.at(0));
        }

        bool isCaseInsensitive() const;
        bool expandEnvironmentVariables() const;
        bool removeQuotes() const;
        std::string getMatchRegex() const;
        double getMin() const;
        double getMax() const;
        std::string getPropertyName() const;
        PropertyValuesMap& getValueMap();
        PropertyType getDefaultValue();
        PropertyFactoryFunction getFactory() const;
        std::string getDefaultAsString() override;

        PropertyType getValue(const std::string& prefix, Ice::PropertiesPtr);
        bool isSet(const std::string& prefix, Ice::PropertiesPtr iceProperties) const;

        virtual
        void
        writeValueToSetter(
            const std::string& prefix,
            Ice::PropertiesPtr) override;

        //! Checks first and last character of input and in case both are quotes, they are removed and the remaining result is stored in output
        static void removeQuotes(const std::string& input, std::string& output);

        /**
         * @see PropertyDefinitionBase::toString()
         */
        std::string toString(PropertyDefinitionFormatter& formatter, const std::string& value) override;

        std::string getDescription() const;

    protected:

        /**
         * Main property map
         */
        PropertyValuesMap propertyValuesMap;

        /**
         * Property name
         */
        std::string propertyName;

        /**
         * Property description
         */
        std::string description;

        /**
         * Fallback/Default property value
         */
        PropertyType defaultValue;

        /**
         * Reference to a variable to set
         */
        PropertyType* setterRef;

        /**
          * Builder function
          */
        PropertyFactoryFunction factory;

        /**
         * Regular expression to approve a required value pattern
         */
        std::string regex;

        /**
         * Case sensitivity indicator
         */
        bool caseInsensitive;

        /**
         * Exand environments variables indicator (standard: true)
         */
        bool expandEnvVars;

        /**
         * Remove leading and trailing quotes indicator (standard: true)
         * First and last character of a string property value are checked for quotes.
         * E.g.
         *     "test" -> test
         *     'test' -> test
         */
        bool stringRemoveQuotes;

        /**
         * Upper bound of numeric values (used for numeric value retrieval
         * without mapping)
         */
        double max;

        /**
         * Lower bound of numeric values (used for numeric value retrieval
         * without mapping)
         */
        double min;

    private:

        void initHook();

        /**
         * @brief Get a required value.
         * @throw armarx::exceptions::local::ValueRangeExceededException
         * @throw armarx::exceptions::local::InvalidPropertyValueException
         */
        PropertyType getValueRequired(const std::string& prefix, Ice::PropertiesPtr iceProperties);
        /**
         * @brief Get an optional value.
         * @throw armarx::exceptions::local::ValueRangeExceededException
         * @throw armarx::exceptions::local::InvalidPropertyValueException
         */
        PropertyType getValueOptional(const std::string& prefix, Ice::PropertiesPtr iceProperties);

        /**
         * Returns the property string value. This function adapts the value
         * case according to the case sensitivity state. If no case sensitivity
         * is required, the return value will always be transformed to lower
         * case. This is mainly used to generate the value keys used in mapping.
         *
         * @return lower or mixed case eof th raw string value
         */
        std::string getPropertyValue(const std::string& prefix, Ice::PropertiesPtr iceProperties) const;

        /**
         * Returns the raw string value of this property.
         *
         * @return raw string value.
         */
        std::string getRawPropertyValue(const std::string& prefix, Ice::PropertiesPtr iceProperties) const;

        /**
         * Checks if this property is required and throws an exception if the
         * property is not specified.
         *
         * @throw armarx::exceptions::local::MissingRequiredPropertyException
         */
        void checkRequirement(const std::string& prefix, Ice::PropertiesPtr iceProperties);

        /**
         * Checks whether the a passed string matches a specified regular
         * expression.
         *
         * @param value     The value to check
         *
         * @return true if the string matches, otherwise false
         */
        bool matchRegex(const std::string& value) const;

        /// Return `value`.
        template <typename T>
        T processMappedValue(const T& value);

        /// @brief Expand environment variables and removes quotes, if set respectively.
        std::string processMappedValue(const std::string& value);

        /**
         * Verifies that the passed numericValue is within the defined
         * limits.
         *
         * @param numericValue  The numeric value of the property
         *
         * @throw armarx::exceptions::local::ValueRangeExceededException
         */
        void checkLimits(const std::string& prefix, PropertyType numericValue) const;

        static bool expandEnvironmentVariables(const std::string& input, std::string& output);

    };

    // Helper functions to prevent boost include files
    std::string PropertyDefinition_lexicalCastToString(double input);
    std::string PropertyDefinition_lexicalCastToString(float input);
    std::string PropertyDefinition_lexicalCastToString(long input);
    std::string PropertyDefinition_lexicalCastToString(int input);
    std::string PropertyDefinition_lexicalCastToString(unsigned long input);
    std::string PropertyDefinition_lexicalCastToString(unsigned int input);
    std::string PropertyDefinition_lexicalCastToString(std::string const& input);

    template <typename T>
    T PropertyDefinition_lexicalCastTo(std::string const& input);

    template <>
    double PropertyDefinition_lexicalCastTo<double>(std::string const& input);
    template <>
    float PropertyDefinition_lexicalCastTo<float>(std::string const& input);
    template <>
    long PropertyDefinition_lexicalCastTo<long>(std::string const& input);
    template <>
    int PropertyDefinition_lexicalCastTo<int>(std::string const& input);
    template <>
    unsigned long PropertyDefinition_lexicalCastTo<unsigned long>(std::string const& input);
    template <>
    unsigned int PropertyDefinition_lexicalCastTo<unsigned int>(std::string const& input);
    template <>
    char PropertyDefinition_lexicalCastTo<char>(std::string const& input);
    template <>
    unsigned char PropertyDefinition_lexicalCastTo<unsigned char>(std::string const& input);
    template <>
    bool PropertyDefinition_lexicalCastTo<bool>(std::string const& input);


    extern template class PropertyDefinition<int>;
    extern template class PropertyDefinition<float>;
    extern template class PropertyDefinition<double>;
    extern template class PropertyDefinition<std::string>;
    extern template class PropertyDefinition<bool>;
}

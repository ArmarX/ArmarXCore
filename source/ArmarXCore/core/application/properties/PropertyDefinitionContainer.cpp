/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Mirko Waechter (waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>


// STD/STL
#include <map>
#include <string>

#include <SimoxUtility/algorithm/string/string_tools.h>

// Ice
#include <Ice/Properties.h>

// ArmarXCore
#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/core/exceptions/local/InvalidPropertyValueException.h>
#include <ArmarXCore/core/exceptions/local/MissingRequiredPropertyException.h>
#include <ArmarXCore/core/exceptions/local/UnmappedValueException.h>
#include <ArmarXCore/core/exceptions/local/ValueRangeExceededException.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/application/properties/Property.h>
#include <ArmarXCore/core/application/properties/PropertyDefinition.h>
#include <ArmarXCore/util/check.h>


namespace armarx
{


    PropertyDefinitionContainer::PropertyDefinitionContainer(const std::string& prefix)
    {
        setDescription(prefix + " properties");

        this->prefix = prefix + ".";
    }


    PropertyDefinitionContainer::~PropertyDefinitionContainer()
    {
        // Delete property definitions.
        for (auto it = definitions.begin(); it != definitions.end(); ++it)
        {
            delete it->second;
        }

        // Delete proxy definitions,
        for (auto it = proxies.begin(); it != proxies.end(); ++it)
        {
            delete it->second;
        }
    }


    void
    PropertyDefinitionContainer::writeValues()
    {
        if (properties)
        {
            for (auto& [name, definition] : definitions)
            {
                definition->writeValueToSetter(getPrefix(), properties);
            }
        }
    }


    void
    PropertyDefinitionContainer::writeProxyValues(IceManagerPtr ice_manager)
    {
        if (properties)
        {
            for (auto& [name, definition] : proxies)
            {
                ARMARX_CHECK(definitions.find(name) != definitions.end())
                        << "`" << name << "` must be in definitions and proxies";

                // Get proxy name, fetch proxy and write to passed setter.
                const std::string proxy_name = getDefintion<std::string>(name)
                                               .getValue(getPrefix(), properties);
                definition->write_proxy_to_setter(ice_manager, proxy_name);
            }
        }
    }


    std::map<std::string, ProxyPropertyDefinitionBase*>
    PropertyDefinitionContainer::getProxyDefinitions()
    {
        return proxies;
    }


    std::vector<std::string>
    PropertyDefinitionContainer::getSubscribedTopicDefinitions()
    {
        return subscribedTopics;
    }


    Ice::PropertiesPtr PropertyDefinitionContainer::getProperties()
    {
        return properties;
    }


    void PropertyDefinitionContainer::setProperties(Ice::PropertiesPtr properties)
    {
        this->properties  = properties;
    }


    PropertyDefinitionBase*
    PropertyDefinitionContainer::getDefinitionBase(const std::string& name)
    {
        // check definition existance
        if (definitions.find(name) == definitions.end())
        {
            throw armarx::LocalException(name + " property is not defined.");
        }

        PropertyDefinitionBase* def = definitions[name];
        return def;
    }


    std::string PropertyDefinitionContainer::toString(PropertyDefinitionFormatter& formatter)
    {
        formatter.setPrefix(prefix);
        std::string definitionStr;

        DefinitionContainer::iterator it = definitions.begin();
        Ice::PropertyDict propertiesMap;

        if (properties)
        {
            propertiesMap = properties->getPropertiesForPrefix(getPrefix());
        }
        while (it != definitions.end())
        {
            definitionStr += it->second->toString(formatter, getValue(it->first));
            propertiesMap.erase(it->first);
            it++;
        }

        if (properties)
        {
            formatter.setPrefix("");
        }

        Ice::StringSeq values;
        for (Ice::PropertyDict::iterator property = propertiesMap.begin(); property != propertiesMap.end(); property++)
        {
            definitionStr += formatter.formatDefinition(property->first,
                             "",
                             "",
                             "",
                             "",
                             "",
                             "",
                             "",
                             values,
                             property->second);
        }

        return definitionStr;
    }

    std::string PropertyDefinitionContainer::getDescription() const
    {
        return description;
    }

    void PropertyDefinitionContainer::setDescription(const std::string& description)
    {
        this->description = description;
    }

    void PropertyDefinitionContainer::setPrefix(std::string prefix)
    {
        this->prefix = prefix;
    }

    std::string PropertyDefinitionContainer::getPrefix()
    {
        return prefix;
    }

    bool PropertyDefinitionContainer::isPropertySet(const std::string& name)
    {
        Ice::PropertyDict selectedProperties = properties->getPropertiesForPrefix(getPrefix());
        return (selectedProperties.count(name) > 0);
    }

    std::string PropertyDefinitionContainer::getValue(const std::string& name)
    {
        if (properties)
        {
            return properties->getPropertiesForPrefix(getPrefix())[name];
        }
        else
        {
            return "";
        }
    }

    std::map<std::string, std::string> PropertyDefinitionContainer::getPropertyValues(const std::string& prefix) const
    {
        std::map<std::string, std::string> result;
        for (const auto& elem : definitions)
        {
            std::string value;
            if (properties)
            {
                value = properties->getProperty(this->prefix + elem.first);
            }
            else
            {
                ARMARX_INFO << " properties NULL";
            }
            if (value.empty())
            {
                PropertyDefinitionBase* def = elem.second;
                value = def->getDefaultAsString();
                //            PropertyDefinition<std::string>* def = dynamic_cast<PropertyDefinition<std::string>*>(elem.second);
                //        result[elem.first] = Property<std::string>(
                //                                 *def,
                //                                 prefix,
                //                                 properties).getValue();
            }

            result[prefix + elem.first] = value;

        }
        return result;
    }

    std::string PropertyDefinitionContainer::eigenVectorPropertyDescription(
        const std::string& description, long size, std::string delim)
    {
        std::stringstream ss;
        ss << description << "\n";
        ss << "(Format: Sequence of ";
        if (size < 0)
        {
            ss << "n";
        }
        else
        {
            ss << size;
        }
        ss << " numbers delimited by '" << delim << "'.)";

        return ss.str();
    }

    std::string PropertyDefinitionContainer_ice_class_name(std::string const& full_type_name)
    {
        std::vector<std::string> splt = simox::alg::split(full_type_name, ":");
        std::string class_name = splt.at(splt.size() - 1);

        if (class_name == "ComponentInterface" and splt.size() >= 2)
        {
            /*
             * The specific interface of a modern class with name like:
             * package::components::name::ComponentInterface.
             * The component's name is the last namespace, i.e. the item before the class name.
             */
            class_name = splt.at(splt.size() - 2);
        }
        else
        {
            // Remove common suffix patterns.
            std::vector<std::string> trims = {
                "InterfaceListener", "_interface_listener", "Interface", "_interface",
                "Listener", "_listener"
            };
            for (const std::string& suffix : trims)
            {
                if (ends_with(class_name, suffix))
                {
                    class_name.erase(class_name.length() - suffix.length());
                    break;
                }
            }
        }

        return class_name;
    }


}

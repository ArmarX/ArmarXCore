/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "../services/profiler/Profiler.h"  // for ProfilerPtr
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <IceUtil/Time.h>               // for Time

#include <map>                          // for map
#include <string>                       // for string, operator<<
#include <vector>                       // for vector
#include <ostream>                      // for operator<<, size_t
#include <unordered_set>

namespace armarx
{
    struct ThreadUsage
    {
        int threadId;
        int processId;
        IceUtil::Time lastUpdate;
        int lastJiffies;
        double load;
        bool operator <(const ThreadUsage& rhs) const;
        bool operator ==(const ThreadUsage& rhs) const;
    };

    struct CpuUsage
    {
        long processId;
        int lastUtime;
        int lastStime;
        int lastCUtime;
        int lastCStime;
        IceUtil::Time lastUpdate;
        double proc_total_time;
    };

    struct MemoryUsage
    {
        int fastbinBlocks; // fsmblocks: space in freed fastbin blocks (bytes)
        int totalAllocatedSpace; // uordblks: total allocated space (bytes)
        int totalFreeSpace; // fordblks: total free space (bytes)
    };
}

namespace std
{
    template<>
    class hash<armarx::ThreadUsage>
    {
    public:
        template <class T>
        static void hash_combine(std::size_t& seed, const T& v)
        {
            std::hash<T> hasher;
            seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
        }
        size_t operator()(const armarx::ThreadUsage& t) const
        {
            std::size_t val = 0;
            hash_combine(val, t.processId);
            hash_combine(val, t.threadId);
            return val;
        }
    };
}

namespace armarx
{
    /**
     * @brief The ProcessWatcher class is instantiated once in each armarx::Application an monitors thread, CPU, and memory utilization
     */
    class ProcessWatcher
    {
    public:
        ProcessWatcher(Profiler::ProfilerPtr profiler);
        void start();
        void stop();


        void addThread(int processId, int threadId);
        void addThread(int threadId);
        void addAllChildThreads(int processId, int threadId);


        void removeAllThreads();
        void removeThread(int processId, int threadId);
        void removeThread(int threadId);

        void reportCpuUsage();
        void reportMemoryUsage();

        double getThreadLoad(int processId, int threadId);
        double getThreadLoad(int threadId);

        armarx::CpuUsage getProcessCpuUsage();

        static int GetThreadJiffies(int processId, int threadId);
        static std::map<int, int> GetThreadListJiffies(int processId, std::vector<int> threadIds);
        static int GetHertz();
    private:
        enum PROC_INDEX
        {
            eUTime = 13,
            eSTime = 14,
            eCUTIME = 15,
            eCSTIME = 16
        };

        void watch();
        void runThreadWatchList();
        void cpuUsageFromProcFile();
        void getMemoryUsage();

        long Hertz;
        std::string processCpuDataFilename;

        PeriodicTask<ProcessWatcher>::pointer_type watcherTask;

        std::mutex processCpuUsageMutex;
        armarx::CpuUsage processCpuUsage;

        std::mutex processMemoryUsageMutex;
        armarx::MemoryUsage memoryUsage;

        std::mutex threadWatchListMutex;
        std::unordered_set<ThreadUsage> threadWatchList;

        /**
         * Holds an instance of armarx::Profiler wich is set in the constructor of CPULoadWatcher.
         */
        Profiler::ProfilerPtr profiler;

    };
}




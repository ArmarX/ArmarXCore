/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Jan Issac (jan dot issac at gmx dot de)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/exceptions/Exception.h>

#include <boost/thread/locks.hpp>

#include <boost/thread/mutex.hpp>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/shared_mutex.hpp>
#include <boost/thread/condition_variable.hpp>

#define MUTEX_TIMEOUT_MSEC 10000
namespace armarx::exceptions::local
{
    /**
     * @struct MutexTimeoutException
     * @ingroup Threads
     * @brief The MutexTimeoutException struct
     */
    struct MutexTimeoutException : LocalException
    {
        MutexTimeoutException(int timeoutDelayMs, std::string mutexName = "")
        {
            std::stringstream reason;
            reason << "A Thread failed to get a mutex '" + mutexName + "' after " <<
                   timeoutDelayMs * 0.001 <<
                   " seconds!";
            setReason(reason.str());
        }
        std::string name() const override
        {
            return "armarx::exceptions::local::MutexTimeoutException";
        }
    };

    struct MutexDestructionException : LocalException
    {
        MutexDestructionException(std::string mutexName = "") : LocalException("A mutex '" + mutexName + "' was not already unlocked in the destructor!") {}
        std::string name() const override
        {
            return "armarx::exceptions::local::MutexDestructionException";
        }
    };
}

namespace armarx
{
    /**
     * @class HiddenTimedMutex
     * @defgroup Threads
     * @brief The HiddenTimedMutex class is a mutex, that has a normal boost::mutex
     * interface, but will timeout after fixed (defined in MUTEX_TIMEOUT_MSEC)
     * interval and throw a MutexTimeoutException, if it could not aquire the
     * mutex in that time.<br/>
     * This way, the stacktrace to the deadlock can be easily retrieved.
     */
    class HiddenTimedMutex
    {
        boost::timed_mutex timedMutex;
        int timeoutDelayMs;
        std::string mutexName;
    public:
        HiddenTimedMutex(int timeout = MUTEX_TIMEOUT_MSEC, std::string mutexName = ""): timeoutDelayMs(timeout), mutexName(mutexName) {}
        ~HiddenTimedMutex()
        {
            if (!try_lock())
            {
                // throw exceptions::local::MutexDestructionException();
                std::cout << "A mutex ('" << mutexName << "') was still locked in the destructor! Fix Application!\nBacktrace:\n" << LocalException::generateBacktrace() << std::endl;
            }
            else
            {
                unlock();
            }
        }

        void lock()
        {
            boost::system_time timeout = boost::get_system_time() +
                                         boost::posix_time::milliseconds(timeoutDelayMs);

            if (!timedMutex.timed_lock(timeout))
            {
                throw exceptions::local::MutexTimeoutException(timeoutDelayMs, mutexName);
            }
        }

        bool try_lock()
        {
            return timedMutex.try_lock();
        }

        void unlock()
        {
            timedMutex.unlock();
        }

        using ScopedLock = boost::unique_lock<HiddenTimedMutex>;
        using ScopedTryLock = boost::detail::try_lock_wrapper<HiddenTimedMutex>;
    };
    /**
      \addtogroup Threads
    @{
    */


    using Mutex = boost::mutex                           ;
    using ScopedLock = Mutex::scoped_lock                     ;
    using ScopedLockPtr = std::shared_ptr<ScopedLock>          ;
    using ScopedTryLock = Mutex::scoped_try_lock                 ;

    using TimedMutex = boost::timed_mutex                     ;
    using ScopedTimedMutex = TimedMutex::scoped_lock                ;
    using ScopedTimedMutexPtr = std::shared_ptr<ScopedTimedMutex>    ;
    using ScopedTimedTryMutex = TimedMutex::scoped_try_lock            ;

    using RecursiveMutex = boost::recursive_mutex                 ;
    using ScopedRecursiveLock = RecursiveMutex::scoped_lock            ;
    using ScopedRecursiveLockPtr = std::shared_ptr<ScopedRecursiveLock> ;
    using ScopedRecursiveTryLock = RecursiveMutex::scoped_try_lock        ;

    using TimedRecursiveMutex = boost::recursive_timed_mutex           ;
    using ScopedTimedRecursiveLock = TimedRecursiveMutex::scoped_lock       ;
    using ScopedTimedRecursiveLockPtr = std::shared_ptr<ScopedTimedRecursiveLock>;
    using ScopedTimedRecursiveTryLock = TimedRecursiveMutex::scoped_try_lock   ;

    using SharedMutex = boost::shared_mutex                    ;

    using ScopedUniqueLock = boost::unique_lock<boost::shared_mutex>;
    using ScopedSharedLock = boost::shared_lock<boost::shared_mutex>;

    using ScopedUniqueLockPtr = std::shared_ptr<ScopedUniqueLock>;
    using ScopedSharedLockPtr = std::shared_ptr<ScopedSharedLock  >;

    using ConditionalVariable = boost::condition_variable              ;
    /*
     @}
    */
}


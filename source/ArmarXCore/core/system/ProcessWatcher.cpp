/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <IceUtil/Handle.h>             // for HandleBase

#include <SimoxUtility/algorithm/string/string_tools.h>

#include <ctype.h>                      // for isspace
#include <ext/alloc_traits.h>

#include <memory>                       // for allocator, operator==
#include <sstream>                      // for operator<<, ifstream, etc
#include <fstream>
#include <unordered_set>

#include "ArmarXCore/core/logging/LogSender.h"  // for LogSender
#include "ArmarXCore/core/logging/Logging.h"  // for ARMARX_WARNING_S
#include "ArmarXCore/core/services/tasks/PeriodicTask.h"
#include "ArmarXCore/core/system/../services/profiler/Profiler.h"
#include "ArmarXCore/interface/core/ThreadingIceBase.h"  // for upCast

#include "../exceptions/local/ExpressionException.h"

#include "ProcessWatcher.h"

#include <sys/utsname.h>                // for uname, utsname
#include <malloc.h>                     // for mallinfo
#include <unistd.h>                     // for sysconf, _SC_CLK_TCK

namespace armarx
{
    ProcessWatcher::ProcessWatcher(Profiler::ProfilerPtr profiler) :
        profiler(profiler)
    {
        long pid = LogSender::getProcessId();
        Hertz = sysconf(_SC_CLK_TCK);
        std::stringstream ss;
        ss << "/proc/" << pid << "/stat";
        processCpuDataFilename = ss.str();
        processCpuUsage.processId = pid;
        cpuUsageFromProcFile();
    }

    void ProcessWatcher::start()
    {
        if (!watcherTask)
        {
            watcherTask = new PeriodicTask<ProcessWatcher>(this, &ProcessWatcher::watch, 300, false);
        }

        watcherTask->start();
    }

    void ProcessWatcher::stop()
    {
        watcherTask->stop();
        watcherTask = nullptr;
    }

    void ProcessWatcher::addThread(int processId, int threadId)
    {
        ARMARX_TRACE;
        std::unique_lock lock(threadWatchListMutex);
        ThreadUsage usage;
        usage.processId = processId;
        usage.threadId = threadId;
        usage.lastJiffies = GetThreadJiffies(processId, threadId);
        usage.lastUpdate = IceUtil::Time::now();
        usage.load = 0;
        threadWatchList.insert(usage);
    }

    void ProcessWatcher::addThread(int threadId)
    {
        addThread(LogSender::getProcessId(), threadId);
    }

    void ProcessWatcher::watch()
    {
        ARMARX_TRACE;
        runThreadWatchList();
        cpuUsageFromProcFile();
        getMemoryUsage();
        reportCpuUsage();
        reportMemoryUsage();
    }

    void ProcessWatcher::runThreadWatchList()
    {
        ARMARX_TRACE;
        std::unique_lock lock(threadWatchListMutex);
        std::unordered_set<ThreadUsage> newset;
        std::unordered_set<ThreadUsage>::iterator it =  threadWatchList.begin();

        for (; it != threadWatchList.end(); it++)
        {
            ARMARX_TRACE;
            ThreadUsage usage = *it;
            IceUtil::Time queryTime = IceUtil::Time::now();
            int curJiffies = GetThreadJiffies(usage.processId, usage.threadId);

            if (GetHertz() != -1)
            {
                usage.load = (curJiffies - usage.lastJiffies) / ((queryTime - usage.lastUpdate).toSecondsDouble() * GetHertz());
            }
            else
            {
                usage.load = 0;
            }

            usage.lastJiffies = curJiffies;
            usage.lastUpdate = queryTime;
            newset.insert(usage);
        }

        threadWatchList.swap(newset);
    }


#if !defined(_WIN32) && !defined(__APPLE__)
    int ProcessWatcher::GetHertz()
    {
        ARMARX_TRACE;
        static std::mutex mutex;
        std::unique_lock lock(mutex);
        static int value = -2;

        if (value == -2) // init value
        {
            ARMARX_TRACE;
            static std::string _release;

            if (_release.empty())
            {
                struct utsname utsinfo;
                uname(&utsinfo);
                _release = utsinfo.release;
            }

            std::string fileName = "/boot/config-" + _release;
            std::ifstream configFile(fileName.c_str());

            ARMARX_TRACE;
            //    procFile.open(fileName, ios::out);
            if (!configFile.is_open())
            {
                ARMARX_WARNING_S << "Cannot open "  << fileName << " for reading the cpu hertz value";
                value = -1;
            }
            else
            {
                value = -1;
                std::string line;
                const std::string searchString = "CONFIG_HZ=";

                while (getline(configFile, line))
                {
                    ARMARX_TRACE;
                    std::size_t pos = line.find(searchString);

                    if (pos != std::string::npos)
                    {
                        std::string hzString = line.substr(pos + searchString.length());
                        value = std::stoi(hzString);
                        break;
                    }
                }
            }
        }

        return value;


    }

    void ProcessWatcher::cpuUsageFromProcFile()
    {
        ARMARX_TRACE;
        std::string line;

        std::ifstream file(processCpuDataFilename);

        if (!file.is_open())
        {
            ARMARX_WARNING_S << "File " << processCpuDataFilename << " does not exist";
        }


        std::getline(file, line);
        file.close();

        std::vector<std::string> pidElements = simox::alg::split(line, "\t ");

        int currentUtime = std::stoi(pidElements.at(eUTime));
        int currentStime = std::stoi(pidElements.at(eSTime));

        int currentCUtime = std::stoi(pidElements.at(eCUTIME));
        int currentCStime = std::stoi(pidElements.at(eCSTIME));
        {
            ARMARX_TRACE;
            std::unique_lock guard{processCpuUsageMutex};

            IceUtil::Time queryTime = IceUtil::Time::now();

            processCpuUsage.proc_total_time = 100 * (double)((currentStime + currentCStime - processCpuUsage.lastStime - processCpuUsage.lastCStime) + (currentUtime + currentCUtime - processCpuUsage.lastCUtime - processCpuUsage.lastUtime)) / ((queryTime - processCpuUsage.lastUpdate).toSecondsDouble() * Hertz);

            processCpuUsage.lastUpdate = queryTime;
            processCpuUsage.lastUtime = currentUtime;
            processCpuUsage.lastStime = currentStime;

            processCpuUsage.lastCUtime = currentCUtime;
            processCpuUsage.lastCStime = currentCStime;
        }
    }

    void ProcessWatcher::getMemoryUsage()
    {
        ARMARX_TRACE;
        struct mallinfo _mallinfo;

        _mallinfo = mallinfo();

        {
            std::unique_lock guard{processMemoryUsageMutex};
            memoryUsage.fastbinBlocks = _mallinfo.fsmblks;
            memoryUsage.totalAllocatedSpace = _mallinfo.uordblks;
            memoryUsage.totalFreeSpace = _mallinfo.fordblks;

        }

    }


    std::map<int, int> ProcessWatcher::GetThreadListJiffies(int processId, std::vector<int> threadIds)
    {
        ARMARX_TRACE;
        std::map<int, int> resultMap;
        for (int threadId : threadIds)
        {
            ARMARX_TRACE;
            resultMap[threadId] = 0;
            std::stringstream fileName;
            fileName << "/proc/" << threadId << "/task/" <<  threadId << "/stat";
            std::ifstream statFile(fileName.str().c_str());

            if (!statFile.is_open())
            {
                ARMARX_WARNING_S << "Cannot open "  << fileName.str() << " for reading the hertz value";
                resultMap[threadId] = 0;
                continue;
            }

            std::string line;

            while (getline(statFile, line))
            {
                ARMARX_TRACE;
                simox::alg::trim(line);

                std::vector<std::string> stringVec = simox::alg::split(line, " ");

                ARMARX_CHECK_LESS(14, stringVec.size());
                int userTimeJiffies = atoi(stringVec.at(13).c_str());
                int kernelTimeJiffies = atoi(stringVec.at(14).c_str());

                resultMap[threadId] = userTimeJiffies + kernelTimeJiffies;
            }
        }

        return resultMap;
    }
#else

    int ProcessWatcher::GetHertz()
    {
        return -1;
    }

    std::map<int, int> ProcessWatcher::GetThreadListJiffies(int processId, std::vector<int> threadIds)
    {
        return std::map<int, int>();
    }

    void ProcessWatcher::cpuUsageFromProcFile()
    {
    }

    void ProcessWatcher::getMemoryUsage()
    {
    }

#endif

    bool ThreadUsage::operator <(const ThreadUsage& rhs) const
    {
        if (processId < rhs.processId)
        {
            return true;
        }

        if (threadId < rhs.threadId)
        {
            return true;
        }

        return false;
    }

    bool ThreadUsage::operator ==(const ThreadUsage& rhs) const
    {
        if (processId == rhs.processId && threadId == rhs.threadId)
        {
            return true;
        }

        return false;
    }

    int ProcessWatcher::GetThreadJiffies(int processId, int threadId)
    {
        ARMARX_TRACE;
        std::map<int, int> result = GetThreadListJiffies(processId, std::vector<int>(1, threadId));
        std::map<int, int>::iterator it = result.find(threadId);

        if (it == result.end())
        {
            return 0;
        }

        return it->second;
    }


    void ProcessWatcher::reportCpuUsage()
    {
        std::unique_lock guard{processCpuUsageMutex};
        profiler->logProcessCpuUsage(processCpuUsage.proc_total_time);
    }

    void ProcessWatcher::reportMemoryUsage()
    {
        std::unique_lock guard{processMemoryUsageMutex};
        profiler->logProcessMemoryUsage(memoryUsage.totalFreeSpace);
    }

    double ProcessWatcher::getThreadLoad(int threadId)
    {
        return getThreadLoad(LogSender::getProcessId(), threadId);
    }


    double ProcessWatcher::getThreadLoad(int processId, int threadId)
    {
        ARMARX_TRACE;
        ThreadUsage query;
        query.processId = processId;
        query.threadId = threadId;
        std::unique_lock lock(threadWatchListMutex);
        auto it = threadWatchList.find(query);

        ARMARX_TRACE;
        if (it != threadWatchList.end())
        {
            return it->load * 100;
        }
        else
        {
            return 0.0;
        }
    }

    armarx::CpuUsage ProcessWatcher::getProcessCpuUsage()
    {
        std::unique_lock guard{processCpuUsageMutex};
        return processCpuUsage;
    }


    void ProcessWatcher::removeThread(int processId, int threadId)
    {
        ARMARX_TRACE;
        ThreadUsage query;
        query.processId = processId;
        query.threadId = threadId;
        std::unique_lock lock(threadWatchListMutex);
        threadWatchList.erase(query);
    }

    void ProcessWatcher::removeThread(int threadId)
    {
        removeThread(LogSender::getProcessId(), threadId);
    }
}

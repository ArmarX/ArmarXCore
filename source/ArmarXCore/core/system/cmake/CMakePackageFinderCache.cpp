#include "CMakePackageFinderCache.h"

namespace armarx
{
    CMakePackageFinderCache CMakePackageFinderCache::GlobalCache;

    CMakePackageFinderCache::CMakePackageFinderCache(const CMakePackageFinderCache& s) :
        packageFinders(s.packageFinders),
        timeout(s.timeout)
    {

    }

    CMakePackageFinderCache::CMakePackageFinderCache(IceUtil::Time timeout) :
        timeout(timeout)
    {

    }

    const CMakePackageFinder& CMakePackageFinderCache::findPackage(const std::string& packageName, const std::filesystem::path& packagePath, bool suppressStdErr, bool usePackagePathOnlyAsHint)
    {
        std::unique_lock lock(mutex);
        auto it = packageFinders.find(packageName);
        if (it != packageFinders.end())
        {
            if ((IceUtil::Time::now() - it->second.first) > timeout)
            {
                it->second.second = CMakePackageFinder(packageName, packagePath, suppressStdErr, usePackagePathOnlyAsHint);
                it->second.first = IceUtil::Time::now();
            }
            return it->second.second;
        }
        else
        {
            auto insertion = packageFinders.insert(std::make_pair(packageName, std::make_pair(IceUtil::Time::now(), CMakePackageFinder(packageName))));
            return insertion.first->second.second;
        }
    }
}

#pragma once

#include "CMakePackageFinder.h"

#include <IceUtil/Time.h>

#include <mutex>
#include <map>

namespace armarx
{
    class CMakePackageFinderCache;
    class CMakePackageFinderCache
    {
    public:
        CMakePackageFinderCache(const CMakePackageFinderCache& s);
        CMakePackageFinderCache(IceUtil::Time timeout = IceUtil::Time::seconds(10));
        const CMakePackageFinder& findPackage(const std::string& packageName, const std::filesystem::path& packagePath = "", bool suppressStdErr = false, bool usePackagePathOnlyAsHint = false);
        static CMakePackageFinderCache GlobalCache;
    private:
        std::mutex mutex;
        std::map < std::string, std::pair<IceUtil::Time, CMakePackageFinder> > packageFinders;
        IceUtil::Time timeout;
    };

}


/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu)
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <string_view>
#include <sstream>

#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/gzip.hpp>

#include <Ice/InputStream.h>

//raw
namespace armarx
{
    template<class T>
    void iceBlobToObject(T& result, const std::string_view& sv)
    {
        const auto first = reinterpret_cast<const Ice::Byte*>(sv.data());
        const auto last = first + sv.size();
        Ice::InputStream{std::make_pair(first, last)}.read(result);
    }

    template<class T>
    T iceBlobToObject(const std::string_view& sv)
    {
        T result;
        iceBlobToObject(result, sv);
        return result;
    }

    template<class T>
    struct IceBlobToObjectDeserializer
    {
        void deserialize(T& result, const std::string_view& sv)
        {
            iceBlobToObject(result, sv);
        }
        T deserialize(const std::string_view& sv)
        {
            return iceBlobToObject<T>(sv);
        }
    };
}

//compressed
namespace armarx
{
    template<class T>
    void compressedIceBlobToObject(T& result, const std::string_view& sv)
    {
        std::stringstream istr;
        istr << sv;
        boost::iostreams::filtering_istreambuf in;
        in.push(boost::iostreams::gzip_decompressor());
        in.push(istr);
        std::stringstream ostr;
        boost::iostreams::copy(in, ostr);

        iceBlobToObject(result, ostr.str());
    }

    template<class T>
    T compressedIceBlobToObject(const std::string_view& sv)
    {
        T result;
        compressedIceBlobToObject(result, sv);
        return result;
    }

    template<class T>
    struct CompressedIceBlobToObjectDeserializer
    {
        void deserialize(T& result, const std::string_view& sv)
        {
            compressedIceBlobToObject(result, sv);
        }
        T deserialize(const std::string_view& sv)
        {
            return compressedIceBlobToObject<T>(sv);
        }
    };
}

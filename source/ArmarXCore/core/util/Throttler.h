/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/time/DateTime.h>
#include <ArmarXCore/core/time/Duration.h>

namespace armarx
{

    /**
     * @brief The Throttler class. Use this class to e.g. process data only at a certain frequency.
     *
     */
    class Throttler
    {
    public:
        /**
         * @brief Construct a new Throttler object
         *
         * @param frequency in [Hz] to check
         */
        explicit Throttler(float frequency);

        /**
         * @brief Check if enough time has passed since this function last returned true.
         *
         * @param timestamp in [µs]
         * @return true if enough time has passed since last time when function
         * returned true.
         * @return false otherwise
         */
        bool check(const armarx::DateTime& timestamp) noexcept;

    private:
        const armarx::Duration delay;

        armarx::DateTime lastTimeTrue{0};
    };

} // namespace armarx

/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu)
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <string_view>
#include <sstream>

#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/gzip.hpp>

#include <Ice/OutputStream.h>

//raw
namespace armarx
{
    template<class T>
    struct ObjectToIceBlobSerializer
    {
        ObjectToIceBlobSerializer(const T& obj)
        {
            serialize(obj);
        }
        ObjectToIceBlobSerializer() = default;

        void serialize(const T& obj)
        {
            _out.clear();
            _out.write(obj);
            std::tie(_begin, _end) = _out.finished();
        }

        void clear()
        {
            _out.clear();
            _begin = nullptr;
            _end = nullptr;
        }

        const Ice::Byte* begin() const
        {
            return _begin;
        }
        const Ice::Byte* end() const
        {
            return _end;
        }

        std::ptrdiff_t size() const
        {
            return _end - _begin;
        }
    private:
        Ice::OutputStream _out;
        const Ice::Byte* _begin{nullptr};
        const Ice::Byte* _end{nullptr};
    };
}

//compressed
namespace armarx
{
    template<class T>
    struct ObjectToCompressedIceBlobSerializer
    {
        ObjectToCompressedIceBlobSerializer(const T& obj)
        {
            serialize(obj);
        }
        ObjectToCompressedIceBlobSerializer() = default;

        void serialize(const T& obj)
        {
            _ser.serialize(obj);
            const std::string_view sv(reinterpret_cast<const char*>(_ser.begin()), _ser.size());

            std::stringstream istr;
            istr << sv;
            boost::iostreams::filtering_istreambuf in;
            in.push(boost::iostreams::gzip_compressor());
            in.push(istr);
            std::stringstream ostr;
            boost::iostreams::copy(in, ostr);

            _ser.clear();
            _str = ostr.str();
        }

        void clear()
        {
            _ser.clear();
            _str.clear();
        }

        const char* begin() const
        {
            return _str.data();
        }

        std::size_t size() const
        {
            return _str.size();
        }

        const char* end() const
        {
            return begin() + size();
        }

    private:
        ObjectToIceBlobSerializer<T> _ser;
        std::string _str;
    };
}

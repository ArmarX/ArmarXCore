/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/util/CPPUtility/GetTypeString.h>

#include <unordered_map>
#include <sstream>
#include <string>
#include <vector>
#include <deque>
#include <map>

namespace armarx
{
    using std::to_string;

    inline const std::string& to_string(const std::string& s)
    {
        return s;
    }

    inline std::string to_string(std::string s)
    {
        return s;
    }

    /**
     * @brief Converts a string to float and uses always *dot* as seperator.
     */
    float toFloat(const std::string& input);
    int toInt(const std::string& input);
    unsigned int toUInt(const std::string& input);

    template <typename T>
    std::string ValueToString(const T& value)
    {
        std::stringstream str;
        str << value;
        return str.str();
    }

    bool Contains(const std::string& haystack, const std::string& needle, bool caseInsensitive = false);

    std::vector<std::string> Split(const std::string& source, const std::string& splitBy, bool trimElements = false, bool removeEmptyElements = false);
    std::vector<std::string> split(const std::string& source, const std::string& splitBy, bool trimElements = false, bool removeEmptyElements = false);
    bool starts_with(const std::string& haystack, const std::string& needle);
    bool ends_with(const std::string& haystack, const std::string& needle);

    inline void EncodeInline(std::string& data)
    {
        std::string buffer;
        buffer.reserve(data.size());
        for (size_t pos = 0; pos != data.size(); ++pos)
        {
            switch (data[pos])
            {
                case '&':
                    buffer.append("&amp;");
                    break;
                case '\"':
                    buffer.append("&quot;");
                    break;
                case '\'':
                    buffer.append("&apos;");
                    break;
                case '<':
                    buffer.append("&lt;");
                    break;
                case '>':
                    buffer.append("&gt;");
                    break;
                default:
                    buffer.append(&data[pos], 1);
                    break;
            }
        }
        data.swap(buffer);
    }

    std::string Encode(const std::string& data);
}

namespace std
{
    template <typename T>
    std::string& operator <<(std::string& str, const T& value)
    {
        str += armarx::ValueToString(value);
        return str;
    }

    template<typename T>
    ostream&
    operator<<(ostream& str, const std::vector<T>& vector)
    {
        str << "Vector<" << armarx::GetTypeString<T>() << ">(" << vector.size() << "):\n";

        for (unsigned int i = 0; i < vector.size(); ++i)
        {
            str << "\t(" << i << "): " << vector.at(i) << "\n";
        }

        return str;
    }

    template<typename T>
    ostream&
    operator<<(ostream& str, const std::deque<T>& deque)
    {
        str << "Deque<" << armarx::GetTypeString<T>() << ">(" << deque.size() << "):\n";

        for (unsigned int i = 0; i < deque.size(); ++i)
        {
            str << "\t(" << i << "): " << deque.at(i) << "\n";
        }

        return str;
    }

    template<typename T1, typename T2>
    ostream&
    operator<<(ostream& str, const std::pair<T1, T2>& pair)
    {
        str << "Pair<" << armarx::GetTypeString<T1>() << ", " <<  armarx::GetTypeString<T2>() << ">:\n";
        str << "\t(" << pair.first << ", " << pair.second << ")\n";
        return str;
    }

    template<typename T1, typename T2>
    ostream&
    operator<<(ostream& str, const std::map<T1, T2>& map)
    {
        str << "Map<" << armarx::GetTypeString<T1>() << ", " <<  armarx::GetTypeString<T2>() << ">(" << map.size() << "):\n";
        for (const auto& pair : map)
        {
            str << "\t" << pair.first << ": " << pair.second << "\n";
        }
        return str;
    }

    template<typename T1, typename T2>
    ostream&
    operator<<(ostream& str, const std::unordered_map<T1, T2>& map)
    {
        str << "Unordered Map<" << armarx::GetTypeString<T1>() << ", " <<  armarx::GetTypeString<T2>() << ">(" << map.size() << "):\n";
        for (const auto& pair : map)
        {
            str << "\t" << pair.first << ": " << pair.second << "\n";
        }
        return str;
    }

    inline ostream& operator<<(ostream& str, std::nullptr_t)
    {
        return str << static_cast<void*>(nullptr);
    }
}

#define VAROUT(x) std::string(std::string(#x) +": " + armarx::ValueToString(x)) + " "


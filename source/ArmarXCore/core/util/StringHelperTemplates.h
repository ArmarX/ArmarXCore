/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "StringHelpers.h"
#include "TemplateMetaProgramming.h"

#include <boost/lexical_cast.hpp>

namespace armarx
{

    template<typename T>
    inline std::vector<T> Split(const std::string& source, const std::string& splitBy, bool trimElements = false, bool removeEmptyElements = false)
    {
        if (source.empty())
        {
            return std::vector<T>();
        }

        std::vector<std::string> components = Split(source, splitBy, trimElements, removeEmptyElements);
        //        boost::split(components, source, boost::is_any_of(splitBy));

        std::vector<T> result(components.size());
        std::transform(components.begin(), components.end(), result.begin(), boost::lexical_cast<T, std::string>);
        return result;
    }

    namespace detail
    {
        template<class> struct  ToStringFTuple;

        template<std::size_t...Is>
        struct ToStringFTuple<meta::IndexSequence<Is...>>
        {
            template<class...Ts>
            static std::string Format(const std::string& form, const std::tuple<Ts...>& tuple)
            {
                //this has to be done via a string
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-security"
                char buff[512];
                int sz = std::snprintf(buff, sizeof(buff), form.c_str(), std::get<Is>(tuple)...);
                if (0 < sz && static_cast<std::size_t>(sz) < sizeof(buff))
                {
                    return buff;
                }
                std::vector<char> buffd(sz + 1); // note +1 for null terminator
                if (std::snprintf(buff, sizeof(buff), form.c_str(), std::get<Is>(tuple)...) != sz)
                {
                    throw std::logic_error
                    {
                        __FILE__ " " + std::to_string(__LINE__) +
                        " std::snprintf behaved unexpectedly"
                    };
                }
#pragma GCC diagnostic pop
                return buffd.data();
            }
            static std::string Format(const std::string& form, std::tuple<>)
            {
                return form;
            }
        };

    }

    template<std::size_t From, std::size_t To, class...Ts>
    std::string TupleToStringF(const std::string& form, const std::tuple<Ts...>& tuple)
    {
        return armarx::detail::ToStringFTuple<meta::MakeIndexRange<From, To>>::Format(form, tuple);
    }

    template<std::size_t From, class...Ts>
    std::string TupleToStringF(const std::string& form, const std::tuple<Ts...>& tuple)
    {
        return TupleToStringF<From, sizeof...(Ts)>(form, tuple);
    }

    template<class...Ts>
    std::string TupleToStringF(const std::string& form, const std::tuple<Ts...>& tuple)
    {
        return TupleToStringF<0>(form, tuple);
    }

}



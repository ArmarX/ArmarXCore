#include "Throttler.h"
#include "ArmarXCore/core/time/DateTime.h"

namespace armarx
{
    Throttler::Throttler(const float frequency)
        : delay(Duration::MicroSeconds(1 / frequency * 1'000'000)) {}

    bool Throttler::check(const armarx::DateTime& timestamp) noexcept
    {
      const core::time::Duration timeSinceLastTrue = timestamp - lastTimeTrue;

      if (timeSinceLastTrue >= delay) 
      {
        lastTimeTrue = timestamp;
        return true;
      }

      return false;
    }

} // namespace armarx

/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2021, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Raphael Grimm (raphael dot grimm at kit dot edu)
* @author     Rainer Kartmann (rainer dot kartmann at kit dot edu)
* @date       2021
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <set>
#include <string>
#include <vector>

#include <ArmarXCore/core/ManagedIceObjectPlugin.h>
#include <ArmarXCore/core/application/properties/forward_declarations.h>


namespace armarx
{
    class ComponentPlugin : public ManagedIceObjectPlugin
    {
    protected:

        friend class Component;

        using ManagedIceObjectPlugin::ManagedIceObjectPlugin;


        virtual void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties);

        virtual void componentPropertiesUpdated(const std::set<std::string>& changedProperties);

    };
}

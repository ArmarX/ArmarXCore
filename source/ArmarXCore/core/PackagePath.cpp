/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core::PackagePath
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <ArmarXCore/core/PackagePath.h>


// ArmarX
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>


armarx::PackagePath::PackagePath(const std::string& package_name, const std::filesystem::path& rel_path) :
    m_pp{package_name, rel_path.string()}
{

}


armarx::PackagePath::PackagePath(const data::PackagePath& pp) :
    m_pp{pp}
{

}


armarx::PackagePath::~PackagePath()
{

}


std::filesystem::path
armarx::PackagePath::toSystemPath(const data::PackagePath& pp)
{
    CMakePackageFinder pkg_finder(pp.package);
    const std::filesystem::path pkg_data_dir{pkg_finder.getDataDir()};

    if (pkg_data_dir.empty())
    {
        throw PackageNotFoundException{pp.package};
    }

    return pkg_data_dir / std::filesystem::path{pp.package} / pp.path;
}


std::filesystem::path
armarx::PackagePath::toSystemPath() const
{
    return PackagePath::toSystemPath(m_pp);
}


armarx::data::PackagePath
armarx::PackagePath::serialize() const
{
    return m_pp;
}


namespace armarx
{
    
    std::ostream& operator<<(std::ostream& os, const armarx::PackagePath& packagePath) 
    {
        os << "PackagePath { " << packagePath.serialize().package << " : " << packagePath.serialize().path << " }";
        return os;
    }

}

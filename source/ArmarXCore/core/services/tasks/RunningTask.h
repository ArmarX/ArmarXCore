/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Kai Welke (welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/interface/core/ThreadingIceBase.h>
#include <ArmarXCore/util/CPPUtility/GetTypeString.h>

#include <IceUtil/Thread.h>

#include <functional>
#include <memory>

namespace armarx
{
    class ThreadList;
    using ThreadListPtr = IceInternal::Handle<ThreadList>;

    class ARMARXCORE_IMPORT_EXPORT RunningTaskBase
        : virtual public IceUtil::Thread
        , virtual protected RunningTaskIceBase
    {
    public:
        using CallbackT = std::function<void()>;

        CallbackT callback;

        RunningTaskBase(std::string const& name);

        /**
         * Constructs a running task within the class parent which calls the \p runningFn in
         * a new thread.
         *
         * @param name of this thread, that describes what it is doing. If string
         *        is empty, it will use the name of the class with RTTI
         */
        template <typename T>
        RunningTaskBase(T* parent, void (T::* runningFn)(), const std::string& name = "")
            : RunningTaskBase(name.empty() ? GetTypeString<T>() : name)
        {
            callback = [parent, runningFn]()
            {
                return (parent->*runningFn)();
            };
        }

        /**
         * Destructor stops the thread and waits for completion.
         */
        ~RunningTaskBase() override;

        void setName(const std::string& name);

        void setThreadList(ThreadListPtr threadList);

        /**
         * Starts the thread. Only has effect if the task has not been started. After completion of the task, it cannot be started again.
         */
        void start();

        /**
         * Stops the thread. Only has effect if the task has been started.
         *
         * @param waitForJoin wait for the thread to terminate and join with the process.
         */
        void stop(bool waitForJoin = true);

        /**
         * @brief Wait for the RunningTask to finish *without* telling it to finish.
         */
        void join();

        /**
         * Retrieve running state of the thread. The task is running when it is started until the thread method joints.
         *
         * @return running state
         */
        bool isRunning() const;

        /**
         * Retrieve finished state of the thread. The task is finished once the thread has joined.
         *
         * @return finished state
         */
        bool isFinished() const;

        /**
         * wait blocking for thread to be finished. The task is finished once the thread has joined.
         * @param timeoutMS timeout in milliseconds. If -1 there is no timeout.
         * @return returns true if the task finished in the given time interval
         */
        bool waitForFinished(int timeoutMS = -1);

        /**
         * Retrieve whether stop() has been called. Has to be handled in thread function implementation for proper exit.
         *
         * @return stopped state
         */
        bool isStopped();

        /**
         * Wait blocking for thread until stop() has been called. Can be used for blocking wait for stop in thread function implementation
         */
        void waitForStop();

        std::string getName() const;

    private:
        void run() override;

        struct Impl;
        std::unique_ptr<Impl> impl;
    };

    /**
      \page ThreadsDoc ArmarX Threads
      ArmarX provides utility methods in order to include different types of threads. In order to support the inclusion
      of multiple threads in a single objects, without the neccessity to write additional thread classes the mechanism
      uses functionoids of class methods.
      Two types of threads are provided by the ArmarX core: the RunningTask and the PeriodicTask. These can be
      extended in projects which use the ArmarX core.

      The API documentation can be found here: \ref Threads

      \section RunningTasks Using running tasks
      The running task executes one thread method once. The task can be started only once.
      \include RunningTask.dox

      \section PeriodicTasks Using periodic tasks
      The periodic task executes one thread method repeatedly using the time period specified in the constructor.
      \include PeriodicTask.dox



      \defgroup Threads
      \ingroup core-utility

    * \class RunningTask
    * \ingroup Threads
    */
    template <class T>
    class ARMARXCORE_IMPORT_EXPORT RunningTask
        : public RunningTaskBase
    {
    public:
        /**
         * Typedef for the thread method. Thread methods need to follow the template
         * void methodName(void). Parameter passing is not used since methods are members
         * of the class.
         */
        typedef void (T::*method_type)(void);

        /**
         * Shared pointer type for convenience.
         */
        using pointer_type = IceUtil::Handle<RunningTask<T> >;

        /**
         * Constructs a running task within the class parent which calls the \p runningFn in
         * a new thread.
         *
         * @param name of this thread, that describes what it is doing. If string
         *        is empty, it will use the name of the class with RTTI
         */
        RunningTask(T* parent, method_type runningFn, const std::string& name = "")
            : RunningTaskBase(parent, runningFn, name)
        {
        }
    };



}



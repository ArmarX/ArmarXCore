/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core::services::profiler
 * @author     Manfred Kroehnert ( manfred dot kroehnert at dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/core/application/Application.h>  // for Application
#include <Ice/Config.h>                 // for Long
#include <Ice/Handle.h>                 // for Handle
#include <Ice/Object.h>                 // for Object
#include <IceUtil/Handle.h>             // for HandleBase
#include <map>                          // for _Rb_tree_const_iterator, etc
#include <utility>                      // for pair

#include "ArmarXCore/core/services/profiler/../../services/tasks/PeriodicTask.h"
#include "ArmarXCore/interface/core/Profiler.h"
#include "ArmarXCore/interface/core/ThreadingIceBase.h"  // for upCast
#include "IceLoggingStrategy.h"


armarx::Profiler::IceLoggingStrategy::IceLoggingStrategy(armarx::ProfilerListenerPrx profilerTopic) :
    profilerListenerPrx(profilerTopic)
{
}

armarx::Profiler::IceLoggingStrategy::~IceLoggingStrategy()
{
}

void armarx::Profiler::IceLoggingStrategy::logEvent(pid_t processId, uint64_t timestamp, const std::string& executableName, const std::string& timestampUnit, const std::string& eventName, const std::string& parentName, const std::string& functionName)
{
    ProfilerEvent event = {processId, Ice::Long(timestamp),  timestampUnit, executableName, eventName, parentName, functionName};
    profilerListenerPrx->reportEvent(event);
}

void armarx::Profiler::IceLoggingStrategy::logStatechartTransition(const ProfilerStatechartTransition& transition)
{
    profilerListenerPrx->reportStatechartTransition(transition);
}

void armarx::Profiler::IceLoggingStrategy::logStatechartInputParameters(pid_t processId, uint64_t timestamp, const std::string& stateIdentifier, const armarx::StateParameterMap& inputParameterMap)
{
    ProfilerStatechartParameters parameters = {processId, Ice::Long(timestamp), stateIdentifier, inputParameterMap};
    profilerListenerPrx->reportStatechartInputParameters(parameters);
}

void armarx::Profiler::IceLoggingStrategy::logStatechartLocalParameters(pid_t processId, uint64_t timestamp, const std::string& stateIdentifier, const armarx::StateParameterMap& localParameterMap)
{
    ProfilerStatechartParameters parameters = {processId, Ice::Long(timestamp), stateIdentifier, localParameterMap};
    profilerListenerPrx->reportStatechartLocalParameters(parameters);
}

void armarx::Profiler::IceLoggingStrategy::logStatechartOutputParameters(pid_t processId, uint64_t timestamp, const std::string& stateIdentifier, const armarx::StateParameterMap& outputParameterMap)
{
    ProfilerStatechartParameters parameters = {processId, Ice::Long(timestamp), stateIdentifier, outputParameterMap};
    profilerListenerPrx->reportStatechartOutputParameters(parameters);
}

void armarx::Profiler::IceLoggingStrategy::logStatechartTransitionWithParameters(pid_t processId, uint64_t timestamp, const TransitionIceBase& transition)
{
    ARMARX_TRACE;

    ProfilerStatechartTransitionWithParameters profilerTransition = armarx::Profiler::IceBufferedLoggingStrategy::toProfilerTransition(processId, timestamp, transition);
    profilerListenerPrx->reportStatechartTransitionWithParameters(profilerTransition);
}

void armarx::Profiler::IceLoggingStrategy::logProcessCpuUsage(pid_t processId, uint64_t timestamp, float cpuUsage)
{
    ProfilerProcessCpuUsage process = {processId, Ice::Long(timestamp), ::armarx::Application::getInstance()->getName(), cpuUsage};
    profilerListenerPrx->reportProcessCpuUsage(process);
}

void armarx::Profiler::IceLoggingStrategy::logProcessMemoryUsage(pid_t processId, u_int64_t timestamp, int memoryUsage)
{
    ProfilerProcessMemoryUsage processMemoryUsage = {processId, Ice::Long(timestamp), ::armarx::Application::getInstance()->getName(), memoryUsage};
    profilerListenerPrx->reportProcessMemoryUsage(processMemoryUsage);
}



#define ARMARX_ICE_LOGGING_BUFFER_SIZE 500

armarx::Profiler::IceBufferedLoggingStrategy::IceBufferedLoggingStrategy(armarx::ProfilerListenerPrx profilerTopic) :
    profilerListenerPrx(profilerTopic)
{
    profilerEvents.reserve(ARMARX_ICE_LOGGING_BUFFER_SIZE);
    profilerStatechartTransitions.reserve(ARMARX_ICE_LOGGING_BUFFER_SIZE);
    profilerStatechartInputParameters.reserve(ARMARX_ICE_LOGGING_BUFFER_SIZE);
    profilerStatechartLocalParameters.reserve(ARMARX_ICE_LOGGING_BUFFER_SIZE);
    profilerStatechartOutputParameters.reserve(ARMARX_ICE_LOGGING_BUFFER_SIZE);
    profilerStatechartTransitionsWithParameters.reserve(ARMARX_ICE_LOGGING_BUFFER_SIZE);
    profilerProcessCpuUsages.reserve(ARMARX_ICE_LOGGING_BUFFER_SIZE);
    profilerProcessMemoryUsages.reserve(ARMARX_ICE_LOGGING_BUFFER_SIZE);

    publisherTask = new PeriodicTask<IceBufferedLoggingStrategy>(this, &IceBufferedLoggingStrategy::publishData, 500);
    publisherTask->start();
}

armarx::Profiler::IceBufferedLoggingStrategy::~IceBufferedLoggingStrategy()
{
    publisherTask->stop();
}

armarx::StateParameterMap armarx::Profiler::IceBufferedLoggingStrategy::copyDictionary(const armarx::StateParameterMap& source)
{
    armarx::StateParameterMap destination;

    for (armarx::StateParameterMap::const_iterator it = source.begin(); it != source.end(); it++)
    {
        destination[it->first] =  armarx::StateParameterIceBasePtr::dynamicCast(it->second->ice_clone());
    }
    return destination;
}

armarx::ProfilerStatechartTransitionWithParameters
armarx::Profiler::IceBufferedLoggingStrategy::toProfilerTransition(pid_t processId, uint64_t timestamp,
        const armarx::TransitionIceBase& transition)
{
    StateIceBasePtr destinationState = StateIceBasePtr::dynamicCast(transition.destinationState);
    ARMARX_CHECK_NOT_NULL(destinationState);

    StateParameterMap inputCopy = copyDictionary(destinationState->inputParameters);
    StateParameterMap localCopy = copyDictionary(destinationState->localParameters);
    StateParameterMap outputCopy;
    if (transition.sourceState)
    {
        outputCopy = copyDictionary(StateIceBasePtr::dynamicCast(transition.sourceState)->outputParameters);
    }

    ProfilerStatechartTransitionWithParameters profilerTransition;
    profilerTransition.processId = processId;
    profilerTransition.timestamp = timestamp;

    profilerTransition.sourceStateIdentifier = transition.sourceState ? StateIceBasePtr::dynamicCast(transition.sourceState)->globalStateIdentifier : "";
    profilerTransition.targetStateIdentifier = destinationState->globalStateIdentifier;
    profilerTransition.targetStateType = destinationState->stateType;
    profilerTransition.eventName = transition.evt->eventName;

    profilerTransition.inputParameters = inputCopy;
    profilerTransition.localParameters = localCopy;
    profilerTransition.outputParameters = outputCopy;

    return profilerTransition;
}


void armarx::Profiler::IceBufferedLoggingStrategy::logEvent(pid_t processId, uint64_t timestamp, const std::string& executableName, const std::string& timestampUnit, const std::string& eventName, const std::string& parentName, const std::string& functionName)
{
    ProfilerEvent event = { processId, Ice::Long(timestamp), executableName, timestampUnit, eventName, parentName, functionName};
    {
        std::unique_lock lock(profilerEventsMutex);
        profilerEvents.push_back(event);
    }
}

void armarx::Profiler::IceBufferedLoggingStrategy::logStatechartTransition(const ProfilerStatechartTransition& transition)
{
    std::unique_lock lock(profilerStatechartTransitionsMutex);
    profilerStatechartTransitions.push_back(transition);
}

void armarx::Profiler::IceBufferedLoggingStrategy::logStatechartInputParameters(pid_t processId, uint64_t timestamp, const std::string& stateIdentifier, const armarx::StateParameterMap& inputParameterMap)
{
    armarx::StateParameterMap copy = copyDictionary(inputParameterMap);

    ProfilerStatechartParameters parameters = { processId, Ice::Long(timestamp), stateIdentifier, copy};
    {
        std::unique_lock lock(profilerStatechartInputParametersMutex);
        profilerStatechartInputParameters.push_back(parameters);
    }
}

void armarx::Profiler::IceBufferedLoggingStrategy::logStatechartLocalParameters(pid_t processId, uint64_t timestamp, const std::string& stateIdentifier, const armarx::StateParameterMap& localParameterMap)
{
    armarx::StateParameterMap copy = copyDictionary(localParameterMap);
    ProfilerStatechartParameters parameters = { processId, Ice::Long(timestamp), stateIdentifier, copy};
    {
        std::unique_lock lock(profilerStatechartLocalParametersMutex);
        profilerStatechartLocalParameters.push_back(parameters);
    }
}

void armarx::Profiler::IceBufferedLoggingStrategy::logStatechartOutputParameters(pid_t processId, uint64_t timestamp, const std::string& stateIdentifier, const armarx::StateParameterMap& outputParameterMap)
{
    armarx::StateParameterMap copy = copyDictionary(outputParameterMap);
    ProfilerStatechartParameters parameters = { processId, Ice::Long(timestamp), stateIdentifier, copy};
    {
        std::unique_lock lock(profilerStatechartOutputParametersMutex);
        profilerStatechartOutputParameters.push_back(parameters);
    }
}

void armarx::Profiler::IceBufferedLoggingStrategy::logStatechartTransitionWithParameters(pid_t processId, uint64_t timestamp, const TransitionIceBase& transition)
{
    ARMARX_TRACE;

    ProfilerStatechartTransitionWithParameters profilerTransition = toProfilerTransition(processId, timestamp, transition);
    {
        std::unique_lock lock(profilerStatechartTransitionsWithParametersMutex);
        profilerStatechartTransitionsWithParameters.push_back(profilerTransition);
    }
}


void armarx::Profiler::IceBufferedLoggingStrategy::logProcessCpuUsage(pid_t processId, uint64_t timestamp, float cpuUsage)
{
    ProfilerProcessCpuUsage process = {processId, Ice::Long(timestamp), ::armarx::Application::getInstance()->getName(), cpuUsage};
    {
        std::unique_lock lock(profilerCpuUsagesMutex);
        profilerProcessCpuUsages.push_back(process);
    }
}

void armarx::Profiler::IceBufferedLoggingStrategy::logProcessMemoryUsage(pid_t processId, uint64_t timestamp, int memoryUsage)
{
    ProfilerProcessMemoryUsage processMemoryUsage = {processId, Ice::Long(timestamp), ::armarx::Application::getInstance()->getName(), memoryUsage};
    {
        std::unique_lock lock(profilerProcessMemoryUsagesMutex);
        profilerProcessMemoryUsages.push_back(processMemoryUsage);
    }
}

void armarx::Profiler::IceBufferedLoggingStrategy::publishData()
{
    if (!profilerEvents.empty())
    {
        ProfilerEventList eventsCopy;
        {
            std::unique_lock lock(profilerEventsMutex);
            profilerEvents.swap(eventsCopy);
        }
        profilerListenerPrx->reportEventList(eventsCopy);
    }

    if (!profilerStatechartTransitions.empty())
    {
        ProfilerStatechartTransitionList transitionsCopy;
        {
            std::unique_lock lock(profilerStatechartTransitionsMutex);
            profilerStatechartTransitions.swap(transitionsCopy);
        }
        profilerListenerPrx->reportStatechartTransitionList(transitionsCopy);
    }

    if (!profilerStatechartInputParameters.empty())
    {
        ProfilerStatechartParametersList parametersCopy;
        {
            std::unique_lock lock(profilerStatechartInputParametersMutex);
            profilerStatechartInputParameters.swap(parametersCopy);
        }
        profilerListenerPrx->reportStatechartInputParametersList(parametersCopy);
    }

    if (!profilerStatechartLocalParameters.empty())
    {
        ProfilerStatechartParametersList parametersCopy;
        {
            std::unique_lock lock(profilerStatechartLocalParametersMutex);
            profilerStatechartLocalParameters.swap(parametersCopy);
        }
        profilerListenerPrx->reportStatechartLocalParametersList(parametersCopy);
    }

    if (!profilerStatechartOutputParameters.empty())
    {
        ProfilerStatechartParametersList parametersCopy;
        {
            std::unique_lock lock(profilerStatechartOutputParametersMutex);
            profilerStatechartOutputParameters.swap(parametersCopy);
        }
        profilerListenerPrx->reportStatechartOutputParametersList(parametersCopy);
    }

    if (!profilerStatechartTransitionsWithParameters.empty())
    {
        ProfilerStatechartTransitionWithParametersList transitionsWithParametersCopy;
        {
            std::unique_lock lock(profilerStatechartTransitionsWithParametersMutex);
            profilerStatechartTransitionsWithParameters.swap(transitionsWithParametersCopy);
        }
        profilerListenerPrx->reportStatechartTransitionWithParametersList(transitionsWithParametersCopy);
    }

    if (!profilerProcessCpuUsages.empty())
    {
        ProfilerProcessCpuUsageList cpuUsagesCopy;
        {
            std::unique_lock lock(profilerCpuUsagesMutex);
            profilerProcessCpuUsages.swap(cpuUsagesCopy);
        }
        profilerListenerPrx->reportProcessCpuUsageList(cpuUsagesCopy);
    }

    if (!profilerProcessMemoryUsages.empty())
    {
        ProfilerProcessMemoryUsageList memoryUsageCopy;
        {
            std::unique_lock lock(profilerProcessMemoryUsagesMutex);
            profilerProcessMemoryUsages.swap(memoryUsageCopy);
        }

        profilerListenerPrx->reportProcessMemoryUsageList(memoryUsageCopy);
    }
}

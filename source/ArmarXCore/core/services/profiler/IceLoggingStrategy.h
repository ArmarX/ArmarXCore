#pragma once

/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core::services::profiler
 * @author     Manfred Kroehnert ( manfred dot kroehnert at dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <ArmarXCore/interface/core/Profiler.h>

#include <stdint.h>                     // for uint64_t
#include <sys/types.h>                  // for pid_t, u_int64_t
#include <string>                       // for string, operator<<
#include <vector>                       // for vector
#include <mutex>

#include "../../services/tasks/PeriodicTask.h"  // for PeriodicTask, etc
#include "ArmarXCore/interface/statechart/StatechartIce.h"
#include "LoggingStrategy.h"            // for LoggingStrategy

namespace armarx::Profiler
{
    class IceLoggingStrategy;

    using IceLoggingStrategyPtr = std::shared_ptr<IceLoggingStrategy>;

    /**
     * @class IceLoggingStrategy
     * @ingroup Profiling
     * @brief IceLoggingStrategy publishes incoming log method calls directly on IceLoggingStrategy::profilerListenerPrx.
     *
     * Instances of this strategy object is used by armarx::Profiler.
     */
    class IceLoggingStrategy :
        virtual public LoggingStrategy
    {
    public:
        IceLoggingStrategy(ProfilerListenerPrx profilerTopic);

        ~IceLoggingStrategy() override;

        void logEvent(pid_t processId, uint64_t timestamp, const std::string& executableName, const std::string& timestampUnit, const std::string& eventName, const std::string& parentName, const std::string& functionName) override;

        void logStatechartTransition(const ProfilerStatechartTransition& transition) override;
        void logStatechartInputParameters(pid_t processId, uint64_t timestamp, const std::string& stateIdentifier, const armarx::StateParameterMap& inputParameterMap) override;
        void logStatechartLocalParameters(pid_t processId, uint64_t timestamp, const std::string& stateIdentifier, const armarx::StateParameterMap& localParameterMap) override;
        void logStatechartOutputParameters(pid_t processId, uint64_t timestamp, const std::string& stateIdentifier, const armarx::StateParameterMap& outputParameterMap) override;

        void logStatechartTransitionWithParameters(pid_t processId, uint64_t timestamp, const TransitionIceBase& transition) override;

        void logProcessCpuUsage(pid_t processId, uint64_t timestamp, float cpuUsage) override;
        void logProcessMemoryUsage(pid_t processId, u_int64_t timestamp, int memoryUsage) override;
    protected:
        ProfilerListenerPrx profilerListenerPrx;
    };


    /**
     * @class IceBufferdLoggingStrategy
     * @ingroup Profiling
     * @brief IceBufferdLoggingStrategy buffers incoming log method calls and publishes them as collections on IceLoggingStrategy::profilerListenerPrx.
     *
     * Instances of this strategy object is used by armarx::Profiler.
     */
    class IceBufferedLoggingStrategy :
        virtual public LoggingStrategy
    {
    public:
        IceBufferedLoggingStrategy(ProfilerListenerPrx profilerTopic);

        ~IceBufferedLoggingStrategy() override;

        void logEvent(pid_t processId, uint64_t timestamp, const std::string& executableName, const std::string& timestampUnit, const std::string& eventName, const std::string& parentName, const std::string& functionName) override;

        void logStatechartTransition(const ProfilerStatechartTransition& transition) override;
        void logStatechartInputParameters(pid_t processId, uint64_t timestamp, const std::string& stateIdentifier, const armarx::StateParameterMap& inputParameterMap) override;
        void logStatechartLocalParameters(pid_t processId, uint64_t timestamp, const std::string& stateIdentifier, const armarx::StateParameterMap& localParameterMap) override;
        void logStatechartOutputParameters(pid_t processId, uint64_t timestamp, const std::string& stateIdentifier, const armarx::StateParameterMap& outputParameterMap) override;

        void logStatechartTransitionWithParameters(pid_t processId, uint64_t timestamp, const TransitionIceBase& transition) override;

        void logProcessCpuUsage(pid_t processId, uint64_t timestamp, float cpuUsage) override;
        void logProcessMemoryUsage(pid_t processId, u_int64_t timestamp, int memoryUsage) override;

        static ProfilerStatechartTransitionWithParameters toProfilerTransition(pid_t processId, uint64_t timestamp, const TransitionIceBase& transition);

    protected:
        void publishData();
        static armarx::StateParameterMap copyDictionary(const armarx::StateParameterMap& source);


        std::mutex profilerEventsMutex;
        ProfilerEventList profilerEvents;

        std::mutex profilerStatechartTransitionsMutex;
        ProfilerStatechartTransitionList profilerStatechartTransitions;

        std::mutex profilerStatechartInputParametersMutex;
        ProfilerStatechartParametersList profilerStatechartInputParameters;

        std::mutex profilerStatechartLocalParametersMutex;
        ProfilerStatechartParametersList profilerStatechartLocalParameters;

        std::mutex profilerStatechartOutputParametersMutex;
        ProfilerStatechartParametersList profilerStatechartOutputParameters;

        std::mutex profilerStatechartTransitionsWithParametersMutex;
        ProfilerStatechartTransitionWithParametersList profilerStatechartTransitionsWithParameters;

        std::mutex profilerCpuUsagesMutex;
        ProfilerProcessCpuUsageList profilerProcessCpuUsages;

        std::mutex profilerProcessMemoryUsagesMutex;
        ProfilerProcessMemoryUsageList profilerProcessMemoryUsages;

        ProfilerListenerPrx profilerListenerPrx;

        PeriodicTask<IceBufferedLoggingStrategy>::pointer_type publisherTask;
    };
}

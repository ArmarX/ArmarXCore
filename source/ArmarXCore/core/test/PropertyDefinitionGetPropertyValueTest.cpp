/**
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Rainer Kartmann (rainer dot kartmann at kit dot edu)
* @date       2019
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::PropertyDefinitionEigenVectorTest
#define ARMARX_BOOST_TEST

#include <ArmarXCore/Test.h>

#include <Eigen/Core>

// ArmarXCore
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/application/properties/Properties.h>
#include <ArmarXCore/core/application/properties/IceProperties.h>
#include <ArmarXCore/core/exceptions/local/InvalidPropertyValueException.h>
#include <ArmarXCore/core/exceptions/local/MissingRequiredPropertyException.h>
#include <ArmarXCore/core/exceptions/local/UnmappedValueException.h>


#include <string>
#include <iostream>


using namespace armarx;

// Once this is supported, we can enable the test. (Now that I have written it ...)
static const bool CHECK_VECTOR_INT = false;


class TestPropertyDefinitions : public ComponentPropertyDefinitions
{
public:
    TestPropertyDefinitions(std::string identifier) :
        ComponentPropertyDefinitions(identifier)
    {
        defineOptionalProperty<std::string>("TestProperty", "Success", "...");

        defineRequiredProperty<std::string>("req-string", "...");
        defineRequiredProperty<int>("req-int", "...");
        defineRequiredProperty<float>("req-float", "...");
        defineRequiredProperty<std::vector<std::string>>("req-vector-string", "...");
        defineRequiredProperty<std::vector<int>>("req-vector-int", "...");

        defineRequiredProperty<int>("req-missing-test", "...");

        defineOptionalProperty<std::string>("opt-string", "string", "...");
        defineOptionalProperty<int>("opt-int", 42, "...");
        defineOptionalProperty<float>("opt-float", -3.5, "...");
        defineOptionalProperty<std::vector<std::string>>("opt-vector-string", { "a", "bc", "", "e" }, "...");
        defineOptionalProperty<std::vector<int>>("opt-vector-int", { -6, 0, 6 }, "...");
    }
};


struct PropertyFixture
{
    PropertyDefinitionsPtr definitions = new TestPropertyDefinitions("");
    Ice::PropertiesPtr properties = new IceProperties(Ice::createProperties());

    PropertyFixture()
    {
        // Validate test setup.
        BOOST_REQUIRE_EQUAL(getProperty<std::string>("TestProperty").getValue(), "Success");
        setPropertyString("TestProperty", "UpdateSuccess");
        BOOST_REQUIRE_EQUAL(getProperty<std::string>("TestProperty").getValue(), "UpdateSuccess");

        BOOST_TEST_MESSAGE("Vector default: "
                           << definitions->getDefintion<std::vector<int>>("opt-vector-int").getDefaultAsString());
    }

    void setPropertyString(const std::string& name, const std::string& value)
    {
        properties->setProperty(definitions->getPrefix() + name, value);
    }

    template <class T>
    Property<T> getProperty(const std::string& name)
    {
        return Property<T>(definitions->getDefintion<T>(name), definitions->getPrefix(), properties);
    }
};


BOOST_FIXTURE_TEST_SUITE(GetPropertyTest, PropertyFixture)


BOOST_AUTO_TEST_CASE(test_get_default_value)
{
    BOOST_CHECK_EQUAL(getProperty<std::string>("opt-string").getValue(), "string");
    BOOST_CHECK_EQUAL(getProperty<int>("opt-int").getValue(), 42);
    BOOST_CHECK_EQUAL(getProperty<float>("opt-float").getValue(), -3.5);

    {
        const std::vector<std::string> vector = getProperty<std::vector<std::string>>("opt-vector-string").getValue();
        const std::vector<std::string> expected = {"a", "bc", "", "e"};
        BOOST_CHECK_EQUAL_COLLECTIONS(vector.begin(), vector.end(), expected.begin(), expected.end());
    }
    {
        const std::vector<int> vector = getProperty<std::vector<int>>("opt-vector-int").getValue();
        const std::vector<int> expected = {-6, 0, 6};
        BOOST_CHECK_EQUAL_COLLECTIONS(vector.begin(), vector.end(), expected.begin(), expected.end());
    }
}


BOOST_AUTO_TEST_CASE(test_get_custom_value_optional)
{
    setPropertyString("opt-string", "custom");
    setPropertyString("opt-int", "-21");
    setPropertyString("opt-float", "7.25");
    setPropertyString("opt-vector-string", "rs,t,,v,xyz"); // Empty entries are discarded.
    setPropertyString("opt-vector-int", "9,0,-9");

    BOOST_CHECK_EQUAL(getProperty<std::string>("opt-string").getValue(), "custom");
    BOOST_CHECK_EQUAL(getProperty<int>("opt-int").getValue(), -21);
    BOOST_CHECK_EQUAL(getProperty<float>("opt-float").getValue(), 7.25);

    {
        const std::vector<std::string> vector = getProperty<std::vector<std::string>>("opt-vector-string").getValue();
        const std::vector<std::string> expected = {"rs", "t", "v", "xyz"};
        BOOST_CHECK_EQUAL_COLLECTIONS(vector.begin(), vector.end(), expected.begin(), expected.end());
    }
    if (CHECK_VECTOR_INT)
    {
        const std::vector<int> vector = getProperty<std::vector<int>>("opt-vector-int").getValue();
        const std::vector<int> expected = {9, 0, -9};
        BOOST_CHECK_EQUAL_COLLECTIONS(vector.begin(), vector.end(), expected.begin(), expected.end());
    }
}


BOOST_AUTO_TEST_CASE(test_get_custom_value_required)
{
    setPropertyString("req-string", "custom");
    setPropertyString("req-int", "-21");
    setPropertyString("req-float", "7.25");
    setPropertyString("req-vector-int", "9;0;-9");

    BOOST_CHECK_EQUAL(getProperty<std::string>("req-string").getValue(), "custom");
    BOOST_CHECK_EQUAL(getProperty<int>("req-int").getValue(), -21);
    BOOST_CHECK_EQUAL(getProperty<float>("req-float").getValue(), 7.25);

    if (CHECK_VECTOR_INT)
    {
        const std::vector<int> vector = getProperty<std::vector<int>>("req-vector-int").getValue();
        const std::vector<int> expected = {9, 0, -9};
        BOOST_CHECK_EQUAL_COLLECTIONS(vector.begin(), vector.end(), expected.begin(), expected.end());
    }
}


BOOST_AUTO_TEST_CASE(test_get_invalid_value)
{
    setPropertyString("opt-int", "abc");
    setPropertyString("opt-float", "7,25");
    setPropertyString("opt-vector-int", "9,zero,-9");

    BOOST_CHECK_THROW(getProperty<int>("opt-int").getValue(),
                      exceptions::local::InvalidPropertyValueException);
    BOOST_CHECK_THROW(getProperty<float>("opt-float").getValue(),
                      exceptions::local::InvalidPropertyValueException);
    BOOST_CHECK_THROW(getProperty<std::vector<int>>("opt-vector-int").getValue(),
                      armarx::LocalException);
    BOOST_CHECK_THROW(getProperty<int>("req-missing-test").getValue(),
                      exceptions::local::MissingRequiredPropertyException);
}


BOOST_AUTO_TEST_SUITE_END()

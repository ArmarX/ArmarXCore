#define BOOST_TEST_MODULE ArmarX::Core::Time
#define ARMARX_BOOST_TEST


#include <sstream>

#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/time.h>
#include <ArmarXCore/core/time/CycleUtil.h>


BOOST_AUTO_TEST_CASE(CoreTypesTest)
{
    using namespace armarx;

    BOOST_CHECK_EQUAL(Duration::MicroSeconds(1).toMicroSeconds(), 1);
    BOOST_CHECK_EQUAL(Duration::MilliSeconds(1).toMicroSeconds(), 1'000);
    BOOST_CHECK_EQUAL(Duration::Seconds(1).toMicroSeconds(), 1'000'000);
}


BOOST_AUTO_TEST_CASE(CoreTypesOperatorsTest)
{
    using namespace armarx;

    const DateTime dt = Clock::Now();
    const Duration d = Duration::Seconds(5);
    const DateTime dt_plus_d = dt + d;

    BOOST_CHECK_EQUAL(dt_plus_d.toMicroSecondsSinceEpoch(),
                      dt.toMicroSecondsSinceEpoch() + d.toMicroSeconds());

    DateTime dtcp = dt;
    dtcp += Duration::MicroSeconds(123);

    BOOST_CHECK_EQUAL(dtcp.toMicroSecondsSinceEpoch(), dt.toMicroSecondsSinceEpoch() + 123);
}


BOOST_AUTO_TEST_CASE(FrequencyTest)
{
    using namespace armarx;

    const Frequency f1_60Hz = 60 / Duration::Seconds(1);  // 60 Hz.
    const Frequency f2_60Hz = Frequency::Hertz(60);  // 60 Hz.
    const Frequency f3_30Hz = 60 / Duration::Seconds(2);  // 60 cycles per 2 seconds => 30 Hz.

    BOOST_CHECK_EQUAL(f1_60Hz, f2_60Hz);

    BOOST_CHECK_LT(f3_30Hz, f1_60Hz);
    BOOST_CHECK_LE(f3_30Hz, f1_60Hz);
}


BOOST_AUTO_TEST_CASE(DateTimeToString1)
{
    using namespace armarx;

    const DateTime dt{1'646'419'509'261'082}; // Some timestamp to simulate now();
    BOOST_CHECK_EQUAL(dt.toDateString(), "2022-03-04");
}


BOOST_AUTO_TEST_CASE(DateTimeToString2)
{
    using namespace armarx;

    const DateTime dt{1'646'419'509'261'082}; // Some timestamp to simulate now();
    BOOST_CHECK_EQUAL(dt.toTimeString(), "19-45-09.261082");
}


BOOST_AUTO_TEST_CASE(DateTimeToString3)
{
    using namespace armarx;

    const DateTime dt{1'646'419'509'261'082}; // Some timestamp to simulate now();

    std::stringstream ss;
    ss << dt;
    BOOST_CHECK_EQUAL(ss.str(), "2022-03-04_19-45-09.261082");
}


BOOST_AUTO_TEST_CASE(DurationToString1)
{
    using namespace armarx;

    const Duration d = Duration::MicroSeconds(10);

    std::stringstream ss;
    ss << d;
    BOOST_CHECK_EQUAL(ss.str(), "10µs");
}


BOOST_AUTO_TEST_CASE(DurationToString2)
{
    using namespace armarx;

    const Duration d = Duration::Minutes(10) + Duration::Seconds(10) + Duration::MilliSeconds(987);
    BOOST_CHECK_EQUAL(d.toDurationString("%Mm %S.%%msecs"), "10m 10.987s");
}


BOOST_AUTO_TEST_CASE(FrequencyToString1)
{
    using namespace armarx;

    const Frequency f1_60Hz = 60 / Duration::Seconds(1);  // 60 Hz.
    const Frequency f2_60Hz = Frequency::Hertz(60);  // 60 Hz.
    const Frequency f3_30Hz = 60 / Duration::Seconds(2);  // 60 cycles per 2 seconds => 30 Hz.
    const Frequency f4_0Hz = Frequency::Hertz(0);

    BOOST_CHECK_EQUAL(f1_60Hz.toFrequencyString(), "60Hz");
    BOOST_CHECK_EQUAL(f2_60Hz.toFrequencyString(), "60Hz");
    BOOST_CHECK_EQUAL(f3_30Hz.toFrequencyString(), "30Hz");
    BOOST_CHECK_EQUAL(f4_0Hz.toFrequencyString(), "0Hz");
}


BOOST_AUTO_TEST_CASE(MetronomeTest)
{
    using namespace armarx;

    Metronome rl{Frequency::Hertz(100), ClockType::Realtime};

    for (int i = 0; i < 100; ++i)
    {
        const Duration waitTime = rl.waitForNextTick();
        BOOST_CHECK_LE(waitTime, Duration::MilliSeconds(10));
    }
}

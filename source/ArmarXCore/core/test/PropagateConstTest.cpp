/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::PropagateConst
#define ARMARX_BOOST_TEST

#include <memory>

#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/util/PropagateConst.h>

using namespace armarx;

struct T
{
    int f()
    {
        return 0;
    }
    int f() const
    {
        return 1;
    }
};

T globalT;

struct Container
{
    PropagateConst<T*> p {&globalT};
    PropagateConst<std::unique_ptr<T>> u {new T};
    PropagateConst<std::shared_ptr<T>> s {new T};
};

BOOST_AUTO_TEST_CASE(testPropagateConstPropagate)
{
    Container n;
    BOOST_CHECK_EQUAL(n.p->f(), 0);
    BOOST_CHECK_EQUAL(n.u->f(), 0);
    BOOST_CHECK_EQUAL(n.s->f(), 0);

    const Container c;
    BOOST_CHECK_EQUAL(c.p->f(), 1);
    BOOST_CHECK_EQUAL(c.u->f(), 1);
    BOOST_CHECK_EQUAL(c.s->f(), 1);
}

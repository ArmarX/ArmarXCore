#define BOOST_TEST_MODULE ArmarX::Core::ice_conversions
#define ARMARX_BOOST_TEST


#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/ice_conversions.h>

#include <Ice/Object.h>


namespace IceConversionsTest
{

struct MyInnerClass
{
    float value = 42.42;

    bool operator==(const MyInnerClass& o) const
    {
        return value == o.value;
    }
    bool operator!=(const MyInnerClass& o) const
    {
        return not (*this == o);
    }
};


// The business object (BO) types (C++ types).

struct MyClass
{
    int i = 42;
    std::string string = "twenty four";
    MyInnerClass inner;

    bool operator==(const MyClass& o) const
    {
        return i == o.i and string == o.string and inner == o.inner;
    }
    bool operator!=(const MyClass& o) const
    {
        return not (*this == o);
    }
};

std::ostream& operator<<(std::ostream& os, const MyInnerClass& rhs)
{
    return os << "value = " << rhs.value;
}
std::ostream& operator<<(std::ostream& os, const MyClass& rhs)
{
    return os << "i = " << rhs.i
              << ", string = " << rhs.string
              << ", inner = (" << rhs.inner << ")";
}


// The data transfer object (DTO) types ("ice types").

namespace dto
{
    struct MyInnerClass
    {
        float value = 42.42;

        bool operator==(const MyInnerClass& o) const
        {
            return value == o.value;
        }
        bool operator!=(const MyInnerClass& o) const
        {
            return not (*this == o);
        }
    };

    struct MyClass : public virtual ::Ice::Object
    {
        int i = 42;
        std::string string = "twenty four";
        MyInnerClass inner;

        bool operator==(const MyClass& o) const
        {
            return i == o.i and string == o.string and inner == o.inner;
        }
        bool operator!=(const MyClass& o) const
        {
            return not (*this == o);
        }
    };

    std::ostream& operator<<(std::ostream& os, const MyInnerClass& rhs)
    {
        return os << "value = " << rhs.value;
    }
    std::ostream& operator<<(std::ostream& os, const MyClass& rhs)
    {
        return os << "i = " << rhs.i
                  << ", string = " << rhs.string
                  << ", inner = (" << rhs.inner << ")";
    }
}




struct Fixture
{
    Fixture()
    {
    }
    ~Fixture()
    {
    }

    const MyClass bo;
    const dto::MyClass dto;

    const std::vector<MyClass> bos;
    const std::vector<dto::MyClass> dtos;
};

}


// In the BO's namespace.

namespace IceConversionsTest
{

void toIce(dto::MyInnerClass& dto, const MyInnerClass& bo)
{
    dto.value = bo.value;
}
void fromIce(const dto::MyInnerClass& dto, MyInnerClass& bo)
{
    bo.value = dto.value;
}
void toIce(dto::MyClass& dto, const MyClass& bo)
{
    using armarx::toIce;

    toIce(dto.i, bo.i);
    toIce(dto.string, bo.string);
    toIce(dto.inner, bo.inner);
}
void fromIce(const dto::MyClass& dto, MyClass& bo)
{
    using armarx::fromIce;

    fromIce(dto.i, bo.i);
    fromIce(dto.string, bo.string);
    fromIce(dto.inner, bo.inner);
}

}


namespace IceConversionsTest::dto
{

::Ice::Object* upCast(MyClass* p)
{
    return p;
}

}


BOOST_FIXTURE_TEST_SUITE(IceConversionsTest, Fixture)


BOOST_AUTO_TEST_CASE(test_toIce_adl)
{
    // Works via ADL.

    dto::MyClass dto;
    toIce(dto, bo);
    BOOST_CHECK_EQUAL(dto, this->dto);
}

BOOST_AUTO_TEST_CASE(test_fromIce_adl)
{
    // Works via ADL.

    MyClass bo;
    fromIce(dto, bo);
    BOOST_CHECK_EQUAL(bo, this->bo);
}


BOOST_AUTO_TEST_CASE(test_vector_toIce_fromIce_compile_test)
{
    std::vector<dto::MyClass> dtos;
    std::vector<MyClass> bos;

    // Just using the armarx namespace does not work.
    {
        using namespace armarx;

        // toIce(dtos, this->bos);
        // fromIce(this->dtos, bos);
    }

    // You need to specify armarx:: explicitly.
    {
        armarx::toIce(dtos, this->bos);
        armarx::fromIce(this->dtos, bos);
    }

    // Or explicitly import the templates into the current namespace
    // (e.g., you can do this once in your ice_conversions.cpp).
    {
        using armarx::toIce;
        using armarx::fromIce;

        toIce(dtos, this->bos);
        fromIce(this->dtos, bos);
    }

    std::vector<std::vector<dto::MyClass>> dtoss;
    std::vector<std::vector<MyClass>> boss;

    {
        using armarx::toIce;
        using armarx::fromIce;

        toIce(dtoss, boss);
        fromIce(dtoss, boss);
    }

    BOOST_CHECK(true);
}


BOOST_AUTO_TEST_CASE(test_return_toIce)
{
    // Requires explicit name import.
    using armarx::toIce;

    dto::MyClass dto = toIce<dto::MyClass>(bo);
    BOOST_CHECK_EQUAL(dto, this->dto);
}

BOOST_AUTO_TEST_CASE(test_return_fromIce)
{
    // Requires explicit name import.
    using armarx::fromIce;

    MyClass bo = fromIce<MyClass>(dto);
    BOOST_CHECK_EQUAL(bo, this->bo);
}


BOOST_AUTO_TEST_CASE(test_vector_toIce)
{
    using armarx::toIce;

    // Vector using the template.
    std::vector<dto::MyClass> dtos;
    toIce(dtos, bos);
    BOOST_CHECK_EQUAL_COLLECTIONS(dtos.begin(), dtos.end(),
                                  this->dtos.begin(), this->dtos.end());
}

BOOST_AUTO_TEST_CASE(test_vector_fromIce)
{
    using armarx::fromIce;

    // Vector using the template.
    std::vector<MyClass> bos;
    fromIce(dtos, bos);
    BOOST_CHECK_EQUAL_COLLECTIONS(bos.begin(), bos.end(),
                                  this->bos.begin(), this->bos.end());
}


BOOST_AUTO_TEST_CASE(test_optional_toIce)
{
    using armarx::toIce;

    {
        const std::optional<MyClass> bo;
        IceInternal::Handle<dto::MyClass> dto;
        toIce(dto, bo);
        BOOST_CHECK(not dto);
    }
    {
        const std::optional<MyClass> bo = MyClass{};
        const auto dto = toIce<IceInternal::Handle<dto::MyClass>>(bo);
        BOOST_REQUIRE(dto);
        BOOST_CHECK_EQUAL(*dto, this->dto);
    }
}

BOOST_AUTO_TEST_CASE(test_optional_fromIce)
{
    using armarx::fromIce;

    {
        const IceInternal::Handle<dto::MyClass> dto;
        std::optional<MyClass> bo;
        fromIce(dto, bo);
        BOOST_CHECK(not bo.has_value());
    }
    {
        const IceInternal::Handle<dto::MyClass> dto { new dto::MyClass };
        const auto bo = fromIce<std::optional<MyClass>>(dto);
        BOOST_REQUIRE(bo.has_value());
        BOOST_CHECK_EQUAL(bo.value(), this->bo);
    }
}


BOOST_AUTO_TEST_CASE(test_vector_in_map_toIce_fromIce_compiles)
{
    using armarx::toIce;
    using armarx::fromIce;

    {
        std::vector<std::map<std::string, MyClass>> bo;
        std::vector<std::map<std::string, dto::MyClass>> dto;
        toIce(dto, bo);
        fromIce(dto, bo);
    }
    {
        std::map<std::string, std::vector<MyClass>> bo;
        std::map<std::string, std::vector<dto::MyClass>> dto;
        toIce(dto, bo);
        fromIce(dto, bo);
    }

    BOOST_CHECK(true);
}


BOOST_AUTO_TEST_SUITE_END()

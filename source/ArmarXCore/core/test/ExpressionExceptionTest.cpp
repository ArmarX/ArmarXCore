/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core::Test
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::Core::ExpressionException
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/test/IceTestHelper.h>

#include<ArmarXCore/core/exceptions/local/ExpressionException.h>

#define check_do_throw(...) BOOST_CHECK_THROW(   do { __VA_ARGS__ ; } while(0), ::armarx::LocalException)
#define check_no_throw(...) BOOST_CHECK_NO_THROW(do { __VA_ARGS__ ; } while(0))


BOOST_AUTO_TEST_CASE(ExpressionExceptionTest)
{

    check_no_throw(ARMARX_CHECK_EXPRESSION(true));
    check_do_throw(ARMARX_CHECK_EXPRESSION(false));

    check_do_throw(ARMARX_CHECK_LESS(1, 1));
    check_do_throw(ARMARX_CHECK_LESS(1, -1));
    check_do_throw(ARMARX_CHECK_LESS(0, 0));
    check_no_throw(ARMARX_CHECK_LESS(-1, 1));

    check_no_throw(ARMARX_CHECK_LESS_EQUAL(1, 1));
    check_do_throw(ARMARX_CHECK_LESS_EQUAL(1, -1));
    check_no_throw(ARMARX_CHECK_LESS_EQUAL(0, 0));
    check_no_throw(ARMARX_CHECK_LESS_EQUAL(-1, 1));

    check_do_throw(ARMARX_CHECK_GREATER(1, 1));
    check_no_throw(ARMARX_CHECK_GREATER(1, -1));
    check_do_throw(ARMARX_CHECK_GREATER(0, 0));
    check_do_throw(ARMARX_CHECK_GREATER(-1, 1));

    check_no_throw(ARMARX_CHECK_GREATER_EQUAL(1, 1));
    check_no_throw(ARMARX_CHECK_GREATER_EQUAL(1, -1));
    check_no_throw(ARMARX_CHECK_GREATER_EQUAL(0, 0));
    check_do_throw(ARMARX_CHECK_GREATER_EQUAL(-1, 1));

    std::vector<int> vec;
    check_no_throw(ARMARX_CHECK_EQUAL(1, 1));
    check_do_throw(ARMARX_CHECK_EQUAL(1, -1));
    check_no_throw(ARMARX_CHECK_EQUAL(0, 0));
    check_no_throw(ARMARX_CHECK_EQUAL(0, vec.size()));
    check_no_throw(ARMARX_CHECK_EQUAL(0u, 0));
    check_no_throw(ARMARX_CHECK_EQUAL(0, 0l));
    check_no_throw(ARMARX_CHECK_EQUAL(0.0, 0));
    check_no_throw(ARMARX_CHECK_EQUAL(0, 0.f));
    check_do_throw(ARMARX_CHECK_EQUAL(-1, 1));

    check_do_throw(ARMARX_CHECK_NOT_EQUAL(1, 1));
    check_no_throw(ARMARX_CHECK_NOT_EQUAL(1, -1));
    check_do_throw(ARMARX_CHECK_NOT_EQUAL(0, 0));
    check_no_throw(ARMARX_CHECK_NOT_EQUAL(-1, 1));



    //            ARMARX_CHECK_POSITIVE
    //            ARMARX_CHECK_NONNEGATIVE
    //            ARMARX_CHECK_FITS_SIZE
    //            ARMARX_CHECK_FINITE
    //            ARMARX_CHECK_IS_NULL
    //            ARMARX_CHECK_NOT_NULL
    //            ARMARX_CHECK_EMPTY
    //            ARMARX_CHECK_NOT_EMPTY
    //            ARMARX_CHECK_MULTIPLE_OF
    //            ARMARX_CHECK_NOT_MULTIPLE_OF
}




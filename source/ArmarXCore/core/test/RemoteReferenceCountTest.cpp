/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core::Test
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::Core::RemoteReferenceCount
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/test/IceTestHelper.h>
#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

using namespace armarx;

class TestApp : public Application
{
public:
    void setup(const ManagedIceObjectRegistryInterfacePtr& registry, Ice::PropertiesPtr properties) override {}

    ArmarXManagerPtr getArmarXManager()
    {
        return Application::getArmarXManager();
    }
};

class TestComponent : public Component
{
public:
    void onInitComponent() override {}
    void onConnectComponent() override {}
    std::string getDefaultName() const override
    {
        return "TestComponent";
    }
};

BOOST_AUTO_TEST_CASE(testRemoteReferenceCount)
{
    const auto delay = std::chrono::seconds {2};
    std::cout << "creating a manager\n";
    IceTestHelper iceTestHelper;
    iceTestHelper.startEnvironment();
    TestArmarXManagerPtr manager = new TestArmarXManager(
        "RemoteReferenceCountTest",
        iceTestHelper.getCommunicator());
    std::cout << "creating a manager...DONE!\n";

    std::cout << "base setup\n";
    int data0 = 0;
    int data1 = 0;
    int data2 = 0;
    int data3 = 0;

    BOOST_CHECK_EQUAL(data0, 0);
    BOOST_CHECK_EQUAL(data1, 0);
    BOOST_CHECK_EQUAL(data2, 0);
    BOOST_CHECK_EQUAL(data3, 0);

    BOOST_CHECK(manager);
    ARMARX_CHECK_EXPRESSION(manager);

    std::cout << "creating refcounts\n";
    auto block0  = manager->createRemoteReferenceCount(
                       [](int* i)
    {
        *i = 1;
        std::cout << "setting value for " << i << std::endl;
    }, &data0,
    "foo",
    IceUtil::Time::seconds(1), IceUtil::Time::seconds(2), 500);

    auto block1  = manager->createRemoteReferenceCount(
                       [&data1]()
    {
        data1 = 1;
        std::cout << "setting value for " << &data1 << std::endl;
    },
    "foo",
    IceUtil::Time::seconds(1), IceUtil::Time::seconds(2), 500);

    auto block2  = manager->createSimpleRemoteReferenceCount(
                       [](int* i)
    {
        *i = 1;
        std::cout << "setting value for " << i << std::endl;
    }, &data2, "foo", IceUtil::Time::seconds(1));

    auto block3  = manager->createSimpleRemoteReferenceCount(
                       [&data3]()
    {
        data3 = 1;
        std::cout << "setting value for " << &data3 << std::endl;
    }, "foo", IceUtil::Time::seconds(1));
    std::cout << "creating refcounts...DONE!\n";

    BOOST_CHECK(block0);
    BOOST_CHECK(block1);
    BOOST_CHECK(block2);
    BOOST_CHECK(block3);

    BOOST_CHECK_EQUAL(data0, 0);
    BOOST_CHECK_EQUAL(data1, 0);
    BOOST_CHECK_EQUAL(data2, 0);
    BOOST_CHECK_EQUAL(data3, 0);

    std::cout << "creating counter0\n";
    auto counter0_0 = block0->getReferenceCounter();
    std::cout << "creating counter1\n";
    auto counter1_0 = block1->getReferenceCounter();
    std::cout << "creating counter2\n";
    auto counter2_0 = block2->getReferenceCounter();
    std::cout << "creating counter3\n";
    auto counter3_0 = block3->getReferenceCounter();
    std::cout << "creating counters...DONE!\n";

    BOOST_CHECK_EQUAL(data0, 0);
    BOOST_CHECK_EQUAL(data1, 0);
    BOOST_CHECK_EQUAL(data2, 0);
    BOOST_CHECK_EQUAL(data3, 0);

    counter0_0 = nullptr;
    counter1_0 = nullptr;
    counter2_0 = nullptr;
    counter3_0 = nullptr;

    std::cout << "sleeping\n";
    std::this_thread::sleep_for(delay);
    std::cout << "sleeping...DONE!\n";

    BOOST_CHECK_EQUAL(data0, 0);
    BOOST_CHECK_EQUAL(data1, 0);
    BOOST_CHECK_EQUAL(data2, 0);
    BOOST_CHECK_EQUAL(data3, 0);

    std::cout << "creating counter0\n";
    counter0_0 = block0->getReferenceCounter();
    std::cout << "creating counter1\n";
    counter1_0 = block1->getReferenceCounter();
    std::cout << "creating counter2\n";
    counter2_0 = block2->getReferenceCounter();
    std::cout << "creating counter3\n";
    counter3_0 = block3->getReferenceCounter();
    std::cout << "creating counters...DONE!\n";

    BOOST_CHECK_EQUAL(data0, 0);
    BOOST_CHECK_EQUAL(data1, 0);
    BOOST_CHECK_EQUAL(data2, 0);
    BOOST_CHECK_EQUAL(data3, 0);

    counter0_0 = nullptr;
    counter1_0 = nullptr;
    counter2_0 = nullptr;
    counter3_0 = nullptr;

    std::cout << "sleeping\n";
    std::this_thread::sleep_for(delay);
    std::cout << "sleeping...DONE!\n";

    BOOST_CHECK_EQUAL(data0, 0);
    BOOST_CHECK_EQUAL(data1, 0);
    BOOST_CHECK_EQUAL(data2, 0);
    BOOST_CHECK_EQUAL(data3, 0);

    std::cout << "creating counter0\n";
    counter0_0 = block0->getReferenceCounter();
    std::cout << "creating counter1\n";
    counter1_0 = block1->getReferenceCounter();
    std::cout << "creating counter2\n";
    counter2_0 = block2->getReferenceCounter();
    std::cout << "creating counter3\n";
    counter3_0 = block3->getReferenceCounter();
    std::cout << "creating counters...DONE!\n";

    BOOST_CHECK_EQUAL(data0, 0);
    BOOST_CHECK_EQUAL(data1, 0);
    BOOST_CHECK_EQUAL(data2, 0);
    BOOST_CHECK_EQUAL(data3, 0);

    std::cout << "activating refcounts\n";
    block0->activateCounting();
    block1->activateCounting();
    block2->activateCounting();
    block3->activateCounting();
    std::cout << "activating refcounts...DONE!\n";

    BOOST_CHECK_EQUAL(data0, 0);
    BOOST_CHECK_EQUAL(data1, 0);
    BOOST_CHECK_EQUAL(data2, 0);
    BOOST_CHECK_EQUAL(data3, 0);

    std::cout << "creating counter0\n";
    auto counter0_1 = block0->getReferenceCounter();
    std::cout << "creating counter1\n";
    auto counter1_1 = block1->getReferenceCounter();
    std::cout << "creating counter2\n";
    auto counter2_1 = block2->getReferenceCounter();
    std::cout << "creating counter3\n";
    auto counter3_1 = block3->getReferenceCounter();
    std::cout << "creating counters...DONE!\n";

    BOOST_CHECK_EQUAL(data0, 0);
    BOOST_CHECK_EQUAL(data1, 0);
    BOOST_CHECK_EQUAL(data2, 0);
    BOOST_CHECK_EQUAL(data3, 0);

    counter0_0 = nullptr;
    counter1_0 = nullptr;
    counter2_0 = nullptr;
    counter3_0 = nullptr;

    std::cout << "sleeping\n";
    std::this_thread::sleep_for(delay);
    std::cout << "sleeping...DONE!\n";

    BOOST_CHECK_EQUAL(data0, 0);
    BOOST_CHECK_EQUAL(data1, 0);
    BOOST_CHECK_EQUAL(data2, 0);
    BOOST_CHECK_EQUAL(data3, 0);

    counter0_1 = nullptr;
    counter1_1 = nullptr;
    counter2_1 = nullptr;
    counter3_1 = nullptr;

    BOOST_CHECK_EQUAL(data0, 0);
    BOOST_CHECK_EQUAL(data1, 0);
    BOOST_CHECK_EQUAL(data2, 0);
    BOOST_CHECK_EQUAL(data3, 0);

    std::cout << "sleeping\n";
    std::this_thread::sleep_for(delay);
    std::cout << "sleeping...DONE!\n";

    BOOST_CHECK_EQUAL(data0, 1);
    BOOST_CHECK_EQUAL(data1, 1);
    BOOST_CHECK_EQUAL(data2, 1);
    BOOST_CHECK_EQUAL(data3, 1);

    std::cout << "ALL DONE!\n";
}



/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core::Test
 * @author     Christian Dreher <c.dreher@kit.edu>
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#define BOOST_TEST_MODULE ArmarX::Core::FiniteStateMachine


#define ARMARX_BOOST_TEST


// STD/STL
#include <string>

// ArmarX
#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/test/IceTestHelper.h>
#include <ArmarXCore/core/util/FiniteStateMachine.h>
using namespace armarx;


class Test
{

public:

    status state_a_ret = status::success;
    status state_b_ret = status::success;
    status state_c_ret = status::success;

    bool state_a_visited = false;
    bool state_b_visited = false;
    bool state_c_visited = false;
    bool state_fail_b_visited = false;
    bool state_fail_default_visited = false;

    Test()
    {

    }

    status state_a()
    {
        state_a_visited = true;
        return state_a_ret;
    }

    status state_b()
    {
        state_b_visited = true;
        return state_b_ret;
    }

    status state_c()
    {
        state_c_visited = true;
        return state_c_ret;
    }

    status state_fail_b()
    {
        state_fail_b_visited = true;
        return status::failure;
    }

    status state_fail_default()
    {
        state_fail_default_visited = true;
        return status::failure;
    }

};


BOOST_AUTO_TEST_CASE(test_full_run_success)
{
    Test t;
    const FiniteStateMachine fsm = BinaryFiniteStateMachine<Test>(&t)
                                   .start_from(&Test::state_a)
                                   .from(&Test::state_a).to(&Test::state_b).on_success()
                                   .from(&Test::state_b).to(&Test::state_c).on_success()
                                   .from(&Test::state_b).to(&Test::state_fail_b).on_failure()
                                   .to(&Test::state_fail_default).on_any_failure();

    BOOST_CHECK(fsm.run() == status::success);

    BOOST_CHECK(t.state_a_visited);
    BOOST_CHECK(t.state_b_visited);
    BOOST_CHECK(t.state_c_visited);
    BOOST_CHECK(not t.state_fail_b_visited);
    BOOST_CHECK(not t.state_fail_default_visited);
}


BOOST_AUTO_TEST_CASE(test_full_run_default_fail_state)
{
    Test t;
    t.state_a_ret = status::failure;
    const FiniteStateMachine fsm = BinaryFiniteStateMachine(&t)
                                   .start_from(&Test::state_a)
                                   .from(&Test::state_a).to(&Test::state_b).on_success()
                                   .from(&Test::state_b).to(&Test::state_c).on_success()
                                   .from(&Test::state_b).to(&Test::state_fail_b).on_failure()
                                   .to(&Test::state_fail_default).on_any_failure();

    BOOST_CHECK(fsm.run() == status::failure);

    BOOST_CHECK(t.state_a_visited);
    BOOST_CHECK(not t.state_b_visited);
    BOOST_CHECK(not t.state_c_visited);
    BOOST_CHECK(not t.state_fail_b_visited);
    BOOST_CHECK(t.state_fail_default_visited);
}


BOOST_AUTO_TEST_CASE(test_full_run_fail_state)
{
    Test t;
    t.state_b_ret = status::failure;
    const FiniteStateMachine fsm = BinaryFiniteStateMachine(&t)
                                   .start_from(&Test::state_a)
                                   .from(&Test::state_a).to(&Test::state_b).on_success()
                                   .from(&Test::state_b).to(&Test::state_c).on_success()
                                   .from(&Test::state_b).to(&Test::state_fail_b).on_failure()
                                   .to(&Test::state_fail_default).on_any_failure();

    BOOST_CHECK(fsm.run() == status::failure);

    BOOST_CHECK(t.state_a_visited);
    BOOST_CHECK(t.state_b_visited);
    BOOST_CHECK(not t.state_c_visited);
    BOOST_CHECK(t.state_fail_b_visited);
    BOOST_CHECK(not t.state_fail_default_visited);
}


BOOST_AUTO_TEST_CASE(test_init_failure_no_start)
{
    Test t;
    const FiniteStateMachine fsm = BinaryFiniteStateMachine(&t)
                                   .from(&Test::state_a).to(&Test::state_b).on_success();

    BOOST_CHECK_THROW(fsm.run(), std::logic_error);
}


BOOST_AUTO_TEST_CASE(test_init_failure_no_from_on_success)
{
    Test t;
    BinaryFiniteStateMachine fsm = BinaryFiniteStateMachine(&t)
                                   .start_from(&Test::state_a)
                                   .from(&Test::state_a).to(&Test::state_b).on_success()
                                   .to(&Test::state_c);

    BOOST_CHECK_THROW(fsm.on_success(), std::logic_error);
}


BOOST_AUTO_TEST_CASE(test_init_failure_no_from_run)
{
    Test t;
    const FiniteStateMachine fsm = BinaryFiniteStateMachine(&t)
                                   .start_from(&Test::state_a)
                                   .from(&Test::state_a).to(&Test::state_b).on_success()
                                   .to(&Test::state_c);

    BOOST_CHECK_THROW(fsm.run(), std::logic_error);
}


BOOST_AUTO_TEST_CASE(test_init_failure_no_to_on_success)
{
    Test t;
    BinaryFiniteStateMachine fsm = BinaryFiniteStateMachine(&t)
                                   .start_from(&Test::state_a)
                                   .from(&Test::state_a).to(&Test::state_b).on_success()
                                   .from(&Test::state_c);

    BOOST_CHECK_THROW(fsm.on_success(), std::logic_error);
}


BOOST_AUTO_TEST_CASE(test_init_failure_no_to_run)
{
    Test t;
    const FiniteStateMachine fsm = BinaryFiniteStateMachine(&t)
                                   .start_from(&Test::state_a)
                                   .from(&Test::state_a).to(&Test::state_b).on_success()
                                   .from(&Test::state_c);

    BOOST_CHECK_THROW(fsm.run(), std::logic_error);
}


BOOST_AUTO_TEST_CASE(test_init_failure_no_from_no_to_on_success)
{
    Test t;
    BinaryFiniteStateMachine fsm = BinaryFiniteStateMachine(&t)
                                   .start_from(&Test::state_a)
                                   .from(&Test::state_a).to(&Test::state_b).on_success();

    BOOST_CHECK_THROW(fsm.on_success(), std::logic_error);
}


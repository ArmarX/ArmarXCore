/**
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Rainer Kartmann (rainer dot kartmann at kit dot edu)
* @date       2019
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::EnumNamesPropertiesTest
#define ARMARX_BOOST_TEST

#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/test/IceTestHelper.h>
#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <ArmarXCore/core/application/properties/PluginEnumNames.h>

#include <string>
#include <iostream>

using namespace armarx;

enum class enum_class
{
    A, B
};
enum enum_legacy
{
    A, B
};

namespace simox::meta
{
    template<>
    const simox::meta::EnumNames<enum_class>
    enum_names<enum_class>
    {
        { enum_class::A, "A" }, { enum_class::B, "B" }
    };

    template<>
    const simox::meta::EnumNames<enum_legacy>
    enum_names<enum_legacy>
    {
        { enum_legacy::A, "A" }, { enum_legacy::B, "B" }
    };
}

class TestApp : public Application
{
public:
    void setup(const ManagedIceObjectRegistryInterfacePtr& registry, Ice::PropertiesPtr properties) override {}

    ArmarXManagerPtr getArmarXManager()
    {
        return Application::getArmarXManager();
    }
};

class TestComponent : public Component
{
public:
    void onInitComponent() override {}
    void onConnectComponent() override {}
    std::string getDefaultName() const override
    {
        return "TestComponent";
    }

    enum_class  val_enum_class  = enum_class::A;
    enum_legacy val_enum_legacy = enum_legacy::A;

    armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
    {
        armarx::PropertyDefinitionsPtr ptr =
            new armarx::ComponentPropertyDefinitions(getConfigIdentifier());
        ptr->optional(val_enum_class, "enum_class", "");
        ptr->optional(val_enum_legacy, "enum_legacy", "");
        ptr->defineOptionalProperty<enum_class>("enum_class_2", enum_class::B, "");
        ptr->defineOptionalProperty<enum_legacy>("enum_legacy_2", enum_legacy::B, "");
        return ptr;
    }
};

BOOST_AUTO_TEST_CASE(EnumNamesPropertiesTest)
{
    std::cout << "creating a manager\n";
    IceTestHelper iceTestHelper;
    iceTestHelper.startEnvironment();
    TestArmarXManagerPtr manager = new TestArmarXManager(
        "EnumNamesPropertiesTest",
        iceTestHelper.getCommunicator());
    std::cout << "creating a manager...DONE!\n";

    auto comp = Component::create<TestComponent>();
    manager->addObject(comp, std::string {"Obj1"}, false, false);
    comp->getObjectScheduler()->waitForObjectStateMinimum(armarx::eManagedIceObjectStarted);

    BOOST_CHECK_EQUAL(comp->val_enum_class, enum_class::A);
    BOOST_CHECK_EQUAL(comp->val_enum_legacy, enum_legacy::A);
    BOOST_CHECK_EQUAL(comp->getProperty<enum_class>("enum_class_2").getValue(), enum_class::B);
    BOOST_CHECK_EQUAL(comp->getProperty<enum_legacy>("enum_legacy_2").getValue(), enum_legacy::B);

    manager->removeObjectBlocking(comp);
    manager->shutdown();
}

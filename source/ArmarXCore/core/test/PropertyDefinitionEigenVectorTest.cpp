/**
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Rainer Kartmann (rainer dot kartmann at kit dot edu)
* @date       2019
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::PropertyDefinitionEigenVectorTest
#define ARMARX_BOOST_TEST

#include <ArmarXCore/Test.h>

#include <Eigen/Core>

// ArmarXCore
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/application/properties/Properties.h>
#include <ArmarXCore/core/application/properties/IceProperties.h>
#include <ArmarXCore/core/application/properties/PluginEigen.h>
#include <ArmarXCore/core/exceptions/local/InvalidPropertyValueException.h>

// Enable Eigen properties
#include <ArmarXCore/core/application/properties/PluginEigen.h>

#include <string>
#include <iostream>


using namespace armarx;
using namespace armarx::exceptions;


class EigenPropertyDefinitions : public ComponentPropertyDefinitions
{
public:
    EigenPropertyDefinitions(std::string identifier) :
        ComponentPropertyDefinitions(identifier)
    {
        defineOptionalProperty<std::string>("TestProperty", "Success", "...");
        defineOptionalPropertyVector<Eigen::Vector3f>("Vector3f del=default", Eigen::Vector3f(-1, 0, 1), "...");
        defineOptionalPropertyVector<Eigen::Vector3f>("Vector3f del=space", Eigen::Vector3f(0, -2, 3), "...", " ");
        defineOptionalPropertyVector<Eigen::Vector3f>("Vector3f del=comma", Eigen::Vector3f(5, 11, -17), "...", ",");
        defineOptionalProperty<Eigen::Vector3f>("normal_call", Eigen::Vector3f(4, 131, -15), "...");
    }
};


struct PropertyFixture
{
    PropertyDefinitionsPtr definitions = new EigenPropertyDefinitions("");
    Ice::PropertiesPtr properties = new IceProperties(Ice::createProperties());

    PropertyFixture()
    {
        // Validate test setup.
        BOOST_REQUIRE_EQUAL(getProperty<std::string>("TestProperty").getValue(), "Success");
        setPropertyString("TestProperty", "UpdateSuccess");
        BOOST_REQUIRE_EQUAL(getProperty<std::string>("TestProperty").getValue(), "UpdateSuccess");
    }

    void setPropertyString(const std::string& name, const std::string& value)
    {
        properties->setProperty(definitions->getPrefix() + name, value);
    }

    template <class T>
    Property<T> getProperty(const std::string& name)
    {
        return Property<T>(definitions->getDefintion<T>(name), definitions->getPrefix(), properties);
    }

};


BOOST_FIXTURE_TEST_SUITE(OptionalPropertyEigenVector, PropertyFixture)


BOOST_AUTO_TEST_CASE(test_get_default_value)
{
    BOOST_CHECK_EQUAL(getProperty<Eigen::Vector3f>("Vector3f del=default").getValue(),
                      Eigen::Vector3f(-1, 0, 1));
    BOOST_CHECK_EQUAL(getProperty<Eigen::Vector3f>("Vector3f del=space").getValue(),
                      Eigen::Vector3f(0, -2, 3));
    BOOST_CHECK_EQUAL(getProperty<Eigen::Vector3f>("Vector3f del=comma").getValue(),
                      Eigen::Vector3f(5, 11, -17));
    BOOST_CHECK_EQUAL(getProperty<Eigen::Vector3f>("normal_call").getValue(),
                      Eigen::Vector3f(4, 131, -15));
}


BOOST_AUTO_TEST_CASE(test_get_custom_value)
{
    setPropertyString("Vector3f del=default", "-2 0 13");
    setPropertyString("Vector3f del=space", "2.1 -0.1 23");
    setPropertyString("normal_call", "[1,1,1]");

    BOOST_CHECK_EQUAL(getProperty<Eigen::Vector3f>("Vector3f del=default").getValue(),
                      Eigen::Vector3f(-2, 0, 13));

    BOOST_CHECK_EQUAL(getProperty<Eigen::Vector3f>("Vector3f del=default").getValue(),
                      Eigen::Vector3f(-2, 0, 13));
    BOOST_CHECK_EQUAL(getProperty<Eigen::Vector3f>("Vector3f del=space").getValue(),
                      Eigen::Vector3f(2.1f, -0.1f, 23));
    BOOST_CHECK_EQUAL(getProperty<Eigen::Vector3f>("normal_call").getValue(),
                      Eigen::Vector3f(1, 1, 1));


    // Without intermediate whitespace.
    setPropertyString("Vector3f del=comma", "23,-2.3,3.14");

    BOOST_CHECK_EQUAL(getProperty<Eigen::Vector3f>("Vector3f del=comma").getValue(),
                      Eigen::Vector3f(23, -2.3f, 3.14f));

    // With intermediate whitespace.
    setPropertyString("Vector3f del=comma", "23, -2.3, 3.14");
    BOOST_CHECK_EQUAL(getProperty<Eigen::Vector3f>("Vector3f del=comma").getValue(),
                      Eigen::Vector3f(23, -2.3f, 3.14f));
}


BOOST_AUTO_TEST_CASE(test_get_invalid_value)
{
    // Whole string invalid.
    setPropertyString("Vector3f del=comma", "abcd");
    BOOST_CHECK_THROW(getProperty<Eigen::Vector3f>("Vector3f del=comma").getValue(),
                      armarx::LocalException);

    // Single element invalid.
    setPropertyString("Vector3f del=comma", "2.1, b, 23");
    BOOST_CHECK_THROW(getProperty<Eigen::Vector3f>("Vector3f del=comma").getValue(),
                      armarx::LocalException);

    // Wrong size.
    setPropertyString("Vector3f del=comma", "23, -2.3");
    BOOST_CHECK_THROW(getProperty<Eigen::Vector3f>("Vector3f del=comma").getValue(),
                      armarx::LocalException);
}


BOOST_AUTO_TEST_SUITE_END()

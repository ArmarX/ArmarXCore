/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Jan Issac (jan dot issac at gmx dot de)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/exceptions/LocalException.h>
#include <ArmarXCore/core/util/StringHelpers.h>

#include <Ice/Handle.h>

#include <string>

namespace Ice
{
    class Properties;
    class LocalObject;
    ICE_API ::Ice::LocalObject* upCast(::Ice::Properties*);
    typedef ::IceInternal::Handle< ::Ice::Properties> PropertiesPtr;
}

namespace armarx::exceptions::local
{
    /**
      \class MissingRequiredPropertyException
      \brief This exception is thrown if a property marked as required has not been specified.
      \ingroup Exceptions
     */
    class MissingRequiredPropertyException: public armarx::LocalException
    {
    public:
        std::string propertyName;

        MissingRequiredPropertyException(std::string propertyName, Ice::PropertiesPtr const& properties);

        ~MissingRequiredPropertyException() noexcept override
        { }

        std::string name() const override
        {
            return "armarx::exceptions::local::MissingRequiredPropertyException";
        }
    };
}

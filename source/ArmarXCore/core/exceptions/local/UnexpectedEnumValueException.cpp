#include "UnexpectedEnumValueException.h"
#include "MissingRequiredPropertyException.h"

#include <Ice/Properties.h>


namespace armarx::exceptions::local
{

    UnexpectedEnumValueException::UnexpectedEnumValueException(
        const std::string& enumName, int value) :
        armarx::LocalException("Unexpected value of enum " + enumName + ": "
                               + std::to_string(value)),
        enumName(enumName), value(value)
    {}

    const std::string& UnexpectedEnumValueException::getEnumName() const
    {
        return enumName;
    }

    int UnexpectedEnumValueException::getValue() const
    {
        return value;
    }

    MissingRequiredPropertyException::MissingRequiredPropertyException(std::string propertyName, Ice::PropertiesPtr const& properties)
        : propertyName(propertyName)
    {
        std::stringstream str;
        str << " Missing required property <" + propertyName + ">" ;

        if (properties)
        {
            str << "\nAvailable Properties: " << properties->getPropertiesForPrefix("");
        }

        setReason(str.str());
    }

}

/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Kai Welke (welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "Component.h"

#include <ArmarXCore/core/ComponentPlugin.h>
#include <ArmarXCore/core/application/properties/Property.h>
#include <ArmarXCore/core/application/properties/PropertyUser.h>

#include <ArmarXCore/interface/core/ManagedIceObjectDefinitions.h>


namespace armarx
{

    ComponentPropertyDefinitions::ComponentPropertyDefinitions(std::string prefix, bool hasObjectNameParameter) :
        PropertyDefinitionContainer(prefix)
    {
        if (hasObjectNameParameter)
        {
            defineOptionalProperty<std::string>("ObjectName", "", "Name of IceGrid well-known object");
        }
        defineOptionalProperty<MessageTypeT>("MinimumLoggingLevel", armarx::MessageTypeT::UNDEFINED, "Local logging level only for this component", PropertyDefinitionBase::eModifiable);
        defineOptionalProperty<bool>("EnableProfiling", false, "enable profiler which is used for logging performance events", PropertyDefinitionBase::eModifiable);
    }


    Component::Component()
    {
        setIceProperties(Ice::createProperties());
        configName = "";
        configDomain = "ArmarX";
        createdByComponentCreate = false;
    }


    std::string Component::getConfigDomain()
    {
        return configDomain;
    }


    std::string Component::getConfigName()
    {
        return configName;
    }


    std::string Component::getConfigIdentifier()
    {
        return configDomain + "." + configName;
    }


    void Component::initializeProperties(const std::string& configName, const Ice::PropertiesPtr& properties, const std::string& configDomain)
    {
        ARMARX_DEBUG << "Initializing properties.";

        this->configDomain = configDomain;
        this->configName = configName;

        // set default object name to configName
        setName(this->configName);

        // set properties
        setIceProperties(properties);
        icePropertiesInitialized();
    }


    void
    Component::preOnInitComponent()
    {
        ARMARX_DEBUG << "Preparing for onInitComponent().";

        // Update all referenced properties.
        updateProperties();

        // Subscribe to all configured topics.
        for (const std::string& offered_topic_name : getSubscribedTopicNames())
        {
            ARMARX_DEBUG << "Subscribing to topic `" << offered_topic_name << "`.";
            usingTopic(offered_topic_name);
        }

        // Signal dependencies on configured components.
        for (const std::string& proxy_name : getComponentProxyNames())
        {
            ARMARX_DEBUG << "Using component proxy `" << proxy_name << "`.";
            usingProxy(proxy_name);
        }

        // Offer all configured topics.
        for (const std::string& proxy_name : getTopicProxyNames())
        {
            ARMARX_DEBUG << "Offering topic `" << proxy_name << "`.";
            offeringTopic(proxy_name);
        }
    }


    void
    Component::preOnConnectComponent()
    {
        ARMARX_DEBUG << "Preparing for onConnectComponent().";

        // Update all referenced proxy properties.
        updateProxies(getIceManager());
    }


    void Component::addPropertyUser(const PropertyUserPtr& subPropertyUser)
    {
        additionalPropertyUsers.push_back(subPropertyUser);
    }


    std::vector<PropertyUserPtr> Component::getAdditionalPropertyUsers() const
    {
        return additionalPropertyUsers;
    }


    void Component::offeringTopicFromProperty(const std::string& propertyName)
    {
        offeringTopic(getProperty<std::string>(propertyName));
    }


    void Component::usingTopicFromProperty(const std::string& propertyName, bool orderedPublishing)
    {
        usingTopic(getProperty<std::string>(propertyName), orderedPublishing);
    }


    bool Component::usingProxyFromProperty(const std::string& propertyName, const std::string& endpoints)
    {
        return usingProxy(getProperty<std::string>(propertyName), endpoints);
    }


    void Component::forceComponentCreatedByComponentCreateFunc()
    {
        this->createdByComponentCreate = true;
    }


    void Component::injectPropertyDefinitions(PropertyDefinitionsPtr& props)
    {
        ARMARX_DEBUG << "call postCreatePropertyDefinitions for all plugins...";
        foreach_plugin([&](const auto & typeidx, const auto & name, const auto & plugin)
        {
            if (auto ptr = dynamic_cast<ComponentPlugin*>(plugin))
            {
                ARMARX_DEBUG << "plugin '" << name
                             << "' (" << GetTypeString(typeidx)
                             << ") postCreatePropertyDefinitions...";
                ptr->postCreatePropertyDefinitions(props);
                ARMARX_DEBUG << "plugin '" << name
                             << "' (" << GetTypeString(typeidx)
                             << ") postCreatePropertyDefinitions...done!";
            }
            else
            {
                ARMARX_DEBUG << "plugin '" << name
                             << "' (" << GetTypeString(typeidx)
                             << ") is no ComponentPlugin (not calling postCreatePropertyDefinitions)";
            }
        }, __LINE__, __FILE__, BOOST_CURRENT_FUNCTION);
        ARMARX_DEBUG << "call postCreatePropertyDefinitions for all plugins...done!";
    }


    PropertyDefinitionsPtr Component::createPropertyDefinitions()
    {
        return new ComponentPropertyDefinitions(getConfigDomain() + "." + getConfigName());
    }


    void Component::icePropertiesUpdated(const std::set<std::string>& changedProperties)
    {
        // check if properties have been initialized
        if (getConfigDomain().empty() || getConfigName().empty())
        {
            return;
        }

        // set the logging behavior for this component
        if (changedProperties.count("MinimumLoggingLevel") && getProperty<MessageTypeT>("MinimumLoggingLevel").isSet())
        {
            setLocalMinimumLoggingLevel(
                getProperty<MessageTypeT>("MinimumLoggingLevel").getValue());
        }

        // get well-known name of this component instance
        // (default is the ManagedIceObject name itself)
        if (changedProperties.count("ObjectName") && getProperty<std::string>("ObjectName").isSet())
        {
            setName(getProperty<std::string>("ObjectName").getValue());
        }

        // set logging tag, may be overwritten by implementer
        setTag(getName());

        // enable Profiling
        // enable via global ArmarX.Profiling property and disable with specific one?
        if (changedProperties.count("EnableProfiling"))
        {
            enableProfiler(getProperty<bool>("EnableProfiling").getValue());
        }

        //only update user properties after the object was initialized
        if (getState() > eManagedIceObjectInitialized)
        {
            componentPropertiesUpdated(changedProperties);
            ARMARX_DEBUG << "call componentPropertiesUpdated for all plugins...";
            foreach_plugin([&](const auto & typeidx, const auto & name, const auto & plugin)
            {
                if (auto ptr = dynamic_cast<ComponentPlugin*>(plugin))
                {
                    ARMARX_DEBUG << "plugin '" << name
                                 << "' (" << GetTypeString(typeidx)
                                 << ") componentPropertiesUpdated...";
                    ptr->componentPropertiesUpdated(changedProperties);
                    ARMARX_DEBUG << "plugin '" << name
                                 << "' (" << GetTypeString(typeidx)
                                 << ") componentPropertiesUpdated...done!";
                }
                else
                {
                    ARMARX_DEBUG << "plugin '" << name
                                 << "' (" << GetTypeString(typeidx.name())
                                 << ") is no ComponentPlugin (not calling componentPropertiesUpdated)";
                }
            }, __LINE__, __FILE__, BOOST_CURRENT_FUNCTION);
            ARMARX_DEBUG << "call componentPropertiesUpdated for all plugins...done!";
        }
    }


    void Component::componentPropertiesUpdated(const std::set<std::string>& changedProperties)
    {
        (void) changedProperties;
    }

}

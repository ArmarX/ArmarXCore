#pragma once

#include "ManagedIceObject.h"

#include <string>
#include <condition_variable>
#include <mutex>

namespace armarx
{
    struct ManagedIceObject::Impl
    {
        /**
         * Name of object. Corresponds to name of Ice well-known object.
         */
        std::string name;

        /**
         * Component manager instance. The manager can be used to add and remove
         * components dynamically.
         */
        ArmarXManagerPtr armarXManager;

        /**
         * The Ice manager used for adapter registration, proxy retrieval topic
         * and session management.
         */
        IceManagerPtr iceManager;

        /**
         * The Ice manager used for adapter registration, proxy retrieval topic
         * and session management.
         */
        ArmarXObjectSchedulerPtr objectScheduler;

        /**
         * State of the ManagedIceObject.
         */
        ManagedIceObjectState objectState;
        std::mutex objectStateMutex;

        /**
         * Proxy of this object needed for storm topic subscriptions.
         */
        Ice::ObjectPrx proxy;

        /**
         * Component object adapter.
         */
        Ice::ObjectAdapterPtr objectAdapter;

        /**
         * connectivity.
         */
        ManagedIceObjectConnectivity connectivity;
        std::map<std::string, bool> orderedTopicPublishing;

        /**
         * Mutex for connectivity.
         */
        std::mutex connectivityMutex;

        /**
         * @brief startedCondition is used to signal waiting threads, that
         * this object is started (i.e. onConnectComponent() was called)
         *
         * @see onConnectComponent(), waitForObjectStart()
         */
        std::condition_variable stateCondition;

        /**
         * Holds an instance of armarx::Profiler wich can be accessed via ManagedIceObject::getProfiler().
         * Enable or disable the profiler via ManagedIceObject::enable().
         */
        Profiler::ProfilerPtr profiler;

        /**
         * @brief enableProfilerFunction is used to defer the creation of the IceLoggingStrategy topic proxy
         *
         * This function pointer normally executes MangedIceObject::Noop() and is called in ManagedIceObject::init() after the IceManager is set.
         *
         * In case ManagedIceObject::enableProfiler() is called before ManagedIceObject::init() the creation of the the IceLoggingStrategy
         * is put into this function object and executed when Ice is available.
         */
        void (*enableProfilerFunction)(ManagedIceObject* object);

        std::mutex metaInfoMapMutex;
        StringVariantBaseMap metaInfoMap;

        std::map<std::string, PeriodicTaskPtr> periodicTasks;
    };
}

#include "Frequency.h"

#include <iomanip>
#include <limits>
#include <sstream>

#include <SimoxUtility/algorithm/string/string_tools.h>

#include <ArmarXCore/core/time/Clock.h>


namespace armarx::core::time
{

    Frequency::Frequency(const Duration& cycleDuration) :
        _cycleDuration{cycleDuration}
    {
        ;
    }


    Frequency
    Frequency::Hertz(const std::int64_t hertz)
    {
        if (hertz == 0)
        {
            return Frequency(Duration::MicroSeconds(std::numeric_limits<std::int64_t>::max()));
        }
        return Frequency(Duration::Seconds(1) / hertz);
    }


    Frequency
    Frequency::HertzDouble(double hertz)
    {
        if (hertz == 0)
        {
            return Frequency(Duration::MicroSeconds(std::numeric_limits<std::int64_t>::max()));
        }
        return Frequency(Duration::Seconds(1) / hertz);
    }


    std::int64_t
    Frequency::toHertz() const
    {
        if (_cycleDuration.isZero())
        {
            return std::numeric_limits<std::int64_t>::max();
        }
        return static_cast<std::int64_t>(toHertzDouble() + 0.5);
    }


    double
    Frequency::toHertzDouble() const
    {
        if (_cycleDuration.isZero())
        {
            return std::numeric_limits<double>::infinity();
        }
        return 1 / _cycleDuration.toSecondsDouble();
    }


    Duration
    Frequency::toCycleDuration() const
    {
        return _cycleDuration;
    }


    std::string
    Frequency::toFrequencyString() const
    {
        if (_cycleDuration.isZero())
        {
            return "infinite";
        }

        return std::to_string(toHertz()) + "Hz";
    }


    Frequency
    Frequency::operator+(const Frequency& rhs) const
    {
        return Frequency::Hertz(toHertzDouble() + rhs.toHertzDouble());
    }


    Frequency&
    Frequency::operator+=(const Frequency& rhs)
    {
        _cycleDuration = Frequency::Hertz(toHertzDouble() + rhs.toHertzDouble()).toCycleDuration();
        return *this;
    }


    Frequency
    Frequency::operator-(const Frequency& rhs) const
    {
        return Frequency::Hertz(toHertzDouble() - rhs.toHertzDouble());
    }


    Frequency&
    Frequency::operator-=(const Frequency& rhs)
    {
        _cycleDuration = Frequency::Hertz(toHertzDouble() - rhs.toHertzDouble()).toCycleDuration();
        return *this;
    }


    Frequency
    Frequency::operator*(double rhs) const
    {
        return Frequency(_cycleDuration / rhs);
    }


    Frequency
    Frequency::operator*(int rhs) const
    {
        return Frequency(_cycleDuration / rhs);
    }


    Frequency
    Frequency::operator*(std::int64_t rhs) const
    {
        return Frequency(_cycleDuration / rhs);
    }


    Frequency&
    Frequency::operator*=(double rhs)
    {
        _cycleDuration /= rhs;
        return *this;
    }


    Frequency&
    Frequency::operator*=(int rhs)
    {
        _cycleDuration /= rhs;
        return *this;
    }


    Frequency&
    Frequency::operator*=(std::int64_t rhs)
    {
        _cycleDuration /= rhs;
        return *this;
    }


    double
    Frequency::operator/(const Frequency& rhs) const
    {
        return toHertzDouble() / rhs.toHertzDouble();
    }


    Frequency
    Frequency::operator/(double rhs) const
    {
        return Frequency(_cycleDuration * rhs);
    }


    Frequency
    Frequency::operator/(int rhs) const
    {
        return Frequency(_cycleDuration * rhs);
    }


    Frequency
    Frequency::operator/(std::int64_t rhs) const
    {
        return Frequency(_cycleDuration * rhs);
    }


    Frequency&
    Frequency::operator/=(double rhs)
    {
        _cycleDuration *= rhs;
        return *this;
    }


    Frequency&
    Frequency::operator/=(int rhs)
    {
        _cycleDuration *= rhs;
        return *this;
    }


    Frequency&
    Frequency::operator/=(std::int64_t rhs)
    {
        _cycleDuration *= rhs;
        return *this;
    }


    bool
    Frequency::operator<(const Frequency& rhs) const
    {
        return _cycleDuration > rhs._cycleDuration;
    }


    bool
    Frequency::operator<=(const Frequency& rhs) const
    {
        return _cycleDuration >= rhs._cycleDuration;
    }


    bool
    Frequency::operator==(const Frequency& rhs) const
    {
        return _cycleDuration == rhs._cycleDuration;
    }


    bool
    Frequency::operator!=(const Frequency& rhs) const
    {
        return _cycleDuration != rhs._cycleDuration;
    }


    bool
    Frequency::operator>=(const Frequency& rhs) const
    {
        return _cycleDuration <= rhs._cycleDuration;
    }


    bool
    Frequency::operator>(const Frequency& rhs) const
    {
        return _cycleDuration < rhs._cycleDuration;
    }


    Frequency
    operator/(const double cyclesPerDuration, const Duration& duration)
    {
        const double cyclesPerSecond = cyclesPerDuration / duration.toSecondsDouble();
        return Frequency(Duration::Seconds(1) / cyclesPerSecond);
    }


    Frequency
    operator/(const int cyclesPerDuration, const Duration& duration)
    {
        return static_cast<double>(cyclesPerDuration) / duration;
    }


    Frequency
    operator/(const std::int64_t cyclesPerDuration, const Duration& duration)
    {
        return static_cast<double>(cyclesPerDuration) / duration;
    }


    std::ostream&
    operator<<(std::ostream& out, const Frequency& rhs)
    {
        out << rhs.toFrequencyString();
        return out;
    }

} // namespace armarx::core::time

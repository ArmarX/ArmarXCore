#include "StopWatch.h"

#include <exception>

#include <IceUtil/Time.h>

#include <ArmarXCore/core/time/TimeUtil.h>


namespace
{
    /**
     * @brief An invalid timestamp.
     */
    const IceUtil::Time timestamp_invalid = IceUtil::Time::milliSeconds(-1);
} // namespace

namespace armarx::core::time
{
    StopWatch::StopWatch(ClockType clockType) :
        _clock{clockType}, _startingTime{_clock.now()}, _time{Duration::Seconds(0)}
    {
        // pass
    }


    StopWatch::~StopWatch()
    {
        // pass
    }


    Duration
    StopWatch::measure(std::function<void(void)> subjectToMeasure, ClockType clockType)
    {
        StopWatch sw{clockType};
        subjectToMeasure();
        return sw.stop();
    }


    Duration
    StopWatch::stop()
    {
        _time = _clock.now() - _startingTime;
        return _time;
    }


    void
    StopWatch::reset()
    {
        _startingTime = _clock.now();
        _time = Duration::Seconds(0);
    }


    bool
    StopWatch::isStopped() const
    {
        return _time.isPositive();
    }


    DateTime
    StopWatch::startingTime() const
    {
        return _startingTime;
    }


    DateTime
    StopWatch::stoppingTime() const
    {
        if (not isStopped())
        {
            throw std::logic_error{"Timer was not stopped yet, cannot assess stopping time."};
        }

        return _startingTime + _time;
    }

    Duration
    StopWatch::stopAndReset()
    {
        const auto duration = stop();
        reset();
        return duration;
    }



} // namespace armarx::core::time


namespace armarx
{

    StopWatch::StopWatch(TimeMode timeMode) :
        m_time_mode{timeMode},
        m_starting_time{TimeUtil::GetTime(m_time_mode)},
        m_time{::timestamp_invalid}
    {
        // pass
    }


    StopWatch::~StopWatch()
    {
        // pass
    }


    IceUtil::Time
    StopWatch::measure(std::function<void(void)> subjectToMeasure, TimeMode timeMode)
    {
        StopWatch sw{timeMode};
        subjectToMeasure();
        return sw.stop();
    }


    IceUtil::Time
    StopWatch::stop()
    {
        m_time = TimeUtil::GetTimeSince(m_starting_time, m_time_mode);
        return m_time;
    }


    void
    StopWatch::reset()
    {
        m_starting_time = TimeUtil::GetTime(m_time_mode);
        m_time = ::timestamp_invalid;
    }


    bool
    StopWatch::isStopped() const
    {
        return m_time != ::timestamp_invalid;
    }


    IceUtil::Time
    StopWatch::startingTime() const
    {
        return m_starting_time;
    }


    IceUtil::Time
    StopWatch::stoppingTime() const
    {
        if (not isStopped())
        {
            throw std::logic_error{"Timer was not stopped yet, cannot assess stopping time."};
        }

        return m_starting_time + m_time;
    }
} // namespace armarx

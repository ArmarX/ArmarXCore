#include "Metronome.h"

#include <ArmarXCore/core/time/Clock.h>


namespace armarx::core::time
{

    Metronome::Metronome(const Frequency& targetFrequency, ClockType clockType) :
        _clock{clockType}, _targetPeriod{targetFrequency.toCycleDuration()}
    {
        reset();
    }


    Metronome::Metronome(const Duration& targetPeriod, ClockType clockType) :
        _clock{clockType}, _targetPeriod{targetPeriod}
    {
        reset();
    }


    Metronome::Metronome(const Duration& targetPeriod, const Clock& clock) :
        _clock{clock}, _targetPeriod(targetPeriod)
    {
        reset();
    }


    Duration
    Metronome::waitForNextTick()
    {
        const Duration waitTime = _clock.waitUntil(_nextCheckpoint);
        _nextCheckpoint += _targetPeriod;
        return waitTime;
    }


    void
    Metronome::reset()
    {
        _nextCheckpoint = _clock.now() + _targetPeriod;
    }

} // namespace armarx::core::time

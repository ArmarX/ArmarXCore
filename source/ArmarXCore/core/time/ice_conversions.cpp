#include "ice_conversions.h"

#include <IceUtil/Time.h>

#include <ArmarXCore/core/time_minimal.h>


namespace armarx::core
{

    void
    time::fromIce(const dto::ClockType::ClockTypeEnum& dto, ClockType& bo)
    {
        switch (dto)
        {
            case dto::ClockType::Realtime:
                bo = ClockType::Realtime;
                break;
            case dto::ClockType::Monotonic:
                bo = ClockType::Monotonic;
                break;
            case dto::ClockType::Virtual:
                bo = ClockType::Virtual;
                break;
            case dto::ClockType::Unknown:
                bo = ClockType::Unknown;
                break;
        }
    }


    void
    time::toIce(dto::ClockType::ClockTypeEnum& dto, const ClockType& bo)
    {
        switch (bo)
        {
            case ClockType::Realtime:
                dto = dto::ClockType::Realtime;
                break;
            case ClockType::Monotonic:
                dto = dto::ClockType::Monotonic;
                break;
            case ClockType::Virtual:
                dto = dto::ClockType::Virtual;
                break;
            case ClockType::Unknown:
                dto = dto::ClockType::Unknown;
                break;
        }
    }


    void
    time::fromIce(const dto::Duration& dto, Duration& bo)
    {
        bo = Duration::MicroSeconds(dto.microSeconds);
    }


    void
    time::toIce(dto::Duration& dto, const Duration& bo)
    {
        dto.microSeconds = bo.toMicroSeconds();
    }


    void
    time::fromIce(const dto::Frequency& dto, Frequency& bo)
    {
        Duration cycleDuration;
        fromIce(dto.cycleDuration, cycleDuration);
        bo = Frequency(cycleDuration);
    }


    void
    time::toIce(dto::Frequency& dto, const Frequency& bo)
    {
        toIce(dto.cycleDuration, bo.toCycleDuration());
    }


    void
    time::fromIce(const dto::DateTime& dto, DateTime& bo)
    {
        Duration duration;
        ClockType clockType;
        fromIce(dto.timeSinceEpoch, duration);
        fromIce(dto.clockType, clockType);
        bo = DateTime(duration, clockType, dto.hostname);
    }


    void
    time::toIce(dto::DateTime& dto, const DateTime& bo)
    {
        toIce(dto.timeSinceEpoch, bo.toDurationSinceEpoch());
        toIce(dto.clockType, bo.clockType());
        dto.hostname = bo.hostname();
    }


    void
    time::fromIce(const IceUtil::Time& dto, Duration& bo)
    {
        bo = Duration::MicroSeconds(dto.toMicroSeconds());
    }


    void
    time::toIce(IceUtil::Time& dto, const Duration& bo)
    {
        dto = IceUtil::Time::microSeconds(bo.toMicroSeconds());
    }


    void
    time::fromIce(const IceUtil::Time& dto, DateTime& bo)
    {
        Duration duration;
        fromIce(dto, duration);
        bo = DateTime(duration, ClockType::Unknown, "unknown");
    }


    void
    time::toIce(IceUtil::Time& dto, const DateTime& bo)
    {
        dto = IceUtil::Time::microSeconds(bo.toMicroSecondsSinceEpoch());
    }

} // namespace armarx::core::time

namespace armarx::core::time
{
    std::ostream& dto::operator<<(std::ostream& os, const DateTime& dto)
    {
        time::DateTime bo;
        fromIce(dto, bo);
        return os << bo;
    }
    std::ostream& dto::operator<<(std::ostream& os, const Duration& dto)
    {
        time::Duration bo;
        fromIce(dto, bo);
        return os << bo;return os;
    }
    std::ostream& dto::operator<<(std::ostream& os, const Frequency& dto)
    {
        time::Duration cycleDuration;
        fromIce(dto.cycleDuration, cycleDuration);
        return os << time::Frequency{cycleDuration};
    }
}


#pragma once

#include <SimoxUtility/meta/enum/EnumNames.hpp>

#include "ClockType.h"


namespace armarx::core::time
{

    extern const simox::meta::EnumNames<ClockType> ClockTypeNames;

} // namespace armarx::core::time


namespace armarx
{
    using core::time::ClockTypeNames;
} // namespace armarx

#include "json_conversions.h"

#include <IceUtil/Time.h>

#include <ArmarXCore/core/time_minimal.h>
#include <ArmarXCore/core/time/ClockTypeNames.h>


namespace armarx::core
{

    void time::to_json(simox::json::json& j, const ClockType& bo)
    {
        j = ClockTypeNames.to_name(bo);
    }

    void time::from_json(const simox::json::json& j, ClockType& bo)
    {
        bo = ClockTypeNames.from_name(j);
    }

    void time::to_json(simox::json::json& j, const Duration& bo)
    {
        j["microSeconds"] = bo.toMicroSeconds();
        j["humanReadable"] = bo.toDurationString();
    }

    void time::from_json(const simox::json::json& j, Duration& bo)
    {
        bo = Duration::MicroSeconds(j.at("microSeconds"));
    }

    void time::to_json(simox::json::json& j, const Frequency& bo)
    {
        j["cycleDuration"] = bo.toCycleDuration();
        j["hertz"] = bo.toHertzDouble();
    }

    void time::from_json(const simox::json::json& j, Frequency& bo)
    {
        bo = Frequency(j.at("cycleDuration").get<Duration>());
    }

    void time::to_json(simox::json::json& j, const DateTime& bo)
    {
        j["durationSinceEpoch"] = bo.toDurationSinceEpoch();
        j["clockType"] = bo.clockType();
        j["hostname"] = bo.hostname();
        j["humanReadable"] = bo.toDateTimeString();
    }

    void time::from_json(const simox::json::json& j, DateTime& bo)
    {
        bo = DateTime(j.at("durationSinceEpoch").get<Duration>(),
                      j.at("clockType").get<ClockType>(),
                      j.at("hostname").get<std::string>());
    }

} // namespace armarx::core

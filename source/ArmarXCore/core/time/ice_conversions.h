#pragma once

#include <ostream>

#include <ArmarXCore/core/time/forward_declarations.h>
#include <ArmarXCore/interface/core/time.h>


namespace IceUtil
{
    class Time;
}


namespace armarx::core::time
{

    void fromIce(const dto::ClockType::ClockTypeEnum& dto, ClockType& bo);
    void toIce(dto::ClockType::ClockTypeEnum& dto, const ClockType& bo);

    void fromIce(const dto::Duration& dto, Duration& bo);
    void toIce(dto::Duration& dto, const Duration& bo);

    void fromIce(const dto::Frequency& dto, Frequency& bo);
    void toIce(dto::Frequency& dto, const Frequency& bo);

    void fromIce(const dto::DateTime& dto, DateTime& bo);
    void toIce(dto::DateTime& dto, const DateTime& bo);

    void fromIce(const IceUtil::Time& dto, Duration& bo);
    void toIce(IceUtil::Time& dto, const Duration& bo);
    void fromIce(const IceUtil::Time& dto, DateTime& bo);
    void toIce(IceUtil::Time& dto, const DateTime& bo);

} // namespace armarx::core::time

namespace armarx::core::time::dto
{
    std::ostream& operator<<(std::ostream& os, const DateTime& dto);
    std::ostream& operator<<(std::ostream& os, const Duration& dto);
    std::ostream& operator<<(std::ostream& os, const Frequency& dto);
}

#include "DateTime.h"

#include <limits>

#include <IceUtil/Time.h>

#include <ArmarXCore/core/time/Clock.h>

#include "ice_conversions.h"


namespace armarx::core::time
{

    const Duration DateTime::_invalidTimeSinceEpoch = Duration::MicroSeconds(std::numeric_limits<long>::min());


    DateTime::DateTime() : _timeSinceEpoch{}, _clockType{ClockType::Unknown}, _hostname{"unknown"}
    {
        ;
    }


    DateTime::DateTime(Duration timeSinceEpoch, ClockType clock, const std::string& hostname) :
        _timeSinceEpoch{timeSinceEpoch}, _clockType{clock}, _hostname{hostname}
    {
        ;
    }


    DateTime::DateTime(std::int64_t microSecondsSinceEpoch,
                       ClockType clock,
                       const std::string& hostname) :
        _timeSinceEpoch{Duration::MicroSeconds(microSecondsSinceEpoch)},
        _clockType{clock},
        _hostname{hostname}
    {
        ;
    }

    DateTime::DateTime(const IceUtil::Time& ice)
    {
        fromIce(ice, *this);
    }

    DateTime::operator IceUtil::Time() const
    {
        IceUtil::Time ice;
        toIce(ice, *this);
        return ice;
    }


    DateTime
    DateTime::Now()
    {
        return Clock::Now();
    }

    DateTime DateTime::Invalid()
    {
        return DateTime(_invalidTimeSinceEpoch);
    }


    std::string
    DateTime::toDateString() const
    {
        return toString("%Y-%m-%d");
    }


    std::string
    DateTime::toTimeString() const
    {
        return toString("%H-%M-%S.%%usec");
    }


    std::string
    DateTime::toDateTimeString() const
    {
        return toDateString() + "_" + toTimeString();
    }


    std::string
    DateTime::toString(const std::string& format) const
    {
        return _timeSinceEpoch.toDurationString(format);
    }


    std::int64_t
    DateTime::toMicroSecondsSinceEpoch() const
    {
        return _timeSinceEpoch.toMicroSeconds();
    }


    std::int64_t
    DateTime::toMilliSecondsSinceEpoch() const
    {
        return _timeSinceEpoch.toMilliSeconds();
    }


    std::int64_t
    DateTime::toSecondsSinceEpoch() const
    {
        return _timeSinceEpoch.toSeconds();
    }


    Duration
    DateTime::toDurationSinceEpoch() const
    {
        return _timeSinceEpoch;
    }


    ClockType
    DateTime::clockType() const
    {
        return _clockType;
    }


    std::string
    DateTime::hostname() const
    {
        return _hostname;
    }

    bool DateTime::isValid() const
    {
        return _timeSinceEpoch != _invalidTimeSinceEpoch;
    }

    bool DateTime::isInvalid() const
    {
        return not isValid();
    }


    DateTime
    DateTime::operator+(const Duration& rhs) const
    {
        return DateTime(_timeSinceEpoch + rhs, _clockType, _hostname);
    }


    DateTime&
    DateTime::operator+=(const Duration& rhs)
    {
        _timeSinceEpoch += rhs;
        return *this;
    }


    DateTime
    DateTime::operator-(const Duration& rhs) const
    {
        return DateTime(_timeSinceEpoch - rhs, _clockType, _hostname);
    }


    DateTime&
    DateTime::operator-=(const Duration& rhs)
    {
        _timeSinceEpoch -= rhs;
        return *this;
    }


    Duration
    DateTime::operator-(const DateTime& rhs) const
    {
        return _timeSinceEpoch - rhs.toDurationSinceEpoch();
    }


    bool
    DateTime::operator<(const DateTime& rhs) const
    {
        return _timeSinceEpoch < rhs.toDurationSinceEpoch();
    }


    bool
    DateTime::operator<=(const DateTime& rhs) const
    {
        return _timeSinceEpoch <= rhs.toDurationSinceEpoch();
    }


    bool
    DateTime::operator==(const DateTime& rhs) const
    {
        return _timeSinceEpoch == rhs.toDurationSinceEpoch();
    }


    bool
    DateTime::operator>(const DateTime& rhs) const
    {
        return _timeSinceEpoch > rhs.toDurationSinceEpoch();
    }


    bool
    DateTime::operator>=(const DateTime& rhs) const
    {
        return _timeSinceEpoch >= rhs.toDurationSinceEpoch();
    }


    bool
    DateTime::operator!=(const DateTime& rhs) const
    {
        return _timeSinceEpoch != rhs.toDurationSinceEpoch();
    }


    std::ostream&
    operator<<(std::ostream& out, const DateTime& rhs)
    {
        out << rhs.toDateTimeString();
        return out;
    }

} // namespace armarx::core::time

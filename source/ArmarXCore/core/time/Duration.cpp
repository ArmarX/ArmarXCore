#include "Duration.h"

#include <iomanip>
#include <sstream>

#include <SimoxUtility/algorithm/string/string_tools.h>

#include <ArmarXCore/core/time/Clock.h>


namespace armarx::core::time
{

    Duration::Duration() : _microSeconds{0}
    {
        ;
    }


    Duration::Duration(std::int64_t microSeconds) : _microSeconds{microSeconds}
    {
        ;
    }


    Duration
    Duration::MicroSeconds(const std::int64_t microSeconds)
    {
        return Duration(microSeconds);
    }


    Duration
    Duration::MicroSecondsDouble(const double microSeconds)
    {
        return Duration(microSeconds);
    }


    std::int64_t
    Duration::toMicroSeconds() const
    {
        return _microSeconds;
    }


    double
    Duration::toMicroSecondsDouble() const
    {
        return static_cast<double>(_microSeconds);
    }


    Duration
    Duration::MilliSeconds(const std::int64_t milliSeconds)
    {
        return Duration::MicroSeconds(1'000 * milliSeconds);
    }


    Duration
    Duration::MilliSecondsDouble(const double milliSeconds)
    {
        return Duration::MicroSeconds(1'000 * milliSeconds);
    }


    std::int64_t
    Duration::toMilliSeconds() const
    {
        return static_cast<std::int64_t>(toMilliSecondsDouble() + 0.5);
    }


    double
    Duration::toMilliSecondsDouble() const
    {
        return static_cast<double>(_microSeconds) / 1'000;
    }


    Duration
    Duration::Seconds(const std::int64_t seconds)
    {
        return Duration::MilliSeconds(1'000 * seconds);
    }


    Duration
    Duration::SecondsDouble(const double seconds)
    {
        return Duration::MilliSeconds(1'000 * seconds);
    }


    std::int64_t
    Duration::toSeconds() const
    {
        return static_cast<std::int64_t>(toSecondsDouble() + 0.5);
    }


    double
    Duration::toSecondsDouble() const
    {
        return toMilliSecondsDouble() / 1'000;
    }


    Duration
    Duration::Minutes(const std::int64_t minutes)
    {
        return Duration::Seconds(60 * minutes);
    }


    Duration
    Duration::MinutesDouble(const double minutes)
    {
        return Duration::Seconds(60 * minutes);
    }


    std::int64_t
    Duration::toMinutes() const
    {
        return static_cast<std::int64_t>(toMinutesDouble() + 0.5);
    }


    double
    Duration::toMinutesDouble() const
    {
        return toSecondsDouble() / 60;
    }


    Duration
    Duration::Hours(const std::int64_t hours)
    {
        return Duration::Minutes(60 * hours);
    }


    Duration
    Duration::HoursDouble(const double hours)
    {
        return Duration::Minutes(60 * hours);
    }


    std::int64_t
    Duration::toHours() const
    {
        return static_cast<std::int64_t>(toHoursDouble() + 0.5);
    }


    double
    Duration::toHoursDouble() const
    {
        return toMinutesDouble() / 60;
    }


    Duration
    Duration::Days(const std::int64_t days)
    {
        return Duration::Hours(24 * days);
    }


    Duration
    Duration::DaysDouble(const double days)
    {
        return Duration::Hours(24 * days);
    }


    std::int64_t
    Duration::toDays() const
    {
        return static_cast<std::int64_t>(toDaysDouble() + 0.5);
    }


    double
    Duration::toDaysDouble() const
    {
        return toHoursDouble() / 24;
    }


    bool
    Duration::isPositive() const
    {
        return _microSeconds > 0;
    }


    bool
    Duration::isZero() const
    {
        return _microSeconds == 0;
    }


    std::string
    Duration::toDurationString() const
    {
        double time_count = toMicroSecondsDouble();
        std::string unit = "µs";

        if (time_count >= 1000)
        {
            time_count /= 1000;
            unit = "ms";

            if (time_count >= 1000)
            {
                time_count /= 1000;
                unit = "s";

                if (time_count >= 60)
                {
                    time_count /= 60;
                    unit = "min";

                    if (time_count >= 60)
                    {
                        time_count /= 60;
                        unit = "h";

                        if (time_count >= 24)
                        {
                            time_count /= 24;
                            unit = "d";
                        }
                    }
                }
            }
        }

        std::stringstream ss;
        ss << std::setprecision(3) << time_count;
        return ss.str() + unit;
    }


    std::string
    Duration::toDurationString(const std::string& format) const
    {
        const std::int64_t usec = _microSeconds % 1'000'000;
        const std::int64_t msec =
            (static_cast<double>(usec) / 1'000) + 0.5; // Rounding towards next integer.
        const time_t time = static_cast<time_t>(static_cast<double>(_microSeconds) / 1'000'000);

        struct tm tr;
        localtime_r(&time, &tr);

        char buf[32];
        if (strftime(buf, sizeof(buf), format.c_str(), &tr) == 0)
        {
            return std::string();
        }

        std::string postformat(buf);
        postformat = simox::alg::replace_all(postformat, "%msec", std::to_string(msec));
        postformat = simox::alg::replace_all(postformat, "%usec", std::to_string(usec));
        return postformat;
    }


    Duration
    Duration::operator+(const Duration& rhs) const
    {
        return Duration(_microSeconds + rhs._microSeconds);
    }


    Duration&
    Duration::operator+=(const Duration& rhs)
    {
        _microSeconds += rhs._microSeconds;
        return *this;
    }


    Duration
    Duration::operator-() const
    {
        return Duration(-_microSeconds);
    }


    Duration
    Duration::operator-(const Duration& rhs) const
    {
        return Duration(_microSeconds - rhs._microSeconds);
    }


    Duration&
    Duration::operator-=(const Duration& rhs)
    {
        _microSeconds -= rhs._microSeconds;
        return *this;
    }


    Duration
    Duration::operator*(const double rhs) const
    {
        Duration t;
        t._microSeconds = static_cast<std::int64_t>(static_cast<double>(_microSeconds) * rhs + 0.5);
        return t;
    }


    Duration
    Duration::operator*(const int rhs) const
    {
        return *this * static_cast<double>(rhs);
    }


    Duration
    Duration::operator*(const std::int64_t rhs) const
    {
        return *this * static_cast<double>(rhs);
    }


    Duration&
    Duration::operator*=(const double rhs)
    {
        _microSeconds = static_cast<std::int64_t>(static_cast<double>(_microSeconds) * rhs);
        return *this;
    }


    Duration&
    Duration::operator*=(const int rhs)
    {
        return *this *= static_cast<double>(rhs);
    }


    Duration&
    Duration::operator*=(const std::int64_t rhs)
    {
        return *this *= static_cast<double>(rhs);
    }


    double
    Duration::operator/(const Duration& rhs) const
    {
        return static_cast<double>(_microSeconds) / static_cast<double>(rhs._microSeconds);
    }


    Duration
    Duration::operator/(const double rhs) const
    {
        Duration t;
        t._microSeconds = static_cast<std::int64_t>(static_cast<double>(_microSeconds) / rhs);
        return t;
    }


    Duration
    Duration::operator/(const int rhs) const
    {
        return *this / static_cast<double>(rhs);
    }


    Duration
    Duration::operator/(const std::int64_t rhs) const
    {
        return *this / static_cast<double>(rhs);
    }


    Duration&
    Duration::operator/=(const double rhs)
    {
        _microSeconds = static_cast<std::int64_t>(static_cast<double>(_microSeconds) / rhs);
        return *this;
    }


    Duration&
    Duration::operator/=(const int rhs)
    {
        return *this /= static_cast<double>(rhs);
    }


    Duration&
    Duration::operator/=(const std::int64_t rhs)
    {
        return *this /= static_cast<double>(rhs);
    }


    bool
    Duration::operator<(const Duration& rhs) const
    {
        return _microSeconds < rhs._microSeconds;
    }


    bool
    Duration::operator<=(const Duration& rhs) const
    {
        return _microSeconds <= rhs._microSeconds;
    }


    bool
    Duration::operator==(const Duration& rhs) const
    {
        return _microSeconds == rhs._microSeconds;
    }


    bool
    Duration::operator!=(const Duration& rhs) const
    {
        return _microSeconds != rhs._microSeconds;
    }


    bool
    Duration::operator>=(const Duration& rhs) const
    {
        return _microSeconds >= rhs._microSeconds;
    }


    bool
    Duration::operator>(const Duration& rhs) const
    {
        return _microSeconds > rhs._microSeconds;
    }


    std::ostream&
    operator<<(std::ostream& out, const Duration& rhs)
    {
        out << rhs.toDurationString();
        return out;
    }

} // namespace armarx::core::time

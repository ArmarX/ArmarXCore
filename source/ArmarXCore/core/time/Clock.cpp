#include "Clock.h"

#include <unistd.h>
#include <limits.h>

#include <chrono>

#include <ArmarXCore/core/time/LocalTimeServer.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/core/time/ice_conversions.h>


namespace armarx::core::time
{

    Clock::Clock(ClockType clockType) : _clockType{clockType}
    {
        ;
    }


    DateTime
    Clock::now() const
    {
        armarx::LocalTimeServerPtr timeServer = armarx::TimeUtil::GetTimeServer();

        std::chrono::microseconds timestamp_usec;

        switch (_clockType)
        {
            case ClockType::Virtual:
            {
                if (timeServer)
                {
                    timestamp_usec = std::chrono::milliseconds{timeServer->getTime()};
                    break;
                }
                else
                {
                    // If no time server is set, use realtime instead.
                    [[fallthrough]];
                }
            }
            case ClockType::Realtime:
            {
                timestamp_usec = std::chrono::time_point_cast<std::chrono::microseconds>(
                                     std::chrono::system_clock::now())
                                     .time_since_epoch();
                break;
            }
            case ClockType::Monotonic:
            {
                timestamp_usec = std::chrono::time_point_cast<std::chrono::microseconds>(
                                     std::chrono::steady_clock::now())
                                     .time_since_epoch();
                break;
            }
            case ClockType::Unknown:
            {
                throw std::runtime_error{"Cannot asses current time from unknown clock."};
            }
        }

        const std::string hostname = []{
            char hostname[HOST_NAME_MAX];
            gethostname(hostname, HOST_NAME_MAX);
            return std::string(hostname);
        }();

        return DateTime(Duration::MicroSeconds(timestamp_usec.count()), _clockType, hostname);
    }


    void
    Clock::waitFor(const Duration& duration) const
    {
        armarx::TimeUtil::SleepUS(duration.toMicroSeconds());
    }


    Duration
    Clock::waitUntil(const DateTime& time) const
    {
        const Duration difference = time - now();
        if (difference.isPositive())
        {
            waitFor(difference);
        }
        return difference;
    }


    Clock Clock::_virtualClock = Clock(ClockType::Virtual);


    DateTime
    Clock::Now()
    {
        return Clock::_virtualClock.now();
    }


    void
    Clock::WaitFor(const Duration& duration)
    {
        Clock::_virtualClock.waitFor(duration);
    }


    Duration
    Clock::WaitUntil(const DateTime& dateTime)
    {
        return Clock::_virtualClock.waitUntil(dateTime);
    }

} // namespace armarx::core::time

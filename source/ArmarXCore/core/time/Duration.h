#pragma once


#include <cstdint>
#include <ostream>
#include <string>


namespace armarx::core::time
{

    /**
     * @brief Represents a duration.
     *
     * API and implementation to match IceUtil::Time (where applicable).
     */
    class Duration
    {
        // Public API.
    public:
        /**
         * @brief Constructs a zero-duration.
         */
        Duration();

        /**
         * @brief Constructs a duration in microseconds.
         * @param microSeconds Amount of microseconds.
         * @return Duration instance.
         */
        static Duration MicroSeconds(std::int64_t microSeconds);

        /**
         * @brief Constructs a duration in microseconds.
         * @param microSeconds Amount of microseconds.
         * @return Duration instance.
         */
        static Duration MicroSecondsDouble(double microSeconds);

        /**
         * @brief Constructs a duration in milliseconds.
         * @param milliSeconds Amount of milliseconds.
         * @return Duration instance.
         */
        static Duration MilliSeconds(std::int64_t milliSeconds);

        /**
         * @brief Constructs a duration in milliseconds.
         * @param milliSeconds Amount of milliseconds.
         * @return Duration instance.
         */
        static Duration MilliSecondsDouble(double milliSeconds);

        /**
         * @brief Constructs a duration in seconds.
         * @param seconds Amount of seconds.
         * @return Duration instance.
         */
        static Duration Seconds(std::int64_t seconds);

        /**
         * @brief Constructs a duration in seconds.
         * @param seconds Amount of seconds.
         * @return Duration instance.
         */
        static Duration SecondsDouble(double seconds);

        /**
         * @brief Constructs a duration in minutes.
         * @param minutes Amount of minutes.
         * @return Duration instance.
         */
        static Duration Minutes(std::int64_t minutes);

        /**
         * @brief Constructs a duration in minutes.
         * @param minutes Amount of minutes.
         * @return Duration instance.
         */
        static Duration MinutesDouble(double minutes);

        /**
         * @brief Constructs a duration in hours.
         * @param hours Amount of hours.
         * @return Duration instance.
         */
        static Duration Hours(std::int64_t hours);

        /**
         * @brief Constructs a duration in hours.
         * @param hours Amount of hours.
         * @return Duration instance.
         */
        static Duration HoursDouble(double hours);

        /**
         * @brief Constructs a duration in days.
         * @param days Amount of days.
         * @return Duration instance.
         */
        static Duration Days(std::int64_t days);

        /**
         * @brief Constructs a duration in days.
         * @param days Amount of days.
         * @return Duration instance.
         */
        static Duration DaysDouble(double days);

        /**
         * @brief Returns the amount of microseconds.
         * @return Amount of microseconds.
         */
        std::int64_t toMicroSeconds() const;

        /**
         * @brief Returns the amount of microseconds.
         * @return Amount of microseconds.
         */
        double toMicroSecondsDouble() const;

        /**
         * @brief Returns the amount of milliseconds.
         * @return Amount of milliseconds.
         */
        std::int64_t toMilliSeconds() const;

        /**
         * @brief Returns the amount of milliseconds.
         * @return Amount of milliseconds.
         */
        double toMilliSecondsDouble() const;

        /**
         * @brief Returns the amount of seconds.
         * @return Amount of seconds.
         */
        std::int64_t toSeconds() const;

        /**
         * @brief Returns the amount of seconds.
         * @return Amount of seconds.
         */
        double toSecondsDouble() const;

        /**
         * @brief Returns the amount of minutes.
         * @return Amount of minutes.
         */
        std::int64_t toMinutes() const;

        /**
         * @brief Returns the amount of minutes.
         * @return Amount of minutes.
         */
        double toMinutesDouble() const;

        /**
         * @brief Returns the amount of hours.
         * @return Amount of hours.
         */
        std::int64_t toHours() const;

        /**
         * @brief Returns the amount of hours.
         * @return Amount of hours.
         */
        double toHoursDouble() const;

        /**
         * @brief Returns the amount of days.
         * @return Amount of days.
         */
        std::int64_t toDays() const;

        /**
         * @brief Returns the amount of days.
         * @return Amount of days.
         */
        double toDaysDouble() const;

        /**
         * @brief Tests whether the duration is positive (value in µs > 0).
         * @return True if duration is positive, else otherwise.
         */
        bool isPositive() const;

        bool isZero() const;

        /**
         * @brief String representation of the current duration in minimal/default format.
         *
         * The minimal representation is a float representation with max. 3 decimals. The unit will
         * be determined by the highest unit whose value is non-zero. For example, 3 seconds and 500
         * milliseconds => "3.5s".
         *
         * @return Formatted duration.
         */
        std::string toDurationString() const;

        /**
         * @brief String representation of the current duration according to given format string.
         *
         * The format is according to https://en.cppreference.com/w/cpp/chrono/c/strftime. For
         * milli seconds and micro seconds, special specifiers "%%msec" and "%%usec" were added
         * respectively.
         *
         * Example format string for "10m 10.987s": "%Mm %S.%%msecs".
         *
         * @param format Format string.
         * @return Formatted duration.
         */
        std::string toDurationString(const std::string& format) const;

        // Operators.
    public:
        Duration operator+(const Duration& rhs) const;

        Duration& operator+=(const Duration& rhs);

        Duration operator-() const;

        Duration operator-(const Duration& rhs) const;

        Duration& operator-=(const Duration& rhs);

        Duration operator*(double rhs) const;

        Duration operator*(int rhs) const;

        Duration operator*(std::int64_t rhs) const;

        Duration& operator*=(double rhs);

        Duration& operator*=(int rhs);

        Duration& operator*=(std::int64_t rhs);

        double operator/(const Duration& rhs) const;

        Duration operator/(double rhs) const;

        Duration operator/(int rhs) const;

        Duration operator/(std::int64_t rhs) const;

        Duration& operator/=(double rhs);

        Duration& operator/=(int rhs);

        Duration& operator/=(std::int64_t rhs);

        bool operator<(const Duration& rhs) const;

        bool operator<=(const Duration& rhs) const;

        bool operator==(const Duration& rhs) const;

        bool operator!=(const Duration& rhs) const;

        bool operator>=(const Duration& rhs) const;

        bool operator>(const Duration& rhs) const;

    protected:
        Duration(std::int64_t microSeconds);

        /**
         * @brief Current duration in microseconds.
         */
        std::int64_t _microSeconds;
    };


    std::ostream& operator<<(std::ostream& out, const Duration& rhs);

} // namespace armarx::core::time


namespace armarx
{
    using core::time::Duration;
} // namespace armarx

/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Clemens Wallrath (uagzs at student dot kit dot edu)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once


#include <ArmarXCore/interface/core/TimeServerInterface.h>

#include <IceUtil/Time.h>

#include <chrono>

/**
 * @page VirtualTimeDoc ArmarX VirtualTime

ArmarX provides the concept of a virtual time which allows components and statecharts to synchronize to time sources other than wall-time/system time.
Time sources are defined via the \ref TimeServerInterface wich is implemented by the ArmarXTimeServer application and the simulator from the ArmarXSimulation package.
Time itself is represented as IceUtil::Time instances.
The "Clock" gui plugin can be used to start, stop, speed up, or slow down the time offered by a TimeServer.

For example, the "Plotter" gui plugin uses virtual time to show all measurements in Simulator time and to pause when the simulation is stopped.

In order to use a TimeServer, one simply has to set the property
\code
ArmarX.UseTimeServer = true
\endcode
in the $HOME/.armarx.default.cfg or the global.cfg of a scenario.
Setting `UseTimeServer=false` will provide each caller of the ArmarX VirtualTime commands with the system time.

To make your components and statecharts VirtualTime compatible, you have to use the following functions

\li replace `IceUtile::Time::now()` with `armarx::TimeUtil::GetTime()` to get the current time.
\li replace `sleep()` and `usleep()` with `armarx::TimeUtil::Sleep()` and `armarx::TimeUtil::MSSleep()`


`armarx::PeriodicTask` uses system time by default, but can be changed to use TimeServer time by setting the `forceSystemTime` constructor parameter to `false`.

Timeouts in statecharts registered with `StateUtility::setTimeoutEvent(...)` will automatically use TimeServer time when `UseTimeServer=true`.
 */

/**
 * @defgroup VirtualTime
 * @ingroup core-utility
 * @copydoc VirtualTimeDoc
 */

namespace armarx
{
    // operator definitions to enable interoperability between IceUtil::Time and std::chrono time
#ifndef BOOST_NO_CXX11_HDR_CHRONO
#define GENERATE_TIME_OPERATOR_COMP(op)\
    template<class Rep, class Period>\
    inline bool operator op(IceUtil::Time a, std::chrono::duration<Rep, Period> b)\
    {\
        return a.toMicroSeconds() op std::chrono::duration_cast<std::chrono::microseconds>(b).count();\
    }\
    template<class Rep, class Period>\
    inline bool operator op(std::chrono::duration<Rep, Period> a, IceUtil::Time b)\
    {\
        return std::chrono::duration_cast<std::chrono::microseconds>(a).count() op b.toMicroSeconds();\
    }
    GENERATE_TIME_OPERATOR_COMP( <=)
    GENERATE_TIME_OPERATOR_COMP( >=)
    GENERATE_TIME_OPERATOR_COMP( <)
    GENERATE_TIME_OPERATOR_COMP( >)
    GENERATE_TIME_OPERATOR_COMP( ==)
    GENERATE_TIME_OPERATOR_COMP( !=)
#undef GENERATE_TIME_OPERATOR_COMP
#define GENERATE_TIME_OPERATORS_ARITH(op)\
    template<class Rep, class Period>\
    inline IceUtil::Time operator op(IceUtil::Time a, std::chrono::duration<Rep, Period> b)\
    {\
        return a op IceUtil::Time::microSeconds(std::chrono::duration_cast<std::chrono::microseconds>(b).count());\
    }\
    template<class Rep, class Period>\
    inline IceUtil::Time operator op(std::chrono::duration<Rep, Period> a, IceUtil::Time b)\
    {\
        return IceUtil::Time::microSeconds(std::chrono::duration_cast<std::chrono::microseconds>(a).count()) op b;\
    }
    GENERATE_TIME_OPERATORS_ARITH(+)
    GENERATE_TIME_OPERATORS_ARITH(-)
    GENERATE_TIME_OPERATORS_ARITH( +=)
    GENERATE_TIME_OPERATORS_ARITH( -=)
#undef GENERATE_TIME_OPERATORS_ARITH
#endif

    // forward declarations to reduce dependency tree
    class LocalTimeServer;
    using LocalTimeServerPtr = IceInternal::Handle<LocalTimeServer>;

    /**
     * @brief Time mode to be used.
     * @ingroup VirtualTime
     *
     * `SystemTime` enforces the local system time, while `VirtualTime` uses the virtual time provided by the time server.
     */
    enum class TimeMode
    {
        SystemTime,
        VirtualTime
    };

    /**
     * @class TimeUtil
     * @brief provides utility functions for getting the current time
     * @ingroup VirtualTime
     */
    class TimeUtil
    {
    public:
        /**
         * @brief Get the current time.
         * @param timeMode Time mode to be used.
         * @return The current time considering timeMode
         */
        static IceUtil::Time GetTime(TimeMode timeMode = TimeMode::VirtualTime);

        /**
         * @deprecated Use `TimeUtil::GetTime(TimeMode)` instead.
         * @brief get the current time
         * @param forceSystemTime If true, this function will always return the time of the system and never the virtual time.
         * @return the current time
         *
         * Depending on if a TimeServer is used, the system time or the
         * TimeServer time is returned
         */
        static IceUtil::Time GetTime(bool forceSystemTime);

        /**
         * @brief Get the difference between the current time and a reference time.
         * @param referenceTime Reference point in time.
         * @param timeMode Time mode to be used.
         * @return Current timestamp minus the given reference time to get the delta.
         */
        static IceUtil::Time GetTimeSince(IceUtil::Time referenceTime,
                                          TimeMode timeMode = TimeMode::VirtualTime);

        /**
         * @deprecated Use `TimeUtil::GetTimeSince(IceUtil::Time, TimeMode)` instead.
         * @brief Get the difference between the current time and a reference time.
         * @param referenceTime Reference point in time.
         * @param forceSystemTime If true, this function will always return the time of the system
         *        and never the virtual time.
         * @return Current timestamp minus the given reference time to get the delta.
         */
        static IceUtil::Time GetTimeSince(IceUtil::Time referenceTime,
                                          bool forceSystemTime);

        /**
         * @brief lock the calling thread for a given duration (like usleep(...) but using Timeserver time)
         * @param duration how long to sleep
         *
         * Sleep for the given duration using timeserver time. Granularity is limited to 1 ms when a Timeserver is running (else 1 us).
         */
        static void Sleep(IceUtil::Time duration);

        /**
         * @brief lock the calling thread for a given duration (like usleep(...) but using Timeserver time)
         * @param duration how long to sleep
         *
         * Sleep for the given duration using timeserver time. Granularity is limited to 1 ms when a Timeserver is running (else 1 us).
         */
        template<class Rep, class Period>
        static void Sleep(std::chrono::duration<Rep, Period> d)
        {
            Sleep(IceUtil::Time::microSeconds(std::chrono::duration_cast<std::chrono::microseconds>(d).count()));
        }

        static void Sleep(float seconds)
        {
            Sleep(IceUtil::Time::microSeconds(int(1000000 * seconds)));
        }
        static void SleepMS(float milliseconds)
        {
            Sleep(IceUtil::Time::microSeconds(int(1000 * milliseconds)));
        }
        static void SleepUS(float microseconds)
        {
            Sleep(IceUtil::Time::microSeconds(int(microseconds)));
        }

        /**
         * @brief lock the calling thread for a given duration (like usleep(...) but using Timeserver time)
         * @param durationMS how long to sleep in milliseconds
         */
        static void MSSleep(int durationMS);
        /**
         * @brief block until the next tick of the timeserver. Noop if no timeserver in use.
         *
         * Can be used to pause execution if the timeserver is paused.
         * If the timeserver is runing, this may block for up to one tick interval (typically 1ms).
         */
        static void WaitForNextTick();

        static void SetTimeServer(LocalTimeServerPtr ts);
        static LocalTimeServerPtr GetTimeServer();
        /**
         * @brief check if we have been initialized with a Timeserver
         **/
        static bool HasTimeServer();

        /**
         * @brief like timed_wait on boost condition_variables, but with timeserver support
         *
         * The implementation only checks for timeout periodically (see granularity parameter), so precision is limited
         * @param cond the boost::condition_variable to wait on (as shared_ptr)
         * @param lock an already locked unique_lock
         * @param duration timeout
         * @param granularity how often timeout is checked, default is to use a tenth of duration
         * @return false, if the thread was unlocked due to a timeout, else true
         **/
        // static bool TimedWait(boost::condition_variable& cond, boost::unique_lock<boost::mutex>& lock, IceUtil::Time duration, IceUtil::Time granularity = IceUtil::Time());


        /**
         * Usleep convenience function that uses internally nanosleep.
         * @note This function does not use virtual time!
         */
        static int USleep(long usec);
        /**
         * Nanosleep convenience function.
         * @note This function does not use virtual time!
         */
        static int NanoSleep(long usec);


        /// Return a date string like "2020-01-31" (Y-M-D).
        static std::string toStringDate(const IceUtil::Time& time);

        /// Return a time string like "15-30-05" (H-M-S).
        static std::string toStringTime(const IceUtil::Time& time);

        /// Return a date & time string like "2020-01-31_15-30-05" (Y-M-D_H-M-S).
        static std::string toStringDateTime(const IceUtil::Time& time);


    protected:

        TimeUtil();

        /**
         * @brief pointer to the applications LocalTimeServer
         * if NULL, system time is used
         */
        static LocalTimeServerPtr timeServerPtr;
    };

    /** \ingroup VirtualTime
     * Helper macro to do timing tests.
     * Usage:
     * \verbatim
       TIMING_START(descriptiveName) // create variables with name descriptiveName for later reference
       // do your stuff
       TIMING_END(descriptiveName) // prints duration with descriptiveName as prefix
       TIMING_CEND(descriptiveName, 10) // only printed if takes longer than 10ms
       \endverbatim
     */
#define TIMING_START(name) auto name = IceUtil::Time::now();

    //! \ingroup VirtualTime
    //! Prints duration with comment in front of it, yet only once per second.
#define TIMING_END_COMMENT_STREAM(name, comment, os) \
    { \
        name = (IceUtil::Time::now() - name); \
        os << deactivateSpam(1, comment) << comment << " - duration: " << name.toMilliSecondsDouble() << " ms"; \
    }

    //! \ingroup VirtualTime
    //! Prints duration with comment in front of it, yet only once per second.
#define TIMING_END_COMMENT(name, comment) TIMING_END_COMMENT_STREAM(name, comment, ARMARX_INFO)

    //! \ingroup VirtualTime
    //! Prints duration
#define TIMING_END(name) TIMING_END_COMMENT(name, #name)

    //! \ingroup VirtualTime
    //! Prints duration
#define TIMING_END_STREAM(name, os) TIMING_END_COMMENT_STREAM(name, #name, os)


    //! \ingroup VirtualTime
    //! Prints duration with comment in front of it if it took longer than threshold
#define TIMING_CEND_COMMENT(name, comment, thresholdMs) if((IceUtil::Time::now()-name).toMilliSecondsDouble() >= thresholdMs) TIMING_END_COMMENT(name, comment)

    //! \ingroup VirtualTime
    //! Prints duration if it took longer than thresholdMs
#define TIMING_CEND(name, thresholdMs) TIMING_CEND_COMMENT(name, #name, thresholdMs)


}


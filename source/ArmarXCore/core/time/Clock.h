#pragma once


#include <ArmarXCore/core/time/ClockType.h>
#include <ArmarXCore/core/time/DateTime.h>
#include <ArmarXCore/core/time/Duration.h>


namespace armarx::core::time
{

    /**
     * @brief Clock to get date/time from a specific clock type or wait for certain durations
     * or until certain date/times in a given clock.
     *
     * In most cases, using the virtual clock is the desired behaviour. The virtual clock reports
     * the current time according to the configured time server, or (as a fallback) the operating
     * systems real time clock. The virtual clock can be accessed through the static methods of
     * this clock.
     *
     * For other cases, i.e., where the actual operating system real time clock is needed
     * (ClockType::Realtime), or a monotonic clock is required (ClockType::Monotonic), a clock
     * with the needed clock type as constructor parameter can be instantiated. The API is then
     * similar to those of the virtual clock using the corresponding non-static member functions.
     */
    class Clock
    {

    public:
        /**
         * @brief Constructs a new clock of given clock type (virtual by default).
         * @param clockType Clock type.
         */
        Clock(ClockType clockType = ClockType::Virtual);

        /**
         * @brief Current date/time of the clock.
         * @return Current date/time.
         */
        DateTime now() const;

        /**
         * @brief Wait for a certain duration.
         * @param duration How long to wait.
         */
        void waitFor(const Duration& duration) const;

        /**
         * @brief Wait and block until the given date/time is surpassed.
         * @param dateTime Date/time to wait until.
         * @return Waiting duration.
         */
        Duration waitUntil(const DateTime& dateTime) const;

        /**
         * @brief Current time on the virtual clock.
         * @return Current date/time.
         */
        static DateTime Now();

        /**
         * @brief Wait for a certain duration on the virtual clock.
         * @param duration How long to wait.
         */
        static void WaitFor(const Duration& duration);

        /**
         * @brief Wait and block until the given date/time is surpassed on the virtual clock.
         * @param dateTime Date/time to wait until.
         * @return Waiting duration.
         */
        static Duration WaitUntil(const DateTime& dateTime);

    private:
        /**
         * @brief Static instance of the virtual clock for static short-hand methods.
         */
        static Clock _virtualClock;

        /**
         * @brief Own clock type.
         */
        ClockType _clockType;
    };

} // namespace armarx::core::time


namespace armarx
{
    using core::time::Clock;
}

#pragma once


namespace armarx::core::time
{
    enum class ClockType;
    class Clock;
    class DateTime;
    class Duration;
    class Frequency;
    class Metronome;
    class ScopedStopWatch;
    class StopWatch;
}

// Ice types.
namespace armarx::core::time::dto
{
    struct DateTime;
    struct Duration;
    struct Frequency;
}

namespace armarx
{
    using core::time::Clock;
    using core::time::ClockType;
    using core::time::DateTime;
    using core::time::Duration;
    using core::time::Frequency;
    using core::time::Metronome;
} // namespace armarx

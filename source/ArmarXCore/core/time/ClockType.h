#pragma once


namespace armarx::core::time
{

    /**
     * @brief Describes the type of clock.
     */
    enum class ClockType
    {
        /// Normalized time as reported by the operating system.
        Realtime,
        /// Monotonic/steady clock of the operating system.
        Monotonic,
        /// Time given by time server if configured, realtime otherwise.
        Virtual,
        /// Unknown source of time.
        Unknown
    };

} // namespace armarx::core::time


namespace armarx
{
    using core::time::ClockType;
} // namespace armarx

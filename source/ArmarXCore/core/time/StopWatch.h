#pragma once


#include <functional>

#include <IceUtil/Time.h>

#include <ArmarXCore/core/time/Clock.h>
#include <ArmarXCore/core/time/ClockType.h>
#include <ArmarXCore/core/time/DateTime.h>
#include <ArmarXCore/core/time/Duration.h>
#include <ArmarXCore/core/time/TimeUtil.h>


namespace armarx::core::time
{
    /**
     * @brief Measures the passed time between the construction or calling `reset()` and `stop()`.
     *
     * The `StopWatch` uses the system time by default, but it may use the virtual time provided
     * by the time server as well.  Also has a static method `measure()`, which takes a lambda, and
     * returns the time it took to execute that lambda.
     *
     * Code examples:
     *
     * @code
     * // By supplied methods.
     * StopWatch sw;
     * long_operation();
     * Duration duration = sw.stop();
     * std::cout << "Operation took " << duration << ".";
     * @endcode
     *
     * @code
     * // By executing a lambda.
     * Duration duration = StopWatch::measure([&]() {
     *     long_operation();
     * });
     * std::cout << "Operation took " << duration << ".";
     * @endcode
     */
    class StopWatch
    {

    public:
        /**
         * @brief Constructs a `StopWatch` and starts it immediately.
         * @param clockType Clock type.
         */
        StopWatch(ClockType clockType = ClockType::Virtual);

        /**
         * @brief Destructs the `StopWatch`.
         */
        virtual ~StopWatch();

        /**
         * @brief Measures the duration needed to execute the given lambda and returns it.
         * @param subjectToMeasure Lambda to be measured
         * @param clockType Clock type.
         * @return Time it took to execute the given lambda.
         */
        static Duration measure(std::function<void(void)> subjectToMeasure,
                                ClockType clockType = ClockType::Virtual);

        /**
         * @brief Stops the timer and returns the measured duration.
         * @return Duration elapsed since construction or last call of `reset()`.
         */
        Duration stop();

        /**
         * @brief Resets the timer.
         */
        void reset();

        /**
         * @brief Returns whether the timer is stopped or is actively measuring time.
         * @return True of timer is stopped, false otherwise.
         */
        bool isStopped() const;

        /**
         * @brief Returns the date/time at starting the timer.
         * @return Date/time at starting the timer.
         */
        DateTime startingTime() const;

        /**
         * @brief Returns the date/time at stopping the timer.
         * @return Date/time at stopping the timer.
         *
         * @throw std::logic_error When the timer was not stopped yet.
         */
        DateTime stoppingTime() const;

        /**
         * @brief Stops and resets the timer. It returns the date/time at stopping the timer.
         * @return Date/time at stopping the timer.
         *
         * @throw std::logic_error When the timer was not stopped yet.
         */
        Duration stopAndReset();

    private:
        Clock _clock;

        DateTime _startingTime;

        Duration _time;
    };

} // namespace armarx::core::time


namespace armarx
{


    class [[deprecated("Use armarx::core::time::StopWatch instead")]] StopWatch
    {

    public:
        StopWatch(armarx::TimeMode timeMode = armarx::TimeMode::SystemTime);
        virtual ~StopWatch();
        static IceUtil::Time measure(std::function<void(void)> subjectToMeasure,
                                     armarx::TimeMode timeMode = armarx::TimeMode::SystemTime);
        IceUtil::Time stop();
        void reset();
        bool isStopped() const;
        IceUtil::Time startingTime() const;
        IceUtil::Time stoppingTime() const;

    private:
        armarx::TimeMode m_time_mode;
        IceUtil::Time m_starting_time;
        IceUtil::Time m_time;
    };
} // namespace armarx

#pragma once

#include <SimoxUtility/json/json.h>

#include <ArmarXCore/core/time/forward_declarations.h>


namespace armarx::core::time
{

    void to_json(simox::json::json& j, const ClockType& bo);
    void from_json(const simox::json::json& j, ClockType& bo);

    void to_json(simox::json::json& j, const Duration& bo);
    void from_json(const simox::json::json& j, Duration& bo);

    void to_json(simox::json::json& j, const Frequency& bo);
    void from_json(const simox::json::json& j, Frequency& bo);

    void to_json(simox::json::json& j, const DateTime& bo);
    void from_json(const simox::json::json& j, DateTime& bo);

} // namespace armarx::core::time


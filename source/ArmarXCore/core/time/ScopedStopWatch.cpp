#include "ScopedStopWatch.h"

#include <functional>

#include <IceUtil/Time.h>

#include <ArmarXCore/core/time/TimeUtil.h>


namespace armarx::core::time
{

    ScopedStopWatch::ScopedStopWatch(std::function<void(const Duration&)> callback,
                                     ClockType clockType) :
        StopWatch(clockType), _callback{callback}
    {
        // pass
    }


    ScopedStopWatch::~ScopedStopWatch()
    {
        _callback(stop());
    }

} // namespace armarx::core::time


namespace armarx
{
    ScopedStopWatch::ScopedStopWatch(std::function<void(IceUtil::Time)> callback,
                                     TimeMode timeMode) :
        StopWatch(timeMode), m_callback{callback}
    {
        // pass
    }


    ScopedStopWatch::~ScopedStopWatch()
    {
        m_callback(stop());
    }
} // namespace armarx

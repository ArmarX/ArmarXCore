#pragma once


#include <cstdint>
#include <ostream>
#include <string>

#include <ArmarXCore/core/time/ClockType.h>
#include <ArmarXCore/core/time/Duration.h>

namespace IceUtil
{
    class Time;
}

namespace armarx::core::time
{

    /**
     * @brief Represents a point in time.
     *
     * API and implementation to match IceUtil::Time (where applicable).
     */
    class DateTime
    {

    public:
        DateTime();

        DateTime(Duration timeSinceEpoch,
                 ClockType clock = ClockType::Virtual,
                 const std::string& hostname = "unknown");

        DateTime(std::int64_t microSecondsSinceEpoch,
                 ClockType clock = ClockType::Virtual,
                 const std::string& hostname = "unknown");

        static DateTime Now();
        static DateTime Invalid();

        std::string toDateString() const;

        std::string toTimeString() const;

        std::string toDateTimeString() const;

        /**
         * @brief String representation of current date time according to given format string.
         *
         * The format is according to https://en.cppreference.com/w/cpp/chrono/c/strftime. For
         * milli seconds and micro seconds, special specifiers "%%msec" and "%%usec" were added
         * respectively.
         *
         * Example format string for "2022-03-04_16-59-10.981789": "%Y-%m-%d_%H-%M-%S.%%usec".
         *
         * @param format Format string.
         * @return Formatted date time.
         */
        std::string toString(const std::string& format) const;

        std::int64_t toMicroSecondsSinceEpoch() const;

        std::int64_t toMilliSecondsSinceEpoch() const;

        std::int64_t toSecondsSinceEpoch() const;

        Duration toDurationSinceEpoch() const;

        ClockType clockType() const;

        std::string hostname() const;

        bool isValid() const;
        bool isInvalid() const;


        // DEPRECATED IceUtil::Time-style API.
        [[deprecated("Using IceUtil::Time in ArmarX is deprecated. Use armarx::DateTime instead.")]]
        DateTime(const IceUtil::Time& ice);

        [[deprecated("Using IceUtil::Time in ArmarX is deprecated. Use armarx::DateTime instead.")]]
        operator IceUtil::Time() const;

        // IceUtil::Time-compatible legacy API.
        [[deprecated("The IceUtil::Time-style API is deprecated. Use DateTime::Now() instead.")]]
        static DateTime now()
        {
            return Now();
        }
        [[deprecated("The IceUtil::Time-style API is deprecated. Use DateTime(Duration::MicroSeconds(...)) instead.")]]
        static DateTime microSeconds(long microSeconds)
        {
            return DateTime(Duration::MicroSeconds(microSeconds));
        }

        [[deprecated("The IceUtil::Time-style API is deprecated. Use toMicroSecondsSinceEpoch() instead.")]]
        std::int64_t toMicroSeconds() const
        {
            return toMicroSecondsSinceEpoch();
        }

        // Operators.
    public:
        DateTime operator+(const Duration& rhs) const;

        DateTime& operator+=(const Duration& rhs);

        DateTime operator-(const Duration& rhs) const;

        DateTime& operator-=(const Duration& rhs);

        Duration operator-(const DateTime& rhs) const;

        bool operator<(const DateTime& rhs) const;

        bool operator<=(const DateTime& rhs) const;

        bool operator==(const DateTime& rhs) const;

        bool operator>(const DateTime& rhs) const;

        bool operator>=(const DateTime& rhs) const;

        bool operator!=(const DateTime& rhs) const;

    protected:

        static const Duration _invalidTimeSinceEpoch;

        Duration _timeSinceEpoch;

        ClockType _clockType;

        std::string _hostname;
    };


    std::ostream& operator<<(std::ostream& out, const DateTime& rhs);

} // namespace armarx::core::time


namespace armarx
{
    using core::time::DateTime;
} // namespace armarx

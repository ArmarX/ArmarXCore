/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Kai Welke (welke at kit dot edu)
* @author     Jan Issac (jan dot issac at gmx dot de)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExport.h>                    // for ARMARXCORE_IMPORT_EXPORT
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>   // for ARMARX_CHECK_EXPRESSION
#include <ArmarXCore/core/exceptions/LocalException.h>              // for LocalException
#include <ArmarXCore/core/logging/Logging.h>                        // for Logging
#include <ArmarXCore/core/ArmarXFwd.h>                              // for ArmarXManagerPtr, etc
#include <ArmarXCore/core/IceManager.h>                             // for IceManagerPtr
#include <ArmarXCore/core/ManagedIceObjectPlugin.h>

#include <Ice/Handle.h>                 // for Handle
#include <Ice/Object.h>                 // for Object
#include <Ice/ProxyF.h>                 // for ObjectPrx
#include <IceUtil/Handle.h>             // for HandleBase

#include <type_traits>
#include <typeindex>
#include <string>                       // for string
#include <vector>                       // for vector
#include <memory>                       // for unique_ptr

// Forward declarations
namespace std::experimental
{
    inline namespace fundamentals_v2
    {
        template <typename T>
        class observer_ptr;
        
        template<typename _Tp>
        observer_ptr<_Tp>
        make_observer(_Tp* __p) noexcept;
    }
}

namespace Ice
{

    class Communicator;
    ICE_API ::Ice::LocalObject* upCast(::Ice::Communicator*);
    typedef ::IceInternal::Handle< ::Ice::Communicator> CommunicatorPtr;

    class ObjectAdapter;
    ICE_API ::Ice::LocalObject* upCast(::Ice::ObjectAdapter*);
    typedef ::IceInternal::Handle< ::Ice::ObjectAdapter> ObjectAdapterPtr;

}

namespace armarx::Profiler
{
    class Profiler;
    using ProfilerPtr = std::shared_ptr<Profiler>;
}
namespace armarx
{
    template<class> class SimplePeriodicTask;
#define TYPEDEF_PTRS_SHARED(T)                                      \
    class T;                                                        \
    using T##Ptr = ::std::shared_ptr<T>;                            \
    using Const##T##Ptr = ::std::shared_ptr<const T>

#define TYPEDEF_PTRS_HANDLE(T)          \
    class T;                            \
    using T##Ptr = IceUtil::Handle<T>

    template<class T, class...Args>
    auto make_shared(Args&& ...args)
    {
        if constexpr(
            std::is_base_of_v<IceUtil::Shared, T> ||
            std::is_base_of_v<IceUtil::SimpleShared, T>
        )
        {
            return IceUtil::Handle<T>(
                       new T(std::forward<Args>(args)...)
                   );
        }
        else
        {
            return std::make_shared<T>(std::forward<Args>(args)...);
        }
    }
}
namespace armarx
{
    class VariantBase;
    typedef ::IceInternal::Handle<::armarx::VariantBase> VariantBasePtr;
    using StringVariantBaseMap = std::map<std::string, VariantBasePtr>;

    /**
     * ManagedIceObject shared pointer for convenience
     */
    class ManagedIceObject;

    struct ManagedIceObjectConnectivity;


    class ManagedIceObjectPlugin;

    using ManagedIceObjectPtr = IceInternal::Handle<ManagedIceObject>;

    /**
     * @class ManagedIceObject
     * @ingroup DistributedProcessingGrp
     * @brief The ManagedIceObject is the base class for all ArmarX objects.
     *
     * ManagedIceObject are the ArmarX equivalent to well-known Ice objects.
     * A defined lifecycle is guaranteed which and state dependent interface
     * is provided by the following framework hooks:
     * @li ManagedIceObjects::onInitComponent()
     * @li ManagedIceObjects::onConnectComponent()
     * @li ManagedIceObjects::onExitComponent()
     *
     * @image html Lifecycle-UML.svg Lifecyle of a ManagedIceObject managed by the ArmarXObjectScheduler
     * Every subclass of ManagedIceObject can specify dependent proxies and topics
     * as well as offered topics in ManagedIceObjects::onInitComponent().
     * These connectivity parameters are specified through the following methods:
     * @li MangedIceObject::usingProxy()
     * @li MangedIceObject::usingTopic()
     * @li MangedIceObject::offeringTopic()
     *
     * These dependencies are resolved at runtime and can be visualized.
     * Additionally it is possible to handle the case of crashed dependencies.
     \code
     MyManagedIceObject::onInitComponent()
     {
         // component depends on proxy "MyDataProvider"
         usingProxy("MyDataProvider");

         // component wants to subscribe to topic "News"
         usingTopic("News");

         // component will offer topic "FilteredNews"
         offeringTopic("FilteredNews");
     }
     \endcode

     @see ArmarXObjectScheduler
     */
    class ARMARXCORE_IMPORT_EXPORT ManagedIceObject :
        virtual public Ice::Object,
        virtual public Logging

    {
    private:
        friend class Component;
        friend class ManagedIceObjectPlugin;
        std::map<std::pair<std::type_index, std::string>, std::unique_ptr<ManagedIceObjectPlugin>> _plugins;

        void foreach_plugin(std::function<void(std::type_index, std::string const&, ManagedIceObjectPlugin*)> const& f,
                            int line, const char* file, const char* function);

    protected:

        std::unique_ptr<ManagedIceObjectPlugin>& getPluginPointer(std::type_info const& type, std::string const& prefix);


        template <class PluginT, class...ParamsT>
        PluginT* addPlugin(const std::string prefix = "", ParamsT && ...params)
        {
            static_assert(std::is_base_of_v<ManagedIceObjectPlugin, PluginT>);
            auto& ptr = getPluginPointer(typeid(PluginT), prefix);
            if (ptr)
            {
                auto tptr = dynamic_cast<PluginT*>(ptr.get());
                ARMARX_CHECK_NOT_NULL(tptr) << "Plugin is of wrong type. This should be impossible!";
                return tptr;
            }
            PluginT* tptr = new PluginT(*this, prefix, std::forward<ParamsT>(params)...);
            ptr.reset(tptr);
            return tptr;
        }

        template<class PluginT, class...ParamsT>
        void addPlugin(PluginT*& targ, const std::string prefix = "", ParamsT && ...params)
        {
            ARMARX_CHECK_NULL(targ) << "This is an out parameter";
            targ = addPlugin<PluginT>(prefix, std::forward<ParamsT>(params)...);
        }

        template<class PluginT, class...ParamsT>
        void addPlugin(std::experimental::observer_ptr<PluginT>& targ, const std::string prefix = "", ParamsT && ...params)
        {
            ARMARX_CHECK_NULL(targ) << "This is an out parameter";
            targ = std::experimental::make_observer(addPlugin<PluginT>(prefix, std::forward<ParamsT>(params)...));
        }


    private:
        friend class ArmarXObjectScheduler;
        friend class ArmarXManager;

    public:
        using PeriodicTaskPtr = IceUtil::Handle<SimplePeriodicTask<std::function<void(void)>>>;

        /**
         * @brief A nullptr to be used when a const ref to a nullptr is required.
         */
        static const ManagedIceObjectPtr NullPtr;

        ManagedIceObject(ManagedIceObject const& other);


        /**
        * Retrieve name of object. Corresponds to well-known object name.
        *
        * @return name
        */
        std::string getName() const;

        /**
         * @brief Generates a unique name for a sub object from a general name and unique name.
         * @param superObjectName The name of the super object (has to be unique)
         * @param subObjectName The sub object's name (has to be unique among the sub objects)
         * @return
         */
        static std::string generateSubObjectName(const std::string& superObjectName, const std::string& subObjectName);

        /**
         * @brief Generates a unique name for a sub object from a general name.(given the current object name is unique)
         * @param subObjectName The sub object's name (has to be unique among the sub objects)
         * @return The unique sub object name
         *
         * Generates a unique name for a sub object from a general name.
         * E.g.:
         * Two objects A (object name "A") and B (object name "B") have a DebugDrawer as sub object.
         * A and B can generate different object names for the debug drawer by calling this function with the same name "DebugDrawer".
         * Additionally the created name contains the super object's name.
         */
        std::string generateSubObjectName(const std::string& subObjectName);

        /**
         * @brief Returns the proxy of this object (optionally it waits for the proxy)
         * @param timeoutMs Timeout in miliseconds until this function stops waiting for the proxy.
         * Set to -1 for infinite waiting.
         * @return object's proxy (will be null if the time runs out/no object scheduler was set (e.g. by calling addObject on an ArmarXManager))
         */
        Ice::ObjectPrx getProxy(long timeoutMs = 0, bool waitForScheduler = true) const;
        template<class Prx>
        Prx getProxy(long timeoutMs = 0, bool waitForScheduler = true) const
        {
            return Prx::checkedCast(getProxy(timeoutMs, waitForScheduler));
        }
        template<class Prx>
        void getProxy(Prx& prx, long timeoutMs = 0, bool waitForScheduler = true) const
        {
            prx = Prx::checkedCast(getProxy(timeoutMs, waitForScheduler));
        }

        /**
        * Retrieve current state of the ManagedIceObject
        *
        * @return state of the ManagedIceObject
        */
        int getState() const;



        ArmarXObjectSchedulerPtr getObjectScheduler() const;

        /**
        * Retrieve connectivity of the object (topcis as well as proxies)
        *
        * @return connectivity
        */
        ManagedIceObjectConnectivity getConnectivity() const;

        /**
         * Returns the ArmarX manager used to add and remove components
         *
         * @retrun pointer to the armarXManager
         */
        ArmarXManagerPtr getArmarXManager() const;

        /**
         * Returns the IceManager
         *
         * @return pointer to IceManager
         */
        IceManagerPtr getIceManager() const;

        /**
         * Registers a proxy for retrieval after initialization and adds it to the dependency list.
         * This ManagedIceObject won't start (onConnectComponent()) until this proxy is avaiable.
         *
         * @param name          Proxy name
         * @param endpoints     Specific endpoints, e.g. tcp ‑p 10002
         * @return returns true if new dependency, else false
         */
        bool usingProxy(const std::string& name,
                        const std::string& endpoints = "");

        void waitForProxy(std::string const& name, bool addToDependencies);

        /**
         * Retrieves a proxy object.
         *
         * @param name         Proxy name, e.g. Log
         * @param addToDependencies If true, this proxy is added to the dependency list and this function won't return until the proxy becomes available.
         * @param endpoints    The endpoints, e.g. tcp ‑p 10002
         * @param throwOnProxyError    If true, throws Exception on any proxy error. If false, just returns zero pointer.
         * @throw LocalException Throws if proxy is not available, this object is not yet starting or the proxyName is empty.
         * @return A proxy of the remote instance.
         */
        template <class ProxyType>
        ProxyType getProxy(const std::string& name,
                           bool addToDependencies = false,
                           const std::string& endpoints = "",
                           bool throwOnProxyError = true)
        {
            waitForProxy(name, addToDependencies);

            // return proxy
            try
            {
                return getIceManager()->getProxy<ProxyType>(name, endpoints);
            }
            catch (...)
            {
                if (throwOnProxyError)
                {
                    throw;
                }
                else
                {
                    return nullptr;
                }
            }
        }

        /**
         * @brief Assigns a proxy to `proxy`.
         * This method can be called without explicit template argument, as
         * the proxy type can be inferred from the arguments.
         * @see getProxy<>()
         */
        template <class ProxyTarg, class...Args>
        void getProxy(IceInternal::ProxyHandle<ProxyTarg>& proxy, const std::string& name, Args&& ...args)
        {
            using ProxyType = IceInternal::ProxyHandle<ProxyTarg>;
            proxy = getProxy<ProxyType>(name, std::forward<Args>(args)...);
        }
        template <class ProxyTarg, class...Args>
        void getProxy(const std::string& name, IceInternal::ProxyHandle<ProxyTarg>& proxy, Args&& ...args)
        {
            using ProxyType = IceInternal::ProxyHandle<ProxyTarg>;
            proxy = getProxy<ProxyType>(name, std::forward<Args>(args)...);
        }
        template <class ProxyTarg, class...Args>
        void getProxy(IceInternal::ProxyHandle<ProxyTarg>& proxy, const char* name, Args&& ...args)
        {
            using ProxyType = IceInternal::ProxyHandle<ProxyTarg>;
            proxy = getProxy<ProxyType>(name, std::forward<Args>(args)...);
        }
        template <class ProxyTarg, class...Args>
        void getProxy(const char* name, IceInternal::ProxyHandle<ProxyTarg>& proxy, Args&& ...args)
        {
            using ProxyType = IceInternal::ProxyHandle<ProxyTarg>;
            proxy = getProxy<ProxyType>(name, std::forward<Args>(args)...);
        }

        /// Overload to allow using string literals as name (solve ambiguous overload).
        template <class ProxyType>
        inline void getProxy(ProxyType& proxy, const char* name,
                             bool addToDependencies = false,
                             const std::string& endpoints = "",
                             bool throwOnProxyError = true)
        {
            getProxy<ProxyType>(proxy, std::string(name), addToDependencies, endpoints, throwOnProxyError);
        }

        /**
         * @brief returns the names of all unresolved dependencies
         */
        std::vector<std::string> getUnresolvedDependencies() const;

        static std::string GetObjectStateAsString(int state);

        /**
         * @brief getProfiler returns an instance of armarx::Profiler
         */
        Profiler::ProfilerPtr getProfiler() const;

        /**
         * @brief setProfiler allows setting ManagedIceObject::profiler to a new instance (if the new instance is actually not a null pointer)
         */
        void enableProfiler(bool enable);

        /**
         * Returns object's Ice adapter
         *
         * @return object's adapter
         */
        Ice::ObjectAdapterPtr getObjectAdapter() const;

        /**
         * Registers a proxy for subscription after initialization
         *
         * @param name          Topic name
         * @see unsubscribeFromTopic()
         */
        void usingTopic(const std::string& name, bool orderedPublishing = false);

        /**
         * @brief Unsubscribe from a topic
         * @param name Name of the topic
         * @return True if the ManagedIceObject was subscribed before, false otherwise.
         */
        bool unsubscribeFromTopic(const std::string& name);

        /**
         * Registers a topic for retrival after initialization
         *
         * @param name          Topic name
         */
        void offeringTopic(const std::string& name);

        void preambleGetTopic(std::string const& name);

        /**
         * Returns a proxy of the specified topic.
         *
         * @param name          Topic name
         *
         * @return Topic proxy of the type \e TopicProxyType
         */
        template <class TopicProxyType>
        TopicProxyType getTopic(const std::string& name)
        {
            preambleGetTopic(name);

            // retrieve topic
            return getIceManager()->getTopic<TopicProxyType>(name);
        }

        /**
         * @brief Assigns a proxy of the specified topic to `topicProxy`.
         * This method can be called without explicit template argument, as
         * the topic proxy type can be inferred from the arguments.
         *
         * @param topicProxy    The topic proxy to be assigned.
         * @param name          Topic name
         *
         * @see getTopic<>()
         */
        template <class TopicProxyType>
        void getTopic(TopicProxyType& topicProxy, const std::string& name)
        {
            topicProxy = getTopic<TopicProxyType>(name);
        }

        /**
         * @brief Waits until the ObjectScheduler could resolve all dependencies.
         */
        void waitForObjectScheduler();

        /**
         * Retrieve default name of component
         *
         * Implement this method in each IceManagedObject. The default name of a
         * is used if no name is specified in the factory method.
         *
         * @return default name of the component (e.g. "KinematicUnit")
         */
        virtual std::string getDefaultName() const = 0;

        /**
         * @brief Allows to set meta information that can be queried live via Ice interface
         * on the ArmarXManager.
         * @param id Id and description of the meta information.
         * @param value Value to be stored. Should only be basic types and strings.
         */
        void setMetaInfo(const std::string& id, const VariantBasePtr& value);
        VariantBasePtr getMetaInfo(const std::string& id);
        StringVariantBaseMap getMetaInfoMap() const;

        void startPeriodicTask(const std::string& uniqueName,
                               std::function<void(void)> f,
                               int periodMs,
                               bool assureMeanInterval = false,
                               bool forceSystemTime = true);
        bool stopPeriodicTask(const std::string& name);
        PeriodicTaskPtr getPeriodicTask(const std::string& name);
    protected:
        /**
         * Protected default constructor. Used for virtual inheritance. Use createManagedIceObject() instead.
         */
        ManagedIceObject();
        ~ManagedIceObject() override;

        /**
         * @brief This function removes the dependency of this object
         * on the in parameter name specified object.
         * @param name name of the depedency proxy
         * @return returns true if depedency was found and removed
         * false if dependency does not exist
         */
        bool removeProxyDependency(const std::string& name);


        /**
         * Initiates termination of this IceManagedObject. Returns immediately.
         */
        void terminate();

        /**
         * Override name of well-known object
         */
        void setName(std::string name);

        /**
         * Pure virtual hook for the subclass. Is called once initialization of the ManagedIceObject is done.
         * This hook is called in the implenting class once and never again during the lifecyle of the object. This function is called
         * as soon as the ManagedIceObject was added to the ArmarXManager. Called in an own thread and not the thread it was created in.
         *
         */
        virtual void onInitComponent() = 0;
        virtual void preOnInitComponent() {}
        virtual void postOnInitComponent() {}

        /**
         * Pure virtual hook for the subclass. Is called once all dependencies of the object have been resolved and Ice connection is established.
         * This hook is called whenever the dependencies are found. That means if the a depedency crashes or shuts down, the ManagedIceObject goes
         * into disconnected state. When the dependencies are found again, this hook is called again.
         *
         *
         * @see onDisconnectComponent()
         */
        virtual void onConnectComponent() = 0;
        virtual void preOnConnectComponent() {}
        virtual void postOnConnectComponent() {}

        /**
         * Hook for subclass. Is called if a dependency of the object got lost (crash, network error, stopped, ...).
         * This hook should be the inverse to onConnectComponent(). So that onDisconnectComponent() and onConnectComponent() can be called alternatingly
         * and the ManagedIceObject remains in valid states.         *
         *
         * @see onConnectComponent
         */
        virtual void onDisconnectComponent() {}
        virtual void preOnDisconnectComponent() {}
        virtual void postOnDisconnectComponent() {}

        /**
         * Hook for subclass. Is called once the component terminates. Use for cleanup. Only called once.
         */
        virtual void onExitComponent() {}
        virtual void preOnExitComponent() {}
        virtual void postOnExitComponent() {}

        Ice::CommunicatorPtr getCommunicator() const;

    private:
        // init sets members and calls onInitComponent hook
        void init(IceManagerPtr iceManager);
        // start sets members and calls onConnectComponent hook
        void start(Ice::ObjectPrx& proxy, const Ice::ObjectAdapterPtr& objectAdapter);
        void disconnect();
        // exit sets members and calls onExitComponent hook
        void exit();

        void setObjectState(int newState);

    private:
        struct Impl;
        std::unique_ptr<Impl> impl;

        /**
         * @brief Noop function which does nothing (Only to be used as default value for enableProfilerFunction function pointer)
         * @param object
         */
        static void Noop(ManagedIceObject* object);

        /**
         * @brief EnableProfilerOn creates an instance of armarx::IceLoggingStrategy and sets it on object->profiler
         * @param object
         */
        static void EnableProfilerOn(ManagedIceObject* object);
    };

}


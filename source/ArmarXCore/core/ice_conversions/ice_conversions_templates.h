#pragma once

#include <map>
#include <memory>
#include <optional>
#include <vector>

#include <Ice/Handle.h>


/**
   This file offers overloads of toIce() and fromIce() functions for STL
   container types.

   To use these overloads for custom types
   `myns::MyClass` (the C++ class, i.e. the business object (BO)) and
   `myns::dto::MyClass` (the ice class, i.e. the data transfer object (DTO))
   (see the `IceConversionsTest for a working example):

   1. Implement free functions toIce() and fromIce() of the form:

   In the header (ice_conversions.h):
   @code
   namespace myns
   {
       void toIce(dto::MyClass& dto, const MyClass& bo);
       void fromIce(const dto::MyClass& dto, MyClass& bo);
   }
   @endcode

   In the source file (ice_conversions.cpp):
   @code
   #include "ice_conversions.h"

   #include <ArmarXCore/core/ice_conversions.h>


   using armarx::toIce;
   using armarx::fromIce;

   void myns::toIce(dto::MyClass& dto, const MyClass& bo)
   {
       toIce(dto.memberA, bo.memberB);
       toIce(dto.memberB, bo.memberB);
   }
   void myns::fromIce(const dto::MyClass& dto, MyClass& bo)
   {
       fromIce(dto.memberA, bo.memberA);
       fromIce(dto.memberB, bo.memberB);
   }
   @endcode

   2. Include the `ice_conversions.h` for `MyClass` as well as the templates
      (this file, i.e. <ArmarXCore/core/ice_conversions.h>) in the .cpp file
      where you want to do the conversions.

   3. Call the conversion functions in these ways:

   @code

   void foo()
   {
       MyClass bo;
       dto::MyClass dto;

       // Works without namespace (via Argument Depdenent Lookup, ADL):
       toIce(dto, bo);
       fromIce(dto, bo);

       // The templates must be explicitly imported into the current namespace
       // (even when `using namespace armarx`):
       using armarx::toIce;
       using armarx::fromIce;

       bo = fromIce<MyClass>(dto);
       dto = toIce<dto::MyClass>(bo);

       std::vector<MyClass> bos;
       std::vector<dto::MyClass> dtos;

       fromIce(dtos, bos);
       toIce(dtos, bos);
   }
   @endcode
 */


namespace armarx
{

    // Same type

    template <class T>
    void toIce(T& dto, const T& bo);
    template <class T>
    void fromIce(const T& dto, T& bo);


    // General return version.

    template <class DtoT, class BoT>
    DtoT toIce(const BoT& bo);
    template <class BoT, class DtoT>
    BoT fromIce(const DtoT& dto);


    // Ice Handle

    template <class DtoT, class BoT>
    void toIce(::IceInternal::Handle<DtoT>& dto, const BoT& bo);
    template <class DtoT, class BoT>
    void fromIce(const ::IceInternal::Handle<DtoT>& dto, BoT& bo);


    // std::unique_ptr

    template <class DtoT, class BoT>
    void toIce(DtoT& dto, const std::unique_ptr<BoT>& boPointer);
    template <class DtoT, class BoT>
    void fromIce(const DtoT& dto, std::unique_ptr<BoT>& boPointer);


    // Ice Handle <-> std::unique_ptr

    template <class DtoT, class BoT>
    void toIce(::IceInternal::Handle<DtoT>& dto, const std::unique_ptr<BoT>& boPointer);
    template <class DtoT, class BoT>
    void fromIce(const ::IceInternal::Handle<DtoT>& dto, std::unique_ptr<BoT>& boPointer);


    // Ice Handle <-> std::optional

    template <class DtoT, class BoT>
    void toIce(::IceInternal::Handle<DtoT>& dto, const std::optional<BoT>& bo);
    template <class DtoT, class BoT>
    void fromIce(const ::IceInternal::Handle<DtoT>& dto, std::optional<BoT>& bo);


    // std::vector

    template <class DtoT, class BoT>
    void toIce(std::vector<DtoT>& dtos, const std::vector<BoT>& bos);
    template <class DtoT, class BoT>
    void fromIce(const std::vector<DtoT>& dtos, std::vector<BoT>& bos);


    // std::map

    template <class DtoKeyT, class IceValueT, class BoKeyT, class BoValueT>
    void toIce(std::map<DtoKeyT, IceValueT>& dtoMap,
               const std::map<BoKeyT, BoValueT>& boMap);
    template <class DtoKeyT, class IceValueT, class BoKeyT, class BoValueT>
    void fromIce(const std::map<DtoKeyT, IceValueT>& dtoMap,
                 std::map<BoKeyT, BoValueT>& boMap);

}

#include "ice_conversions_templates.tpp"

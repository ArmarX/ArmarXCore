#pragma once

#include "ice_conversions_templates.h"


// Same type

template <class T>
void armarx::toIce(T& dto, const T& bo)
{
    dto = bo;
}
template <class T>
void armarx::fromIce(const T& dto, T& bo)
{
    bo = dto;
}


// General return version.

template <class DtoT, class BoT>
DtoT armarx::toIce(const BoT& bo)
{
    DtoT dto;
    toIce(dto, bo);
    return dto;
}
template <class BoT, class DtoT>
BoT armarx::fromIce(const DtoT& dto)
{
    BoT bo;
    fromIce(dto, bo);
    return bo;
}


// Ice Handle

template <class DtoT, class BoT>
void armarx::toIce(::IceInternal::Handle<DtoT>& dto, const BoT& bo)
{
    dto = new DtoT();
    toIce(*dto, bo);
}
template <class DtoT, class BoT>
void armarx::fromIce(const ::IceInternal::Handle<DtoT>& dto, BoT& bo)
{
    if (dto)
    {
        fromIce(*dto, bo);
    }
}


// std::unique_ptr

template <class DtoT, class BoT>
void armarx::toIce(DtoT& dto, const std::unique_ptr<BoT>& boPointer)
{
    if (boPointer)
    {
        toIce(dto, *boPointer);
    }
}
template <class DtoT, class BoT>
void armarx::fromIce(const DtoT& dto, std::unique_ptr<BoT>& boPointer)
{
    boPointer = std::make_unique<BoT>();
    fromIce(dto, *boPointer);
}


// Ice Handle <-> std::unique_ptr

template <class DtoT, class BoT>
void armarx::toIce(::IceInternal::Handle<DtoT>& dto, const std::unique_ptr<BoT>& boPointer)
{
    if (boPointer)
    {
        dto = new DtoT();
        toIce(*dto, *boPointer);
    }
    else
    {
        dto = nullptr;
    }
}
template <class DtoT, class BoT>
void armarx::fromIce(const ::IceInternal::Handle<DtoT>& dto, std::unique_ptr<BoT>& boPointer)
{
    if (dto)
    {
        boPointer = std::make_unique<BoT>();
        fromIce(*dto, *boPointer);
    }
    else
    {
        boPointer = nullptr;
    }
}


// Ice Handle <-> std::optional

template <class DtoT, class BoT>
void armarx::toIce(::IceInternal::Handle<DtoT>& dto, const std::optional<BoT>& bo)
{
    if (bo.has_value())
    {
        dto = new DtoT();
        toIce(*dto, *bo);
    }
    else
    {
        dto = nullptr;
    }
}
template <class DtoT, class BoT>
void armarx::fromIce(const ::IceInternal::Handle<DtoT>& dto, std::optional<BoT>& bo)
{
    if (dto)
    {
        bo.emplace();
        fromIce(*dto, *bo);
    }
    else
    {
        bo = std::nullopt;
    }
}


// std::vector

template <class DtoT, class BoT>
void armarx::toIce(std::vector<DtoT>& dtos, const std::vector<BoT>& bos)
{
    dtos.clear();
    dtos.reserve(bos.size());
    for (const auto& bo : bos)
    {
        toIce(dtos.emplace_back(), bo);
    }
}
template <class DtoT, class BoT>
void armarx::fromIce(const std::vector<DtoT>& dtos, std::vector<BoT>& bos)
{
    bos.clear();
    bos.reserve(dtos.size());
    for (const auto& dto : dtos)
    {
        fromIce(dto, bos.emplace_back());
    }
}


// std::map

template <class DtoKeyT, class IceValueT, class BoKeyT, class BoValueT>
void armarx::toIce(std::map<DtoKeyT, IceValueT>& dtoMap,
           const std::map<BoKeyT, BoValueT>& boMap)
{
    dtoMap.clear();
    for (const auto& [key, value] : boMap)
    {
        dtoMap.emplace(toIce<DtoKeyT>(key), toIce<IceValueT>(value));
    }
}
template <class DtoKeyT, class IceValueT, class BoKeyT, class BoValueT>
void armarx::fromIce(const std::map<DtoKeyT, IceValueT>& dtoMap,
             std::map<BoKeyT, BoValueT>& boMap)
{
    boMap.clear();
    for (const auto& [key, value] : dtoMap)
    {
        boMap.emplace(fromIce<BoKeyT>(key), fromIce<BoValueT>(value));
    }
}

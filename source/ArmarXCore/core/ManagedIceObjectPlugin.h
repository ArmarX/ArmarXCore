/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2021, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Raphael Grimm (raphael dot grimm at kit dot edu)
* @author     Rainer Kartmann (rainer dot kartmann at kit dot edu)
* @date       2021
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <memory>
#include <string>
#include <vector>

// Forward declarations
namespace std::experimental
{
    inline namespace fundamentals_v2
    {
        template <typename T>
        class observer_ptr;

        template<typename _Tp>
        observer_ptr<_Tp>
        make_observer(_Tp* __p) noexcept;
    }
}

namespace armarx
{
    class ManagedIceObject;

    class ManagedIceObjectPlugin
    {
    public:

        virtual ~ManagedIceObjectPlugin() = default;


    protected:

        ManagedIceObjectPlugin(ManagedIceObject& parent, std::string pre);


        virtual void preOnInitComponent() {}
        virtual void postOnInitComponent() {}

        virtual void preOnConnectComponent() {}
        virtual void postOnConnectComponent() {}

        virtual void preOnDisconnectComponent() {}
        virtual void postOnDisconnectComponent() {}

        virtual void preOnExitComponent() {}
        virtual void postOnExitComponent() {}


        friend class ManagedIceObject;


    public:

        template<class T>
        bool parentDerives() const
        {
            return dynamic_cast<T*>(_parent);
        }

        ManagedIceObject& parent();
        const ManagedIceObject& parent() const;

        template<class T>
        T& parent()
        {
            return dynamic_cast<T&>(ManagedIceObjectPlugin::parent());
        }
        template<class T>
        const T& parent() const
        {
            return dynamic_cast<const T&>(ManagedIceObjectPlugin::parent());
        }


    protected:

        template <class PluginT, class...ParamsT>
        PluginT* addPlugin(const std::string prefix = "", ParamsT && ...params)
        {
            static_assert(std::is_base_of_v<ManagedIceObjectPlugin, PluginT>);

            std::unique_ptr<ManagedIceObjectPlugin>& ptr = this->getPlugin(typeid(PluginT), prefix);
            // If ptr != nullptr, we already got an instance of this plugin.
            if (!ptr)
            {
                ptr.reset(new PluginT(parent(), prefix, params...));
            }
            return static_cast<PluginT*>(ptr.get());
        }

        template <class PluginT, class...ParamsT>
        void addPlugin(PluginT*& targ, const std::string prefix = "", ParamsT && ...params)
        {
            checkOutParameter(targ);
            targ = this->addPlugin<PluginT>(prefix, params...);
        }

        template <class PluginT, class...ParamsT>
        void addPlugin(std::experimental::observer_ptr<PluginT>& targ, const std::string prefix = "", ParamsT && ...params)
        {
            checkOutParameter(targ.get());
            targ = std::experimental::make_observer(this->addPlugin<PluginT>(prefix, params...));
        }

        void addPluginDependency(ManagedIceObjectPlugin* dependedOn);
        void addPluginDependency(std::experimental::observer_ptr<ManagedIceObjectPlugin> dependedOn);

        const std::string& prefix() const;
        std::string makePropertyName(const std::string& name);


    private:

        /// Wraps a usage of ARMARX_CHECK_NULL to avoid an include.
        static void checkOutParameter(void* targ);
        /// Wraps a call to parent().getPluginPointer() to allow forward declaration of MangedIceObject.
        std::unique_ptr<ManagedIceObjectPlugin>& getPlugin(const std::type_info& typeInfo, const std::string prefix = "");


    private:

        ManagedIceObject* const              _parent;
        std::string                          _prefix;
        std::vector<ManagedIceObjectPlugin*> _dependsOn;

    };

}


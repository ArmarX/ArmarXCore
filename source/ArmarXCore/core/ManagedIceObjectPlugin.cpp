/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2021, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Raphael Grimm (raphael dot grimm at kit dot edu)
* @author     Rainer Kartmann (rainer dot kartmann at kit dot edu)
* @date       2021
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ManagedIceObjectPlugin.h"

#include <experimental/memory>
#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace armarx
{

    ManagedIceObjectPlugin::ManagedIceObjectPlugin(ManagedIceObject& parent, std::string pre) :
        _parent{&parent},
        _prefix{pre}
    {}


    void ManagedIceObjectPlugin::addPluginDependency(ManagedIceObjectPlugin* dependedOn)
    {
        ARMARX_CHECK_NOT_NULL(dependedOn);
        _dependsOn.emplace_back(dependedOn);
    }

    void ManagedIceObjectPlugin::addPluginDependency(std::experimental::observer_ptr<ManagedIceObjectPlugin> dependedOn)
    {
        addPluginDependency(dependedOn.get());
    }

    std::string ManagedIceObjectPlugin::makePropertyName(const std::string& name)
    {
        if (_prefix.empty())
        {
            return name;
        }
        return _prefix + '_' + name;
    }


    const std::string& ManagedIceObjectPlugin::prefix() const
    {
        return _prefix;
    }


    ManagedIceObject& ManagedIceObjectPlugin::parent()
    {
        return *_parent;
    }


    const ManagedIceObject& ManagedIceObjectPlugin::parent() const
    {
        return *_parent;
    }


    std::unique_ptr<ManagedIceObjectPlugin>& ManagedIceObjectPlugin::getPlugin(const std::type_info& typeInfo, const std::string prefix)
    {
        return parent().getPluginPointer(typeInfo, prefix);
    }


    void ManagedIceObjectPlugin::checkOutParameter(void* targ)
    {
        ARMARX_CHECK_NULL(targ) << "This is an out parameter";
    }

}

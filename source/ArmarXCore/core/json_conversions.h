/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// Simox
#include <SimoxUtility/json.h>

// ArmarX
#include <ArmarXCore/interface/core/PackagePath.h>


namespace armarx::data
{
    void to_json(nlohmann::json& j, const PackagePath& pp)
    {
        j = nlohmann::json
        {
            {"package", pp.package},
            {"path", pp.path}
        };
    }

    void from_json(const nlohmann::json& j, PackagePath& pp)
    {
        j.at("package").get_to(pp.package);
        j.at("path").get_to(pp.path);
    }
}

/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Nils Adermann (naderman at naderman dot de)
* @author     Mirko Waechter (waechter at kit dot edu)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "LoggingUtil.h"

#include <ArmarXCore/core/system/ImportExport.h>

#include <Ice/ProxyHandle.h>
#include <IceUtil/Time.h>

#include <memory>
#include <chrono>

namespace IceProxy::armarx
{
    class Log;
}

namespace armarx
{
    enum class MessageTypeT
    {
        UNDEFINED,
        DEBUG,
        VERBOSE,
        INFO,
        IMPORTANT,
        WARN,
        ERROR,
        FATAL,
        LogLevelCount
    };

    typedef ::IceInternal::ProxyHandle< ::IceProxy::armarx::Log> LogPrx;


    /**
      \class LogSender
      \brief Wrapper for the Log IceStorm topic with convenience methods for logging.
      \ingroup Logging
    */
    class ARMARXCORE_IMPORT_EXPORT LogSender : public std::enable_shared_from_this<LogSender>
    {
    public:
        enum ConsoleColor
        {
            eReset = -1,
            eBlack = 0,
            eRed = 1,
            eGreen = 2,
            eYellow = 3,
            eBlue = 4
        };
        typedef void (LogSender::*manipulator)();

        // this is the type of std::cout
        using CoutType = std::basic_ostream<char, std::char_traits<char> >;

        // this is the function signature of std::endl
        typedef CoutType& (*StandardEndLine)(CoutType&);





        static LogSenderPtr createLogSender();

        LogSender();
        ~LogSender();

        /**
          * Constructor taking a LogPrx pointer to the IceStorm topic.
          *
          * \param componentName All log messages sent through this instance
          *                      will show as having originated from this
          *                      component.
          * \param logProxy      Ice proxy to send messages to.
          */
        LogSender(const std::string& componentName, LogPrx logProxy);

        static void setProxy(const std::string& componentName, LogPrx logProxy);
        static void SetComponentName(const std::string& componentName);
        static void SetLoggingGroup(const std::string& loggingGroup);

        /**
          Appends a variable to the current message stringstream

          \param message
          \return This object for further streaming
          */
        template<typename T>
        LogSender& operator<<(const T& message)
        {
            currentMessage << message;
            return *this;
        }

        /**
         * @brief operator << overload for IceUtil::Time
         * @param timestamp IceUtil::Time
         * @return
         */
        LogSender& operator<<(const IceUtil::Time& timestamp);

        // TODO: C++20 offers operator<< for std::chrono::duration.
        // See: https://en.cppreference.com/w/cpp/chrono/duration/operator_ltlt
        // The operator<<'s for timestamps can then be removed

        LogSender& operator<<(const std::chrono::minutes& timestamp);

        LogSender& operator<<(const std::chrono::seconds& timestamp);

        LogSender& operator<<(const std::chrono::milliseconds& timestamp);

        LogSender& operator<<(const std::chrono::microseconds& timestamp);

        LogSender& operator<<(const std::chrono::nanoseconds& timestamp);


        LogSender& operator<< (const StandardEndLine& manipulator); // overloading for std::endl, but cannot get it to work with template specialization (Mirko 2012)
        /**
          Sends the current message to the log and resets the content.
          */
        void flush();

        static std::string GetColorCodeString(MessageTypeT verbosityLevel);
        static std::string GetColorCodeString(ConsoleColor colorCode);

        static std::string CreateBackTrace(int linesToSkip = 1);
        /**
          Retrieves the current message severity

          \return Current message severity
          */
        MessageTypeT getSeverity();

        /**
          Set the source code filename associated with this message.

          \param filename The file this message was sent from
          \return a pointer to this object
          */
        LogSenderPtr setFile(const std::string& file);

        /**
          Set the source code line associated with this message.

          \param line The line this message was sent from
          \return a pointer to this object
          */
        LogSenderPtr setLine(int line);

        /**
          Set the function name associated with this message.

          \param line The function this message was sent from
          \return a pointer to this object
        */
        LogSenderPtr setFunction(const std::string& function);
        LogSenderPtr setLocalMinimumLoggingLevel(MessageTypeT level);
        LogSenderPtr setBacktrace(bool printBackTrace);
        LogSenderPtr setThreadId(Ice::Int tid);

        LogSenderPtr setTag(const LogTag& tag);

        //        LogSenderPtr setSpamFilter(SpamFilterMapPtr spamFilter);

        static std::string levelToString(MessageTypeT type);

        /**
         * \brief stringToLevel converts a string into a LoggingLevel,
         * if possible.
         *
         * <b>Case-Insensitive</b>
         * \param typeStr string that is to be converted
         * \return The LoggingLevel or Undefined if conversion not possible.
         */
        static MessageTypeT StringToLevel(const std::string& typeStr);

        /**
         * \brief With setGlobalMinimumLoggingLevel the minimum verbosity-level of
         * log-messages can be set for the whole application.<br/>
         * the flag 'minimumLoggingLevel' overrides this setting.
         * \param level The minimum logging level
         */
        static void SetGlobalMinimumLoggingLevel(MessageTypeT level);
        static MessageTypeT GetGlobalMinimumLoggingLevel();
        /**
         * \brief setLoggingActivated() is used to activate or disable
         * the logging facilities in the whole application
         * \param activated
         */
        static void SetLoggingActivated(bool activated = true, bool showMessage = true);
        static void SetSendLoggingActivated(bool activated = true);

        static void SetColoredConsoleActivated(bool activated = true);
        static long getThreadId();
        static long getProcessId();
        template <typename T>
        static std::string GetTypeString();
        static std::string CropFunctionName(const std::string& originalFunctionName);



    protected:
        /**
         * Sends a message to the logging component.
         *
         * \param severity eVERBOSE / eINFO / eWARN / eERROR
         * \param message
         */
        void log(MessageTypeT severity, std::string message);
    private:
        void initConsole();

        /**
          Resets information about associated file, line and function.
         */
        void resetLocation();

        std::stringstream currentMessage;

        struct Impl;
        std::unique_ptr<Impl> impl;
    };

    const LogSender::manipulator flush = &LogSender::flush;

    /**
    * Changes the current message severity for streamed messages
    *
    * \param  severity
    * \return This object for further streaming
    */
    template<>
    ARMARXCORE_IMPORT_EXPORT LogSender& LogSender::operator<< <MessageTypeT>(const MessageTypeT& severity);

    /**
    * Changes the current tag for streamed messages
    *
    * \param  tag
    * \return This object for further streaming
    */
    template<>
    ARMARXCORE_IMPORT_EXPORT LogSender& LogSender::operator<< <LogTag>(const LogTag& tag);

    /**
    * Executes a manipulator like flush on the stream
    *
    * \param  manipulator
    * \return This object for further streaming
    */
    template<>
    ARMARXCORE_IMPORT_EXPORT LogSender& LogSender::operator<< <LogSender::manipulator>(const manipulator& manipulator);

    /**
        * Changes the current message color
        *
        * \param  colorCode Color of the Text
        * \return This object for further streaming
        */
    template<>
    ARMARXCORE_IMPORT_EXPORT LogSender& LogSender::operator<< <LogSender::ConsoleColor>(const LogSender::ConsoleColor& colorCode);

    template<>
    ARMARXCORE_IMPORT_EXPORT LogSender& LogSender::operator<< <bool>(const bool& duality);

    template<>
    ARMARXCORE_IMPORT_EXPORT LogSender& LogSender::operator<< <SpamFilterDataPtr>(const SpamFilterDataPtr& spamFilterData);




}


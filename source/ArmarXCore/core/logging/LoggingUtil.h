/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Mirko Waechter (mirko dot waechter at kit dot edu)
* @date       2017
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

#include <memory>
#include <iosfwd>

namespace armarx::detail
{
    enum class StreamPrinterTag
    {
        tag
    };

    template<class Fnc>
    struct StreamPrinter
    {
        StreamPrinter(Fnc&& f): fnc {std::move(f)} {}
        Fnc fnc;
    };

    template<class Fnc>
    StreamPrinter<Fnc> operator*(StreamPrinterTag, Fnc&& f)
    {
        return StreamPrinter<Fnc> {std::move(f)};
    }
}

namespace armarx
{
    template<class Fnc>
    std::ostream& operator<<(std::ostream& o, const detail::StreamPrinter<Fnc>& f)
    {
        f.fnc(o);
        return o;
    }


    class LogSender;
    /**
    * Typedef of std::shared_ptr for convenience.
    */
    using LogSenderPtr = std::shared_ptr<LogSender>;

    struct LogTag
    {
        LogTag();
        LogTag(const std::string& tagName);
        std::string tagName;
    };

    struct SpamFilterData;
    using SpamFilterDataPtr = std::shared_ptr<SpamFilterData>;
}


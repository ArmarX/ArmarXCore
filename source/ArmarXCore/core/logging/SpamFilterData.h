#pragma once

#include <IceUtil/Time.h>

#include <unordered_map>
#include <memory>
#include <string>
#include <mutex>

namespace armarx
{
    using SpamFilterMap = std::unordered_map<std::string, std::unordered_map<std::string, IceUtil::Time> >;
    using SpamFilterMapPtr = std::shared_ptr<SpamFilterMap>;

    struct SpamFilterData
    {
        SpamFilterMapPtr filterMap{new SpamFilterMap()};
        std::shared_ptr<std::mutex> mutex{new std::mutex()};
        std::string identifier;
        float durationSec = 10.0f;
    };
}

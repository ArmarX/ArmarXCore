/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once


#include <ArmarXCore/core/system/AbstractFactoryMethod.h>
#include <ArmarXCore/statechart/State.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <memory>


namespace armarx
{

    class RapidXmlReader;
    using RapidXmlReaderPtr = std::shared_ptr<RapidXmlReader>;
    class RapidXmlReaderNode;
    using RapidXmlReaderNodePtr = std::shared_ptr<RapidXmlReaderNode>;

    using StringXMLNodeMap = std::map<std::string, RapidXmlReaderPtr>;
    using StringXMLNodeMapPtr = std::shared_ptr<StringXMLNodeMap>;

    class StatechartProfile;
    using StatechartProfilePtr = std::shared_ptr<StatechartProfile>;
    class StatechartProfiles;
    using StatechartProfilesPtr = std::shared_ptr<StatechartProfiles>;

    struct XMLStateConstructorParams
    {
        std::string xmlFilepath;
        RapidXmlReaderPtr reader;
        StatechartProfilePtr selectedProfile;
        StringXMLNodeMapPtr uuidToXMLMap;
        Ice::CommunicatorPtr ic;
        XMLStateConstructorParams(const std::string& xmlFilepath,
                                  RapidXmlReaderPtr reader,
                                  StatechartProfilePtr selectedProfile,
                                  StringXMLNodeMapPtr uuidToXMLMap,
                                  Ice::CommunicatorPtr ic);
    };

    struct XMLStateFactoryBase;
    using XMLStateFactoryBasePtr = IceInternal::Handle<XMLStateFactoryBase >;
    struct XMLStateFactoryBase : virtual Ice::Object,
        virtual public AbstractFactoryMethod<XMLStateFactoryBase, XMLStateConstructorParams, XMLStateFactoryBasePtr >
    {

    };

    class StateParameterDeserialization
    {
    public:
        StateParameterDeserialization(RapidXmlReaderNode const& parameterNode, Ice::CommunicatorPtr ic, StatechartProfilePtr selectedProfile);

        std::string getName();
        bool getOptional();
        std::string getTypeStr();
        ContainerTypePtr getTypePtr();
        VariantContainerBasePtr getContainer();

    protected:
        std::string name;
        bool optional;
        std::string typeStr;
        ContainerTypePtr typePtr;
        VariantContainerBasePtr container;
    };

    VariantContainerBasePtr GetSelectedProfileValue(RapidXmlReaderNode parameterNode, StatechartProfilePtr selectedProfile, Ice::CommunicatorPtr ic, std::string defaultValueJsonString = "");


    struct PrivateXmlStateClass;
    using PrivateXmlStateClassPtr = std::shared_ptr<PrivateXmlStateClass>;
    class XMLState :
        virtual public State
    {
    public:

        XMLState() {}
        XMLState(const XMLStateConstructorParams& stateData);
        ~XMLState() override;

        StatechartProfilePtr getSelectedProfile() const;

    protected:

        StateParameterMap getParameters(RapidXmlReaderNode  parametersNode);

        StatePtr addState(StatePtr state);
        void addXMLSubstates(RapidXmlReaderNode  substatesNode, const std::string& parentStateName);
        StateBasePtr addXMLSubstate(RapidXmlReaderNode  stateNode, const std::string& parentStateName);

        void defineParameters() override;
        void defineSubstates() override;
    private:
        void setXMLStateData(const XMLStateConstructorParams& stateData);


        void addTransitions(const RapidXmlReaderNode& transitionsNode);
        void setStartState(const RapidXmlReaderNode& startNode);
        ParameterMappingPtr getMapping(const RapidXmlReaderNode& mappingNode);
        PrivateXmlStateClassPtr privateStateData;

        template <typename type>
        friend class XMLStateTemplate;
        template <typename ContextType>
        friend class XMLRemoteStateOfferer;

    };

    /**
     * Class for legacy to stay compatible with old statecharts
     */
    template <typename StateType>
    class XMLStateTemplate :
        virtual public XMLState
    {
    public:
        XMLStateTemplate(const XMLStateConstructorParams& params) : XMLState(params)
        {
            setXMLStateData(params);
        }

    };

    struct NoUserCodeState :
        virtual public XMLState,
        public virtual XMLStateFactoryBase
    {
        NoUserCodeState(XMLStateConstructorParams stateData);
        static XMLStateFactoryBasePtr CreateInstance(XMLStateConstructorParams stateData);
        static std::string GetName();

        StateBasePtr clone() const override;
        static SubClassRegistry Registry;

    };
}


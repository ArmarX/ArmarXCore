/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "DoxTransitiongraph.h"
#include <sstream>

using namespace armarx;


DoxTransitionGraph::DoxTransitionGraph()
{ }

void DoxTransitionGraph::addTransition(DoxTransitionPtr transition)
{
    transitions.push_back(transition);
}
void DoxTransitionGraph::addTransition(const std::string& from, const std::string& to, const std::string& eventName)
{
    DoxTransitionPtr transition(new DoxTransition(from, to, eventName));
    transitions.push_back(transition);
}

void DoxTransitionGraph::addNode(const std::string& name, const std::string& attrs)
{
    DoxTransitionGraphNodePtr node(new DoxTransitionGraphNode(name, attrs));
    nodes.push_back(node);
}

int DoxTransitionGraph::transitionCount()
{
    return transitions.size();
}

std::string DoxTransitionGraph::getDoxString()
{
    std::stringstream ss;
    ss << "@dot\ndigraph States {\n";

    for (DoxTransitionGraphNodePtr node : nodes)
    {
        ss << "  " << node->toString() << "\n";
    }

    for (DoxTransitionPtr transition : transitions)
    {
        ss << "  " << transition->toString() << "\n";
    }

    ss << "}\n@enddot";
    return ss.str();
}


DoxTransition::DoxTransition(std::string from, std::string to, std::string eventName)
{
    this->from = from;
    this->to = to;
    this->eventName = eventName;
}

std::string DoxTransition::toString()
{
    return "\"" + from + "\" -> \"" + to + "\" [ label=\"" + eventName + "\" ];";
}


DoxTransitionGraphNode::DoxTransitionGraphNode(const std::string& name, const std::string& shape)
    : name(name), attrs(shape)
{ }

std::string DoxTransitionGraphNode::toString()
{
    return "\"" + name + "\" [" + attrs + "]";
}

/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "StateGroupGenerator.h"

#include <fstream>
#include <time.h>
#include <algorithm>

#include <boost/format.hpp>
#include <SimoxUtility/algorithm/string/string_tools.h>

#include "baseclassgenerator/XmlStateBaseClassGenerator.h"
#include <ArmarXCore/core/exceptions/local/FileIOException.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/util/algorithm.h>

#include <ArmarXCore/statechart/xmlstates/baseclassgenerator/XmlContextBaseClassGenerator.h>
#include "baseclassgenerator/XmlStateBaseClassGenerator.h"

#include <ArmarXCore/statechart/xmlstates/baseclassgenerator/CMakeSourceListGenerator.h>

using namespace armarx;

Ice::StringSeq StatechartGroupGenerator::findStatechartGroupFiles(const std::string& statechartsPath)
{
    Ice::StringSeq groups;

    for (std::filesystem::recursive_directory_iterator end, dir(statechartsPath); dir != end; ++dir)
    {
        // search for all statechart group xml files
        if (dir->path().extension() == ".scgxml" && dir->path().string().find("deprecated") == std::string::npos)
        {
            groups.push_back(dir->path().string());
            ARMARX_DEBUG << dir->path().string();
        }
    }
    return groups;
}

bool StatechartGroupGenerator::generateStateFile(const std::string& statechartGroupXmlFilePath, const std::string& statePath, const std::string& packagePath, const std::optional<std::string>& packageIncludePath)
{
    StatechartGroupXmlReaderPtr reader(new StatechartGroupXmlReader(StatechartProfilePtr()));
    reader->readXml(std::filesystem::path(statechartGroupXmlFilePath));
    //ARMARX_INFO_S << reader->getPackageName();
    CMakePackageFinder finder(reader->getPackageName(), packagePath, false, false);
    ARMARX_CHECK_EXPRESSION(finder.packageFound()) << reader->getPackageName() << " at " << packagePath;
    std::filesystem::path buildDir = finder.getBuildDir();

    VariantInfoPtr variantInfo = VariantInfo::ReadInfoFilesRecursive(reader->getPackageName(), packagePath, false);

    std::filesystem::path boostStatePath(statePath);
    return generateStateFile(boostStatePath.filename().replace_extension().string(),
                      RapidXmlReader::FromFile(statePath),
                      buildDir,
                      reader->getPackageName(),
                      reader->getGroupName(),
                      reader->getProxies(),
                      reader->contextGenerationEnabled(),
                      variantInfo,
                      false,
                      packageIncludePath.value_or(reader->getPackageName()));
}

std::string packageToIncludePath(const std::string& packageName)
{
    const auto splits = simox::alg::split(packageName, "_");
    return simox::alg::join(splits, "/");
}

bool StatechartGroupGenerator::generateStateFile(const std::string& stateName, RapidXmlReaderPtr reader, std::filesystem::path buildDir, const std::string& packageName, const std::string& groupName, const std::vector<std::string>& proxies, bool contextGenerationEnabled, const VariantInfoPtr& variantInfo, bool forceRewrite, const std::optional<std::string>& packageIncludePath)
{
    std::filesystem::path outputPath = buildDir / "source" / packageIncludePath.value_or(packageName) / "statecharts" / groupName / (stateName + ".generated.h");

    std::filesystem::path dir = outputPath;
    dir.remove_filename();
    std::filesystem::create_directories(dir);
    //    ARMARX_INFO_S << "generating into " << outputPath.string();

    std::vector<std::string> namespaces({"armarx", groupName});
    std::string cpp = XmlStateBaseClassGenerator::GenerateCpp(namespaces,
                      reader,
                      proxies,
                      contextGenerationEnabled,
                      groupName,
                      variantInfo);

    time_t rawtime;
    struct tm* timeinfo;
    char buffer [80];
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(buffer, 80, "%Y-%m-%d %H:%M:%S%n", timeinfo);
    writeFileContents(outputPath.string() + ".touch", std::string(buffer));

    if(cpp.empty())
    {
        ARMARX_WARNING << "Could not generate state file!";
        return false;
    }

    if (forceRewrite)
    {
        writeFileContents(outputPath.string(), cpp);
        return true;
    }
    
    writeFileContentsIfChanged(outputPath.string(), cpp);
    return true;

    //ARMARX_INFO_S << "written " << cpp.length() << " bytes to " << outputPath.string();
}

bool StatechartGroupGenerator::generateStatechartContextFile(const std::string& statechartGroupXmlFilePath, const std::string& packagePath, const std::optional<std::string>& packageIncludePath)
{
    StatechartGroupXmlReaderPtr reader(new StatechartGroupXmlReader(StatechartProfilePtr()));
    reader->readXml(std::filesystem::path(statechartGroupXmlFilePath));

    if (!reader->contextGenerationEnabled())
    {
        throw LocalException("Will not generate context for ") << statechartGroupXmlFilePath << ". Context generation is not enabled for this group.";
    }

    VariantInfoPtr variantInfo = VariantInfo::ReadInfoFilesRecursive(reader->getPackageName(), packagePath, false);

    CMakePackageFinder finder(reader->getPackageName(), packagePath, false, false);
    ARMARX_CHECK_EXPRESSION(finder.packageFound()) << reader->getPackageName() << " at " << packagePath;
    std::filesystem::path buildDir = finder.getBuildDir();

    return generateStatechartContextFile(buildDir,
                                  reader->getPackageName(),
                                  reader->getGroupName(),
                                  reader->getProxies(),
                                  variantInfo,
                                  getVariantTypesOfStatesWithNoCpp(reader, variantInfo),
                                  false,
                                  packageIncludePath);
}



bool StatechartGroupGenerator::generateStatechartContextFile(std::filesystem::path buildDir, const std::string& packageName, const std::string& groupName, const std::vector<std::string>& proxies, const VariantInfoPtr& variantInfo, const std::set<std::string>& usedVariantTypes, bool forceRewrite, const std::optional<std::string>& packageIncludePath)
{
    std::filesystem::path outputPath = buildDir / "source" / packageIncludePath.value_or(packageName) / "statecharts" / groupName;
    std::filesystem::path baseClassPath = outputPath / (groupName + "StatechartContextBase.generated.h");
    outputPath /= (groupName + "StatechartContext.generated.h");

    std::filesystem::path dir = outputPath;
    
    dir.remove_filename();
    std::filesystem::create_directories(dir);

    //    ARMARX_INFO_S << "generating into " << outputPath.string();
    std::vector<std::string> namespaces({"armarx", groupName});
    std::string cpp, cppBase;
    std::tie(cpp, cppBase) = XmlContextBaseClassGenerator::GenerateCpp(namespaces,
                             proxies,
                             groupName,
                             variantInfo,
                             usedVariantTypes);

    time_t rawtime;
    struct tm* timeinfo;
    char buffer [80];
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(buffer, 80, "%Y-%m-%d %H:%M:%S%n", timeinfo);
    writeFileContents(outputPath.string() + ".touch", std::string(buffer));
    writeFileContents(baseClassPath.string() + ".touch", std::string(buffer));

    ARMARX_DEBUG << "Filename: " << outputPath.string();
    ARMARX_DEBUG << "Content: \n" << cpp;

    if(cpp.empty())
    {
        ARMARX_WARNING << "Failed to generate statechart context file!";
        return false;
    }

    if (forceRewrite)
    {
        writeFileContents(outputPath.string(), cpp);
        writeFileContents(baseClassPath.string(), cppBase);
        return true;
    }
    
    bool result = writeFileContentsIfChanged(outputPath.string(), cpp);
    writeFileContentsIfChanged(baseClassPath.string(), cppBase) || result;
   
    return true;

    //ARMARX_INFO_S << "written " << cpp.length() << " bytes to " << outputPath.string();
}

bool StatechartGroupGenerator::generateStatechartGroupCMakeSourceListFile(StatechartGroupXmlReaderPtr reader, const std::filesystem::path& buildDir, VariantInfoPtr variantInfo, bool forceRewrite, const std::optional<std::string>& packageIncludePath, const bool nextGenBehavior)
{
    ARMARX_DEBUG << "Generating cmake file for " << reader->getGroupName();

    ARMARX_CHECK_EXPRESSION(std::filesystem::exists(buildDir)) << buildDir.string();
    auto groupName = reader->getGroupName();

    auto packageName = reader->getPackageName();

    std::filesystem::path outputPath = buildDir / "source" / packageIncludePath.value_or(packageName) / "statecharts" / groupName;
    std::filesystem::path generatedFileName = outputPath / (groupName + "Files.generated.cmake");
    std::filesystem::path dir = outputPath;
    dir.remove_filename();
    std::filesystem::create_directories(dir);


    Ice::StringSeq xmlFiles, headerFiles, sourceFiles;
    xmlFiles = reader->getStateFilepaths();


    Ice::StringSeq libs;
    // add libs required for all statecharts:
    libs.push_back("ArmarXCore");
    libs.push_back("ArmarXCoreStatechart");
    libs.push_back("ArmarXCoreObservers");

    for (std::string& xmlFile : xmlFiles)
    {
        auto stateNode = RapidXmlReader::FromFile(xmlFile);
        auto types = XmlStateBaseClassGenerator::GetUsedInnerNonBasicVariantTypes(stateNode->getRoot("State"), variantInfo);
        //        ARMARX_INFO << "types: " << (Ice::StringSeq(types.begin(), types.end()));

        for (std::string& lib : variantInfo->findLibNames(Ice::StringSeq(types.begin(), types.end())))
        {
            auto libName = lib;
            if (!Contains(libs, libName))
            {
                libs.push_back(libName);
            }
        }
    }

    auto proxies = reader->getProxies();

    for (VariantInfo::LibEntryPtr lib : variantInfo->getLibs())
    {
        auto libName = lib->getName();
        for (VariantInfo::ProxyEntryPtr proxy : lib->getProxies())
        {
            std::string proxyMemberName = proxy->getMemberName();

            std::string proxyId = boost::str(boost::format("%s.%s") % libName % proxyMemberName);
            if (std::find(proxies.begin(), proxies.end(), proxyId) != proxies.end())
            {
                if (!Contains(libs, libName))
                {
                    libs.push_back(libName);
                }

                for (auto& additionalLibName : proxy->getLibraries())
                {
                    if (!Contains(libs, additionalLibName))
                    {
                        libs.push_back(additionalLibName);
                    }
                }
            }
        }
    }

    //    ARMARX_INFO << VAROUT(libs);


    std::filesystem::path groupFilePath(reader->getGroupDefinitionFilePath());
    std::filesystem::path groupDir = groupFilePath.parent_path();

    for (std::string& xmlFile : xmlFiles)
    {

        auto headerFilePath = std::filesystem::path(xmlFile).replace_extension("h");
        //        ARMARX_INFO << "Checking " << headerFilePath;
        if (std::filesystem::exists(headerFilePath))
        {
            headerFiles.push_back("./" + ArmarXDataPath::relativeTo(groupDir.string(), headerFilePath.string()));
        }
        auto sourceFilePath = std::filesystem::path(xmlFile).replace_extension("cpp");
        if (std::filesystem::exists(sourceFilePath))
        {
            sourceFiles.push_back("./" + ArmarXDataPath::relativeTo(groupDir.string(), sourceFilePath.string()));
        }
        xmlFile = "./" + ArmarXDataPath::relativeTo(groupDir.string(), xmlFile);
    }

    const std::string cmakeFileContent = [&]() -> std::string {
        if(nextGenBehavior)
        {
            return CMakeSourceListGenerator::GenerateCMakeFileNextGen(groupName, xmlFiles, sourceFiles, headerFiles, libs);
        }

        return CMakeSourceListGenerator::GenerateCMakeFile(groupName, xmlFiles, sourceFiles, headerFiles, libs);
    } ();

    time_t rawtime;
    struct tm* timeinfo;
    char buffer [80];
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(buffer, 80, "%Y-%m-%d %H:%M:%S%n", timeinfo);

    ARMARX_DEBUG << "Writing file " << outputPath;

    writeFileContents(outputPath.string() + ".touch", std::string(buffer));
    // writeFileContents(baseClassPath.string() + ".touch", std::string(buffer));

    if(std::string(buffer).empty())
    {
        ARMARX_WARNING << "Failed to generate statechart context file!";
        return false;
    }

    if (forceRewrite)
    {
        writeFileContents(outputPath.string(), cmakeFileContent);
        return true;
    }
    
    writeFileContentsIfChanged(generatedFileName.string(), cmakeFileContent) ;
   
    return true;
}

bool StatechartGroupGenerator::generateStatechartGroupCMakeSourceListFile(const std::string& statechartGroupXmlFilePath, const std::filesystem::path& buildDir, bool forceRewrite, const std::optional<std::string>& packageIncludePath, const bool nextGenBehavior)
{

    StatechartGroupXmlReaderPtr reader(new StatechartGroupXmlReader(StatechartProfilePtr()));
    reader->readXml(std::filesystem::path(statechartGroupXmlFilePath));

    VariantInfoPtr variantInfo = VariantInfo::ReadInfoFilesRecursive(reader->getPackageName(), buildDir.string(), false);
    return generateStatechartGroupCMakeSourceListFile(reader, buildDir, variantInfo, forceRewrite, packageIncludePath, nextGenBehavior);
}

bool StatechartGroupGenerator::generateStatechartGroupCMakeSourceListFiles(const std::string& packageName, const std::string& statechartsDir, const std::filesystem::path& buildDir, bool forceRewrite, const std::string& dataDir, const std::map<std::string, std::string>& dependencies, const std::optional<std::string>& packageIncludePath, const bool nextGenBehavior)
{
    bool written = false;
    VariantInfoPtr variantInfo = readVariantInfoWithPaths(packageName, buildDir.string(), dataDir, dependencies);
    auto groupFiles = findStatechartGroupFiles(statechartsDir);
    for (auto& group : groupFiles)
    {
        StatechartGroupXmlReaderPtr reader(new StatechartGroupXmlReader(StatechartProfilePtr()));
        reader->readXml(std::filesystem::path(group));

        written |= generateStatechartGroupCMakeSourceListFile(reader, buildDir, variantInfo, forceRewrite, packageIncludePath, nextGenBehavior);

    }
    return written;
}

VariantInfoPtr StatechartGroupGenerator::readVariantInfoWithPaths(const std::string& packageName, const std::string& buildDir, const std::string& dataDir, const std::map<std::string, std::string>& dependencies)
{
    VariantInfoPtr variantInfo(new VariantInfo());
    std::filesystem::path variantInfoFile(dataDir);
    variantInfoFile /= packageName;
    variantInfoFile /= "VariantInfo-" + packageName + ".xml";

    if (!variantInfo->isPackageLoaded(packageName) && std::filesystem::exists(variantInfoFile))
    {
        RapidXmlReaderPtr xmlReader = RapidXmlReader::FromFile(variantInfoFile.string());
        variantInfo->readVariantInfo(xmlReader, buildDir, packageName);
        ARMARX_DEBUG_S << "Read " << variantInfoFile.string();
    }
    for (auto& dep : dependencies)
    {
        variantInfo = VariantInfo::ReadInfoFilesRecursive(dep.first, dep.second, false, variantInfo);
    }
    return variantInfo;
}

bool StatechartGroupGenerator::generateStatechartGroupCMakeSourceListFile(const std::string& packageName, const std::string& statechartGroupXmlFilePath, const std::filesystem::path& buildDir, bool forceRewrite, const std::string& dataDir, const std::map<std::string, std::string>& dependencies, const std::optional<std::string>& packageIncludePath, const bool nextGenBehavior)
{
    auto variantInfo = readVariantInfoWithPaths(packageName, buildDir.string(), dataDir, dependencies);

    StatechartGroupXmlReaderPtr reader(new StatechartGroupXmlReader(StatechartProfilePtr()));
    reader->readXml(std::filesystem::path(statechartGroupXmlFilePath));

    return generateStatechartGroupCMakeSourceListFile(reader, buildDir, variantInfo, forceRewrite, packageIncludePath, nextGenBehavior);
}

bool StatechartGroupGenerator::writeFileContentsIfChanged(const std::string& path, const std::string& contents)
{
    bool exists = std::filesystem::exists(std::filesystem::path(path));
    auto oldContent = exists ? RapidXmlReader::ReadFileContents(path) : "";
    if (exists && oldContent == contents)
    {
        return false;
    }
    ARMARX_DEBUG << "Writing " << path << std::endl;
    writeFileContents(path, contents);
    //    if (!oldContent.empty())
    //    {
    //        writeFileContents(path + ".old", oldContent);
    //    }
    return true;
}

void StatechartGroupGenerator::writeFileContents(const std::string& path, const std::string& contents)
{
    std::filesystem::create_directories(std::filesystem::path(path).parent_path());
    std::ofstream file;
    file.open(path.c_str());

    if (!file.is_open())
    {
        throw armarx::exceptions::local::FileOpenException(path);
    }
    ARMARX_CHECK_EXPRESSION(!contents.empty()) << path;
    ARMARX_DEBUG << "Writing to " << path;
    file << contents;
    file.flush();
    file.close();
}

std::set<std::string> StatechartGroupGenerator::getVariantTypesOfStatesWithNoCpp(StatechartGroupXmlReaderPtr groupReader, const VariantInfoPtr& variantInfo)
{
    std::set<std::string> variantTypes;
    std::set<std::string> alreadyLinkedTypes;
    for (auto& statefile : groupReader->getStateFilepaths())
    {
        std::set<std::string>* currentTypeSet = &variantTypes;
        RapidXmlReaderPtr stateReader = RapidXmlReader::FromFile(statefile);
        auto headerFilePath = std::filesystem::path(statefile);
        headerFilePath.replace_extension(std::filesystem::path("h"));
        if (std::filesystem::exists(headerFilePath))
        {
            currentTypeSet = &alreadyLinkedTypes;
        }
        RapidXmlReaderNode stateNode = stateReader->getRoot("State");
        std::set<std::string> types = XmlStateBaseClassGenerator::GetUsedInnerNonBasicVariantTypes(stateNode, variantInfo);
        currentTypeSet->insert(types.begin(), types.end());
    }
    std::set<std::string> results;
    std::set_difference(variantTypes.begin(), variantTypes.end(),
                        alreadyLinkedTypes.begin(), alreadyLinkedTypes.end(),
                        std::inserter(results, results.end()));
    return results;
}

std::set<std::string> StatechartGroupGenerator::getVariantTypesOfStates(StatechartGroupXmlReaderPtr groupReader, const VariantInfoPtr& variantInfo)
{
    std::set<std::string> variantTypes;

    for (auto& statefile : groupReader->getStateFilepaths())
    {
        RapidXmlReaderPtr stateReader = RapidXmlReader::FromFile(statefile);
        ARMARX_INFO << "Getting Variant types for " << statefile;
        RapidXmlReaderNode stateNode = stateReader->getRoot("State");
        std::set<std::string> types = XmlStateBaseClassGenerator::GetUsedInnerNonBasicVariantTypes(stateNode, variantInfo);
        ARMARX_INFO << "Found variant types: " << Ice::StringSeq(types.begin(), types.end());
        variantTypes.insert(types.begin(), types.end());
    }

    return variantTypes;
}

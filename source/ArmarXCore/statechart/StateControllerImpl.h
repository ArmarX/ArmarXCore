#pragma once

#include "StateController.h"

#include <condition_variable>
#include <mutex>
#include <queue>

namespace armarx
{

    struct StateController::Impl
    {
        IceUtil::Handle<RunningTask<StateController> > __runningTask;

        /*! \brief Buffer for all events that are
        delayed due to unbreakable substates.

        This buffer only holds events that are processed by this state.
          */
        std::queue<EventPtr> __unbreakableBuffer;
        bool __eventBufferedDueToUnbreakableState = false;
        std::vector< std::pair<StateControllerPtr, EventPtr> > __eventBuffer;
        TransitionFunctionMap transitionFunctions;

        bool __finished = false;
        mutable std::mutex __finishedMutex;
        mutable std::condition_variable __finishedCondition;

        /**
         * @brief localProfiler local instance of armarx::Profiler::Profiler which does nothing by default
         *
         * This instance can be changed via armarx::StateController::setProfilerRecursive() (\see armarx::RemoteStateOfferer)
         */
        Profiler::ProfilerSet localProfilers;
        bool profilersDisabled = false;
    };

}

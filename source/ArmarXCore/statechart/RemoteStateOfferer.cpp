#include "RemoteStateOfferer.h"

#include <ArmarXCore/statechart/StateBaseImpl.h>

namespace armarx
{

    void RemoteStateOffererBase::onConnectRemoteStateOfferer()
    {
    }

    void RemoteStateOffererBase::onExitRemoteStateOfferer()
    {
    }

    CreateRemoteStateInstanceOutput RemoteStateOffererBase::createRemoteStateInstanceNew(
        const CreateRemoteStateInstanceInput& input,
        const Ice::Current& /*context*/)
    {
        CreateRemoteStateInstanceOutput result;

        StateBasePtr statePtr = getStatePtr(input.stateClassName);
        // Can this even happen?
        // You would get a segfault in getRemoteInputParameters() since this method does not check
        if (!statePtr)
        {
            std::stringstream msg;
            msg << "Could not find (public) state: " << input.stateClassName;
            HiddenTimedMutex::ScopedLock lock(*StateBase::Impl::__StateInstancesMutex);

            for (const auto& entry : (*impl->stateInstancesPtr))
            {
                StateBase* state = entry.second;

                if (stateName == state->getStateName())
                {
                    msg << "- but found a state instance with same name - maybe you forgot to make the state public?";
                    break;
                }
            }

            throw LocalException(msg.str());
        }

        // getRemoteInputParameters()
        result.inputParameters = statePtr->inputParameters;
        // getRemoteOutputParameters()
        result.outputParameters = statePtr->outputParameters;

        // createRemoteStateInstance
        {
            waitUntilComponentStarted();

            StatePtr newState = StatePtr::dynamicCast(statePtr->clone());
            if (!newState)
            {
                throw exceptions::local::eNullPointerException("Could not cast from StateBasePtr to StatePtr");
            }

            if (!input.instanceName.empty())
            {
                newState->setStateName(input.instanceName);
            }

            RemoteStateWrapperPtr newWrappedState = new RemoteStateWrapper(newState, input.remoteState, input.parentStateLayout);
            RemoteStateData remoteStateData;
            remoteStateData.callerIceName = "";
            remoteStateData.callerStatePrx = input.remoteState;
            remoteStateData.remoteWrappedState = newWrappedState;
            remoteStateData.id = newState->getLocalUniqueId();
            {
                HiddenTimedMutex::ScopedLock lock(stateInstanceListMutex);
                stateInstanceList[remoteStateData.id] = remoteStateData;
            }

            newWrappedState->globalStateIdentifier = input.parentStateItentifierStr;
            newState->__setParentState(newWrappedState._ptr);

            result.remoteStateId = remoteStateData.id;

            // getRemoteStateInstance()
            {
                StatePtr state = newState;
                state->refetchSubstates();
                result.statechartInstance = state;
            }
        }

        return result;
    }

    int RemoteStateOffererBase::createRemoteStateInstance(
        const std::string& stateName,
        const RemoteStateIceBasePrx& remoteStatePrx,
        const std::string& parentStateItentifierStr,
        const std::string& instanceName,
        const Ice::Current& /*context*/)
    {
        waitUntilComponentStarted();

        //look for state with stateName
        StateBasePtr statePtr = getStatePtr(stateName);

        if (!statePtr)
        {
            std::stringstream msg;
            msg << "Could not find (public) state: " << stateName;
            HiddenTimedMutex::ScopedLock lock(*StateBase::Impl::__StateInstancesMutex);

            for (const auto& entry : (*impl->stateInstancesPtr))
            {
                StateBase* state = entry.second;

                if (stateName == state->getStateName())
                {
                    msg << "- but found a state instance with same name - maybe you forgot to make the state public?";
                    break;
                }
            }

            throw LocalException(msg.str());
        }

        //        if(!RemoteAccessableStatePtr::dynamicCast(statePtr))
        //            throw throw UserException("The state "+ statePtr->stateName + " must be derived of armarx::RemoteAccessableState");
        ARMARX_DEBUG << "entering createRemoteStateInstance() with " << parentStateItentifierStr << " stateclass: " << statePtr->stateClassName << " statename: " << statePtr->stateName << flush;
        StatePtr newState = StatePtr::dynamicCast(statePtr->clone());

        if (!newState)
        {
            throw exceptions::local::eNullPointerException("Could not cast from StateBasePtr to StatePtr");
        }

        if (!instanceName.empty())
        {
            newState->setStateName(instanceName);
        }

        ARMARX_DEBUG << "RemoteStateInstanceName: " << newState->getStateName() << " instanceName=" << instanceName;
        RemoteStateWrapperPtr newWrappedState = new RemoteStateWrapper(newState, remoteStatePrx);
        RemoteStateData remoteStateData;
        remoteStateData.callerIceName = "";
        remoteStateData.callerStatePrx = remoteStatePrx;
        remoteStateData.remoteWrappedState = newWrappedState;
        remoteStateData.id = newState->getLocalUniqueId();
        {
            HiddenTimedMutex::ScopedLock lock(stateInstanceListMutex);
            stateInstanceList[remoteStateData.id] = remoteStateData;
        }

        newWrappedState->globalStateIdentifier = parentStateItentifierStr;
        newState->__setParentState(newWrappedState._ptr);


        return remoteStateData.id;
    }

    void RemoteStateOffererBase::updateGlobalStateIdRecursive(int stateId, const std::string& parentId, const Ice::Current& context)
    {
        RemoteStateWrapperPtr wrapper = getInstance(stateId).remoteWrappedState;

        StatePtr state = wrapper->realState;
        if (state)
        {
            state->__getParentState()->globalStateIdentifier = parentId;
            state->__updateGlobalStateIdRecursive();
        }
    }

    void RemoteStateOffererBase::callRemoteState(int stateId, const StringVariantContainerBaseMap& properties, const Ice::Current& context)
    {
        StatePtr state = getInstance(stateId).remoteWrappedState->realState;

        if (state->getStatePhase() >= eExited)
        {
            state->setStatePhase(eDefined);
        }

        state->enter(properties);
    }

    void RemoteStateOffererBase::exitRemoteState(int stateId, const ::Ice::Current& context)
    {
        getInstance(stateId).remoteWrappedState->realState->_baseOnExit();
    }

    bool RemoteStateOffererBase::breakRemoteState(int stateId, const EventBasePtr& evt, const Ice::Current& context)
    {
        ARMARX_DEBUG << "breaking remote state id " << stateId << "\n" << flush;
        bool result = true;
        RemoteStateData stateData;

        try
        {
            stateData = getInstance(stateId);
            result =  stateData.remoteWrappedState->realState->_baseOnBreak(EventPtr::dynamicCast(evt));
        }
        catch (const LocalException&)
        {
            ARMARX_ERROR << "Could not find state with id " << stateId << "  - thus cannot break it" << flush;
            return true;
        }

        return result;
    }

    bool RemoteStateOffererBase::isRemoteStateFinished(int stateId, const Ice::Current& context)
    {
        return getInstance(stateId).remoteWrappedState->realState->isFinished();
    }

    bool RemoteStateOffererBase::breakActiveSubstateRemotely(int stateId, const EventBasePtr& evt, const Ice::Current& context)
    {
        return getInstance(stateId).remoteWrappedState->realState->__breakActiveSubstate(EventPtr::dynamicCast(evt));
    }

    void RemoteStateOffererBase::notifyEventBufferedDueToUnbreakableStateRemote(int stateId, bool eventBuffered, const Ice::Current& context)
    {
        StateControllerPtr statePtr = StateControllerPtr::dynamicCast(getGlobalInstancePtr(stateId));

        statePtr->__notifyEventBufferedDueToUnbreakableState(eventBuffered);
    }

    StateIceBasePtr RemoteStateOffererBase::refetchRemoteSubstates(int stateId, const Ice::Current& context)
    {
        RemoteStateWrapperPtr wrapper = getInstance(stateId).remoteWrappedState;

        std::scoped_lock lock(wrapper->mutex);
        StatePtr state = wrapper->realState;
        state->refetchSubstates();
        return new StateIceBase(*state);
    }

    StateParameterMap RemoteStateOffererBase::getRemoteInputParameters(const std::string& stateName, const Ice::Current& context)
    {
        StateBasePtr statePtr = getStatePtr(stateName);
        return statePtr->inputParameters;
    }

    StateParameterMap RemoteStateOffererBase::getRemoteOutputParameters(const std::string& stateName, const Ice::Current& context)
    {
        StateBasePtr statePtr = getStatePtr(stateName);
        return statePtr->outputParameters;
    }

    StateParameterMap RemoteStateOffererBase::getRemoteInputParametersById(int stateId, const Ice::Current& context)
    {
        return getInstance(stateId).remoteWrappedState->realState->inputParameters;
    }

    StateParameterMap RemoteStateOffererBase::getRemoteOutputParametersById(int stateId, const Ice::Current& context)
    {
        RemoteStateData entry = getInstance(stateId);
        return entry.remoteWrappedState->realState->outputParameters;
    }

    bool RemoteStateOffererBase::hasSubstatesRemote(const std::string& stateName, const Ice::Current& context) const
    {
        StateBasePtr statePtr = getStatePtr(stateName);

        if (statePtr->subStateList.size() > 0)
        {
            return true;
        }

        return false;
    }

    bool RemoteStateOffererBase::hasActiveSubstateRemote(int stateId, const Ice::Current& context)
    {
        return getInstance(stateId).remoteWrappedState->realState->__hasActiveSubstate();
    }

    Ice::StringSeq RemoteStateOffererBase::getAvailableStates(const Ice::Current& context)
    {
        Ice::StringSeq result;

        for (unsigned int i = 0; i < subStateList.size(); ++i)
        {
            result.push_back(StateBasePtr::dynamicCast(subStateList.at(i))->stateName);
        }

        return result;
    }

    StateIdNameMap RemoteStateOffererBase::getAvailableStateInstances(const Ice::Current& context)
    {
        StateIdNameMap result;
        HiddenTimedMutex::ScopedLock lock(stateInstanceListMutex);
        typename std::map<int, RemoteStateData>::iterator it = stateInstanceList.begin();

        for (; it != stateInstanceList.end(); ++it)
        {
            result[it->second.remoteWrappedState->realState->getLocalUniqueId()] = it->second.remoteWrappedState->realState->getGlobalHierarchyString();
        }

        return result;
    }

    StateIceBasePtr RemoteStateOffererBase::getStatechart(const std::string& stateName, const Ice::Current& context)
    {
        StateBasePtr state = getStatePtr(stateName);
        HiddenTimedMutex::ScopedLock lock(impl->__stateMutex);
        return state;
    }

    StateIceBasePtr RemoteStateOffererBase::getStatechartInstance(int stateId, const Ice::Current&)
    {
        StatePtr state = getInstance(stateId).remoteWrappedState->realState;
        state->refetchSubstates();
        return state;
    }

    StateIceBasePtr RemoteStateOffererBase::getStatechartInstanceByGlobalIdStr(const std::string& globalStateIdStr, const Ice::Current&)
    {
        StateIceBasePtr result;
        int stateCount = 0;
        {
            HiddenTimedMutex::ScopedLock lock(stateInstanceListMutex);
            typename std::map<int, RemoteStateData>::iterator it = stateInstanceList.begin();

            for (; it != stateInstanceList.end(); ++it)
            {
                StateBasePtr curState = StateBasePtr::dynamicCast(it->second.remoteWrappedState->realState);

                if (curState->globalStateIdentifier == globalStateIdStr)
                {

                    stateCount++;

                    if (stateCount == 1)
                    {
                        result = curState;
                    }
                }

                if (stateCount == 0)
                {
                    result = getStatechartInstanceByGlobalIdStrRecursive(globalStateIdStr, curState, stateCount);
                }
            }
        }

        if (stateCount > 1)
        {
            ARMARX_WARNING << "Found more than one state with globalStateIdStr '" << globalStateIdStr << "'. Returning first found occurence." << flush;
        }

        StateBasePtr state = StateBasePtr::dynamicCast(result);
        if (state)
        {
            state->refetchSubstates();
        }
        else
        {
            ARMARX_WARNING << "Could not find state instance with id str '" << globalStateIdStr << "'";
        }
        return state;
    }

    bool RemoteStateOffererBase::isHostOfStateByGlobalIdStr(const std::string& globalStateIdStr, const Ice::Current&)
    {
        HiddenTimedMutex::ScopedLock lock(stateInstanceListMutex);
        std::queue<StateBasePtr> stateList;
        StateBasePtr statePtr;
        for (auto& instancePair : stateInstanceList)
        {
            stateList.push(instancePair.second.remoteWrappedState->realState);
        }
        ARMARX_DEBUG << "Query: " << globalStateIdStr;
        do
        {
            if (stateList.size() > 0)
            {
                statePtr = stateList.front();
                stateList.pop();
            }
            if (statePtr)
            {
                ARMARX_DEBUG << "Checking " << statePtr->getGlobalHierarchyString();
            }
            RemoteStatePtr remoteState = RemoteStatePtr::dynamicCast(statePtr);
            //first add all subchildren to list
            if (statePtr && !remoteState) // dont inspect remote states
            {
                for (unsigned int i = 0; i < statePtr->subStateList.size(); i++)
                {
                    stateList.push(StateBasePtr::dynamicCast(statePtr->subStateList.at(i)));
                }
            }
            if (statePtr && statePtr->getGlobalHierarchyString() == globalStateIdStr)
            {
                return true;
            }
        }
        while (stateList.size() > 0);

        return false;
    }

    void RemoteStateOffererBase::removeInstance(int stateId, const Ice::Current&)
    {
        try
        {
            RemoteStateData entry = getInstance(stateId);
            ARMARX_DEBUG << "removing instance of state '" << entry.remoteWrappedState->realState->stateName << "'  \n" << flush;
            entry.remoteWrappedState->realState->clearSelfPointer();
            entry.remoteWrappedState->realState->disableRunFunction();
            HiddenTimedMutex::ScopedLock lock(stateInstanceListMutex);
            stateInstanceList.erase(stateId);
        }
        catch (LocalException& e)
        {
            ARMARX_WARNING << "Couldn't find state with id " << stateId << ". Hence, it could not be removed.\n" << flush;
        }
    }

    void RemoteStateOffererBase::issueEvent(int receivingStateId, const EventBasePtr& event, const Ice::Current&)
    {
        ARMARX_VERBOSE << "received external event '" << event->eventName << "' for state '" << event->eventReceiverName << "'" << flush;
        ARMARX_VERBOSE << "Event Dict Size: " << event->properties.size() << "\n" << StateUtilFunctions::getDictionaryString(event->properties) << flush;
        StateControllerPtr statePtr = StateControllerPtr::dynamicCast(getGlobalInstancePtr(receivingStateId));

        try
        {
            if (impl->manager)
            {
                impl->manager->addEvent(EventPtr::dynamicCast(event), statePtr);
            }
        }
        catch (LocalException& local)
        {
            ARMARX_ERROR << "Caught Exception in issueEvent: " << local.what() << flush;
        }
        catch (const IceUtil::Exception& exception)
        {
            // Handle local (run-time) exceptions
            ARMARX_ERROR
                    << "caught ice exception: " << exception.what()
                    << "\nBacktrace: " << exception.ice_stackTrace()
                    << flush;
        }
    }

    void RemoteStateOffererBase::issueEventWithGlobalIdStr(const std::string& globalStateIdStr, const EventBasePtr& evt, const Ice::Current&)
    {
        try
        {
            int id = StateBasePtr::dynamicCast(getStatechartInstanceByGlobalIdStr(globalStateIdStr))->getLocalUniqueId();
            issueEvent(id, evt);
        }
        catch (Ice::Exception& e)
        {
            ARMARX_ERROR << "Caught Ice::exception: " << e.what() << std::endl << e.ice_stackTrace();
        }
    }

    RemoteStatePtr RemoteStateOffererBase::addRemoteState(std::string stateName, std::string proxyName, std::string instanceName)
    {
        throw exceptions::local::eStatechartLogicError("You cannot add remoteStates directly to the RemoteStateOfferer. You can only add them to substates.");
    }

    RemoteStatePtr RemoteStateOffererBase::addDynamicRemoteState(std::string instanceName)
    {
        throw exceptions::local::eStatechartLogicError("You cannot add dynamicRemoteStates directly to the RemoteStateOfferer. You can only add them to substates.");
    }

    RemoteStateOffererBase::RemoteStateData RemoteStateOffererBase::getInstance(int stateId)
    {
        HiddenTimedMutex::ScopedLock lock(stateInstanceListMutex);
        typename std::map<int, RemoteStateData>::iterator it = stateInstanceList.find(stateId);

        if (it == stateInstanceList.end())
        {
            std::stringstream str;
            str << "Could not find state with id '" << stateId << "'\n";
            str << "Known states:\n";

            for (it = stateInstanceList.begin(); it != stateInstanceList.end(); ++it)
            {
                RemoteStateData& data = it->second;
                str << "\t" << data.remoteWrappedState->stateName << " id: " << data.remoteWrappedState->getLocalUniqueId() << flush;
            }

            throw LocalException(str.str());
        }

        return it->second;
    }

    StateBasePtr RemoteStateOffererBase::getGlobalInstancePtr(int globalId) const
    {
        HiddenTimedMutex::ScopedLock lock(*StateBase::Impl::__StateInstancesMutex);
        typename std::map<int, StateBase*>::iterator it = impl->stateInstancesPtr->find(globalId);

        if (it != impl->stateInstancesPtr->end())
        {
            return it->second;
        }

        std::stringstream str;
        str << "Could not find state with id '" << globalId << "'";
        throw LocalException(str.str());

        return nullptr;
    }

    StateBasePtr RemoteStateOffererBase::getStatePtr(const std::string& stateName) const
    {
        bool found = false;
        StateBasePtr statePtr = nullptr;

        for (unsigned int i = 0; i < subStateList.size(); i++)
        {

            StateBasePtr state = StateBasePtr::dynamicCast(subStateList.at(i));
            ARMARX_CHECK_NOT_NULL(state);
            if (state->stateName == stateName)
            {
                statePtr = state;
                found = true;
                break;
            }
        }

        if (!found)
        {
            ARMARX_ERROR << "Could not find state with name '" << stateName << "'" << flush;
            //            throw LocalException("Could not find state with name '" + stateName + "'");
        }

        return statePtr;
    }

    std::map<int, StateBasePtr> RemoteStateOffererBase::getChildStatesByName(int parentId, std::string stateName)
    {
        StateBasePtr statePtr = getInstance(parentId).remoteWrappedState->realState;
        std::map<int, StateBasePtr> stateList;
        std::map<int, StateBasePtr> result;

        do
        {
            //first add all subchildren to list
            for (unsigned int i = 0; i < statePtr->subStateList.size(); i++)
            {
                stateList.insert(std::pair<int, StateBasePtr> (StateBasePtr::dynamicCast(statePtr->subStateList.at(i))->getLocalUniqueId(), StateBasePtr::dynamicCast(statePtr->subStateList.at(i))));
            }

            if (parentId != statePtr->getLocalUniqueId())// dont add parentstate
            {
                if (StateBasePtr::dynamicCast(statePtr)->stateName == stateName)
                {
                    result.insert(std::pair<int, StateBasePtr> (statePtr->getLocalUniqueId(), statePtr));
                }

                stateList.erase(statePtr->getLocalUniqueId());
            }

            if (stateList.size() > 0)
            {
                statePtr = stateList.begin()->second;
            }
        }
        while (stateList.size() > 0);

        return result;
    }

    StateIceBasePtr RemoteStateOffererBase::getStatechartInstanceByGlobalIdStrRecursive(const std::string& globalStateIdStr, StateBasePtr state, int& stateCounter)
    {
        StateIceBasePtr result;
        StateList::iterator it = state->subStateList.begin();

        for (; it != state->subStateList.end(); ++it)
        {
            StateBasePtr curState = StateBasePtr::dynamicCast(*it);

            if (curState->globalStateIdentifier == globalStateIdStr)
            {
                stateCounter++;

                if (stateCounter == 1)
                {
                    result = curState;
                }
            }

            if (stateCounter == 0)
            {
                result = getStatechartInstanceByGlobalIdStrRecursive(globalStateIdStr, curState, stateCounter);
            }
        }

        return result;
    }

    void RemoteStateOffererBase::onConnectStatechartImpl()
    {
        ARMARX_DEBUG << "Starting RemoteStateOfferer ";
        onConnectRemoteStateOfferer();
    }

    void RemoteStateOffererBase::onExitStatechartImpl()
    {
        onExitRemoteStateOfferer();

        for (size_t i = 0; i < subStateList.size(); i++)
        {
            StateControllerPtr c = StateControllerPtr::dynamicCast(subStateList.at(i));

            if (c)
            {
                c->disableRunFunction();
            }
        }

        subStateList.clear();

        for (auto& e : stateInstanceList)
        {
            StateControllerPtr c = StateControllerPtr::dynamicCast(e.second.remoteWrappedState->realState);

            if (c)
            {
                c->disableRunFunction();
            }
        }

        stateInstanceList.clear();
    }

    void RemoteStateOffererBase::run()
    {
    }

    void RemoteStateOffererBase::initState(State& state)
    {
        state.init(impl->context, impl->manager);
    }

}

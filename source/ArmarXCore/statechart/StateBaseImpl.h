#pragma once

#include "StateBase.h"

#include <ArmarXCore/core/system/Synchronization.h>

namespace armarx
{

    struct StateBase::Impl
    {
        Impl() :
            statePhase(ePreDefined),
            __eventUnbreakableBufferMutex(30000, "__eventUnbreakableBufferMutex"),
            __processEventsMutex(30000, "__processEventsMutex"),
            __stateMutex(30000, "__stateMutex"),
            __useRunFunction(true),
            __parentState(nullptr)
        {

        }

        mutable ConditionalVariable initCondition;
        mutable Mutex initMutex;

        StatePhase statePhase;
        mutable Mutex statePhaseMutex;

        /**
         *Pointer to the StatechartContext to the single instance per
         *application. Contains proxies to observers and units. Used to operate
         *with distributed components over ice.
         **/
        StatechartContextInterface* context;
        StatechartManager* manager;

        //! Unique Identifier counter for this process for identifing states.
        static int __LocalIdCounter;
        static HiddenTimedMutex* __StateInstancesMutex; // pointer to prevent premature destruction
        RecursiveMutex __eventBufferMutex;
        HiddenTimedMutex __eventUnbreakableBufferMutex;
        RecursiveMutex __processEventMutex;
        HiddenTimedMutex __processEventsMutex;
        HiddenTimedMutex __stateMutex;

        std::vector<std::string> inputInheritance;
        long visitCounter;


        bool __useRunFunction;

        /**
         * @brief This variable contains the event of the end substate that was entered to leave this state.
         */
        EventPtr triggeredEndstateEvent;

        using StateInstanceMap = std::map<int, StateBase*>;
        using StateInstanceMapPtr = std::shared_ptr<StateInstanceMap>;
        //! Static map that contains all the states that are alive in this process
        static StateInstanceMapPtr StateInstances;
        StateInstanceMapPtr stateInstancesPtr;

        StateBase* __parentState;

        /**
         *  An in this process unique id for the state
         */
        int localUniqueId;

        /**
         * @brief This property is needed for identification at what time this
         * state was entered the last time.
         *
         * The reason is, that events should only be processed by a state, if
         * the events were installed in this visit to this state.
         * @note not used yet.
         */
        long visitTimeStamp;

        /**
         * @brief GetActiveStateLeafs returns the list of leaf-states that are
         * active at the moment.
         * @param toplevelState State from which the active substate is to be returned.
         * @return List of active leaf-states. If no orthogonal states are used,
         * the list has at most 1 item.
         */

        bool cancelEnteringSubstates;
    };

}

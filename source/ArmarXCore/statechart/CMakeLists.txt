armarx_set_target("Core Library: ArmarXCoreStatechart")
find_package(Eigen3 QUIET)
find_package(jsoncpp QUIET)

armarx_build_if(Eigen3_FOUND "Eigen3 not available")
armarx_build_if(jsoncpp_FOUND "jsoncpp not available")

set(LIB_NAME       ArmarXCoreStatechart)

set(LIBS ArmarXCoreObservers ArmarXCoreJsonObject cppgen ${Boost_SYSTEM_LIBRARY} ${Boost_FILESYSTEM_LIBRARY} ${Boost_REGEX_LIBRARY})

set(LIB_FILES StatechartEventDistributor.cpp
              State.cpp
              StateController.cpp
              StateBase.cpp
              StatechartContext.cpp
              StatechartManager.cpp
              StateUtil.cpp
              StateUtilFunctions.cpp
              StateParameter.cpp
              ParameterMapping.cpp
              RemoteState.cpp
              DynamicRemoteState.cpp
              RemoteStateOfferer.cpp
              RemoteStateWrapper.cpp
              standardstates/FinalState.cpp
              xmlstates/XMLState.cpp
              xmlstates/GroupXmlReader.cpp
              xmlstates/XMLStateComponent.cpp
              xmlstates/StateGroupDocGenerator.cpp
              xmlstates/StateGroupGenerator.cpp
              xmlstates/baseclassgenerator/XmlStateBaseClassGenerator.cpp
              xmlstates/baseclassgenerator/XmlContextBaseClassGenerator.cpp
              xmlstates/baseclassgenerator/CMakeSourceListGenerator.cpp
              xmlstates/baseclassgenerator/DoxTable.cpp
              xmlstates/baseclassgenerator/DoxLine.cpp
              xmlstates/baseclassgenerator/DoxDoc.cpp
              xmlstates/baseclassgenerator/DoxTransitiongraph.cpp
              xmlstates/profiles/StatechartProfiles.cpp
              )

set(LIB_HEADERS StatechartEventDistributor.h
                Exception.h
                StatechartObjectFactories.h
                ParameterMapping.h
                RemoteState.h
                DynamicRemoteState.h
                RemoteStateOfferer.h
                RemoteStateWrapper.h
                State.h
                StateController.h
                StateControllerImpl.h
                StateBase.h
                StateBaseImpl.h
                Statechart.h
                StatechartContextInterface.h
                StatechartContext.h
                StatechartManager.h
                StateUtilFunctions.h
                StateUtil.h
                StateTemplate.h
                StateParameter.h
                standardstates/FinalState.h
                standardstates/CounterState.h
                xmlstates/XMLState.h
                xmlstates/GroupXmlReader.h
                xmlstates/XMLStateComponent.h
                xmlstates/XMLRemoteStateOfferer.h
                xmlstates/StateGroupDocGenerator.h
                xmlstates/StateGroupGenerator.h
                xmlstates/baseclassgenerator/XmlStateBaseClassGenerator.h
                xmlstates/baseclassgenerator/XmlContextBaseClassGenerator.h
                xmlstates/baseclassgenerator/CMakeSourceListGenerator.h
                xmlstates/baseclassgenerator/DoxEntry.h
                xmlstates/baseclassgenerator/DoxTable.h
                xmlstates/baseclassgenerator/DoxLine.h
                xmlstates/baseclassgenerator/DoxDoc.h
                xmlstates/baseclassgenerator/DoxTransitiongraph.h
                xmlstates/profiles/StatechartProfiles.h
                test/StatechartScenarioTestEnv.h
                test/StatechartTestEnv.h
                )

armarx_add_library("${LIB_NAME}"  "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")


if(Eigen3_FOUND AND jsoncpp_FOUND)
    target_include_directories("${LIB_NAME}" SYSTEM PUBLIC ${Eigen3_INCLUDE_DIR})
    target_include_directories("${LIB_NAME}" SYSTEM PUBLIC ${jsoncpp_INCLUDE_DIR})
endif()

add_subdirectory(test)

armarx_generate_and_add_component_executable(
    APPLICATION_NAME XMLRemoteStateOffererRun
    COMPONENT_NAME   XMLStateComponent)

/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::ArmarXObjects::DebugObserverHelper
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <SimoxUtility/meta/boost_hana.h>
#include <SimoxUtility/meta/enum/adapt_enum.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/core/ManagedIceObjectPlugin.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/observers/variant/TimedVariant.h>
#include <ArmarXCore/observers/variant/TimestampVariant.h>
#include <ArmarXCore/util/CPPUtility/TemplateMetaProgramming.h>
#include <ArmarXCore/util/CPPUtility/trace.h>


namespace armarx
{
    /**
    * @defgroup Library-DebugObserverHelper DebugObserverHelper
    * @ingroup ArmarXCore
    * A description of the library DebugObserverHelper.
    *
    * @class DebugObserverHelper
    * @ingroup Library-DebugObserverHelper
    * @brief Brief description of class DebugObserverHelper.
    *
    * Detailed description of class DebugObserverHelper.
    */
    class DebugObserverHelper
    {
        //create
    public:
        DebugObserverHelper(const DebugObserverInterfacePrx& prx = nullptr, bool batchmode = false);
        template<class T>
        DebugObserverHelper(const T& name, const DebugObserverInterfacePrx& prx = nullptr, bool batchmode = false) :
            DebugObserverHelper(prx, batchmode)
        {
            setChannelName(name);
        }

        //configure
    public:
        const DebugObserverInterfacePrx& getDebugObserver() const;
        void setDebugObserver(const DebugObserverInterfacePrx& prx);

        const std::string& getChannelName() const;
        void setChannelName(const std::string& name);
        void setChannelName(const ManagedIceObject& obj);
        void setChannelName(const ManagedIceObject* obj);
        void setChannelName(const ManagedIceObjectPlugin& obj);
        void setChannelName(const ManagedIceObjectPlugin* obj);

        //management
    public:
        void setDebugObserverBatchModeEnabled(bool enable);
        void sendDebugObserverBatch();

        //setDebugObserverDatafield
    public:
        void setDebugObserverDatafield(const std::string& channelName, const std::string& datafieldName,  const TimedVariantPtr& value) const;
        void setDebugObserverDatafield(const std::string& channelName, const std::string& datafieldName,  const VariantPtr& value) const;
        void setDebugObserverDatafield(const std::string& channelName, const std::string& datafieldName,  const VariantBasePtr& value) const;
        template<class T>
        void setDebugObserverDatafield(const std::string& channelName, const std::string& datafieldName, const T& t)
        {
            setDebugObserverDatafield(channelName, datafieldName, IceUtil::Time::now(), t);
        }
        template<class T>
        void setDebugObserverDatafield(const std::string& channelName, const std::string& datafieldName, const IceUtil::Time& time, const T& value) const
        {
            TimedVariantPtr var;
            using VariantIntMaxT = Ice::Int;
            [[maybe_unused]] static constexpr std::uint64_t IceIntTMax =
                static_cast<std::uint64_t>(std::numeric_limits<VariantIntMaxT>::max());
            if constexpr(
                std::is_same_v<T, IceUtil::Time> ||
                meta::TypeTemplateTraits::IsInstanceOfV<std::chrono::duration, T>   ||
                meta::TypeTemplateTraits::IsInstanceOfV<std::chrono::time_point, T>
            )
            {
                var = new TimedVariant(TimestampVariant(value), time);
            }
            else if constexpr(std::is_unsigned_v<T>&& !std::is_same_v<bool, T>)
            {
                if (_float_fallback_for_big_too_large_ints)
                {
                    if (value < IceIntTMax)
                    {
                        var = new TimedVariant(Variant(static_cast<VariantIntMaxT>(value)), time);
                    }
                    else
                    {
                        var = new TimedVariant(Variant(static_cast<double>(value)), time);
                    }
                }
                else
                {
                    ARMARX_CHECK_LESS_EQUAL(value, IceIntTMax);
                    var = new TimedVariant(Variant(static_cast<VariantIntMaxT>(value)), time);
                }
            }
            else if constexpr(simox::meta::has_hana_accessor_v<T>)
            {
                namespace hana = boost::hana;
                static constexpr auto accessors = hana::accessors<T>();
                hana::for_each(accessors, [&](auto & e)
                {
                    ARMARX_TRACE_LITE;
                    const auto varname = hana::to<char const*>(hana::first(e));
                    const auto elemName = datafieldName + '_' + varname;
                    setDebugObserverDatafield(channelName, elemName, time, hana::second(e)(value));
                });
                return;
            }
            else if constexpr(simox::meta::is_enum_adapted_v<T>)
            {
                var = new TimedVariant(Variant(std::to_string(value)), time);
            }
            else if constexpr(std::is_same_v<char, T>)
            {
                var = new TimedVariant(Variant(static_cast<int>(value)), time);
            }
            else
            {
                var = new TimedVariant(Variant(value), time);
            }
            setDebugObserverDatafield(channelName, datafieldName, var);
        }
        template<class T, class...Ts>
        void setDebugObserverDatafield(const std::string& channelName, const std::string& datafieldName, const std::chrono::duration<Ts...> duration, const T& value) const
        {
            setDebugObserverDatafield(
                channelName, datafieldName,
                IceUtil::Time::microSeconds(std::chrono::duration_cast<std::chrono::microseconds>(duration).count()),
                value);
        }
        template<class T, class...Ts>
        void setDebugObserverDatafield(const std::string& channelName, const std::string& datafieldName, const std::chrono::time_point<Ts...> timepoint, const T& value) const
        {
            setDebugObserverDatafield(channelName, datafieldName, timepoint.time_since_epoch(), value);
        }

        void setDebugObserverDatafield(const std::string& channelName, const std::string& datafieldName, const IceUtil::Time& time, const VariantPtr& value) const;
        template<class...Ts>
        void setDebugObserverDatafield(const std::string& channelName, const std::string& datafieldName, const std::chrono::duration<Ts...> duration, const VariantPtr& value) const
        {
            setDebugObserverDatafield(
                channelName, datafieldName,
                IceUtil::Time::microSeconds(std::chrono::duration_cast<std::chrono::microseconds>(duration).count()),
                value);
        }
        template<class...Ts>
        void setDebugObserverDatafield(const std::string& channelName, const std::string& datafieldName, const std::chrono::time_point<Ts...> timepoint, const VariantPtr& value) const
        {
            setDebugObserverDatafield(channelName, datafieldName, timepoint.time_since_epoch(), value);
        }

        void setDebugObserverDatafield(const std::string& channelName, const std::string& datafieldName, const IceUtil::Time& time, const Variant& value) const;
        template<class...Ts>
        void setDebugObserverDatafield(const std::string& channelName, const std::string& datafieldName, const std::chrono::duration<Ts...> duration, const Variant& value) const
        {
            setDebugObserverDatafield(
                channelName, datafieldName,
                IceUtil::Time::microSeconds(std::chrono::duration_cast<std::chrono::microseconds>(duration).count()),
                value);
        }
        template<class...Ts>
        void setDebugObserverDatafield(const std::string& channelName, const std::string& datafieldName, const std::chrono::time_point<Ts...> timepoint, const Variant& value) const
        {
            setDebugObserverDatafield(channelName, datafieldName, timepoint.time_since_epoch(), value);
        }

        template<class T>
        void setDebugObserverDatafield(const std::string& datafieldName, const T& t)
        {
            setDebugObserverDatafield(_channelName, datafieldName, t);
        }

        //set channel / remove
    public:
        void setDebugObserverChannel(const std::string& channelName, StringVariantBaseMap valueMap) const;
        void removeDebugObserverChannel(const std::string& channelname) const;
        void removeDebugObserverDatafield(const std::string& channelName, const std::string& datafieldName) const;
        void removeAllDebugObserverChannels() const;


        void setFloatFallbackForBigTooLargeInts(bool useFallback);


        //data
    private:
        std::string _channelName;
        DebugObserverInterfacePrx _observer;
        bool _batchMode{false};
        mutable std::map<std::string, StringVariantBaseMap> _batch;
        bool _float_fallback_for_big_too_large_ints = true;

    };

}

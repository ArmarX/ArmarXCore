#pragma once

#include <cstdint>
#include <mutex>
#include <vector>

#include "ArmarXCore/core/services/tasks/TaskUtil.h"
#include "ArmarXCore/core/time/DateTime.h"
#include "ArmarXCore/interface/observers/ObserverInterface.h"

namespace armarx
{

    /**
     * @brief The FrequencyReporter class.
     *
     * Use this class to monitor any periodic event.
     * The frequency (mean, std) are sent to the DebugObserver.
     *
     * Note: This class is thread safe.
     */
    class FrequencyReporter
    {
    public:
        /**
        * @brief Construct a new Frequency Reporter object
        *
        * @param debugObserver
        * @param name used to set the debug observer's channel name
        */
        FrequencyReporter(DebugObserverInterfacePrx debugObserver, const std::string& name);
        FrequencyReporter(FrequencyReporter&) = delete;
        virtual ~FrequencyReporter();

        /**
         * @brief Add a new timestamp to the reporter.
         *
         * @param timestamp in [µs]
         */
        void add(armarx::core::time::DateTime timestamp);

    private:
        /**
         * @brief Sends accumulated data to the debug observer.
         *
         * Will be called periodically.
         */
        void report();

        DebugObserverInterfacePrx debugObserver;
        std::string name;

        std::mutex mtx;
        std::vector<std::int64_t> timestamps; // [ms]

        SimplePeriodicTask<>::pointer_type task;

        const float frequency {5.F}; // [Hz]
    };
} // namespace armarx

#pragma once

#include <ArmarXCore/core/Component.h>

#include <Ice/PropertiesF.h>

#include <string>

namespace armarx
{

    typedef armarx::ComponentPtr CreateComponentFunction(Ice::PropertiesPtr properties,
            std::string const& configName,
            std::string const& configDomain);

    struct Decoupled
    {
        template <typename ComponentT>
        static bool registerComponent(std::string const& name)
        {
            return setCreateComponentFunction(&armarx::Component::create<ComponentT>,
                                              name);
        }

        static bool setCreateComponentFunction(CreateComponentFunction* function,
                                               std::string const& name);
    };



#define ARMARX_REGISTER_COMPONENT_EXECUTABLE(ComponentT, applicationName) \
    const static bool global_register_ ## Component = ::armarx::Decoupled::registerComponent< ComponentT >( applicationName )


#define ARMARX_DECOUPLED_REGISTER_COMPONENT(ComponentT) \
    ARMARX_REGISTER_COMPONENT_EXECUTABLE( ComponentT, (#ComponentT) )

}

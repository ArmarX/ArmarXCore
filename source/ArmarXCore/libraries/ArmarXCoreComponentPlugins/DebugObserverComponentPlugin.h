/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::ArmarXGuiComponentPlugins
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <utility>  // for std::forward

#include <ArmarXCore/core/ComponentPlugin.h>
#include <ArmarXCore/util/CPPUtility/TemplateMetaProgramming.h>

#include <ArmarXCore/libraries/DebugObserverHelper/DebugObserverHelper.h>


namespace armarx::plugins
{
    /**
    * @defgroup Library-ArmarXCoreComponentPlugins ArmarXCoreComponentPlugins
    * @ingroup ArmarXGui
    * A description of the library ArmarXCoreComponentPlugins.
    */

    /**
     * @class DebugObserverComponentPlugin
     * @ingroup Library-ArmarXCoreComponentPlugins
     * @brief Brief description of class DebugObserverComponentPlugin.
     *
     * Detailed description of class DebugObserverComponentPlugin.
     */
    class DebugObserverComponentPlugin :
        public ComponentPlugin,
        public DebugObserverHelper
    {
    public:

        using ComponentPlugin::ComponentPlugin;

        void setDebugObserverTopic(const std::string& name);
        std::string getDebugObserverTopic();


    protected:

        void preOnInitComponent() override;
        void postOnInitComponent() override;
        void preOnConnectComponent() override;

        void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;


    private:

        static constexpr auto _propertyName = "DebugObserverTopicName";
        std::string           _topicName;

    };
}


#include <ArmarXCore/core/ManagedIceObject.h>

namespace armarx
{
    class DebugObserverComponentPluginUser : virtual public ManagedIceObject
    {
    public:

        using DebugObserverComponentPlugin = armarx::plugins::DebugObserverComponentPlugin;


    public:

        DebugObserverComponentPluginUser();

        plugins::DebugObserverComponentPlugin& getDebugObserverComponentPlugin();


        template<class...Ts>
        void setDebugObserverDatafield(Ts&& ...ts) const
        {
            _debugObserverComponentPlugin->setDebugObserverDatafield(std::forward<Ts>(ts)...);
        }

        void setDebugObserverChannel(const std::string& channelName, StringVariantBaseMap valueMap) const;
        void removeDebugObserverChannel(const std::string& channelname) const;
        void removeDebugObserverDatafield(const std::string& channelName, const std::string& datafieldName) const;
        void removeAllDebugObserverChannels() const;

        const DebugObserverInterfacePrx& getDebugObserver() const;

        void setDebugObserverBatchModeEnabled(bool enable);
        void sendDebugObserverBatch();


    private:

        DebugObserverComponentPlugin* _debugObserverComponentPlugin{nullptr};

    };
}

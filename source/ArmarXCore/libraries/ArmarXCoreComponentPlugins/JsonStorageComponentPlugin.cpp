#include "JsonStorageComponentPlugin.h"

#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/components/JsonStorageInterface.h>


namespace armarx::plugins
{
    struct JsonStorageComponentPlugin::Impl
    {
        std::string propertyName;
        JsonStorageInterfacePrx proxy;

        std::string componentName;
    };

    static const char* const JsonStorage_PropertyName = "JsonStorageName";

    JsonStorageComponentPlugin::~JsonStorageComponentPlugin()
    {

    }

    void JsonStorageComponentPlugin::preOnInitComponent()
    {
        if (!impl)
        {
            impl.reset(new Impl);
            impl->propertyName = makePropertyName(JsonStorage_PropertyName);
            impl->componentName = parent<Component>().getName();
        }

        if (!impl->proxy)
        {
            parent<Component>().usingProxyFromProperty(impl->propertyName);
        }
    }

    void JsonStorageComponentPlugin::preOnConnectComponent()
    {
        parent<Component>().getProxyFromProperty(impl->proxy, impl->propertyName);
    }

    void JsonStorageComponentPlugin::postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties)
    {
        if (!properties->hasDefinition(makePropertyName(JsonStorage_PropertyName)))
        {
            properties->defineOptionalProperty<std::string>(
                makePropertyName(JsonStorage_PropertyName),
                "JsonStorage",
                "Name of the JSON storage (duh)");
        }
    }

    void JsonStorageComponentPlugin::storeValue(const std::string& key, const nlohmann::json& value)
    {
        JsonStoreValue insert;
        insert.key = key;
        insert.value = value.dump();
        insert.provider = impl->componentName;
        impl->proxy->storeJsonValue(insert);
    }

    JsonStorageRetrievedValue JsonStorageComponentPlugin::retrieveValue(const std::string& key)
    {
        JsonRetrieveValue iceResult = impl->proxy->retrieveValue(key);

        JsonStorageRetrievedValue result;
        result.valid = !iceResult.key.empty();
        if (result.valid)
        {
            result.key = iceResult.key;
            result.value = nlohmann::json::parse(iceResult.value);
            result.provider = iceResult.provider;
            result.revision = iceResult.revision;
            result.storeTimestamp = IceUtil::Time::microSeconds(iceResult.timestampInMicroSeconds);
        }

        return result;
    }


}

namespace armarx
{
    void JsonStorageComponentPluginUser::JsonStorage_storeValue(const std::string& key, const nlohmann::json& value)
    {
        plugin->storeValue(key, value);
    }

    JsonStorageRetrievedValue JsonStorageComponentPluginUser::JsonStorage_retrieveValue(const std::string& key)
    {
        return plugin->retrieveValue(key);
    }
}

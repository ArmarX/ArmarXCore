set(LIB_NAME       ArmarXCoreComponentPlugins)

armarx_component_set_name("${LIB_NAME}")
armarx_set_target("Library: ${LIB_NAME}")

armarx_add_library(
    LIBS     ArmarXCoreObservers
             DebugObserverHelper
    SOURCES  DebugObserverComponentPlugin.cpp
             JsonStorageComponentPlugin.cpp
    HEADERS  DebugObserverComponentPlugin.h
             JsonStorageComponentPlugin.h
)

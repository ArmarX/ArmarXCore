/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "MetaClass.h"

#include "CppEnum.h"
#include "CppMethod.h"
#include "CppField.h"
#include "CppCtor.h"

namespace armarx
{
    class CppClass;
    using CppClassPtr = std::shared_ptr<CppClass>;

    class CppClass :
        virtual public MetaClass
    {
    public:
        CppClass(std::vector<std::string> namespaces, std::string name, std::string templates = "");

        virtual void write(MetaWriterPtr writer) override;
        static void Write(std::vector<MetaClassPtr> classes, MetaWriterPtr writer);

        void writeCpp(CppWriterPtr writer);
        static void WriteCpp(std::vector<CppClassPtr> classes, CppWriterPtr writer);

        void addMethod(CppMethodPtr method);
        CppMethodPtr addMethod(const std::string& header, const std::string& doc = "");
        CppMethodPtr addMethod(const boost::basic_format<char>& header, const std::string& doc = "");

        void addCtor(const CppCtorPtr&);
        CppCtorPtr addCtor(const std::string arguments);
        CppCtorPtr addCtor(const std::string arguments, const CppBlockPtr& b);

        void addPublicField(const CppFieldPtr& field);
        void addPublicField(const std::string& field);
        void addPublicField(const boost::basic_format<char>& field);

        void addPrivateField(const CppFieldPtr& field);
        void addPrivateField(const std::string& field);
        void addPrivateField(const boost::basic_format<char>& field);

        void addProtectedField(const CppFieldPtr& field);
        void addProtectedField(const std::string& field);
        void addProtectedField(const boost::basic_format<char>& field);

        void addInnerClass(CppClassPtr inner);
        CppClassPtr addInnerClass(std::string name);

        void addInnerEnum(CppEnumPtr inner);
        CppEnumPtr addInnerEnum(std::string name);

        void addInherit(const std::string& inherit);
        void addInherit(const boost::basic_format<char>& inherit);

        void addInclude(const std::string include);
        void addInclude(const boost::basic_format<char>& include);

        void addClassDoc(const std::string& doc);
        bool hasInclude(const std::string include);

        std::string getName() const;
        void setPragmaOnceIncludeGuard(bool);
        void setIncludeGuard(const std::string& includeGuard);

        void setTemplates(const std::string&);


    private:
        void addDoc(CppWriterPtr writer);

        std::vector<std::string> namespaces;
        std::string templates;
        std::vector<CppMethodPtr> methods;
        std::vector<std::string> privateFields;
        std::vector<std::string> protectedFields;
        std::vector<std::string> publicFields;
        std::vector<std::string> inherit;
        std::vector<CppEnumPtr> innerEnums;
        std::vector<CppClassPtr> innerClasses;
        std::vector<CppCtorPtr> ctors;
        std::vector<std::string> includes;
        bool pragmaOnce = false;
        std::string includeGuard;
    };
}


/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "CppBlock.h"

#include <boost/format.hpp>

using namespace armarx;

CppBlock::CppBlock()
{
}

CppBlockStringEntry::CppBlockStringEntry(const std::string& s) :
    line(s)
{

}

CppBlockBlockEntry::CppBlockBlockEntry(const CppBlockPtr& b) :
    nestedBlock(b)
{

}

void CppBlockStringEntry::writeCpp(CppWriterPtr writer)
{
    writer->line(line);
}

void CppBlockBlockEntry::writeCpp(CppWriterPtr writer)
{
    nestedBlock->writeCpp(writer);
}

void CppBlock::writeCpp(CppWriterPtr writer)
{
    writer->startBlock();
    for (const auto& entry : entries)
    {
        entry->writeCpp(writer);
    }
    writer->endBlock();
}

std::string CppBlockStringEntry::getAsSingleLine()
{
    return line;
}

std::string CppBlockBlockEntry::getAsSingleLine()
{
    return nestedBlock->getAsSingleLine();
}

std::string CppBlock::getAsSingleLine()
{
    std::stringstream ss;
    ss << "{ ";

    for (const auto& entry : entries)
    {
        ss << entry->getAsSingleLine() << " ";
    }

    ss << "}";
    return ss.str();
}

void CppBlock::addLine(const std::string& line)
{
    CppBlockEntryPtr t = CppBlockEntryPtr(new CppBlockStringEntry(line));
    entries.push_back(t);
}

void CppBlock::addLine(const boost::basic_format<char>& line)
{
    addLine(boost::str(line));
}

void CppBlock::addCommentLine(const std::string& line)
{
    addLine("//" + line);
}

void CppBlock::addCommentLine(const boost::basic_format<char>& line)
{
    addLine("//" + boost::str(line));
}

void CppBlock::addCommentLines(const std::vector<std::string>& lines)
{
    addLine("/*");
    for (const auto& line : lines)
    {
        addLine(" * " + line);
    }
    addLine(" */");
}

void CppBlock::addCommentLines(const std::vector<boost::basic_format<char>>& lines)
{
    addLine("/*");
    for (const auto& line : lines)
    {
        addLine(" * " + boost::str(line));
    }
    addLine(" */");
}

void CppBlock::addBlock(const CppBlockPtr& block)
{
    CppBlockEntryPtr t = CppBlockEntryPtr(new CppBlockBlockEntry(block));
    entries.push_back(t);
}

void CppBlock::appendBlock(const CppBlockPtr& block)
{
    for (const auto& entry : block->entries)
    {
        addEntry(entry);
    }
}

void CppBlock::addLineAsBlock(const std::string& line)
{
    CppBlockPtr b = std::make_shared<CppBlock>();
    b->addLine(line);
    this->addBlock(b);
}

void CppBlock::addEntry(const CppBlockEntryPtr& entry)
{
    entries.push_back(entry);
}

CppBlockPtr CppBlock::MergeBlocks(const CppBlockPtr& block1, const CppBlockPtr& block2)
{
    CppBlockPtr ret = CppBlockPtr(new CppBlock());
    for (const auto& entry : block1->entries)
    {
        ret->addEntry(entry);
    }

    for (const auto& entry : block2->entries)
    {
        ret->addEntry(entry);
    }
    return ret;
}

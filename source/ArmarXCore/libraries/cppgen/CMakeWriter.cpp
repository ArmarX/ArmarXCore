#include "CMakeWriter.h"

using namespace armarx;

CMakeWriter::CMakeWriter()
{
    indent = 0;
    indentChars = "    ";
    lastLineType = Empty;
}



void CMakeWriter::startBlock(const std::string& line)
{
    this->line(line);
    //    this->line("{");
    indent++;
    lastLineType = StartBlock;
}


void CMakeWriter::endBlock(const std::string& line)
{
    indent--;
    this->line(std::string("}") + line);
    lastLineType = EndBlock;
}

void CMakeWriter::endBlockComment(std::string comment)
{
    indent--;
    this->line(std::string("# ") + comment);
    lastLineType = EndBlock;
}

void CMakeWriter::line()
{
    lineInternal("", indent);
}

void CMakeWriter::line(const std::string& line)
{
    lineInternal(line, indent);
}

void CMakeWriter::line(const std::string& line, int indentDelta)
{
    lineInternal(line, indent + indentDelta);
}

std::string CMakeWriter::getString()
{
    return ss.str();
}

CMakeWriter::LineType CMakeWriter::getLastLineType()
{
    return lastLineType;
}

void CMakeWriter::lineInternal(const std::string& line, int indent)
{
    for (int i = 0; i < indent; i++)
    {
        ss << indentChars;
    }

    ss << line << std::endl;
    lastLineType = line.empty() ? Empty : Normal;
}

/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <memory>
#include <vector>

#include "CppWriter.h"

namespace armarx
{
    class CppEnumField;
    using CppEnumFieldPtr = std::shared_ptr<CppEnumField>;

    class CppEnumField
    {
    public:
        CppEnumField(const std::string& name);
        CppEnumField(const std::string& name, int value);

        std::string toString() const;

    private:
        std::string name;
        int value = -1;
        bool hasValue = false;
    };

    class CppEnum;
    using CppEnumPtr = std::shared_ptr<CppEnum>;

    class CppEnum
    {
    public:
        CppEnum(const std::string& name, const std::string& doc = "");
        CppEnum(const std::string& name, const std::vector<CppEnumFieldPtr>& fields, const std::string& doc = "");

        void addField(const CppEnumFieldPtr&);

        void writeCpp(CppWriterPtr writer);

    private:
        std::string name;
        std::vector<CppEnumFieldPtr> fields;
        std::string doc;
    };
}

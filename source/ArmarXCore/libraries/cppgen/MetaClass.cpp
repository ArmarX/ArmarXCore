/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "MetaClass.h"
#include <boost/format.hpp>
#include <SimoxUtility/algorithm/string/string_tools.h>
#include <set>


using namespace armarx;

MetaClass::MetaClass(std::string name)
{
    this->name = name;
}

void MetaClass::addClassDoc(const std::string& doc)
{
    docString = doc;
}

std::string MetaClass::getName() const
{
    return name;
}

void MetaClass::convertToTextWithMaxWidth(unsigned int maxTextWidth, const std::vector<std::string>& docLines, std::vector<std::string>& linesOut)
{
    for (std::string line : docLines)
    {
        while (line.size() > maxTextWidth)
        {
            auto pos = line.rfind(" ", maxTextWidth);
            linesOut.push_back(line.substr(0, pos));
            line.erase(0, pos);
        }

        linesOut.push_back(line);
    }
}

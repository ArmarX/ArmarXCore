/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "MetaWriter.h"

using namespace armarx;

MetaWriter::MetaWriter()
{
    indent = 0;
    indentChars = "    ";
    lastLineType = Empty;
}

void MetaWriter::startBlock()
{
    this->line("{");
    indent++;
    lastLineType = StartBlock;
}

void MetaWriter::startBlock(const std::string& line)
{
    this->line(line);
    this->line("{");
    indent++;
    lastLineType = StartBlock;
}

void MetaWriter::endBlock()
{
    indent--;
    this->line("}");
    lastLineType = EndBlock;
}

void MetaWriter::endBlock(const std::string& line)
{
    indent--;
    this->line(std::string("}") + line);
    lastLineType = EndBlock;
}

void MetaWriter::endBlockComment(std::string comment)
{
    indent--;
    this->line(std::string("} // ") + comment);
    lastLineType = EndBlock;
}

void MetaWriter::line()
{
    lineInternal("", indent);
}

void MetaWriter::line(const std::string& line)
{
    std::vector<std::string> docLines = simox::alg::split(line, "\n");

    for (auto& l : docLines)
    {
        lineInternal(l, indent);
    }
}

void MetaWriter::line(const std::string& line, int indentDelta)
{
    std::vector<std::string> docLines = simox::alg::split(line, "\n");

    for (auto& l : docLines)
    {
        lineInternal(l, indent + indentDelta);
    }
}

std::string MetaWriter::getString()
{
    return ss.str();
}

MetaWriter::LineType MetaWriter::getLastLineType()
{
    return lastLineType;
}

void MetaWriter::lineInternal(const std::string& line, int indent)
{
    for (int i = 0; i < indent; i++)
    {
        ss << indentChars;
    }

    ss << line << std::endl;
    lastLineType = line.empty() ? Empty : Normal;
}

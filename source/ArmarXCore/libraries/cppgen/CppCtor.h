/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "CppBlock.h"

namespace armarx
{
    class CppCtor;
    using CppCtorPtr = std::shared_ptr<CppCtor>;

    class CppCtor
    {
    public:
        CppCtor(const std::string& header);
        CppCtor(const std::string& header, const CppBlockPtr& b);

        void addInitListEntries(const std::vector<std::pair<std::string, std::string>>&);
        void addInitListEntry(const std::string& target, const std::string& expr);
        void addLine(const std::string& line);
        void addLine(const boost::basic_format<char>& line);

        void setBlock(const CppBlockPtr&);
        CppBlockPtr getBlock() const;

        void writeCpp(CppWriterPtr writer);

    private:
        std::string header;
        std::vector<std::string> initList;
        CppBlockPtr block;
    };
}

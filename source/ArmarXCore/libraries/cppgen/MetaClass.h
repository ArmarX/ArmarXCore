/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <memory>
#include <vector>
#include "MetaWriter.h"

namespace armarx
{
    class MetaClass;
    using MetaClassPtr = std::shared_ptr<MetaClass>;

    class MetaClass
    {
    public:
        class EmptyLineHelper;
        using EmptyLineHelperPtr = std::shared_ptr<EmptyLineHelper>;

        class EmptyLineHelper
        {
        public:
            EmptyLineHelper(MetaWriterPtr writer)
                : writer(writer) {}
            void addEmptyLine()
            {
                switch (writer->getLastLineType())
                {
                    case MetaWriter::Normal:
                    case MetaWriter::EndBlock:
                        writer->line();
                        break;

                    case MetaWriter::Empty:
                    case MetaWriter::StartBlock:
                        // no empty line needed
                        break;
                }
            }

            void line(std::string line, int indentDelta = 0)
            {
                addEmptyLine();
                writer->line(line, indentDelta);
            }

        private:
            MetaWriterPtr writer;
        };

        MetaClass(std::string name);

        virtual void write(MetaWriterPtr writer) = 0;
        // static void Write(vec, w) = 0;

        void addClassDoc(const std::string& doc);

        std::string getName() const;
        void convertToTextWithMaxWidth(unsigned int maxTextWidth, const std::vector<std::string>& docLines, std::vector<std::string>& linesOut);

    protected:
        std::string name;
        std::string docString;
    };
}

/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "CppCtor.h"

#include <SimoxUtility/algorithm/string/string_tools.h>
#include <boost/format.hpp>

namespace armarx
{
    CppCtor::CppCtor(const std::string& header)
    {
        this->header = header;
        this->block.reset(new CppBlock());
    }

    CppCtor::CppCtor(const std::string& header, const CppBlockPtr& b)
    {
        this->header = header;
        this->block = b;
    }

    void CppCtor::addInitListEntries(const std::vector<std::pair<std::string, std::string>>& list)
    {
        for (const auto& [target, expr] : list)
        {
            addInitListEntry(target, expr);
        }
    }

    void CppCtor::addInitListEntry(const std::string& target, const std::string& expr)
    {
        this->initList.push_back(boost::str(boost::format("%s(%s)") % target % expr));
    }

    void CppCtor::addLine(const std::string& line)
    {
        block->addLine(line);
    }

    void CppCtor::addLine(const boost::basic_format<char>& line)
    {
        block->addLine(line);
    }

    void CppCtor::setBlock(const CppBlockPtr& b)
    {
        block = b;
    }

    CppBlockPtr CppCtor::getBlock() const
    {
        return block;
    }

    void CppCtor::writeCpp(CppWriterPtr writer)
    {
        writer->line(header);

        for (size_t i = 0; i < initList.size(); i++)
        {
            writer->line(boost::str(boost::format("%s%s%s") % (i == 0 ? ": " : "  ") % initList.at(i) % (i < initList.size() - 1 ? "," : "")), 1);
        }

        if (block != nullptr)
        {
            block->writeCpp(writer);
        }
    }
}

/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "CppField.h"

#include <SimoxUtility/algorithm/string/string_tools.h>
#include <boost/format.hpp>

namespace armarx
{

    CppField::CppField(const std::string& type, const std::string& name, const std::string& default_value, const std::string& doc):
        type(type),
        name(name),
        default_value(default_value),
        doc(doc)
    {
    }

    std::string CppField::toString() const
    {
        std::stringstream ss;
        if (doc != "")
        {
            auto lines = simox::alg::split(doc, "\n", true, false);
            for (const auto& line : lines)
            {
                ss << "/// " << line;
            }
            ss << "\n";
        }
        if (default_value != "")
        {
            ss << type + " " + name + " = " + default_value + ";";
        }
        else
        {
            ss << type + " " + name + ";";
        }
        return ss.str();
    }
}

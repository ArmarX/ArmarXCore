/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "CppClass.h"

#include <SimoxUtility/algorithm/string/string_tools.h>

#include <boost/format.hpp>

#include <set>


using namespace armarx;

CppClass::CppClass(std::vector<std::string> namespaces, std::string name, std::string templates)
    : MetaClass(name)
{
    this->namespaces = namespaces;
    this->templates = templates;
}

void CppClass::write(MetaWriterPtr writer)
{
    CppWriterPtr w = std::dynamic_pointer_cast<CppWriter>(writer);
    writeCpp(w);
}

void CppClass::writeCpp(CppWriterPtr writer)
{
    if (pragmaOnce)
    {
        writer->line(boost::str(boost::format("#pragma once")));
        writer->line();
    }
    if (!includeGuard.empty() && !pragmaOnce)
    {
        writer->line(boost::str(boost::format("#ifndef %s") % includeGuard));
        writer->line(boost::str(boost::format("#define %s") % includeGuard));
        writer->line();
    }

    for (std::vector<std::string>::iterator it = includes.begin(); it != includes.end(); it++)
    {
        writer->line(boost::str(boost::format("#include %s") % *it));
    }

    if (includes.size() > 0)
    {
        writer->line("");
    }

    for (std::vector<std::string>::iterator it = namespaces.begin(); it != namespaces.end(); it++)
    {
        writer->startBlock(std::string("namespace ") + *it);
    }

    addDoc(writer);
    if (!templates.empty())
    {
        writer->line(templates);
    }

    writer->line(std::string("class ") + name);

    for (size_t i = 0; i < inherit.size(); i++)
    {
        writer->line(boost::str(boost::format("%s%s%s") % (i == 0 ? ": " : "  ") % inherit.at(i) % (i < inherit.size() - 1 ? "," : "")), 1);
    }

    writer->startBlock();

    EmptyLineHelperPtr elh(new EmptyLineHelper(writer));

    if (innerEnums.size() > 0)
    {
        elh->line("public:", -1);
    }

    for (std::vector<CppEnumPtr>::iterator it = innerEnums.begin(); it != innerEnums.end(); it++)
    {
        (*it)->writeCpp(writer);
        elh->addEmptyLine();
    }

    if (innerClasses.size() > 0)
    {
        elh->line("protected:", -1);
    }

    for (std::vector<CppClassPtr>::iterator it = innerClasses.begin(); it != innerClasses.end(); it++)
    {
        (*it)->writeCpp(writer);
        elh->addEmptyLine();
    }

    if (privateFields.size() > 0)
    {
        elh->line("private:", -1);
    }

    for (std::vector<std::string>::iterator it = privateFields.begin(); it != privateFields.end(); it++)
    {
        writer->line(*it);
    }

    if (protectedFields.size() > 0)
    {
        elh->line("protected:", -1);
    }

    for (std::vector<std::string>::iterator it = protectedFields.begin(); it != protectedFields.end(); it++)
    {
        writer->line(*it);
    }

    if (publicFields.size() > 0)
    {
        elh->line("public:", -1);
    }

    for (std::vector<std::string>::iterator it = publicFields.begin(); it != publicFields.end(); it++)
    {
        writer->line(*it);
    }

    if (ctors.size() > 0)
    {
        elh->line("public:", -1);
    }

    for (std::vector<CppCtorPtr>::iterator it = ctors.begin(); it != ctors.end(); it++)
    {
        (*it)->writeCpp(writer);
    }

    elh->line("public:", -1);
    elh->line("virtual ~" + name + "() = default;");

    if (methods.size() > 0)
    {
        elh->line("public:", -1);
    }

    for (std::vector<CppMethodPtr>::iterator it = methods.begin(); it != methods.end(); it++)
    {
        (*it)->writeCpp(writer);
    }

    writer->endBlock("; // class " + name); // end of class

    for (std::vector<std::string>::reverse_iterator it = namespaces.rbegin(); it != namespaces.rend(); it++)
    {
        writer->endBlockComment(std::string("namespace ") + *it);
    }

    if (!includeGuard.empty())
    {
        writer->line();
        writer->line(boost::str(boost::format("#endif // %s") % includeGuard));
    }

}

void CppClass::Write(std::vector<MetaClassPtr> classes, MetaWriterPtr writer)
{
    std::vector<CppClassPtr> c;
    for (const auto& meta : classes)
    {
        CppClassPtr cpp = std::dynamic_pointer_cast<CppClass>(meta);
        c.push_back(cpp);
    }

    CppWriterPtr w = std::dynamic_pointer_cast<CppWriter>(writer);
    CppClass::WriteCpp(c, w);
}

void CppClass::WriteCpp(std::vector<CppClassPtr> classes, CppWriterPtr writer)
{
    bool allSamePragmaOnce = true;
    bool pragmaOnceValue = false;
    if (classes.size() > 0)
    {
        pragmaOnceValue = classes[0]->pragmaOnce;
        for (unsigned int i = 1; i < classes.size(); ++i)
        {
            CppClassPtr cppClass = classes[i];
            if (cppClass->pragmaOnce != pragmaOnceValue)
            {
                allSamePragmaOnce = false;
                // throw exception?
                break;
            }
        }
    }
    bool hasPragmaOnce = allSamePragmaOnce && pragmaOnceValue;

    if (hasPragmaOnce)
    {
        writer->line(boost::str(boost::format("#pragma once")));
        writer->line();
    }

    bool foundIncludeGuard = false;
    if (!hasPragmaOnce) // we ignore include guards if we apply pragma once
    {
        for (CppClassPtr& cppClass : classes)
        {
            if (!cppClass->includeGuard.empty())
            {
                foundIncludeGuard = true;
                break;
            }
        }
    }
    if (foundIncludeGuard)
    {
        for (CppClassPtr& cppClass : classes)
        {
            if (!cppClass->includeGuard.empty())
            {
                writer->line(boost::str(boost::format("#ifndef %s") % cppClass->includeGuard));
                writer->line(boost::str(boost::format("#define %s") % cppClass->includeGuard));
                writer->line();
                break; // only use the first found includeGuard.
            }
        }
    }

    bool foundIncludes = false;
    std::set<std::string> alreadyIncluded;
    for (CppClassPtr& cppClass : classes)
    {
        for (std::vector<std::string>::iterator it = cppClass->includes.begin(); it != cppClass->includes.end(); it++)
        {
            if (alreadyIncluded.find(*it) == alreadyIncluded.end())
            {
                writer->line(boost::str(boost::format("#include %s") % *it));
                alreadyIncluded.insert(*it);
                foundIncludes = true;
            }
        }
    }

    if (foundIncludes)
    {
        writer->line("");
    }
    for (CppClassPtr& cppClass : classes)
    {
        for (std::vector<std::string>::iterator it = cppClass->namespaces.begin(); it != cppClass->namespaces.end(); it++)
        {
            writer->startBlock(std::string("namespace ") + *it);
        }

        if (!cppClass->templates.empty())
        {
            writer->line(cppClass->templates);
        }

        cppClass->addDoc(writer);
        writer->line(std::string("class ") + cppClass->name);

        for (size_t i = 0; i < cppClass->inherit.size(); i++)
        {
            writer->line(boost::str(boost::format("%s%s%s") % (i == 0 ? ": " : "  ") % cppClass->inherit.at(i) % (i < cppClass->inherit.size() - 1 ? "," : "")), 1);
        }

        writer->startBlock();

        EmptyLineHelperPtr elh(new EmptyLineHelper(writer));

        if (cppClass->innerEnums.size() > 0)
        {
            elh->line("public:", -1);
        }

        for (std::vector<CppEnumPtr>::iterator it = cppClass->innerEnums.begin(); it != cppClass->innerEnums.end(); it++)
        {
            (*it)->writeCpp(writer);
            elh->addEmptyLine();
        }

        if (cppClass->innerClasses.size() > 0)
        {
            elh->line("protected:", -1);
        }

        for (std::vector<CppClassPtr>::iterator it = cppClass->innerClasses.begin(); it != cppClass->innerClasses.end(); it++)
        {
            (*it)->writeCpp(writer);
            elh->addEmptyLine();
        }

        if (cppClass->privateFields.size() > 0)
        {
            elh->line("private:", -1);
        }

        for (std::vector<std::string>::iterator it = cppClass->privateFields.begin(); it != cppClass->privateFields.end(); it++)
        {
            writer->line(*it);
        }

        if (cppClass->protectedFields.size() > 0)
        {
            elh->line("protected:", -1);
        }

        for (std::vector<std::string>::iterator it = cppClass->protectedFields.begin(); it != cppClass->protectedFields.end(); it++)
        {
            writer->line(*it);
        }

        if (cppClass->publicFields.size() > 0)
        {
            elh->line("public:", -1);
        }

        for (std::vector<std::string>::iterator it = cppClass->publicFields.begin(); it != cppClass->publicFields.end(); it++)
        {
            writer->line(*it);
        }

        if (cppClass->ctors.size() > 0)
        {
            elh->line("public:", -1);
        }

        for (std::vector<CppCtorPtr>::iterator it = cppClass->ctors.begin(); it != cppClass->ctors.end(); it++)
        {
            (*it)->writeCpp(writer);
        }

        if (cppClass->methods.size() > 0)
        {
            elh->line("public:", -1);
        }

        for (std::vector<CppMethodPtr>::iterator it = cppClass->methods.begin(); it != cppClass->methods.end(); it++)
        {
            (*it)->writeCpp(writer);
            elh->addEmptyLine();
        }

        writer->endBlock("; // class " + cppClass->name); // end of class

        for (std::vector<std::string>::reverse_iterator it = cppClass->namespaces.rbegin(); it != cppClass->namespaces.rend(); it++)
        {
            writer->endBlockComment(std::string("namespace ") + *it);
        }

        writer->line();
    }

    if (foundIncludeGuard)
    {
        writer->line();
        writer->line("#endif");
    }

}

void CppClass::addMethod(CppMethodPtr method)
{
    methods.push_back(method);
}

CppMethodPtr CppClass::addMethod(const std::string& header, const std::string& doc)
{
    CppMethodPtr method(new CppMethod(header, doc));
    methods.push_back(method);
    return method;
}

CppMethodPtr CppClass::addMethod(const boost::basic_format<char>& header, const std::string& doc)
{
    return addMethod(boost::str(header), doc);
}

void CppClass::addCtor(const CppCtorPtr& ctor)
{
    ctors.push_back(ctor);
}

CppCtorPtr CppClass::addCtor(const std::string arguments)
{
    CppCtorPtr ctor(new CppCtor(boost::str(boost::format("%s(%s)") % name % arguments)));
    ctors.push_back(ctor);
    return ctor;
}

CppCtorPtr CppClass::addCtor(const std::string arguments, const CppBlockPtr& b)
{
    CppCtorPtr ctor(new CppCtor(boost::str(boost::format("%s(%s)") % name % arguments), b));
    ctors.push_back(ctor);
    return ctor;
}

void CppClass::addPrivateField(const CppFieldPtr& field)
{
    privateFields.push_back(field->toString());
}

void CppClass::addPrivateField(const std::string& field)
{
    privateFields.push_back(field);
}

void CppClass::addPrivateField(const boost::basic_format<char>& field)
{
    privateFields.push_back(boost::str(field));
}

void CppClass::addProtectedField(const std::string& field)
{
    publicFields.push_back(field);
}

void CppClass::addProtectedField(const CppFieldPtr& field)
{
    protectedFields.push_back(field->toString());
}

void CppClass::addProtectedField(const boost::basic_format<char>& field)
{
    protectedFields.push_back(boost::str(field));
}

void CppClass::addPublicField(const std::string& field)
{
    publicFields.push_back(field);
}

void CppClass::addPublicField(const CppFieldPtr& field)
{
    publicFields.push_back(field->toString());
}

void CppClass::addPublicField(const boost::basic_format<char>& field)
{
    publicFields.push_back(boost::str(field));
}

CppClassPtr CppClass::addInnerClass(std::string name)
{
    CppClassPtr innerClass(new CppClass(std::vector<std::string>(), name));
    innerClasses.push_back(innerClass);
    return innerClass;
}

void CppClass::addInnerClass(CppClassPtr inner)
{
    innerClasses.push_back(inner);
}

void CppClass::addInnerEnum(CppEnumPtr inner)
{
    innerEnums.push_back(inner);
}

CppEnumPtr CppClass::addInnerEnum(std::string name)
{
    CppEnumPtr innerEnum(new CppEnum(name));
    innerEnums.push_back(innerEnum);
    return innerEnum;
}

void CppClass::addInherit(const std::string& inherit)
{
    this->inherit.push_back(inherit);
}

void CppClass::addInherit(const boost::basic_format<char>& inherit)
{
    this->inherit.push_back(boost::str(inherit));
}

void CppClass::addInclude(const std::string include)
{
    includes.push_back(include);
}

void CppClass::addInclude(const boost::basic_format<char>& include)
{
    includes.push_back(boost::str(include));
}

void CppClass::addClassDoc(const std::string& doc)
{
    docString = doc;
}

bool CppClass::hasInclude(const std::string include)
{
    return (std::find(includes.begin(), includes.end(), include) != includes.end());

}

std::string CppClass::getName() const
{
    return name;
}

void CppClass::setPragmaOnceIncludeGuard(bool v)
{
    pragmaOnce = v;
}

void CppClass::setIncludeGuard(const std::string& includeGuard)
{
    this->includeGuard = includeGuard;
}

void CppClass::setTemplates(const std::string& s)
{
    templates = s;
}

void CppClass::addDoc(CppWriterPtr writer)
{
    const std::string preSpace = " ";

    if (!docString.empty())
    {
        writer->line("/**");
        std::vector<std::string> docLines = simox::alg::split(docString, "\n");
        std::vector<std::string> blockLines;
        convertToTextWithMaxWidth(80, docLines, blockLines);

        for (auto& line : blockLines)
        {
            writer->line(preSpace + line);
        }

        writer->line(" */");
    }
}

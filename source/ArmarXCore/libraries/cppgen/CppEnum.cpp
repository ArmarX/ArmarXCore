/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "CppEnum.h"

#include <SimoxUtility/algorithm/string/string_tools.h>
#include <boost/format.hpp>

namespace armarx
{

    CppEnumField::CppEnumField(const std::string& name) :
        name(name), value(-1), hasValue(false)
    {

    }

    CppEnumField::CppEnumField(const std::string& name, int value) :
        name(name), value(value), hasValue(true)
    {

    }

    std::string CppEnumField::toString() const
    {
        if (hasValue)
        {
            return name + " = " + std::to_string(value);
        }
        return name;
    }

    CppEnum::CppEnum(const std::string& name, const std::string& doc) :
        name(name), doc(doc)
    {

    }

    CppEnum::CppEnum(const std::string& name, const std::vector<CppEnumFieldPtr>& fields, const std::string& doc) :
        name(name), fields(fields), doc(doc)
    {

    }

    void CppEnum::addField(const CppEnumFieldPtr& f)
    {
        fields.push_back(f);
    }

    void CppEnum::writeCpp(CppWriterPtr writer)
    {
        if (!doc.empty())
        {
            std::string delimiters = "\n";
            std::vector<std::string> doclines = simox::alg::split(doc, delimiters);
            writer->line("/**");

            for (auto& line : doclines)
            {
                writer->line(" * " + line);
            }

            writer->line(" */");
        }

        writer->line("enum class " + name);
        writer->startBlock();
        for (unsigned int i = 0; i < fields.size(); ++i)
        {
            auto& f = fields[i];
            writer->line("\t" + f->toString() + (i == fields.size() ? "" : ","));
        }
        writer->endBlock("; // enum class " + name); // end of enum
    }
}

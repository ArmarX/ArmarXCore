/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::StatechartExamples::TimeoutExample
* @author     Mirko Waechter (mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXCore/statechart/Statechart.h>

namespace armarx
{
    class ARMARXCOMPONENT_IMPORT_EXPORT TimeoutExample :
        virtual public StatechartContext
    {
    public:

        StatePtr statechart;

        // inherited from Component
        std::string getDefaultName() const override
        {
            return "TimeoutExample";
        };
        void onInitStatechart() override;
        void onConnectStatechart() override;
        void run();
    };
    struct stateInstallTimeout;
    DEFINEEVENT(Next)
    DEFINEEVENT(TimerExpired)
    struct TimeoutExampleStatechart : StateTemplate<TimeoutExampleStatechart>
    {
        void defineState() override
        {
            //            setStateName("StatechartTimeoutexample");
        }
        void defineSubstates() override
        {
            //add substates
            setInitState(addState<stateInstallTimeout>("InstallTimeout"));

            StateBasePtr finalSuccess = addState<SuccessState>("Success");
            StateBasePtr finalFailure = addState<FailureState>("Failure");

            // add transition
            addTransition<Next>(getInitState(),
                                getInitState());
            addTransition<TimerExpired>(getInitState(),
                                        finalFailure);
            addTransition<Success>(getInitState(),
                                   finalSuccess);

        }
    };


    struct DelayState;
    struct stateInstallTimeout : StateTemplate<stateInstallTimeout>
    {


        void onEnter() override
        {

            setTimeoutEvent(5000, createEvent<TimerExpired>());
        }
        void onExit() override
        {

        }
    };


}


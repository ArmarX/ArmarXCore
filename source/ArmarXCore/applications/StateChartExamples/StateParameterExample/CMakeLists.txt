armarx_component_set_name(StateParameterExample)
set(COMPONENT_LIBS ArmarXCoreStatechart)
set(SOURCES main.cpp
    StateParameterExample.h
    StateParameterExample.cpp)
armarx_add_component_executable("${SOURCES}")

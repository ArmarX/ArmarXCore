/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::StatechartExamples::StateParameterExample
* @author     Mirko Waechter (mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/statechart/Statechart.h>

namespace armarx
{
    class ARMARXCOMPONENT_IMPORT_EXPORT StateParameterExample :
        virtual public StatechartContext
    {
    public:
        // inherited from Component
        std::string getDefaultName() const override
        {
            return "StateParameterExample";
        };
        void onInitStatechart() override;
        void onConnectStatechart() override;
        void run();
    };

    // Define Events before the first state they are used in
    DEFINEEVENT(EvInit)  // this macro declares a new event-class derived vom Event, to have a compiletime check for typos in events
    DEFINEEVENT(EvNext)

    struct StateResult;
    struct Statechart_StateParameterExample : StateTemplate<Statechart_StateParameterExample>
    {

        void defineState() override;

        void defineSubstates() override;
        void defineParameters() override;

        void onEnter() override;
    };

    struct StateRun : StateTemplate<StateRun>
    {
        ConditionIdentifier condId;
        void defineParameters() override;
        void onEnter() override;
        void onBreak() override;

        void onExit() override;
    };

    struct StateResult : StateTemplate<StateResult>
    {
        void defineParameters() override;
        void onEnter() override;

    };

}



armarx_component_set_name(StatechartPerformanceTest)

set(COMPONENT_LIBS  ArmarXCoreInterfaces ArmarXCore ArmarXCoreStatechart)

set(SOURCES main.cpp
    StatechartPerformanceTest.cpp
    StatechartPerformanceTest.h
)

armarx_add_component_executable("${SOURCES}")

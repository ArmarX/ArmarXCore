armarx_component_set_name("ExampleUnitApp")

set(COMPONENT_LIBS ArmarXCore ArmarXCoreObservers)
set(EXE_SOURCE main.cpp)

armarx_add_component_executable("${EXE_SOURCE}")

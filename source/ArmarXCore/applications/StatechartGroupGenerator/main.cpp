/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Core::application::StatechartGroupDocGenerator
 * @author     Mirko Waechter
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <SimoxUtility/algorithm/string.h>

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/statechart/xmlstates/StateGroupGenerator.h>

int main(int argc, char* argv[])
{
    auto parseDeps = [](std::string const & dependencies)
    {
        auto depSplit = armarx::Split(dependencies, ";", true, true);
        std::map<std::string, std::string> depsMap;
        for (auto& pairStr : depSplit)
        {
            auto pair = armarx::Split(pairStr, ":", true, true);

            if(pair.size() != 2)
            {
                ARMARX_WARNING << "Dependency '" << pair.at(0) << "' without path!";
                continue;
            }
            depsMap[pair[0]] = pair[1];
        }
        return depsMap;
    };

    auto getPackageNameFromPackagePath = [](std::string const & packagePath /* path to <PackageName>/build */)
    {
        namespace fs = std::filesystem;

        std::optional<std::string> ret;

        auto source = fs::path(simox::alg::remove_suffix(packagePath, "/")).parent_path() / "source";
        for (const auto& f : fs::directory_iterator(source))
        {
            if (fs::is_directory(f))
            {
                ret = std::make_optional(std::string(f.path().filename()));
                break;
            }
        }
        return ret;
    };

    if (argc == 5 && std::string(argv[1]) == "context")
    {
        ARMARX_DEBUG << "Generating statechart context header file.";

        std::string statechartGroupXmlFilePath(argv[2]);
        std::string packagePath(argv[3]);
        const std::string packageIncludePath(argv[4]);

        if(armarx::StatechartGroupGenerator::generateStatechartContextFile(statechartGroupXmlFilePath, packagePath, packageIncludePath))
        {
            return EXIT_SUCCESS;
        }

        return EXIT_FAILURE;
    }
    else if (argc == 4 && std::string(argv[1]) == "context") // legacy
    {
        ARMARX_DEBUG << "Generating statechart context header file (legacy mode).";

        std::string statechartGroupXmlFilePath(argv[2]);
        std::string packagePath(argv[3]);
        
        if(armarx::StatechartGroupGenerator::generateStatechartContextFile(statechartGroupXmlFilePath, packagePath, std::nullopt))
        {
            return EXIT_SUCCESS;
        }

        ARMARX_WARNING << "Failure";
        return EXIT_FAILURE;
    }
    else if (argc == 8 && std::string(argv[1]) == "package-cmake")
    {
        ARMARX_DEBUG << "Generating statechart CMake package source file.";

        std::string packageName(argv[2]);
        std::string statechartsDir(argv[3]);
        std::string buildDir(argv[4]);
        std::string dataDir(argv[5]);
        std::string dependencies(argv[6]);
        auto depsMap = parseDeps(dependencies);
        const std::string packageIncludePath(argv[7]);
        
        if(armarx::StatechartGroupGenerator::generateStatechartGroupCMakeSourceListFiles(packageName, statechartsDir, buildDir, false, dataDir, depsMap, packageIncludePath, true))
        {
            return EXIT_SUCCESS;
        }

        return EXIT_FAILURE;
    }
    else if (argc == 7 && std::string(argv[1]) == "package-cmake") // legacy
    {
        ARMARX_DEBUG << "Generating statechart CMake package source file (legacy mode).";

        std::string packageName(argv[2]);
        std::string statechartsDir(argv[3]);
        std::string buildDir(argv[4]);
        std::string dataDir(argv[5]);
        std::string dependencies(argv[6]);
        auto depsMap = parseDeps(dependencies);
        const std::string& packageIncludePath = packageName;

        if(armarx::StatechartGroupGenerator::generateStatechartGroupCMakeSourceListFiles(packageName, statechartsDir, buildDir, false, dataDir, depsMap, packageIncludePath))
        {
            return EXIT_SUCCESS;
        }

        return EXIT_FAILURE;

    }
    else if (argc == 8 && std::string(argv[1]) == "cmake")
    {
        ARMARX_DEBUG << "Generating statechart CMake source file.";

        std::string packageName(argv[2]);
        std::string statechartGroupXmlFilePath(argv[3]);
        std::string buildDir(argv[4]);
        std::string dataDir(argv[5]);
        std::string dependencies(argv[6]);
        auto depsMap = parseDeps(dependencies);
        const std::string packageIncludePath(argv[7]);
        
        if(armarx::StatechartGroupGenerator::generateStatechartGroupCMakeSourceListFile(packageName, statechartGroupXmlFilePath, buildDir, false, dataDir, depsMap, packageIncludePath, true))
        {
            return EXIT_SUCCESS;
        }

        return EXIT_FAILURE;
    
    }
    else if (argc == 7 && std::string(argv[1]) == "cmake") // legacy
    {
        ARMARX_DEBUG << "Generating statechart CMake source file (legacy mode).";

        std::string packageName(argv[2]);
        std::string statechartGroupXmlFilePath(argv[3]);
        std::string buildDir(argv[4]);
        std::string dataDir(argv[5]);
        std::string dependencies(argv[6]);
        auto depsMap = parseDeps(dependencies);
        const std::string& packageIncludePath = packageName;

        if(armarx::StatechartGroupGenerator::generateStatechartGroupCMakeSourceListFile(packageName, statechartGroupXmlFilePath, buildDir, false, dataDir, depsMap, packageIncludePath))
        {
            return EXIT_SUCCESS;
        }

        return EXIT_FAILURE;
    }
    else if (argc == 5 && std::string(argv[1]).find(".scgxml") != std::string::npos)
    {
        ARMARX_DEBUG << "Generating state header file.";
        
        std::string statechartGroupXmlFilePath(argv[1]);
        std::string stateFile(argv[2]);
        std::string packagePath(argv[3]);
        const std::string packageIncludePath(argv[4]);

        ARMARX_DEBUG << "packageIncludePath " << packageIncludePath;
        if(armarx::StatechartGroupGenerator::generateStateFile(statechartGroupXmlFilePath, stateFile, packagePath, packageIncludePath))
        {
            return EXIT_SUCCESS;
        }

        return EXIT_FAILURE;
    }
    else if (argc == 4 && std::string(argv[1]).find(".scgxml") != std::string::npos) // legacy
    {
        ARMARX_TRACE;

        ARMARX_DEBUG << "Generating state header file (legacy mode).";
        
        std::string statechartGroupXmlFilePath(argv[1]);
        std::string stateFile(argv[2]);
        std::string packagePath(argv[3]);

        packagePath = simox::alg::remove_suffix(packagePath, "/");

        const std::optional<std::string> pName = getPackageNameFromPackagePath(packagePath);
        if (!pName.has_value())
        {
            ARMARX_DEBUG << "Could not extract the package name of " << packagePath;
        }

        const std::string packageName = pName.value(); //getPackageNameFromPackagePath(packagePath);
        const std::string& packageIncludePath = packageName;

        if(armarx::StatechartGroupGenerator::generateStateFile(statechartGroupXmlFilePath, stateFile, packagePath, packageIncludePath))
        {
            return EXIT_SUCCESS;
        }

        return EXIT_FAILURE;
    }
    else
    {
        throw armarx::LocalException("Incorrect parameters. Expected: (<Statechartgroupfile.scgxml> <statefile.xml> <packagePath> "
                                     "| context <Statechartgroupfile.scgxml> <packagePath> "
                                     "| cmake <PackageName> <Statechartgroupfile.scgxml> <builddir> <datadir> <dependenciesWithPaths>"
                                     "| package-cmake <PackageName> <StatechartDir> <builddir> <datadir> <dependenciesWithPaths>) .");
    }

    return EXIT_FAILURE;
}

#pragma once

#include "../trace.h"

#include "common.h"

//fwd
namespace armarx::meta::cfg
{
    template<class CL, class MT, MT CL::* ptr>
    struct element_default;
}

//possible details
namespace armarx::meta::cfg
{
    template<class CL, class MT, MT CL::* ptr>
    using element_widget = undefined_t;

    template<class T>
    using config_layout = undefined_t;

    template<class CL, class = void>
    struct struct_default : std::false_type
    {
        static const CL& get()
        {
            static const CL cl;
            return cl;
        }
        static void set(CL& cl)
        {
            cl = get();
        }
    };

    template<class CL>
    struct struct_default <
        CL,
        std::enable_if_t<simox::meta::has_hana_accessor_v<CL> >
        > : std::false_type
    {
    private:
        template<class VarName, class MT, MT CL::* ptr>
        static void DoSetElement(
            const boost::hana::pair<VarName, boost::hana::struct_detail::member_ptr<MT CL::*, ptr>>&,
            CL& val
        )
        {
            if constexpr(element_default<CL, MT, ptr>::value)
            {
                element_default<CL, MT, ptr>::set(val);
            }
        }
    public:
        static void set(CL& cl)
        {
            static constexpr auto accessors = boost::hana::accessors<CL>();
            boost::hana::for_each(accessors, [&](auto & e)
            {
                ARMARX_TRACE_LITE;
                DoSetElement(e, cl);
            });
        }
        static const CL& get()
        {
            static const CL cl;
            set(cl);
            return cl;
        }
    };
}

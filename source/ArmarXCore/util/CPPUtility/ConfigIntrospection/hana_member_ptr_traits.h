#pragma once

#include "common.h"

namespace armarx::meta::cfg
{
    template<class T>
    struct hana_member_ptr_traits : std::false_type {};

    template<class T>
    struct hana_member_ptr_traits<const T>
        : hana_member_ptr_traits<T> {};

    template<class T>
    struct hana_member_ptr_traits<const T&>
        : hana_member_ptr_traits<T> {};

    template<class T>
    struct hana_member_ptr_traits<T&>
        : hana_member_ptr_traits<T> {};

    template<class T1, class T2>
    struct hana_member_ptr_traits<boost::hana::tuple<T1, T2>> :
                hana_member_ptr_traits<T2>
    {};

    template <class CL, class MT, MT CL::* ptr>
    struct hana_member_ptr_traits<hana::struct_detail::member_ptr<MT CL::*, ptr>>
                : std::true_type
    {
        using class_t = CL;
        using member_t = MT;
        using pointer_t = MT CL::*;
        static constexpr pointer_t pointer = ptr;

        template<template <class CL2, class MT2, MT2 CL2::*> class Templ>
        using pass_to = Templ<CL, MT, pointer>;
    };
}


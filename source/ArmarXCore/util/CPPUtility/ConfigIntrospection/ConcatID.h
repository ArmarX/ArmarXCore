#pragma once

#include <string>

namespace armarx::meta::cfg
{
    inline std::string ConcatID(const std::string& prefix, const std::string& suffix)
    {
        static const std::string delim{"_"};
        return prefix.empty() ? suffix : prefix + delim + suffix;
    }
}


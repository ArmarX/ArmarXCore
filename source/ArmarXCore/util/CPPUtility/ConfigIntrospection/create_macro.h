#pragma once

#include <array>
#include  <experimental/array>
#include <SimoxUtility/meta/eigen.h>
#include <boost/hana/adapt_struct.hpp>

#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/variadic/to_seq.hpp>

#include "element_details.h"

namespace armarx::meta::detail
{
    template <typename T, typename = void>
    struct make_array
    {
        static constexpr auto value = [](auto ...t)
        {
            return std::experimental::make_array<T>(std::forward<decltype(t)>(t)...);
        };
    };
    template<typename T>
    struct make_array<T, std::enable_if_t<simox::meta::is_eigen_matrix_v<T>>>
    {
        static constexpr auto value = [](auto ...t)
        {
            return std::experimental::make_array<float>(std::forward<decltype(t)>(t)...);
        };
    };
}
// ////////////////////////////////////////////////////////////////////////// //
// ////////////////////////////////////////////////////////////////////////// //
//base
//remove the parentheses around a tuple
#define _detail_AX_MK_CFG_rem_paren(...) __VA_ARGS__

//get the first element of a tuple
#define _detail_AX_MK_CFG_t0(e0, ...) e0
//get the second element of a tuple
#define _detail_AX_MK_CFG_tail(e0, ...) __VA_ARGS__

#define _detail_AX_MK_CFG_applicator_i2(m, ...) m(__VA_ARGS__)
#define _detail_AX_MK_CFG_applicator_i1(...)                                    \
    _detail_AX_MK_CFG_applicator_i2(__VA_ARGS__)

// see _detail_AX_MK_CFG_appl_FOR_EACH
#define _detail_AX_MK_CFG_applicator(r, tup, elem)                              \
    _detail_AX_MK_CFG_applicator_i1(                                            \
            _detail_AX_MK_CFG_t0 tup,                                           \
            r,                                                                  \
            _detail_AX_MK_CFG_tail tup,                                         \
            _detail_AX_MK_CFG_rem_paren elem                                    \
                                   )

// call macro(stype, ELEMS_i) for each tuple of ELEMS in the parameterlist
// eg:
// _detail_AX_MK_CFG_appl_FOR_EACH(stype, macro, (0,1), (2,3))
// -> macro(r, stype,0,1) macro(r+1, stype,2,3)
#define _detail_AX_MK_CFG_appl_FOR_EACH(stype, elemmacro, ...)                  \
    BOOST_PP_SEQ_FOR_EACH(                                                      \
            _detail_AX_MK_CFG_applicator,                                       \
            (elemmacro, stype),                                                 \
            BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__)                               \
                         )

// ////////////////////////////////////////////////////////////////////////// //
// ////////////////////////////////////////////////////////////////////////// //
//define struct
#define _detail_AX_MK_CFG_member_def(R, sname, type, vname, ...) type vname;
#define _detail_AX_MK_CFG_struct_def(Namespace, Name, ...)                      \
    namespace Namespace                                                         \
    {                                                                           \
        struct Name                                                             \
        {                                                                       \
            BOOST_PP_EXPAND(                                                    \
                    _detail_AX_MK_CFG_appl_FOR_EACH(                            \
                            Namespace::Name,                                    \
                            _detail_AX_MK_CFG_member_def,                       \
                            __VA_ARGS__                                         \
                                                   ))                           \
            Name();                                                             \
            Name(Name&&) = default;                                             \
            Name(const Name&) = default;                                        \
            Name& operator=(Name&&) = default;                                  \
            Name& operator=(const Name&) = default;                             \
        };                                                                      \
    } static_assert(true, "force the usage of a trailing semicolon")

#define _detail_AX_MK_CFG_ctor_def(Namespace, Name, ...)                        \
    namespace Namespace                                                         \
    {                                                                           \
        inline Name::Name()                                                     \
        {                                                                       \
            armarx::meta::cfg::struct_default<Name>::set(*this);                \
        }                                                                       \
    } static_assert(true, "force the usage of a trailing semicolon")
// ////////////////////////////////////////////////////////////////////////// //
// ////////////////////////////////////////////////////////////////////////// //
//configure elements
#define ARMARX_CONFIG_STRUCT_CONFIGURE(Namespace, Name, ...)                    \
    namespace armarx::meta::cfg                                                 \
    {                                                                           \
        BOOST_PP_EXPAND(                                                        \
                _detail_AX_MK_CFG_appl_FOR_EACH(                                \
                        Namespace::Name,                                        \
                        _detail_AX_MK_CFG_cfg_elem,                             \
                        __VA_ARGS__                                             \
                                               ))                               \
    } static_assert(true, "force the usage of a trailing semicolon")
// ////////////////////////////////////////////////////////////////////////// //
//configure element
#define _detail_AX_MK_CFG_cfg_elem_app_i4(sname, type, vname, m, ...)           \
    m(sname, type, vname, __VA_ARGS__)

#define _detail_AX_MK_CFG_cfg_elem_app_i3(...)                                  \
    _detail_AX_MK_CFG_cfg_elem_app_i4(__VA_ARGS__)

#define _detail_AX_MK_CFG_cfg_elem_app_i2(sname, type, vname, ...)              \
    _detail_AX_MK_CFG_cfg_elem_app_i3(                                          \
            sname, type, vname,                                                 \
            _detail_AX_MK_CFG_cfg_elem_app_ ## __VA_ARGS__)

#define _detail_AX_MK_CFG_cfg_elem_app_i1(...)                                  \
    _detail_AX_MK_CFG_cfg_elem_app_i2(__VA_ARGS__)

// see _detail_AX_MK_CFG_appl_FOR_EACH
#define _detail_AX_MK_CFG_cfg_elem_app(r, tup, elem)                            \
    _detail_AX_MK_CFG_cfg_elem_app_i1(                                          \
            _detail_AX_MK_CFG_rem_paren tup,                                    \
            elem                                                                \
                                     )

// 2 lvl BOOST_PP_SEQ_FOR_EACH recursion
//see https://stackoverflow.com/a/35755736
//see http://boost.2283326.n4.nabble.com/preprocessor-nested-for-each-not-working-like-expected-td4650470.html
#define _detail_AX_MK_CFG_DEFER(x) x BOOST_PP_EMPTY()
#define _detail_AX_MK_CFG_SEQ_FOR_EACH_R_ID() BOOST_PP_SEQ_FOR_EACH_R

#define _detail_AX_MK_CFG_cfg_elem(R, sname, type, vname, ...)                  \
    _detail_AX_MK_CFG_DEFER(_detail_AX_MK_CFG_SEQ_FOR_EACH_R_ID)()(             \
            R, _detail_AX_MK_CFG_cfg_elem_app, (sname, type, vname),            \
            BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__)                    \
                                                                  )
// ////////////////////////////////////////////////////////////////////////// //
//default
#define _detail_AX_MK_CFG_cfg_elem_app_AX_DEFAULT(...)                          \
    _detail_AX_MK_CFG_cfg_elem_app_AX_DEFAULT_i, __VA_ARGS__

#define _detail_AX_MK_CFG_cfg_elem_app_AX_DEFAULT_i(sname, type, vname, ...)    \
    template<>                                                                  \
    struct element_default<sname, type, &sname::vname> : std::true_type /* IN CASE OF AN ERROR: You might have two AX_DEFAULT */ \
    {                                                                                                                            \
        static type get()                                                                                                        \
        {                                                                                                                        \
            return type(__VA_ARGS__);                                                                                            \
        }                                                                                                                        \
        static void set(sname& s)                                                                                                \
        {                                                                                                                        \
            s.vname = get();                                                                                                     \
        }                                                                                                                        \
    };
// ////////////////////////////////////////////////////////////////////////// //
//min/max....
#define _detail_AX_MK_CFG_cfg_elem_app_(...)


#define _detail_AX_MK_CFG_cfg_elem_app_AX_MIN(...)                              \
    _detail_AX_MK_CFG_cfg_elem_app_AX_MIN_i, __VA_ARGS__

#define _detail_AX_MK_CFG_cfg_elem_app_AX_MIN_i(sname, type, vname, ...)        \
    template<> constexpr auto element_min<sname, type, &sname::vname> =         \
            armarx::meta::detail::make_array<type>::value(__VA_ARGS__);


#define _detail_AX_MK_CFG_cfg_elem_app_AX_MAX(...)                              \
    _detail_AX_MK_CFG_cfg_elem_app_AX_MAX_i, __VA_ARGS__

#define _detail_AX_MK_CFG_cfg_elem_app_AX_MAX_i(sname, type, vname, ...)        \
    template<> constexpr auto element_max<sname, type, &sname::vname> =         \
            armarx::meta::detail::make_array<type>::value(__VA_ARGS__);


#define _detail_AX_MK_CFG_cfg_elem_app_AX_DESCRIPTION(...)                      \
    _detail_AX_MK_CFG_cfg_elem_app_AX_DESCRIPTION_i, __VA_ARGS__

#define _detail_AX_MK_CFG_cfg_elem_app_AX_DESCRIPTION_i(sname, type, vname, ...)\
    template<> constexpr const char*                                            \
    element_description<sname, type, &sname::vname> = __VA_ARGS__;


#define _detail_AX_MK_CFG_cfg_elem_app_AX_PROPERTY_NAME(...)                    \
    _detail_AX_MK_CFG_cfg_elem_app_AX_PROP_NAME_i, __VA_ARGS__

#define _detail_AX_MK_CFG_cfg_elem_app_AX_PROP_NAME_i(sname, type, vname, ...)  \
    template<> constexpr const char*                                            \
    element_property_name<sname, type, &sname::vname> = __VA_ARGS__;


#define _detail_AX_MK_CFG_cfg_elem_app_AX_DECIMALS(...)                         \
    _detail_AX_MK_CFG_cfg_elem_app_AX_DECIMALS_i, __VA_ARGS__

#define _detail_AX_MK_CFG_cfg_elem_app_AX_DECIMALS_i(sname, type, vname, ...)   \
    template<> constexpr auto  element_decimals<sname, type, &sname::vname> =   \
            std::experimental::make_array<int>(__VA_ARGS__);


#define _detail_AX_MK_CFG_cfg_elem_app_AX_STEPS(...)                            \
    _detail_AX_MK_CFG_cfg_elem_app_AX_STEPS_i, __VA_ARGS__

#define _detail_AX_MK_CFG_cfg_elem_app_AX_STEPS_i(sname, type, vname, ...)      \
    template<> constexpr auto element_steps<sname, type, &sname::vname> =       \
            std::experimental::make_array<int>(__VA_ARGS__);


#define _detail_AX_MK_CFG_cfg_elem_app_AX_LABEL(...)                            \
    _detail_AX_MK_CFG_cfg_elem_app_AX_LABEL_i, __VA_ARGS__

#define _detail_AX_MK_CFG_cfg_elem_app_AX_LABEL_i(sname, type, vname, ...)      \
    template<> constexpr const char*                                            \
    element_label<sname, type, &sname::vname> = {__VA_ARGS__};


#define _detail_AX_MK_CFG_cfg_elem_app_AX_NO_PROPERTY(...)                      \
    _detail_AX_MK_CFG_cfg_elem_app_AX_NO_PROPERTY_i, __VA_ARGS__

#define _detail_AX_MK_CFG_cfg_elem_app_AX_NO_PROPERTY_i(sname, type, vname, ...)\
    template<> constexpr std::true_type                                         \
    element_no_prop<sname, type, &sname::vname> = {};

#define _detail_AX_MK_CFG_cfg_elem_app_AX_NO_REMOTE_GUI(...)                    \
    _detail_AX_MK_CFG_cfg_elem_app_AX_NO_REMOTE_GUI_i, __VA_ARGS__

#define _detail_AX_MK_CFG_cfg_elem_app_AX_NO_REMOTE_GUI_i(sname, type, vname, ...)  \
    template<> constexpr std::true_type                                             \
    element_no_rem_gui<sname, type, &sname::vname> = {};

#define _detail_AX_MK_CFG_cfg_elem_app_AX_EXCLUDE_FROM_ALL(...)                    \
    _detail_AX_MK_CFG_cfg_elem_app_AX_EXCLUDE_FROM_ALL_i, __VA_ARGS__

#define _detail_AX_MK_CFG_cfg_elem_app_AX_EXCLUDE_FROM_ALL_i(sname, type, vname, ...)  \
    template<> constexpr std::true_type                                             \
    element_no_prop<sname, type, &sname::vname> = {};                               \
    template<> constexpr std::true_type                                             \
    element_no_rem_gui<sname, type, &sname::vname> = {};                                                                                    \
    // ////////////////////////////////////////////////////////////////////////// //
// ////////////////////////////////////////////////////////////////////////// //
//hana adapt
#define _detail_AX_MK_CFG_member_names(R, sname, type, name, ...) , name
#define ARMARX_CONFIG_STRUCT_ADAPT(Namespace, Name, ...)                        \
    BOOST_HANA_ADAPT_STRUCT(                                                    \
            Namespace::Name /* , will con from next macro*/                     \
            _detail_AX_MK_CFG_appl_FOR_EACH(                                    \
                    Namespace::Name,                                            \
                    _detail_AX_MK_CFG_member_names,                             \
                    __VA_ARGS__                                                 \
                                           )                                    \
                           )
// ////////////////////////////////////////////////////////////////////////// //
// ////////////////////////////////////////////////////////////////////////// //
/**
  * Usage
  * ARMARX_CONFIG_STRUCT_DEFINE_ADAPT(
  *     foospace, foo,
  *     (float, flooat, AX_DEFAULT(1234), AX_MIN(-42), AX_MAX(1337))
  * )
  */
#define ARMARX_CONFIG_STRUCT_DEFINE_ADAPT(Namespace, Name, ...)                 \
    _detail_AX_MK_CFG_struct_def  (Namespace, Name, __VA_ARGS__);               \
    ARMARX_CONFIG_STRUCT_ADAPT    (Namespace, Name, __VA_ARGS__);               \
    _detail_AX_MK_CFG_ctor_def    (Namespace, Name, __VA_ARGS__)

/**
  * Usage
  * ARMARX_CONFIG_STRUCT_DEFINE_ADAPT_CONFIGURE(
  *     foospace, foo,
  *     (float, flooat, AX_DEFAULT(1234), AX_MIN(-42), AX_MAX(1337))
  * )
  */
#define ARMARX_CONFIG_STRUCT_DEFINE_ADAPT_CONFIGURE(Namespace, Name, ...)       \
    ARMARX_CONFIG_STRUCT_DEFINE_ADAPT(Namespace, Name, __VA_ARGS__);            \
    ARMARX_CONFIG_STRUCT_CONFIGURE   (Namespace, Name, __VA_ARGS__)

/**
  * Usage
  * ARMARX_CONFIG_STRUCT_CONFIGURE_ADAPT(
  *     foospace, foo,
  *     (float, flooat, AX_DEFAULT(1234), AX_MIN(-42), AX_MAX(1337))
  * )
  */
#define ARMARX_CONFIG_STRUCT_CONFIGURE_ADAPT(Namespace, Name, ...)              \
    ARMARX_CONFIG_STRUCT_ADAPT    (Namespace, Name, __VA_ARGS__);               \
    ARMARX_CONFIG_STRUCT_CONFIGURE(Namespace, Name, __VA_ARGS__)

#define ARMARX_CONFIG_STRUCT_ADAPT_CONFIGURE(Namespace, Name, ...)              \
    ARMARX_CONFIG_STRUCT_CONFIGURE_ADAPT(Namespace, Name, __VA_ARGS__)

#pragma once

#include <string>

#include <SimoxUtility/meta/boost_hana.h>
#include <SimoxUtility/meta/undefined_t.h>

namespace armarx::meta
{
    using namespace simox::meta;
}

namespace armarx::meta::cfg
{
    namespace  hana = boost::hana;

    using undefined_t = ::simox::meta::undefined_t;

    template<class...Ts>
    using first_not_undefined_t = ::simox::meta::first_not_undefined_t<Ts...>;
}

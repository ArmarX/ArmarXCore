/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Raphael Grimm (raphael dot grimm at kit dot edu)
* @date       2019
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "Preprocessor.h"
#include "trace.h"

#define _detail_ARMARX_MAKE_ENUM_CONVERTERS(...) _detail_ARMARX_MAKE_ENUM_CONVERTERS_i(__VA_ARGS__)
#define _detail_ARMARX_MAKE_ENUM_CONVERTERS_i(r, data, elem) case data::elem: return #data "::" #elem;

#define ARMARX_MAKE_ENUM_CONVERTERS(type, ...)                                                              \
    static_assert(std::is_enum_v<type>, "Parameters to ARMARX_MAKE_ENUM_CONVERTERS must be enum types");    \
    inline std::string to_string(type v)                                                                    \
    {                                                                                                       \
        using base = std::underlying_type_t<type>;                                                          \
        switch(v)                                                                                           \
        {                                                                                                   \
                ARMARX_VARIADIC_FOR_EACH(_detail_ARMARX_MAKE_ENUM_CONVERTERS, type, __VA_ARGS__)            \
        }                                                                                                   \
        throw std::invalid_argument{"Unknown enum value " + std::to_string(static_cast<base>(v))};          \
    }                                                                                                       \
    inline std::ostream& operator << (std::ostream& out, type v)                                            \
    {                                                                                                       \
        return out << to_string(v);                                                                         \
    }                                                                                                       \
    inline type operator++(type& x) {return x = (type)(((int)(x)+1));}                                      \
    inline type operator++(type& x, int) {type y = x; x = (type)(((int)(x)+1)); return y;}

#define ARMARX_MAKE_ENUM_AND_CONVERTERS(name, ...)  \
    enum class name { __VA_ARGS__ };                \
    ARMARX_MAKE_ENUM_CONVERTERS(name, __VA_ARGS__)

/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::CPPUtility::Test
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#define BOOST_TEST_MODULE ArmarX::CPPUtility::ConfigIntrospection

#define ARMARX_BOOST_TEST

#include <ArmarXCore/Test.h>
#include <ArmarXCore/util/CPPUtility/ConfigIntrospection.h>

#define stringy(...) stringy_i(__VA_ARGS__)
#define stringy_i(...) # __VA_ARGS__

#define test(name, expect, ...)                                                 \
    std::cout << #name;                                                         \
    {                                                                           \
        const std::string got = stringy( __VA_ARGS__);                          \
        BOOST_CHECK_EQUAL(expect, got);                                         \
        if(got == expect)                                                       \
        {                                                                       \
            std::cout << "...ALL OK!\n";                                        \
        }                                                                       \
        else                                                                    \
        {                                                                       \
            std::cout << "\n    !!!!!!!!!!ERROR!!!!!!!!!!\n";                   \
            std::cout << "    EXPECT: " << expect << "\n";                      \
            std::cout << "    GOT   : " << got << "\n";                         \
        }                                                                       \
    } static_assert(true, "force the usage of a trailing semicolon")


BOOST_AUTO_TEST_CASE(ARMARX_CONFIG_STRUCT_MACROS)
{
    // ////////////////////////////////////////////////////////////////////// //
    test(
        _detail_AX_MK_CFG_appl_FOR_EACH,
        "macario(2, typerio, 0, 1) macario(3, typerio, 2, 3)",
        _detail_AX_MK_CFG_appl_FOR_EACH(typerio, macario, (0, 1), (2, 3))
    );

    test(
        _detail_AX_MK_CFG_member_def,
        "float flooat;",
        _detail_AX_MK_CFG_member_def(R, sname, float, flooat, AX_DEFAULT(1234),
                                     AX_MIN(-42), AX_MAX(1337))
    );
    // ////////////////////////////////////////////////////////////////////// //
    test(
        _detail_AX_MK_CFG_appl_FOR_EACH _detail_AX_MK_CFG_member_def,
        "float flooat;",
        _detail_AX_MK_CFG_appl_FOR_EACH(
            foospace::foo,
            _detail_AX_MK_CFG_member_def,
            (float, flooat, AX_DEFAULT(1234), AX_MIN(-42), AX_MAX(1337))
        )
    );

    test(
        _detail_AX_MK_CFG_struct_def,
        "namespace foospace { struct foo { float flooat; int tint; foo(); foo(foo&&) = default; foo(const foo&) = default; foo& operator=(foo&&) = default; foo& operator=(const foo&) = default; }; } static_assert(true, \"force the usage of a trailing semicolon\")",
        _detail_AX_MK_CFG_struct_def(
            foospace, foo,
            (float, flooat, AX_DEFAULT(1234), AX_MIN(-42), AX_MAX(1337)),
            (int, tint, AX_DEFAULT(0), AX_MIN(-1), AX_MAX(1))
        )
    );
    test(
        _detail_AX_MK_CFG_ctor_def,
        "namespace foospace { inline foo::foo() { armarx::meta::cfg::struct_default<foo>::set(*this); } } static_assert(true, \"force the usage of a trailing semicolon\")",
        _detail_AX_MK_CFG_ctor_def(
            foospace, foo,
            (float, flooat, AX_DEFAULT(1234), AX_MIN(-42), AX_MAX(1337)),
            (int, tint, AX_DEFAULT(0), AX_MIN(-1), AX_MAX(1))
        )
    );
    // ////////////////////////////////////////////////////////////////////// //
    test(
        _detail_AX_MK_CFG_cfg_elem AX_DEFAULT,
        "template<> struct element_default<foospace::foo, float, &foospace::foo::flooat> : std::true_type { static float get() { return float(1234); } static void set(foospace::foo& s) { s.flooat = get(); } };",
        _detail_AX_MK_CFG_cfg_elem(1, foospace::foo, float, flooat, AX_DEFAULT(1234))
    );

    test(
        _detail_AX_MK_CFG_cfg_elem AX_MIN,
        "template<> constexpr auto element_min<foospace::foo, float, &foospace::foo::flooat> = std::experimental::make_array<float>(-42);",
        _detail_AX_MK_CFG_cfg_elem(1, foospace::foo, float, flooat, AX_MIN(-42))
    );

    test(
        _detail_AX_MK_CFG_cfg_elem AX_MAX,
        "template<> constexpr auto element_max<foospace::foo, float, &foospace::foo::flooat> = std::experimental::make_array<float>(1337);",
        _detail_AX_MK_CFG_cfg_elem(1, foospace::foo, float, flooat, AX_MAX(1337))
    );

    test(
        _detail_AX_MK_CFG_cfg_elem AX_DESCRIPTION,
        "template<> constexpr const char* element_description<foospace::foo, float, &foospace::foo::flooat> = \"descr\";",
        _detail_AX_MK_CFG_cfg_elem(1, foospace::foo, float, flooat, AX_DESCRIPTION("descr"))
    );

    test(
        _detail_AX_MK_CFG_cfg_elem AX_DECIMALS,
        "template<> constexpr auto element_decimals<foospace::foo, float, &foospace::foo::flooat> = std::experimental::make_array<int>(4);",
        _detail_AX_MK_CFG_cfg_elem(1, foospace::foo, float, flooat, AX_DECIMALS(4))
    );

    test(
        _detail_AX_MK_CFG_cfg_elem AX_STEPS,
        "template<> constexpr auto element_steps<foospace::foo, float, &foospace::foo::flooat> = std::experimental::make_array<int>(11);",
        _detail_AX_MK_CFG_cfg_elem(1, foospace::foo, float, flooat, AX_STEPS(11))
    );

    test(
        _detail_AX_MK_CFG_cfg_elem AX_LABEL,
        "template<> constexpr const char* element_label<foospace::foo, float, &foospace::foo::flooat> = {\"label\"};",
        _detail_AX_MK_CFG_cfg_elem(1, foospace::foo, float, flooat, AX_LABEL("label"))
    );
    // ////////////////////////////////////////////////////////////////////// //
    test(
        _detail_AX_MK_CFG_appl_FOR_EACH _detail_AX_MK_CFG_cfg_elem,
        "template<> constexpr auto element_steps<foospace::foo, float, &foospace::foo::flooat> = std::experimental::make_array<int>(1337); "
        "template<> constexpr const char* element_label<foospace::foo, float, &foospace::foo::flooat> = {\"LALALA\"}; "
        "template<> constexpr auto element_max<foospace::foo, int, &foospace::foo::tint> = std::experimental::make_array<int>(1337); "
        "template<> constexpr auto element_min<foospace::foo, int, &foospace::foo::tint> = std::experimental::make_array<int>(-42);",
        _detail_AX_MK_CFG_appl_FOR_EACH(
            foospace::foo,
            _detail_AX_MK_CFG_cfg_elem,
            (float, flooat, AX_STEPS(1337), AX_LABEL("LALALA")),
            (int, tint, AX_MAX(1337), AX_MIN(-42))
        )
    );

    test(
        ARMARX_CONFIG_STRUCT_CONFIGURE,
        "namespace armarx::meta::cfg { template<> constexpr auto element_steps<foospace::foo, float, &foospace::foo::flooat> = std::experimental::make_array<int>(1337);"
        " template<> constexpr const char* element_label<foospace::foo, float, &foospace::foo::flooat> = {\"LALALA\"}; "
        "template<> constexpr auto element_max<foospace::foo, int, &foospace::foo::tint> = std::experimental::make_array<int>(1337); "
        "template<> constexpr auto element_min<foospace::foo, int, &foospace::foo::tint> = std::experimental::make_array<int>(-42); } "
        "static_assert(true, \"force the usage of a trailing semicolon\")",
        ARMARX_CONFIG_STRUCT_CONFIGURE(
            foospace, foo,
            (float, flooat, AX_STEPS(1337), AX_LABEL("LALALA")),
            (int, tint, AX_MAX(1337), AX_MIN(-42))
        )
    );
    // ////////////////////////////////////////////////////////////////////// //
    test(
        ARMARX_CONFIG_STRUCT_DEFINE_ADAPT_CONFIGURE,
        "namespace armarx::cfg::pcl2gc_locshape { struct Regions { "
        "float radius; float boarder; float spacing; "
        "Regions(); Regions(Regions&&) = default; Regions(const Regions&) = default; "
        "Regions& operator=(Regions&&) = default; Regions& operator=(const Regions&) = default; "
        "}; } static_assert(true, \"force the usage of a trailing semicolon\"); "
        "template <> struct BOOST_HANA_ADAPT_STRUCT_must_be_called_in_the_global_namespace<>; "
        "namespace boost { namespace hana { "
        "template <> struct accessors_impl<armarx::cfg::pcl2gc_locshape::Regions> "
        "{ static constexpr auto apply() { struct member_names { "
        "static constexpr auto get() { "
        "return ::boost::hana::make_tuple( \"radius\", \"boarder\", \"spacing\" ); } }; "
        "return ::boost::hana::make_tuple( ::boost::hana::make_pair("
        "::boost::hana::struct_detail::prepare_member_name<0, member_names>(), "
        "::boost::hana::struct_detail::member_ptr<decltype(&armarx::cfg::pcl2gc_locshape::Regions::radius), "
        "&armarx::cfg::pcl2gc_locshape::Regions::radius>{}), ::boost::hana::make_pair("
        "::boost::hana::struct_detail::prepare_member_name<1, member_names>(), "
        "::boost::hana::struct_detail::member_ptr<decltype(&armarx::cfg::pcl2gc_locshape::Regions::boarder), "
        "&armarx::cfg::pcl2gc_locshape::Regions::boarder>{}), ::boost::hana::make_pair("
        "::boost::hana::struct_detail::prepare_member_name<2, member_names>(), "
        "::boost::hana::struct_detail::member_ptr<decltype(&armarx::cfg::pcl2gc_locshape::Regions::spacing), "
        "&armarx::cfg::pcl2gc_locshape::Regions::spacing>{}) ); } }; }} "
        "static_assert(true, \"force the usage of a trailing semicolon\"); "
        "namespace armarx::cfg::pcl2gc_locshape { inline Regions::Regions() { "
        "armarx::meta::cfg::struct_default<Regions>::set(*this); } } "
        "static_assert(true, \"force the usage of a trailing semicolon\"); "
        "namespace armarx::meta::cfg { template<> struct element_default<armarx::cfg::pcl2gc_locshape::Regions, "
        "float, &armarx::cfg::pcl2gc_locshape::Regions::radius> : std::true_type { static float get() { return float(200); } "
        "static void set(armarx::cfg::pcl2gc_locshape::Regions& s) { s.radius = get(); } }; "
        "template<> constexpr auto element_min<armarx::cfg::pcl2gc_locshape::Regions, float, "
        "&armarx::cfg::pcl2gc_locshape::Regions::radius> = std::experimental::make_array<float>(0); "
        "template<> constexpr auto element_max<armarx::cfg::pcl2gc_locshape::Regions, float, "
        "&armarx::cfg::pcl2gc_locshape::Regions::radius> = std::experimental::make_array<float>(1000); "
        "template<> struct element_default<armarx::cfg::pcl2gc_locshape::Regions, float, "
        "&armarx::cfg::pcl2gc_locshape::Regions::boarder> : std::true_type { static float get() "
        "{ return float(50); } static void set(armarx::cfg::pcl2gc_locshape::Regions& s) { s.boarder = get(); } }; "
        "template<> constexpr auto element_min<armarx::cfg::pcl2gc_locshape::Regions, float, "
        "&armarx::cfg::pcl2gc_locshape::Regions::boarder> = std::experimental::make_array<float>(0); "
        "template<> constexpr auto element_max<armarx::cfg::pcl2gc_locshape::Regions, float, "
        "&armarx::cfg::pcl2gc_locshape::Regions::boarder> = std::experimental::make_array<float>(1000); "
        "template<> struct element_default<armarx::cfg::pcl2gc_locshape::Regions, float, "
        "&armarx::cfg::pcl2gc_locshape::Regions::spacing> : std::true_type { static float get() "
        "{ return float(200); } static void set(armarx::cfg::pcl2gc_locshape::Regions& s) "
        "{ s.spacing = get(); } }; template<> constexpr auto element_min<armarx::cfg::pcl2gc_locshape::Regions, "
        "float, &armarx::cfg::pcl2gc_locshape::Regions::spacing> = std::experimental::make_array<float>(1); "
        "template<> constexpr auto element_max<armarx::cfg::pcl2gc_locshape::Regions, float, "
        "&armarx::cfg::pcl2gc_locshape::Regions::spacing> = std::experimental::make_array<float>(1000); } "
        "static_assert(true, \"force the usage of a trailing semicolon\");",
        ARMARX_CONFIG_STRUCT_DEFINE_ADAPT_CONFIGURE(
            armarx::cfg::pcl2gc_locshape, Regions,
            (float, radius,  AX_DEFAULT(200), AX_MIN(0), AX_MAX(1000)),
            (float, boarder, AX_DEFAULT(50),  AX_MIN(0), AX_MAX(1000)),
            (float, spacing, AX_DEFAULT(200), AX_MIN(1), AX_MAX(1000))
        );
    );
    // ////////////////////////////////////////////////////////////////////// //
}


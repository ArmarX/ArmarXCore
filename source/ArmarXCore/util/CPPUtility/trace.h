#pragma once

#include <boost/current_function.hpp>

#include <iosfwd>
#include <cstdint>
#include <string>

namespace armarx::detail
{
    struct Trace;

    struct LocationProvider
    {
        struct Location
        {
            const std::string file;
            const std::string func;
            const int         line;
        };
        using FncLoc = Location(*)();
        FncLoc const location;
    };
    struct Trace
    {
        // ///////////////////////////////////////////// //
        // /////////////////// data //////////////////// //
        // ///////////////////////////////////////////// //
    public:
        const Trace* parent;
        const int uncaughtExceptions;
        const LocationProvider location;
    private:
        static thread_local const Trace* Top;
        static thread_local std::uintmax_t Size;
        // ///////////////////////////////////////////// //
        // ////////////// static functions ///////////// //
        // ///////////////////////////////////////////// //
    public:
        static void PrintStackTrace(std::ostream& out, const std::string& pre = "");

        static void PrintExceptionBacktrace(std::ostream& out, const std::string& pre = "");

        static void ClearExceptionBacktrace();
        static std::uintmax_t GetStackTraceSize()
        {
            return Size;
        }
        // ///////////////////////////////////////////// //
        // ///////////////// functions ///////////////// //
        // ///////////////////////////////////////////// //
    protected:
        Trace(LocationProvider::FncLoc l)
            : parent{Top},
              uncaughtExceptions{std::uncaught_exceptions()},
              location{l}
        {
            Top = this;
            ++Size;
        }
    public:
        ~Trace();
        bool exceptional() const
        {
            return uncaughtExceptions != std::uncaught_exceptions();
        }
    };
}
#define ARMARX_TRACE _detail_TRACE_expand(__FILE__, __LINE__, __COUNTER__)
#define _detail_TRACE_expand(...) _detail_TRACE_mk_var(__VA_ARGS__)
#define _detail_TRACE_mk_var(fi, li, cnt) _detail_TRACE(fi, li, _detail_trace_variable_ ## cnt ## _ ## li)
#define _detail_TRACE(fi, li, var)                                                                          \
    static constexpr auto var ## _cfunc = BOOST_CURRENT_FUNCTION;                          \
    struct var ## TraceType: ::armarx::detail::Trace                                       \
    {                                                                                                       \
        ::armarx::detail::LocationProvider::Location static Location() {return {fi, var ## _cfunc, li};}    \
        var ## TraceType() : Trace{var ## TraceType::Location} {}                                           \
    } var

namespace armarx
{
    class LogSender;
}

#define ARMARX_TRACE_LITE _detail_TRACElite_expand(__FILE__, __LINE__, __COUNTER__)
#define _detail_TRACElite_expand(...) _detail_TRACElite_mk_var(__VA_ARGS__)
#define _detail_TRACElite_mk_var(fi, li, cnt) _detail_TRACElite_f(fi, li, _detail_trace_variable_ ## cnt ## _ ## li)
#define _detail_TRACElite_f(fi, li, var)                                                                                \
    [[maybe_unused]] struct var ## TraceType: ::armarx::detail::Trace                                                   \
    {                                                                                                                   \
        ::armarx::detail::LocationProvider::Location static Location() {return {fi, "fuction name is unknown!", li};}   \
        var ## TraceType(const ::armarx::LogSender&) : Trace{var ## TraceType::Location} {}                             \
        var ## TraceType() : Trace{var ## TraceType::Location} {}                                                       \
    } var

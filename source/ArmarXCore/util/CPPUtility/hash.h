/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Raphael Grimm (raphael dot grimm at kit dot edu)
* @date       2019
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <functional>
#include <tuple>

#include <boost/functional/hash.hpp>

namespace std
{
    template<class T1, class T2> struct hash<std::pair<T1, T2>>
    {
        using argument_type = std::pair<T1, T2>;
        using result_type = std::size_t;
        result_type operator()(argument_type const& s) const noexcept
        {
            result_type seed = 0;
            boost::hash_combine(seed, std::hash<T1> {}(s.first));
            boost::hash_combine(seed, std::hash<T2> {}(s.second));

            return seed;
        }
    };
    template<class...Ts> struct hash<std::tuple<Ts...>>
    {
    public:
        using argument_type = std::tuple<Ts...>;
        using result_type = std::size_t;
    private:
        template <std::size_t N>
        using type = typename std::tuple_element<N, argument_type>::type;

        template<class = std::make_index_sequence<sizeof...(Ts)>>
        struct helper;

        template<std::size_t...Is>
        struct helper<std::index_sequence<Is...>>
        {
            static result_type hash(argument_type const& s) noexcept
            {
                result_type seed = 0;
                (boost::hash_combine(seed, std::hash<type<Is>> {}(std::get<Is>(s))), ...);
                return seed;
            }
        };
    public:
        result_type operator()(argument_type const& s) const noexcept
        {
            return helper<>::hash(s);
        }
    };
}

/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Raphael Grimm (raphael dot grimm at kit dot edu)
* @date       2019
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <sstream>
#include <stdexcept>
#include <type_traits>
#include <experimental/source_location>

namespace std
{
    using source_location = experimental::source_location;
}

namespace armarx::detail
{
    template<class, class = void>
    struct HasGetMember : std::false_type {};

    template<class T>
struct HasGetMember<T, std::void_t<decltype(&T::get)>> : std::true_type {};
}

namespace armarx
{
    template<class T>
    bool isNullptr(const T& p)
    {
        if constexpr(detail::HasGetMember<T>::value)
        {
            return p.get() == nullptr;
        }
        else
        {
            return p == nullptr;
        }
    }

    template<class T, class ExceptionType = std::invalid_argument>
    auto CheckedDeref(const T& ptr, const std::source_location& loc = std::source_location::current())
    {
        if (isNullptr(ptr))
        {
            std::stringstream s;
            s << "Ptr passed to CheckedDeref is NULL"
              << "\nfile    : " << loc.file_name()
              << "\nline    : " << loc.line()
              << "\nfunction: " << loc.function_name();
            throw ExceptionType {s.str()};
        }
        return *ptr;
    }
    template<class T, class M, class ExceptionType = std::invalid_argument>
    auto CheckedDeref(const T& ptr, M && msg, const std::source_location& loc = std::source_location::current())
    {
        if (isNullptr(ptr))
        {
            std::stringstream s;
            s << "Ptr passed to CheckedDeref is NULL"
              << "\nfile    : " << loc.file_name()
              << "\nline    : " << loc.line()
              << "\nfunction: " << loc.function_name()
              << "\nmessage :\n" << msg;
            throw ExceptionType {s.str()};
        }
        return *ptr;
    }
}


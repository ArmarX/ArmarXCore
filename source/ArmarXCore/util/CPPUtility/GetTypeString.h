/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Raphael Grimm (raphael dot grimm at kit dot edu)
* @date       2019
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <string>
#include <typeindex>

namespace armarx::detail
{
    std::pair<std::string, std::string> GetTypeStringFromMangled(const std::string& mangled);
}
namespace armarx
{
    inline std::string GetTypeString(const std::type_info& tinf, bool withoutNamespaceSpecifier = false)
    {
        const auto names = detail::GetTypeStringFromMangled(tinf.name());
        return withoutNamespaceSpecifier ? names.second : names.first;
    }
    inline std::string GetTypeString(const std::type_index& tind, bool withoutNamespaceSpecifier = false)
    {
        const auto names = detail::GetTypeStringFromMangled(tind.name());
        return withoutNamespaceSpecifier ? names.second : names.first;
    }

    template <typename T>
    const std::string& GetTypeString(bool withoutNamespaceSpecifier = false)
    {
        static const auto names = detail::GetTypeStringFromMangled(typeid(T).name());
        return withoutNamespaceSpecifier ? names.second : names.first;
    }

    template <typename T>
    std::string GetTypeString(const T& t, bool withoutNamespaceSpecifier = false)
    {
        return GetTypeString(typeid(t), withoutNamespaceSpecifier);
    }
}

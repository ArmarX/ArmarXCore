/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       24.02.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// from https://bitwizeshift.github.io/posts/2021/03/09/getting-an-unmangled-type-name-at-compile-time/
#pragma once

#include <string>
#include <string_view>
#include <array>   // std::array
#include <utility> // std::index_sequence

namespace armarx::meta
{

    template <std::size_t...Idxs>
    constexpr auto substring_as_array(std::string_view str, std::index_sequence<Idxs...>)
    {
        return std::array{str[Idxs]..., '\n'};
    }

    template <typename T>
    constexpr auto type_name_array()
    {
        #if defined(__clang__)
        constexpr auto prefix   = std::string_view{"[T = "};
        constexpr auto suffix   = std::string_view{"]"};
        constexpr auto function = std::string_view{__PRETTY_FUNCTION__};
        #elif defined(__GNUC__)
        constexpr auto prefix = std::string_view{"with T = "};
        constexpr auto suffix = std::string_view{"]"};
        constexpr auto function = std::string_view{__PRETTY_FUNCTION__};
        #elif defined(_MSC_VER)
        constexpr auto prefix   = std::string_view{"type_name_array<"};
        constexpr auto suffix   = std::string_view{">(void)"};
        constexpr auto function = std::string_view{__FUNCSIG__};
        #else
        # error Unsupported compiler
        #endif

        constexpr auto start = function.find(prefix) + prefix.size();
        constexpr auto end = function.rfind(suffix);

        static_assert(start < end);

        constexpr auto name = function.substr(start, (end - start));
        return substring_as_array(name, std::make_index_sequence<name.size()>{});
    }

    template <typename T>
    struct type_name_holder
    {
        static inline constexpr auto value = type_name_array<T>();
    };

    template <typename T>
    constexpr auto type_name() -> std::string_view
    {
        constexpr auto& value = type_name_holder<T>::value;
        return std::string_view{value.data(), value.size()};
    }

}

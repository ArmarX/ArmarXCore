#include "trace.h"

#include <iostream>
#include <iomanip>
#include <cmath>
#include <deque>
#include <memory>

// ///////////////////////////////////////////////////////////////////////////////////////////////////// //
// /////////////////////////////////////////////// debug /////////////////////////////////////////////// //
// ///////////////////////////////////////////////////////////////////////////////////////////////////// //
#define ACTIVATE_TRACE_LIB_TERMINATE_ON_INTERNAL_ERROR
#ifdef ACTIVATE_TRACE_LIB_TERMINATE_ON_INTERNAL_ERROR
#define TerminateIf(...)                                \
    {                                                       \
        if(__VA_ARGS__)                                     \
        {                                                   \
            std::cerr << "TERMINATING!:\n    ("             \
                      << #__VA_ARGS__ << ") == true\n    "  \
                      << "'"     << BOOST_CURRENT_FUNCTION  \
                      << "' @ "  << __LINE__                \
                      << " in '" << __FILE__ << "'\n    "   \
                      << "Stack Trace:\n";                  \
            Trace::PrintStackTrace(std::cerr, "        ");       \
            std::cerr << std::flush;                        \
            std::terminate();                               \
        }                                                   \
    } do{}while(false)
#else
#define TerminateIf(...) do{}while(false)
#endif
// ///////////////////////////////////////////////////////////////////////////////////////////////////// //
// /////////////////////////////////////////////// types /////////////////////////////////////////////// //
// ///////////////////////////////////////////////////////////////////////////////////////////////////// //
namespace armarx::detail
{
    struct ExceptionBacktraceTree
    {
        static void PrintExceptionBacktrace(std::ostream& out, const std::string& pre);
        static void ClearExceptionBacktrace();

        static thread_local ExceptionBacktraceTree* Active;
        static thread_local std::deque<ExceptionBacktraceTree> All;

        friend class Trace;

        ExceptionBacktraceTree(ExceptionBacktraceTree* parent)
            : parent{parent},
              parentEntryOffset{parent ? parent->entries.size() : 0},
              currentStackSize{Trace::GetStackTraceSize()}
        {}

        ExceptionBacktraceTree* const parent;
        const std::size_t parentEntryOffset;

        std::size_t currentStackSize;
        std::deque<ExceptionBacktraceTree> children;
        std::deque<LocationProvider> entries;

        void addEntry(const Trace* t) noexcept
        {
            entries.emplace_back(t->location);
            --currentStackSize;
        }

        void print(std::ostream& out, const std::string& prefix = "", std::size_t levelOffset = 0);
    };
}
// ///////////////////////////////////////////////////////////////////////////////////////////////////// //
// //////////////////////////////////////////////// data /////////////////////////////////////////////// //
// ///////////////////////////////////////////////////////////////////////////////////////////////////// //
namespace armarx::detail
{
    thread_local const Trace* Trace::Top = nullptr;
    thread_local std::uintmax_t Trace::Size = 0;
    thread_local ExceptionBacktraceTree* ExceptionBacktraceTree::Active = nullptr;
    thread_local std::deque<ExceptionBacktraceTree> ExceptionBacktraceTree::All;
}
// ///////////////////////////////////////////////////////////////////////////////////////////////////// //
// ///////////////////////////////////////////// management //////////////////////////////////////////// //
// ///////////////////////////////////////////////////////////////////////////////////////////////////// //
namespace armarx::detail
{
    void ExceptionBacktraceTree::ClearExceptionBacktrace()
    {
        ExceptionBacktraceTree::All.clear();
        ExceptionBacktraceTree::Active = nullptr;
    }

    void Trace::ClearExceptionBacktrace()
    {
        ExceptionBacktraceTree::ClearExceptionBacktrace();
    }

    Trace::~Trace()
    {
        TerminateIf(Top != this);
        if (exceptional())
        {
            if (ExceptionBacktraceTree::Active)
            {
                if (Size > ExceptionBacktraceTree::Active->currentStackSize)
                {
                    //add subtree
                    ExceptionBacktraceTree::Active->children.emplace_back(ExceptionBacktraceTree::Active);
                    ExceptionBacktraceTree::Active = &(ExceptionBacktraceTree::Active->children.back());
                }
                while (Size == (ExceptionBacktraceTree::Active->parent ? ExceptionBacktraceTree::Active->parent->currentStackSize : 0))
                {
                    //go to parent tree
                    ExceptionBacktraceTree::Active = ExceptionBacktraceTree::Active->parent;
                }
            }
            else
            {
                //add new root tree
                ExceptionBacktraceTree::All.emplace_back(nullptr);
                ExceptionBacktraceTree::Active = &ExceptionBacktraceTree::All.back();
            }
            //add entry to curent tree
            ExceptionBacktraceTree::Active->addEntry(this);
            while (
                ExceptionBacktraceTree::Active &&
                Size - 1 == (ExceptionBacktraceTree::Active->parent ? ExceptionBacktraceTree::Active->parent->currentStackSize : 0))
            {
                //go to parent tree
                ExceptionBacktraceTree::Active = ExceptionBacktraceTree::Active->parent;
            }
        }
        else if (ExceptionBacktraceTree::Active)
        {
            auto p = ExceptionBacktraceTree::Active->parent;
            auto& deque = p ? p->children : ExceptionBacktraceTree::All;
            TerminateIf(&deque.back() != ExceptionBacktraceTree::Active);
            deque.pop_back();
            ExceptionBacktraceTree::Active = p;
        }
        Top = parent;
        --Size;
    }
}
// ///////////////////////////////////////////////////////////////////////////////////////////////////// //
// /////////////////////////////////////////////// output ////////////////////////////////////////////// //
// ///////////////////////////////////////////////////////////////////////////////////////////////////// //
namespace armarx::detail
{
    std::ostream& operator<<(std::ostream& out, const LocationProvider::Location& l)
    {
        return out << l.file << ":" << l.line << " : " << l.func;
    }

    std::ostream& operator<<(std::ostream& out, const LocationProvider& t)
    {
        return out << t.location();
    }

    std::ostream& operator<<(std::ostream& out, const Trace& t)
    {
        return out << t.location;
    }

    void Trace::PrintStackTrace(std::ostream& out, const std::string& pre)
    {
        const Trace* trace = Top;

        const auto numW = Size ? std::log10(Size) + 1 : 1;

        for (std::uintmax_t cnt = 0; trace; ++cnt, trace = trace->parent)
        {
            out << pre
                << std::setw(numW) << cnt << std::setw(0)
                << ": " << *trace << "\n";
        }
    }

    void ExceptionBacktraceTree::PrintExceptionBacktrace(std::ostream& out, const std::string& pre)
    {
        const auto numW = ExceptionBacktraceTree::All.size() ? std::log10(ExceptionBacktraceTree::All.size()) + 1 : 1;
        for (std::size_t i = 0; i < ExceptionBacktraceTree::All.size(); ++i)
        {
            if (i)
            {
                out << "--------------------------" << std::string(numW, '-') << "\n";
            }
            if (ExceptionBacktraceTree::All.size() > 1)
            {
                out << "Exception Backtrace Tree "
                    << std::setw(numW) << i << std::setw(0) << ":\n";
            }
            ExceptionBacktraceTree::All.at(i).print(out, pre);
            Trace::PrintStackTrace(out, pre + "*  ");
        }
    }

    void Trace::PrintExceptionBacktrace(std::ostream& out, const std::string& pre)
    {
        ExceptionBacktraceTree::PrintExceptionBacktrace(out, pre);
    }

    void ExceptionBacktraceTree::print(std::ostream& out, const std::string& prefix, std::size_t levelOffset)
    {
        TerminateIf(entries.empty());
        const auto numW = std::log10(entries.size()) + 1;

        std::size_t nextChildParentEntryOffset = std::numeric_limits<std::size_t>::max();

        std::size_t nextChildIndex = 0;
        auto updateNextChildEntryOffset = [&]
        {
            nextChildParentEntryOffset = std::numeric_limits<std::size_t>::max();
            if (nextChildIndex < children.size())
            {
                nextChildParentEntryOffset = children.at(nextChildIndex).parentEntryOffset;
            }
        };
        updateNextChildEntryOffset();

        for (std::size_t entryIdx = 0; entryIdx < entries.size(); ++entryIdx)
        {
            const std::size_t level =  /*parentEntryOffset +*/ levelOffset + entries.size() - entryIdx;
            while (entryIdx >= nextChildParentEntryOffset && nextChildIndex < children.size())
            {
                children.at(nextChildIndex).print(out, prefix + "| ", level);
                out << prefix << "|/\n";
                ++nextChildIndex;
                updateNextChildEntryOffset();
            }
            out << prefix << "* -"
                << std::setw(numW) << level << std::setw(0) << ": "
                << entries.at(entryIdx) << "\n";
        }
        //print children after this branch
        for (; nextChildIndex < children.size(); ++nextChildIndex)
        {
            children.at(nextChildIndex).print(out, prefix + "| ");
            out << prefix << "|/\n";
        }
    }
}

/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Raphael Grimm (raphael dot grimm at kit dot edu)
* @date       2019
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <variant>
#include <boost/variant.hpp>

namespace boost
{
    template<int N, class... Ts> using NthTypeOf = typename std::tuple_element<N, std::tuple<Ts...>>::type;

    template<int N, class... Ts>
    auto& get_index(boost::variant<Ts...>& v)
    {
        using target = NthTypeOf<N, Ts...>;
        return boost::get<target>(v);
    }

    template<int N, class... Ts>
    const auto& get_index(const boost::variant<Ts...>& v)
    {
        using target = NthTypeOf<N, Ts...>;
        return boost::get<target>(v);
    }
}

namespace armarx
{
    template<int N, class... Ts> using NthTypeOf = typename std::tuple_element<N, std::tuple<Ts...>>::type;

    template<int N, class... Ts>
    auto* get_index_ptr(boost::variant<Ts...>& v)
    {
        using target = NthTypeOf<N, Ts...>;
        return boost::get<target>(&v);
    }

    template<int N, class... Ts>
    const auto* get_index_ptr(const boost::variant<Ts...>& v)
    {
        using target = NthTypeOf<N, Ts...>;
        return boost::get<const target>(&v);
    }


    template<int N, class... Ts>
    auto* get_index_ptr(std::variant<Ts...>& v)
    {
        using target = NthTypeOf<N, Ts...>;
        return std::holds_alternative<target>(v) ?
               &std::get<target>(v) :
               nullptr;
    }

    template<int N, class... Ts>
    const auto* get_index_ptr(const std::variant<Ts...>& v)
    {
        using target = NthTypeOf<N, Ts...>;
        return std::holds_alternative<const target>(v) ?
               &std::get<const target>(v) :
               nullptr;
    }
}

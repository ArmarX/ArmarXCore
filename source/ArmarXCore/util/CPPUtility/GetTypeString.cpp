/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Raphael Grimm (raphael dot grimm at kit dot edu)
* @date       2019
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#include <boost/core/demangle.hpp>

#include "GetTypeString.h"

namespace armarx::detail
{
    std::pair<std::string, std::string> GetTypeStringFromMangled(const std::string& mangled)
    {
        const std::string demangled = boost::core::demangle(mangled.c_str());
        static const auto removeNamespaceSpecifier = [](const std::string & demangled)
        {
            std::size_t substrStart = 0;
            std::size_t level = 0;
            if (!demangled.empty())
                for (int i = static_cast<int>(demangled.size() - 1); i >= 0; --i)
                {
                    if (!level && demangled.at(i) == ':')
                    {
                        substrStart = i + 1;
                        break;
                    }
                    level += (demangled.at(i) == '>') - (demangled.at(i) == '<');
                }
            return demangled.substr(substrStart);
        };
        return
        {
            demangled,
            removeNamespaceSpecifier(demangled)
        };
    }
}

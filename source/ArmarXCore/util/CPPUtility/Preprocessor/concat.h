/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

/**
 * @brief concatenates its 5 parameters to one token (macros are not expanded)
 * Use ARMARX_CONCATENATE_5 instead!
 * @see ARMARX_CONCATENATE_5
 */
#define ARMARX_CONCATENATE_5_IMPL(s1, s2, s3, s4, s5) s1##s2##s3##s4##s5

/**
 * @brief concatenates its 5 parameters to one token (macros are expanded)
 * \ingroup CPreprocessor
 */
#define ARMARX_CONCATENATE_5(s1, s2, s3, s4, s5) ARMARX_CONCATENATE_5_IMPL(s1, s2, s3, s4, s5)


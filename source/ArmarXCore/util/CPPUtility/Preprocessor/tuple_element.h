/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

/**
 * @brief Expands the given parameters and returns the first
 * \ingroup CPreprocessor
 */
#define ARMARX_FIRST_PARAMETER(...) ARMARX_FIRST_PARAMETER_IMPL(__VA_ARGS__,)
#define ARMARX_FIRST_PARAMETER_IMPL(x1,...) x1

/**
 * @brief Expands the given parameters and returns the second
 * \ingroup CPreprocessor
 */
#define ARMARX_SECOND_PARAMETER(...) ARMARX_SECOND_PARAMETER_IMPL(__VA_ARGS__,)
#define ARMARX_SECOND_PARAMETER_IMPL(x1,x2,...) x2

/**
 * @brief Expands the given parameters and returns the third
 * \ingroup CPreprocessor
 */
#define ARMARX_THIRD_PARAMETER(...) ARMARX_THIRD_PARAMETER_IMPL(__VA_ARGS__,,)
#define ARMARX_THIRD_PARAMETER_IMPL(x1,x2,x3,...) x3


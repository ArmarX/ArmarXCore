/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       24.02.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// http://www.nirfriedman.com/2018/04/29/unforgettable-factory/
#pragma once

#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>
#include <boost/version.hpp>

#if (BOOST_VERSION < 106701)

#include <boost/functional/hash.hpp>

#else
#include <boost/container_hash/hash.hpp>
#endif

#include <ArmarXCore/util/CPPUtility/trace.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/util/CPPUtility/TemplateMetaProgramming.h>
#include <ArmarXCore/util/CPPUtility/ctti.h>


namespace armarx
{

    template <class Base, typename Identification, class... Args>
    class Factory
    {
    public:

        virtual const Identification getID() const = 0;

        virtual ~Factory() = default;

        static auto make(const Identification& s, Args&& ... args)
        {
            ARMARX_TRACE;
            auto itr = data().find(s);
            if constexpr(!meta::is_streamable<std::ostream, Identification>::value)
            {
                ARMARX_CHECK(itr != data().end()) << "Product for Factory " << meta::type_name<Base>()
                                                  << " was not registered!";
            } else
            {
                ARMARX_CHECK(itr != data().end()) << "Product " << s << " for Factory " << meta::type_name<Base>()
                                                  << " was not registered!";
            }
            return itr->second(std::forward<Args>(args)...);
        }

        template <typename Derived>
        struct Registrar : Base
        {

            friend Derived;

            static bool registerT()
            {
                ARMARX_TRACE;
                constexpr auto name = Derived::id;
                Factory::data()[name] = [](Args... args) -> std::unique_ptr<Base>
                {
                    return std::make_unique<Derived>(std::forward<Args>(args)...);
                };
                return true;
            }

            inline static const bool registered{registerT()};

            const Identification getID() const override
            {
                return Derived::id;
            }

        private:
            class Registration
            {
            };

            // IMPORTANT: this forces the derived constructor to not be = default; otherwise std::out_of_range: _Map_base::at error is thrown
            //  this is, because the registration does not work then
            // see https://stackoverflow.com/questions/59919831/unforgettable-factory-when-is-constructor-instantiated?noredirect=1&lq=1
            explicit Registrar(Registration) : Base(Key{})
            {
                (void) registered;
            }


        };

        friend Base;

    private:
        class Key
        {
            Key()
            {
            };
            template <typename T> friend
            struct Registrar;
        };

        using FuncType = std::unique_ptr<Base> (*)(Args...);

        Factory() = default;

        static auto& data()
        {
            ARMARX_TRACE;
            static std::unordered_map<Identification, FuncType> s;
            return s;
        }
    };

    template <class Base, typename Identification1, typename Identification2, class... Args>
    class Factory2D
    {
    public:

        virtual const Identification1 getFirstID() const = 0;

        virtual const Identification2 getSecondID() const = 0;

        virtual ~Factory2D() = default;

        static auto make(const Identification1& f, const Identification2& s, Args&& ... args)
        {
            ARMARX_TRACE;
            auto itr = data().find(std::make_pair(f, s));
            ARMARX_CHECK(itr != data().end()) << "Product was not registered!";
            return itr->second(std::forward<Args>(args)...);
        }

        template <typename Derived>
        struct Registrar : Base
        {

            friend Derived;

            static bool registerT()
            {
                ARMARX_TRACE;
                constexpr auto name = std::make_pair(Derived::idDimOne, Derived::idDimTwo);
                Factory2D::data()[name] = [](Args... args) -> std::unique_ptr<Base>
                {
                    return std::make_unique<Derived>(std::forward<Args>(args)...);
                };
                return true;
            }

            inline static const bool registered{registerT()};

            const Identification1 getFirstID() const override
            {
                return Derived::idDimOne;
            }

            const Identification2 getSecondID() const override
            {
                return Derived::idDimTwo;
            }

        private:
            class Registration
            {
            };

            // IMPORTANT: this forces the derived constructor to not be = default; otherwise std::out_of_range: _Map_base::at error is thrown
            //  this is, because the registration does not work then
            // see https://stackoverflow.com/questions/59919831/unforgettable-Factory2D-when-is-constructor-instantiated?noredirect=1&lq=1
            explicit Registrar(Registration) : Base(Key{})
            {
                (void) registered;
            }


        };

        friend Base;

    private:
        class Key
        {
            Key()
            {
            };
            template <typename T> friend
            struct Registrar;
        };

        using FuncType = std::unique_ptr<Base> (*)(Args...);

        Factory2D() = default;

        static auto& data()
        {
            ARMARX_TRACE;
            static std::unordered_map<std::pair<Identification1, Identification2>, FuncType,
                                      boost::hash<std::pair<Identification1, Identification2>>> s;
            return s;
        }
    };
}


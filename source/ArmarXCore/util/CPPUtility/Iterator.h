/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Raphael Grimm (raphael dot grimm at kit dot edu)
* @date       2019
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once


#include <tuple>
#include <utility>
#include <stdexcept>
#include <algorithm>

#include <boost/iterator/iterator_facade.hpp>

namespace armarx
{
    template<class Ituple, class Rtuple, class Idx>
    class ZipIteratorBase;

    template<class Ituple, class Rtuple, std::size_t...Idxs>
    class ZipIteratorBase < Ituple, Rtuple, std::index_sequence<Idxs...>> :
                public boost::iterator_facade <
                ZipIteratorBase<Ituple, Rtuple, std::index_sequence<Idxs...>>,
                Rtuple,
                boost::forward_traversal_tag,
                Rtuple
                >
    {
    public:
        using IteratorTuple = Ituple;
        using reference = Rtuple;
        using difference_type = std::ptrdiff_t;
    private:
        IteratorTuple _iters;
    public:
        ZipIteratorBase(ZipIteratorBase&&) = default;
        ZipIteratorBase(const ZipIteratorBase&) = default;

        ZipIteratorBase& operator=(ZipIteratorBase&&) = default;
        ZipIteratorBase& operator=(const ZipIteratorBase&) = default;

        ZipIteratorBase(IteratorTuple its) : _iters{std::move(its)} {}
    private:
        friend class boost::iterator_core_access;

        // Implementation of Iterator Operations
        // =====================================
        reference dereference() const
        {
            return std::forward_as_tuple(*std::get<Idxs>(_iters)...);
        }

        bool equal(const ZipIteratorBase& other) const
        {
            return _iters == other._iters;
        }

        // Advancing a zip iterator means to advance all iterators in the
        // iterator tuple.
        void advance(difference_type n)
        {
            ((std::get<Idxs>(_iters) += n), ...);
        }
        // Incrementing a zip iterator means to increment all iterators in
        // the iterator tuple.
        void increment()
        {
            (++std::get<Idxs>(_iters), ...);
        }

        // Decrementing a zip iterator means to decrement all iterators in
        // the iterator tuple.
        void decrement()
        {
            (--std::get<Idxs>(_iters), ...);
        }

        //    // Distance is calculated using the first iterator in the tuple.
        //    template<typename OtherIteratorTuple>
        //    typename super_t::difference_type distance_to(
        //        const zip_iterator<OtherIteratorTuple>& other
        //    ) const
        //    {
        //        return fusion::at_c<0>(other.get_iterator_tuple()) -
        //               fusion::at_c<0>(this->get_iterator_tuple());
        //    }


    };

    template<class It0, class...Its>
    class ZipIterator :
        public ZipIteratorBase
        <
        std::tuple<It0, Its...>,
        std::tuple
        <
        typename std::iterator_traits<It0>::reference&&,
        typename std::iterator_traits<Its>::reference&& ...
        >,
        std::make_index_sequence < 1 + sizeof...(Its) >
        >
    {
    public:
        using Base = ZipIteratorBase
                     <
                     std::tuple<It0, Its...>,
                     std::tuple
                     <
                     typename std::iterator_traits<It0>::reference &&,
                     typename std::iterator_traits<Its>::reference && ...
                     >,
                     std::make_index_sequence < 1 + sizeof...(Its) >
                     >;

        ZipIterator(It0&& it0, Its&& ...its) :
            Base({std::forward<It0>(it0), std::forward<Its>(its)...})
        {}
        using Base::Base;
        using Base::operator=;
    };
}

#include <utility>
namespace armarx
{
    template<class It>
    struct IteratorRange
    {
        IteratorRange(It b, It e)
            : _begin{std::move(b)}, _end{std::move(e)}
        {}
        It begin() const
        {
            return _begin;
        }
        It end() const
        {
            return _end;
        }
    private:
        It _begin;
        It _end;
    };
}

#include <boost/iterator/zip_iterator.hpp>
#include <boost/iterator/counting_iterator.hpp>
namespace armarx
{
    template<class Container, class...Containers>
    auto MakeIndexedContainer(Container& c, Containers& ...cs)
    {
        if constexpr(sizeof...(Containers))
        {
            const auto initlist = {c.size(), cs.size()...};
            const auto [lo, hi] = std::minmax(initlist);
            if (lo != hi)
            {
                throw std::invalid_argument{"All containers have to be of the same size!"};
            }
        }
        return IteratorRange
        {
            ZipIterator(boost::counting_iterator<std::size_t>(0), c.begin(), cs.begin()...),
            ZipIterator(boost::counting_iterator<std::size_t>(c.size()), c.end(), cs.end()...)
        };
    }

    template<class Container, class...Containers>
    auto MakeZippedContainer(Container& c, Containers& ...cs)
    {
        if constexpr(sizeof...(Containers))
        {
            const auto initlist = {c.size(), cs.size()...};
            const auto [lo, hi] = std::minmax(initlist);
            if (lo != hi)
            {
                throw std::invalid_argument{"All containers have to be of the same size!"};
            }
        }
        return IteratorRange
        {
            ZipIterator(c.begin(), cs.begin()...),
            ZipIterator(c.end(), cs.end()...)
        };
    }

    template<class Container>
    auto MakeReversedRange(Container& c)
    {
        return IteratorRange{c.rbegin(), c.rend()};
    }
}

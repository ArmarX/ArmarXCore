#pragma once

#include "TemplateMetaProgramming.h"

#include <boost/shared_ptr.hpp>

#include <memory>

namespace armarx::meta
{
    template<class T, class = void>
    struct pointer_type_traits : std::false_type {};

    template<class T>
    struct pointer_type_traits<T*, void> : std::true_type
    {
        using pointee_t = T;
    };

    template<class T>
    struct pointer_type_traits <
        T,
        std::enable_if_t<TypeTemplateTraits::IsInstanceOfAnyV<T, boost::shared_ptr, std::shared_ptr, std::unique_ptr>>
                > : std::true_type
    {
        using pointee_t = typename T::element_type;
    };

    template<class T>
    static constexpr bool is_pointer_type_v = pointer_type_traits<T>::value;

    template<class T>
    using pointer_type_pointee_t = typename pointer_type_traits<T>::pointee_t;
}

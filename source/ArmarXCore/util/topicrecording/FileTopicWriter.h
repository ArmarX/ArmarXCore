/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

#include "TopicWriterInterface.h"

#include <filesystem>



namespace armarx
{

    class FileTopicWriter : public TopicWriterInterface
    {
    public:
        FileTopicWriter(const std::filesystem::path& path);
        FileTopicWriter(std::ostream* stream);
        ~FileTopicWriter() override;
    private:
        std::ostream* stream;
        std::filesystem::path filepath;


        // TopicWriterInterface interface
    public:
        void write(const TopicUtil::TopicData& topicData) override;
        std::filesystem::path getFilepath() const;
    };

}


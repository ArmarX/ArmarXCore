/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Jan Issac (jan dot issac at gmx dot de)
* @author     Kai Welke (welke at kit dot edu)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "BoostAssertionHandler.h"

namespace armarx::exceptions::local
{
    BoostAssertException::BoostAssertException(const std::string& reason) :
        _what{"A boost exception was raised: " + reason} {}

    const char* BoostAssertException::what() const noexcept
    {
        return _what.c_str();
    }
}

#include <sstream>

namespace boost
{
    void assertion_failed(char const* expr,
                          char const* function, char const* file, long line)
    {
        std::stringstream str;
        str << "At " << file << ":" << line << " in function " << function << " the following expression evaluted to false: " << expr;
        throw armarx::exceptions::local::BoostAssertException(str.str());
    }

    void assertion_failed_msg(char const* expr, char const* msg,
                              char const* function, char const* file, long line)
    {
        std::stringstream str;
        str << "At " << file << ":" << line << " in function " << function << " the following expression evaluted to false:" << expr  << "\n" << msg;
        throw armarx::exceptions::local::BoostAssertException(str.str());

    }
}

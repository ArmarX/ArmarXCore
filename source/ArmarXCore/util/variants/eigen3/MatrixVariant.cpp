/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::Core
 * @author     Peter Kaiser <peter.kaiser@kit.edu>
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MatrixVariant.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/observers/AbstractObjectSerializer.h>

#include <SimoxUtility/algorithm/string/string_tools.h>

#include <Eigen/Geometry>


namespace armarx
{
    MatrixFloat::MatrixFloat()
    {
        rows = 0;
        cols = 0;
    }

    MatrixFloat::MatrixFloat(int rows, int cols)
    {
        this->rows = rows;
        this->cols = cols;
        this->data.resize(rows * cols);
    }

    MatrixFloat::MatrixFloat(const Eigen::MatrixXf& m)
    {
        this->rows = m.rows();
        this->cols = m.cols();
        int i = 0;
        data = std::vector<float>(rows * cols);

        for (int col = 0; col < cols; col++)
        {
            for (int row = 0; row < rows; row++)
            {
                data[i] = m(row, col);
                i++;
            }
        }
    }


    MatrixFloat::MatrixFloat(int rows, int cols, const std::vector<float>& entries)
    {
        this->rows = rows;
        this->cols = cols;
        this->data = entries;
    }

    /*void MatrixFloat::setMatrix(int width, int height, const std::vector<float> &entries)
    {
        this->width = width;
        this->height = height;
        data = entries;
    }*/

    Eigen::MatrixXf MatrixFloat::toEigen() const
    {
        int i = 0;
        Eigen::MatrixXf m(rows, cols);

        for (int col = 0; col < cols; col++)
        {
            for (int row = 0; row < rows; row++)
            {
                m(row, col) = data[i];
                i++;
            }
        }

        return m;
    }

    std::vector<float> MatrixFloat::toVector() const
    {
        return data;
    }

    float& MatrixFloat::operator()(const int row, const int col)
    {
        return data.at(row + col * rows);
    }

    std::string MatrixFloat::toJsonRowMajor()
    {
        std::stringstream stream;
        stream << "[";

        for (int row = 0; row < rows; row++)
        {
            stream << (row > 0 ? ", [" : "[");

            for (int col = 0; col < cols; col++)
            {
                stream << (col > 0 ? ", " : "");
                stream << (*this)(row, col);
            }

            stream << "]";
        }

        stream << "]";
        return stream.str();
    }


    void MatrixFloat::serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current&) const
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);
        obj->setInt("rows", rows);
        obj->setInt("cols", cols);
        Ice::StringSeq rowContent;
        Eigen::MatrixXf m = toEigen();

        for (int row = 0; row < rows; row++)
        {
            std::stringstream ss;

            for (int col = 0; col < cols; col++)
            {
                ss << m(row, col) << (col < cols - 1 ? "," : "");
            }

            rowContent.push_back(ss.str());
        }

        obj->setStringArray("rowContent", rowContent);
    }

    void MatrixFloat::deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& c)
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);
        rows = obj->getInt("rows");
        cols = obj->getInt("cols");
        Ice::StringSeq rowContent;
        obj->getStringArray("rowContent", rowContent);

        if ((int)rowContent.size() != rows)
        {
            throw LocalException("unexcepted row count: ") << rowContent.size() << ", but expected " << rows;
        }

        data.resize(rows * cols);

        for (size_t row = 0; row < rowContent.size(); row++)
        {
            Ice::StringSeq values = simox::alg::split(rowContent[row], ",");
            int col = 0;

            if ((int)values.size() != cols)
            {
                throw LocalException("unexcepted column count: ") << values.size() << ", but expected " << cols;
            }

            for (std::string v : values)
            {
                data.at(col * rows + row) = atof(v.c_str());
                col++;
            }
        }
    }





    MatrixDouble::MatrixDouble()
    {
        rows = 0;
        cols = 0;
    }

    MatrixDouble::MatrixDouble(int rows, int cols)
    {
        this->rows = rows;
        this->cols = cols;
        this->data.resize(rows * cols);
    }

    MatrixDouble::MatrixDouble(const Eigen::MatrixXd& m)
    {
        this->rows = m.rows();
        this->cols = m.cols();
        int i = 0;
        data = std::vector<double>(rows * cols);

        for (int col = 0; col < cols; col++)
        {
            for (int row = 0; row < rows; row++)
            {
                data[i] = m(row, col);
                i++;
            }
        }
    }


    MatrixDouble::MatrixDouble(int rows, int cols, const std::vector<double>& entries)
    {
        this->rows = rows;
        this->cols = cols;
        this->data = entries;
    }

    /*void MatrixDouble::setMatrix(int width, int height, const std::vector<double> &entries)
    {
        this->width = width;
        this->height = height;
        data = entries;
    }*/

    Eigen::MatrixXd MatrixDouble::toEigen() const
    {
        int i = 0;
        Eigen::MatrixXd m(rows, cols);

        for (int col = 0; col < cols; col++)
        {
            for (int row = 0; row < rows; row++)
            {
                m(row, col) = data[i];
                i++;
            }
        }

        return m;
    }

    double& MatrixDouble::operator()(const int row, const int col)
    {
        return data.at(row + col * rows);
    }

    std::string MatrixDouble::toJsonRowMajor()
    {
        std::stringstream stream;
        stream << "[";

        for (int row = 0; row < rows; row++)
        {
            stream << (row > 0 ? ", [" : "[");

            for (int col = 0; col < cols; col++)
            {
                stream << (col > 0 ? ", " : "");
                stream << (*this)(row, col);
            }

            stream << "]";
        }

        stream << "]";
        return stream.str();
    }


    void MatrixDouble::serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current&) const
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);
        obj->setInt("rows", rows);
        obj->setInt("cols", cols);
        Ice::StringSeq rowContent;
        Eigen::MatrixXd m = toEigen();

        for (int row = 0; row < rows; row++)
        {
            std::stringstream ss;

            for (int col = 0; col < cols; col++)
            {
                ss << m(row, col) << (col < cols - 1 ? "," : "");
            }

            rowContent.push_back(ss.str());
        }

        obj->setStringArray("rowContent", rowContent);
    }

    void MatrixDouble::deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& c)
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);
        rows = obj->getInt("rows");
        cols = obj->getInt("cols");
        Ice::StringSeq rowContent;
        obj->getStringArray("rowContent", rowContent);

        if ((int)rowContent.size() != rows)
        {
            throw LocalException("unexcepted row count: ") << rowContent.size() << ", but expected " << rows;
        }

        data.resize(rows * cols);

        for (size_t row = 0; row < rowContent.size(); row++)
        {
            Ice::StringSeq values = simox::alg::split(rowContent[row], ",");
            int col = 0;

            if ((int)values.size() != cols)
            {
                throw LocalException("unexcepted column count: ") << values.size() << ", but expected " << cols;
            }

            for (std::string v : values)
            {
                data.at(col * rows + row) = atof(v.c_str());
                col++;
            }
        }
    }
}

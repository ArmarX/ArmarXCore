/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "ArmarXCore/util/ScenarioManagerCommon/generator/IceGridXmlGenerator.h"

#include "ArmarXCore/util/ScenarioManagerCommon/parser/DependenciesGenerator.h"

#include <fstream>
#include <sstream>

#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlWriter.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/IceManager.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/IceGridAdmin.h>
#include <ArmarXCore/util/ScenarioManagerCommon/executor/IceStatusReader.h>

using namespace ScenarioManager;
using namespace Generator;
using namespace armarx;

void IceGridXmlGenerator::generateLocalApplication(Data_Structure::ScenarioPtr scenario)
{
    RapidXmlWriter writer;
    RapidXmlWriterNode rootNode = writer.createRootNode("icegrid");
    CMakePackageFinder finder(scenario->getPackage()->getName());

    RapidXmlWriterNode applicationNode = rootNode.append_node("application");
    applicationNode.append_attribute("name", scenario->getName());
    applicationNode.append_attribute("import-default-templates", "true");

    RapidXmlWriterNode includeNode = applicationNode.append_node("include");
    includeNode.append_attribute("file", ArmarXDataPath::GetDefaultUserConfigPath() + "/icegrid-sync-variables.icegrid.xml");

    RapidXmlWriterNode variableScenarioDirNode = applicationNode.append_node("variable");
    variableScenarioDirNode.append_attribute("name", "ARMARX_SCENARIO_DIR");
    variableScenarioDirNode.append_attribute("value", scenario->getFolder());

    RapidXmlWriterNode variableScenarioDependencyNode = applicationNode.append_node("variable");
    variableScenarioDependencyNode.append_attribute("name", "ARMARX_SCENARIO_DEPENDENCY_CONFIG");
    variableScenarioDependencyNode.append_attribute("value", "dependencies.cfg");

    RapidXmlWriterNode variablePackageConfigDir = applicationNode.append_node("variable");
    variablePackageConfigDir.append_attribute("name", "ARMARX_PACKAGE_CONFIG_DIR");
    variablePackageConfigDir.append_attribute("value", finder.getConfigDir());

    RapidXmlWriterNode variableArmarXLDPath = applicationNode.append_node("variable");
    variableArmarXLDPath.append_attribute("name", "ArmarX_LD_LIB_PATH");
    variableArmarXLDPath.append_attribute("value", "");
    //    variableArmarXLDPath.append_attribute("value", "${ARMARX_SYNC_DIR}/lib:${ARMARX_SYNC_DIR}/usr/lib");

    RapidXmlWriterNode variableArmarXPath = applicationNode.append_node("variable");
    variableArmarXPath.append_attribute("name", "ArmarX_PATH");
    variableArmarXPath.append_attribute("value", "");
    //    variableArmarXPath.append_attribute("value", "${ARMARX_SYNC_DIR}/bin:${ARMARX_SYNC_DIR}/usr/bin");

    for (auto dep : Parser::DependenciesGenerator::getDependencieTree(scenario->getPackage()->getName()))
    {
        CMakePackageFinder currentFinder(dep);
        RapidXmlWriterNode binDirNode = applicationNode.append_node("variable");
        binDirNode.append_attribute("name", dep + "_BIN_DIR");
        binDirNode.append_attribute("value", currentFinder.getBinaryDir());
    }

    std::map<std::string, RapidXmlWriterNode> existingNodes;

    for (auto app : *scenario->getApplications())
    {
        if (!existingNodes.count(app->getEffectiveNodeName()))
        {
            RapidXmlWriterNode node = applicationNode.append_node("node");
            node.append_attribute("name", app->getEffectiveNodeName());
            existingNodes.insert(make_pair(app->getEffectiveNodeName(), node));
        }

        generateComponent(app);
        RapidXmlWriterNode includeNode = existingNodes.find(app->getEffectiveNodeName())->second.append_node("include");
        if (app->getInstanceName().empty())
        {
            includeNode.append_attribute("file", "icegrid/" + app->getName() + ".icegrid.xml");
        }
        else
        {
            includeNode.append_attribute("file", "icegrid/" + app->getName() + "." + app->getInstanceName() + ".icegrid.xml");
        }
    }

    writer.saveToFile(scenario->getFolder() + "/" +  scenario->getName() + ".icegrid.xml", true);
}

void IceGridXmlGenerator::generateRemoteApplication(Data_Structure::ScenarioPtr scenario)
{
    RapidXmlWriter writer;
    RapidXmlWriterNode rootNode = writer.createRootNode("icegrid");
    CMakePackageFinder finder(scenario->getPackage()->getName());

    RapidXmlWriterNode applicationNode = rootNode.append_node("application");
    applicationNode.append_attribute("name", scenario->getName());
    applicationNode.append_attribute("import-default-templates", "true");

    RapidXmlWriterNode includeNode = applicationNode.append_node("include");
    includeNode.append_attribute("file", ArmarXDataPath::GetDefaultUserConfigPath() + "/icegrid-sync-variables.icegrid.xml");

    RapidXmlWriterNode variableScenarioDirNode = applicationNode.append_node("variable");
    variableScenarioDirNode.append_attribute("name", "ARMARX_SCENARIO_DIR");
    variableScenarioDirNode.append_attribute("value", "${ARMARX_SYNC_DIR}/share/" + scenario->getPackage()->getName() + "/scenarios/" + scenario->getName());

    RapidXmlWriterNode variableScenarioDependencyNode = applicationNode.append_node("variable");
    variableScenarioDependencyNode.append_attribute("name", "ARMARX_SCENARIO_DEPENDENCY_CONFIG");
    variableScenarioDependencyNode.append_attribute("value", "dependencies.remote.cfg");

    RapidXmlWriterNode variablePackageConfigDir = applicationNode.append_node("variable");
    variablePackageConfigDir.append_attribute("name", "ARMARX_PACKAGE_CONFIG_DIR");
    variablePackageConfigDir.append_attribute("value", "${ARMARX_SYNC_DIR}/share/" + scenario->getPackage()->getName() + "/config");

    RapidXmlWriterNode variableArmarXLDPath = applicationNode.append_node("variable");
    variableArmarXLDPath.append_attribute("name", "ArmarX_LD_LIB_PATH");
    variableArmarXLDPath.append_attribute("value", "${ARMARX_SYNC_DIR}/lib");

    RapidXmlWriterNode variableArmarXPath = applicationNode.append_node("variable");
    variableArmarXPath.append_attribute("name", "ArmarX_PATH");
    variableArmarXPath.append_attribute("value", "");
    //    variableArmarXPath.append_attribute("value", "${ARMARX_SYNC_DIR}/bin:${ARMARX_SYNC_DIR}/usr/bin");

    for (auto dep : Parser::DependenciesGenerator::getDependencieTree(scenario->getPackage()->getName()))
    {
        RapidXmlWriterNode binDirNode = applicationNode.append_node("variable");
        binDirNode.append_attribute("name", dep + "_BIN_DIR");
        binDirNode.append_attribute("value", "${ARMARX_SYNC_DIR}/bin");
    }

    std::map<std::string, RapidXmlWriterNode> existingNodes;

    for (auto app : *scenario->getApplications())
    {
        if (!existingNodes.count(app->getEffectiveNodeName()))
        {
            RapidXmlWriterNode node = applicationNode.append_node("node");
            node.append_attribute("name", app->getEffectiveNodeName());
            existingNodes.insert(make_pair(app->getEffectiveNodeName(), node));
        }

        generateComponent(app);
        RapidXmlWriterNode includeNode = existingNodes.find(app->getEffectiveNodeName())->second.append_node("include");
        if (app->getInstanceName().empty())
        {
            includeNode.append_attribute("file", "icegrid/" + app->getName() + ".icegrid.xml");
        }
        else
        {
            includeNode.append_attribute("file", "icegrid/" + app->getName() + "." + app->getInstanceName() + ".icegrid.xml");
        }
    }

    writer.saveToFile(scenario->getFolder() + "/" +  scenario->getName() + ".remote.icegrid.xml", true);
}

void IceGridXmlGenerator::generateComponent(Data_Structure::ApplicationInstancePtr app)
{
    RapidXmlWriter writer;
    RapidXmlWriterNode rootNode = writer.createRootNode("icegrid");

    RapidXmlWriterNode applicationNode = rootNode.append_node("server-instance");
    applicationNode.append_attribute("template", "ArmarXComponentTemplate");
    applicationNode.append_attribute("component", app->getName());
    applicationNode.append_attribute("componentId", Exec::IceStatusReader::GetApplicationId(app));
    applicationNode.append_attribute("componentExeDir", "${" + app->getPackageName() + "_BIN_DIR}");
    applicationNode.append_attribute("configDomain", app->getConfigDomain());

    std::string armarXConfigValue = (app->getScenario()->isGlobalConfigFileexistent() ? app->getScenario()->getGlobalConfigPath() + "," : "")
                                    + (app->getInstanceName().empty() ? "${ARMARX_SCENARIO_DIR}/config/" + app->getName() + ".cfg" : "${ARMARX_SCENARIO_DIR}/config/" + app->getName() + "." + app->getInstanceName() + ".cfg");

    applicationNode.append_attribute("ArmarXConfig", armarXConfigValue);


    //    if (app->getInstanceName().empty())
    //    {
    //        armarXConfigValue
    //        applicationNode.append_attribute("ArmarXConfig", "${ARMARX_SCENARIO_DIR}/config/global.cfg,${ARMARX_SCENARIO_DIR}/config/" + app->getName() + ".cfg");
    //    }
    //    else
    //    {
    //        applicationNode.append_attribute("ArmarXConfig", "${ARMARX_SCENARIO_DIR}/config/global.cfg,${ARMARX_SCENARIO_DIR}/config/" + app->getName() + "." + app->getInstanceName() + ".cfg");
    //    }

    applicationNode.append_attribute("ArmarXDependenciesConfig", "${ARMARX_PACKAGE_CONFIG_DIR}/${ARMARX_SCENARIO_DEPENDENCY_CONFIG}");
    applicationNode.append_attribute("ArmarXLoggingGroup", app->getScenario()->getName());

    std::filesystem::create_directory(app->getScenario()->getFolder() + "/icegrid");
    if (app->getInstanceName().empty())
    {
        writer.saveToFile(app->getScenario()->getFolder() + "/icegrid/" + app->getName() + ".icegrid.xml", true);
    }
    else
    {
        writer.saveToFile(app->getScenario()->getFolder() + "/icegrid/" + app->getName() + "." + app->getInstanceName() + ".icegrid.xml", true);
    }
}

IceGrid::ApplicationDescriptor IceGridXmlGenerator::generateEmptyLocalApplicationDescriptor(Data_Structure::ScenarioPtr scenario, const IceGrid::AdminPrx iceAdmin)
{
    CMakePackageFinder finder(scenario->getPackage()->getName());

    IceGrid::ApplicationDescriptor result;
    result.name = scenario->getName();

    IceGrid::StringStringDict variables;
    variables["ARMARX_SYNC_DIR"] = getDefaultSyncFile();
    variables["ARMARX_SCENARIO_DIR"] = scenario->getFolder();
    variables["ARMARX_SCENARIO_DEPENDENCY_CONFIG"] = "dependencies.cfg";
    variables["ARMARX_PACKAGE_CONFIG_DIR"] = finder.getConfigDir();
    variables["ArmarX_LD_LIB_PATH"] = "";//"${ARMARX_SYNC_DIR}/lib:${ARMARX_SYNC_DIR}/usr/lib";
    variables["ArmarX_PATH"] = "";//"${ARMARX_SYNC_DIR}/bin:${ARMARX_SYNC_DIR}/usr/bin";
    variables["ARMARX_LOGGING_GROUP"] = scenario->getName();

    for (auto dep : Parser::DependenciesGenerator::getDependencieTree(scenario->getPackage()->getName()))
    {
        CMakePackageFinder currentFinder(dep);
        variables[dep + "_BIN_DIR"] = currentFinder.getBinaryDir();
    }

    result.variables = variables;

    IceGrid::ApplicationDescriptor defaultDesc = iceAdmin->getDefaultApplicationDescriptor();
    result.serverTemplates = defaultDesc.serverTemplates;
    auto v = IceGrid::ServerDescriptorPtr::dynamicCast(result.serverTemplates["ArmarXComponentTemplate"].descriptor.get());
    ARMARX_CHECK_EXPRESSION(v);
    //    v->activationTimeout = "10";
    for (auto variable : scenario->getIceEnviromentVariables())
    {
        v->envs.push_back(variable.first + "=" + variable.second);
    }

    result.serviceTemplates = defaultDesc.serviceTemplates;

    return result;
}

IceGrid::ApplicationDescriptor IceGridXmlGenerator::generateLocalApplicationDescriptor(Data_Structure::ScenarioPtr scenario, const IceGrid::AdminPrx iceAdmin)
{
    IceGrid::ApplicationDescriptor result = generateEmptyLocalApplicationDescriptor(scenario, iceAdmin);

    for (auto app : *scenario->getApplications())
    {
        if (!app->getEnabled())
        {
            continue;
        }

        result.nodes[app->getEffectiveNodeName()].serverInstances.push_back(generateComponentNode(app));
    }

    return result;
}

IceGrid::ApplicationDescriptor IceGridXmlGenerator::generateEmptyRemoteApplicationDescriptor(Data_Structure::ScenarioPtr scenario, const IceGrid::AdminPrx iceAdmin)
{
    IceGrid::ApplicationDescriptor result;
    result.name = scenario->getName();

    IceGrid::StringStringDict variables;
    variables["ARMARX_SYNC_DIR"] = getDefaultSyncFile();
    variables["ARMARX_SCENARIO_DIR"] = "${ARMARX_SYNC_DIR}/share/" + scenario->getPackage()->getName() + "/scenarios/" + scenario->getName();
    variables["ARMARX_SCENARIO_DEPENDENCY_CONFIG"] = "dependencies.remote.cfg";
    variables["ARMARX_PACKAGE_CONFIG_DIR"] = "${ARMARX_SYNC_DIR}/share/" + scenario->getPackage()->getName() + "/config";
    variables["ArmarX_LD_LIB_PATH"] = "${ARMARX_SYNC_DIR}/lib:${ARMARX_SYNC_DIR}/usr/lib";
    variables["ArmarX_PATH"] = "${ARMARX_SYNC_DIR}/bin:${ARMARX_SYNC_DIR}/usr/bin";
    variables["ARMARX_LOGGING_GROUP"] = scenario->getName();
    //    variables["CMAKE_PREFIX_PATH"] = "${ARMARX_SYNC_DIR}";


    for (auto dep : Parser::DependenciesGenerator::getDependencieTree(scenario->getPackage()->getName()))
    {
        variables[dep + "_BIN_DIR"] = "${ARMARX_SYNC_DIR}/bin";
    }

    result.variables = variables;

    IceGrid::ApplicationDescriptor defaultDesc = iceAdmin->getDefaultApplicationDescriptor();
    result.serverTemplates = defaultDesc.serverTemplates;
    auto v = IceGrid::ServerDescriptorPtr::dynamicCast(result.serverTemplates["ArmarXComponentTemplate"].descriptor.get());
    ARMARX_CHECK_EXPRESSION(v);
    //    v->activationTimeout = "10";
    for (auto variable : scenario->getIceEnviromentVariables())
    {
        v->envs.push_back(variable.first + "=" + variable.second);
    }

    result.serviceTemplates = defaultDesc.serviceTemplates;

    return result;
}

IceGrid::ApplicationDescriptor IceGridXmlGenerator::generateRemoteApplicationDescriptor(Data_Structure::ScenarioPtr scenario, const IceGrid::AdminPrx iceAdmin)
{
    //CMakePackageFinder finder(scenario->getPackage()->getName());

    IceGrid::ApplicationDescriptor result = generateEmptyRemoteApplicationDescriptor(scenario, iceAdmin);

    for (auto app : *scenario->getApplications())
    {
        if (!app->getEnabled())
        {
            continue;
        }
        result.nodes[app->getEffectiveNodeName()].serverInstances.push_back(generateComponentNode(app));
    }

    return result;
}

IceGrid::ServerInstanceDescriptor IceGridXmlGenerator::generateComponentNode(Data_Structure::ApplicationInstancePtr app)
{
    IceGrid::ServerInstanceDescriptor result;
    result._cpp_template = "ArmarXComponentTemplate";
    result.parameterValues["component"] = app->getName();
    result.parameterValues["componentId"] = Exec::IceStatusReader::GetApplicationId(app);
    result.parameterValues["componentExeDir"] = "${" + app->getPackageName() + "_BIN_DIR}";

    std::string armarXConfigValue = (app->getScenario()->isGlobalConfigFileexistent() ? app->getScenario()->getGlobalConfigPath() + "," : "")
                                    + (app->getInstanceName().empty() ? "${ARMARX_SCENARIO_DIR}/config/" + app->getName() + ".cfg" : "${ARMARX_SCENARIO_DIR}/config/" + app->getName() + "." + app->getInstanceName() + ".cfg");

    result.parameterValues["configDomain"] = app->getConfigDomain();
    result.parameterValues["ArmarXConfig"] = armarXConfigValue;
    result.parameterValues["ArmarXDependenciesConfig"] = "${ARMARX_PACKAGE_CONFIG_DIR}/${ARMARX_SCENARIO_DEPENDENCY_CONFIG}";
    result.parameterValues["ArmarXLoggingGroup"] = app->getScenario()->getName();
    result.parameterValues["activationType"] = app->getIceAutoRestart() ? "always" : "manual";
    result.parameterValues["activationTimeout"] = "10";


    return result;
}

std::string IceGridXmlGenerator::getSyncDirFromFile(std::string path)
{
    RapidXmlReaderPtr doc = RapidXmlReader::FromFile(path);
    RapidXmlReaderNode rootNode = doc->getRoot();
    try
    {
        return rootNode.first_node("variable").attribute_value("value");
    }
    catch (exceptions::local::RapidXmlReaderException& exception)
    {
        ARMARX_DEBUG_S << "Your icegrid-sync-variables file is ill formatted (Error: " << exception.what() << ")";
        return "";
    }
}

void IceGridXmlGenerator::setDefaultSyncFileDir(std::string path)
{
    RapidXmlWriter doc;
    RapidXmlWriterNode root = doc.createRootNode("icegrid");
    RapidXmlWriterNode variableNode = root.append_node("variable");
    variableNode.append_attribute("name", "ARMARX_SYNC_DIR");
    variableNode.append_attribute("value", path);

    doc.saveToFile(ArmarXDataPath::GetDefaultUserConfigPath() + "/icegrid-sync-variables.icegrid.xml", true);
}

IceGrid::ApplicationUpdateDescriptor IceGridXmlGenerator::generateUpdateDescriptor(IceGrid::ApplicationDescriptor descriptor)
{
    IceGrid::ApplicationUpdateDescriptor result;
    result.name = descriptor.name;
    //There is currently no simple way to deep copy these constructs so just reuse the needed ones
    for (auto nodes : descriptor.nodes)
    {
        IceGrid::NodeUpdateDescriptor nodeUpdate;
        nodeUpdate.name = nodes.first;
        nodeUpdate.serverInstances = nodes.second.serverInstances;
        //nodeUpdate.description = nodes.second.description;
        nodeUpdate.propertySets = nodes.second.propertySets;
        nodeUpdate.serverInstances = nodes.second.serverInstances;
        nodeUpdate.servers = nodes.second.servers;
        nodeUpdate.variables = nodes.second.variables;
        result.nodes.push_back(nodeUpdate);
    }

    result.serverTemplates = descriptor.serverTemplates;
    result.serviceTemplates = descriptor.serviceTemplates;
    result.variables = result.variables;

    return result;
}

std::string IceGridXmlGenerator::getDefaultSyncFile()
{
    return getSyncDirFromFile(ArmarXDataPath::GetDefaultUserConfigPath() + "/icegrid-sync-variables.icegrid.xml");
}

/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#ifndef ARMARX_ScenarioManager_ICEGRIDXMLGENERATOR_H
#define ARMARX_ScenarioManager_ICEGRIDXMLGENERATOR_H

#include <string>
#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Scenario.h>
#include <ArmarXCore/core/ArmarXManager.h>
#include <IceGrid/Descriptor.h>
#include <IceGrid/Admin.h>

namespace ScenarioManager::Generator
{
    class IceGridXmlGenerator
    {
    public:
        void generateLocalApplication(Data_Structure::ScenarioPtr scenario);
        void generateRemoteApplication(Data_Structure::ScenarioPtr scenario);
        void generateComponent(Data_Structure::ApplicationInstancePtr app);

        IceGrid::ApplicationDescriptor generateLocalApplicationDescriptor(Data_Structure::ScenarioPtr scenario, const IceGrid::AdminPrx iceAdmin);
        IceGrid::ApplicationDescriptor generateEmptyLocalApplicationDescriptor(Data_Structure::ScenarioPtr scenario, const IceGrid::AdminPrx iceAdmin);
        IceGrid::ApplicationDescriptor generateRemoteApplicationDescriptor(Data_Structure::ScenarioPtr scenario, const IceGrid::AdminPrx iceAdmin);
        IceGrid::ApplicationDescriptor generateEmptyRemoteApplicationDescriptor(Data_Structure::ScenarioPtr scenario, const IceGrid::AdminPrx iceAdmin);

        IceGrid::ServerInstanceDescriptor generateComponentNode(Data_Structure::ApplicationInstancePtr app);


        IceGrid::ApplicationUpdateDescriptor generateUpdateDescriptor(IceGrid::ApplicationDescriptor descriptor);

        std::string getDefaultSyncFile();
        void setDefaultSyncFileDir(std::string path);
    private:
        std::string getSyncDirFromFile(std::string path);
    };
}

#endif

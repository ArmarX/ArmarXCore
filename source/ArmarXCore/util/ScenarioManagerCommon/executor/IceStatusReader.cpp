#include "IceStatusReader.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

using namespace ScenarioManager;
using namespace Exec;
using namespace Data_Structure;

IceStatusReader::IceStatusReader(const IceGrid::AdminPrx admin) : admin(admin)
{
}

std::string IceStatusReader::getStatus(Data_Structure::ApplicationInstancePtr application)
{
    ARMARX_CHECK_EXPRESSION(application);
    fetch(application);
    if (statusMap.count(GetApplicationId(application)))
    {
        return statusMap[GetApplicationId(application)];
    }
    else
    {
        return ApplicationStatus::Unknown;
    }
}

void IceStatusReader::fetch(Data_Structure::ApplicationInstancePtr application)
{
    //    ARMARX_WARNING << deactivateSpam(5, application->getName()) << "Getting status for " << application->getName();
    ARMARX_CHECK_EXPRESSION(application);
    ARMARX_CHECK_EXPRESSION(admin);
    auto appId = GetApplicationId(application);
    try
    {
        IceGrid::ServerState state = admin->getServerState(appId);

        switch (state)
        {
            case IceGrid::ServerState::Active:
            case IceGrid::ServerState::Deactivating:
            case IceGrid::ServerState::Destroying:
                statusMap[appId] = ApplicationStatus::Running;
                break;
            case IceGrid::ServerState::Inactive:
            {
                auto pid = admin->getServerPid(appId);
                if (pid <= 0)
                {
                    application->setPid(-1);
                }
                statusMap[appId] = ApplicationStatus::Inactive;
            }
            break;
            case IceGrid::ServerState::Activating:
            case IceGrid::ServerState::ActivationTimedOut:
            case IceGrid::ServerState::Destroyed:
                application->setPid(-1);
                statusMap[appId] = ApplicationStatus::Stopped;
                break;
            default:
                statusMap[appId] = ApplicationStatus::Stopped;
                break;
        }
    }
    catch (IceGrid::ServerNotExistException& ex)
    {
        application->setPid(-1);
        statusMap[appId] = ApplicationStatus::Stopped;

    }
    catch (IceGrid::NodeUnreachableException& ex)
    {
        statusMap[appId] = ApplicationStatus::Stopped;
    }
    catch (IceGrid::DeploymentException& ex)
    {
        application->setPid(-1);
        statusMap[appId] = ApplicationStatus::Stopped;
    }
    catch (...)
    {
        ARMARX_WARNING_S << deactivateSpam(1) << "Unknown error while getting IceApplication state";
        application->setPid(-1);
        statusMap[appId] = ApplicationStatus::Stopped;
    }
}

std::string IceStatusReader::GetApplicationId(const ApplicationInstancePtr& application)
{
    return application->getScenario()->getName() + "_" + application->getName() +
           (application->getInstanceName().empty() ? "" : "_") + application->getInstanceName() +
           "Server";
}


/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nicola Miskowiec
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "../data_structure/ApplicationInstance.h"
#include "ApplicationStopper.h"

#include <IceGrid/Admin.h>


namespace ScenarioManager::Exec
{
    class IceStopper : public ApplicationStopper
    {
        const IceGrid::AdminPrx admin;

    public:
        IceStopper(const IceGrid::AdminPrx admin);
        void stop(Data_Structure::ApplicationInstancePtr application) override;

        void kill(Data_Structure::ApplicationInstancePtr application) override;

        virtual void removeApplication(Data_Structure::ApplicationInstancePtr application, StatusManager statusManager) override;
        virtual void removeScenario(Data_Structure::ScenarioPtr scenario, StatusManager statusManager) override;
    };

}

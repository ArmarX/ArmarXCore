#ifndef ICESTATUSREADER_H
#define ICESTATUSREADER_H

#include "../data_structure/ApplicationInstance.h"
#include "../parser/StatusManager.h"

#include <map>
#include <string>
#include <IceGrid/Admin.h>

namespace ScenarioManager::Exec
{
    class IceStatusReader
    {
    public:
        IceStatusReader(const IceGrid::AdminPrx admin);

        virtual std::string getStatus(Data_Structure::ApplicationInstancePtr application);
        void fetch(Data_Structure::ApplicationInstancePtr application);
        static std::string GetApplicationId(const ApplicationInstancePtr& application);
    private:
        const IceGrid::AdminPrx admin;
        std::map<std::string, std::string> statusMap;
    };
}

#endif // ICESTATUSREADER_H

/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nicola Miskowiec
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "ApplicationStarter.h"
#include "../data_structure/ApplicationInstance.h"
#include "../parser/StatusManager.h"
#include "IceStatusReader.h"

#include <map>
#include <IceGrid/Admin.h>

namespace ScenarioManager::Exec
{
    class IceStarter : public ApplicationStarter
    {
    private:
        const IceGrid::AdminPrx admin;
        IceStatusReader statusReader;

    public:
        IceStarter(const IceGrid::AdminPrx admin);

        virtual void startApplication(Data_Structure::ApplicationInstancePtr application, StatusManager statusManager, const std::string& commandLineParameters = "", bool printOnly = false) override;
        virtual void startScenario(Data_Structure::ScenarioPtr scenario, StatusManager statusManager, const std::string& commandLineParameters = "", bool printOnly = false) override;

        virtual void deployApplication(Data_Structure::ApplicationInstancePtr application, StatusManager statusManager, const std::string& commandLineParameters = "", bool printOnly = false) override;
        virtual void deployScenario(Data_Structure::ScenarioPtr scenario, StatusManager statusManager, const std::string& commandLineParameters = "", bool printOnly = false) override;

        virtual std::string getStatus(Data_Structure::ApplicationInstancePtr application, StatusManager statusManager) override;

        virtual bool isApplicationDeployed(Data_Structure::ApplicationInstancePtr application) override;
        virtual bool isScenarioDeployed(Data_Structure::ScenarioPtr scenario) override;
    };

}

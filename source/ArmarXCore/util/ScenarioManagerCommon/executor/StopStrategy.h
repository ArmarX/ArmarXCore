#pragma once

#include "../data_structure/ApplicationInstance.h"
#include "../parser/StatusManager.h"
#include "ApplicationStopper.h"

using ApplicationInstancePtr = std::shared_ptr<ScenarioManager::Data_Structure::ApplicationInstance>;
namespace ScenarioManager::Data_Structure
{
    using ApplicationInstancePtr = std::shared_ptr<ApplicationInstance>;
}
namespace ScenarioManager::Exec
{
    class ApplicationStopper;
    typedef std::shared_ptr<ApplicationStopper> ApplicationStopperPtr;

    /**
    * @class StopStrategy
    * @ingroup exec
    * @brief Interface for classes that define how an application get stopped.
    */
    class StopStrategy
    {
    protected:
        ApplicationStopperPtr stopper;

    public:
        StopStrategy(ApplicationStopperPtr stopper);

        /**
        * Stops an application. Implement this method to define the behaviour when stopping applications.
        * @param application application to be stopped.
        */
        virtual void stop(Data_Structure::ApplicationInstancePtr application) = 0;
        virtual void removeApplication(Data_Structure::ApplicationInstancePtr application, StatusManager statusManager);
        virtual void removeScenario(Data_Structure::ScenarioPtr scenario, StatusManager statusManager);
    };

    using StopStrategyPtr = std::shared_ptr<StopStrategy>;
}

/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nicola Miskowiec
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "ApplicationStarter.h"
#include "../parser/StatusManager.h"
#include "../data_structure/ApplicationInstance.h"
#include "../data_structure/Scenario.h"
#include "StopStrategy.h"
#include "ApplicationStarter.h"
#include <memory>
#include <future>
#include <map>

namespace ScenarioManager
{
    class StatusManager;
}

namespace ScenarioManager::Exec
{
    class StopStrategy;
    using StopStrategyPtr = std::shared_ptr<StopStrategy>;

    /**
    * @class Executor
    * @ingroup exec
    * @brief Starts, stops and restarts applications and scenarios.
    * Can also be used to request the status of an application. The proper system-specific stopstrategy and starter have to be set in the constructor.
    */
    class Executor
    {

    private:
        StopStrategyPtr defaultStopStrategy;
        ApplicationStarterPtr defaultStartStrategy;
        std::map<Data_Structure::ScenarioPtr, StopStrategyPtr> stopStrategy;
        std::map<Data_Structure::ScenarioPtr, ApplicationStarterPtr> starter;
        StatusManager statusManager;


        void asyncApplicationRestart(Data_Structure::ApplicationInstancePtr application, bool printOnly);

        void asyncScenarioStop(Data_Structure::ScenarioPtr scenario);
        std::string asyncGetScenarioStatus(Data_Structure::ScenarioPtr scenario);
        void asyncScenarioRestart(Data_Structure::ScenarioPtr scenario, bool printOnly);

    public:
        /**
        * Constructor that sets StopStrategy and ApplicationStarter.
        * @param strategy Strategy used to stop applications.
        * @param starter Starter used to start applications and request their status.
        */
        Executor(StopStrategyPtr strategy, ApplicationStarterPtr starter);

        /**
        * Starts an application.
        * @param application application to be started.
        * @return {@code true} if the application was successfully started
        */
        std::future<void> startApplication(Data_Structure::ApplicationInstancePtr application, bool printOnly = false, const std::string& commandLineParameters = "");
        std::future<void> deployApplication(Data_Structure::ApplicationInstancePtr application, bool printOnly = false, const std::string& commandLineParameters = "");

        /**
        * Stops an application
        * @param application application to be stopped
        * @return {@code true} if the application was successfully stopped
        */
        std::future<void> stopApplication(Data_Structure::ApplicationInstancePtr application);
        std::future<void> removeApplication(Data_Structure::ApplicationInstancePtr application);

        /**
        * Restarts an application. Stops and starts an application.
        * @param application application to be restarted
        * @return {@code true} if the application was successfully restarted. Successfull restart requires successfull stop and successfull start.
        * An already stopped application also counts as successfully restarted, if successfully started.
        */
        std::future<void> restartApplication(Data_Structure::ApplicationInstancePtr application, bool printOnly = false);

        /**
        * Returns the status of an application. Uses the ApplicationStarter to request the status, therefore the proper
        * system-specific starter has to be set, even to use this method.
        * @param application application whose status is returned
        * @return status of the application
        */
        std::string getApplicationStatus(Data_Structure::ApplicationInstancePtr application);

        /**
        * Starts a scenario. Iterates over the applications in the scenario and starts them.
        * @param scenario scenario to be started
        * @return {@code true} if all applications were successfully started
        */
        std::future<void> startScenario(Data_Structure::ScenarioPtr scenario, bool printOnly = false, const std::string& commandLineParameters = "");
        std::future<void> deployScenario(Data_Structure::ScenarioPtr scenario, bool printOnly = false, const std::string& commandLineParameters = "");

        /**
        * Stops a scenario. Iterates over the applications in the scenario and stops them.
        * @param scenario scenario to be stopped
        * @return {@code true} if all applications were successfully stopped
        */
        std::future<void> stopScenario(Data_Structure::ScenarioPtr scenario);
        std::future<void> removeScenario(Data_Structure::ScenarioPtr scenario);

        /**
        * Restarts a scenario. Iterates over the applications in the scenario and restarts them.
        * @param scenario scenario to be restarted
        * @return {@code true} if all applications were successfully restarted
        */
        std::future<void> restartScenario(Data_Structure::ScenarioPtr scenario, bool printOnly = false);

        /**
        * Generates an XML file of the given application and saves it in the specified path.
        * If there already is an XML file at that location it only reloads the file if the executable is more recently changed
        * than the Xml file
        * @param application Application whose XML file is to be generate
        * @param path location to save the XML, must be a folder
        * @param reload forces to reload the cached XML
        * @param set if true the newly Cached Xml gets loaded into the Application (Warning: If you load an ApplicationInstance the cfg parameter have to be reloaded)
        */
        void loadAndSetCachedProperties(Data_Structure::ApplicationPtr application, std::string path, bool reload = false, bool set = true);

        /**
        * Sets the strategy this Executor uses to stop applications. Needs to be system specific.
        * Use the Factory-Classes to get the proper strategy for your system.
        * @param strategy strategy to be set
        * @see StopStrategyFactory
        */
        void setDefaultStopStrategy(StopStrategyPtr strategy);
        void setStopStrategy(StopStrategyPtr strategy, Data_Structure::ScenarioPtr scenario);

        bool isApplicationDeployed(Data_Structure::ApplicationInstancePtr application);
        bool isScenarioDeployed(Data_Structure::ScenarioPtr scenario);

        /**
        * Sets the starter this Executor uses to start applications and request statuses. Needs to be system specific.
        * Use the Factory-Classes to get the proper starter for your system.
        * @param starter starter to be set
        * @see StarterFactory
        * @see StarterFactoryLinux
        */
        void setDefaultStarter(ApplicationStarterPtr starter);
        void setStarter(ApplicationStarterPtr starter, Data_Structure::ScenarioPtr scenario);

        ApplicationStarterPtr getStarter(Data_Structure::ScenarioPtr scenario);
        StopStrategyPtr getStopStrategy(Data_Structure::ScenarioPtr scenario);
        StopStrategyPtr getDefaultStopStrategy();
    };
    using ExecutorPtr = std::shared_ptr<Executor>;
}

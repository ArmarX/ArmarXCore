/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef ARMARX_ScenarioManager_KILL_H
#define ARMARX_ScenarioManager_KILL_H

#include "../data_structure/ApplicationInstance.h"
#include "StopStrategy.h"

namespace ScenarioManager:: Exec
{
    /**
    * @class Stop
    * @ingroup exec
    * @brief Simply stops the application, using the given ApplicationStopper.
    * This StopStrategy can be made system-specific by switching out its ApplicationStopper.
    */
    class Kill : public StopStrategy
    {
    public:
        /**
        * Constructor that sets the ApplicationStopper used by this StopStrategy.
        * @param stopper stopper used in this strategy
        */
        Kill(ApplicationStopperPtr stopper);

        /**
        * Stops an application.
        * @param application application to be stopped
        */
        void stop(Data_Structure::ApplicationInstancePtr application);

        /**
        * Returns the stopper used by this StopStrategy.
        * @return stopper used by this strategy
        */
        ApplicationStopperPtr getStopper();
    };

}

#endif

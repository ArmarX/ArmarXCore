#include "Kill.h"

using namespace ScenarioManager;
using namespace Exec;

Kill::Kill(ApplicationStopperPtr stopper) : StopStrategy(stopper)
{

}

void Kill::stop(Data_Structure::ApplicationInstancePtr application)
{
    stopper->kill(application);
    application->setStatusWriteBlock(false);
}

ApplicationStopperPtr Kill::getStopper()
{
    return this->getStopper();
}

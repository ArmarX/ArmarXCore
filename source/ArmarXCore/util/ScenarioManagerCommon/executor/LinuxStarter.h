/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nicola Miskowiec
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "../data_structure/ApplicationInstance.h"
#include "ApplicationStarter.h"
#include "../parser/StatusManager.h"


namespace ScenarioManager::Exec
{
    /**
    * @class LinuxStarter
    * @ingroup exec
    * @brief Starts applications in Linux. Shouldn't be used in any other OS, due to system calls being used.
    * Applications started this way will still run, even when the main application dies.
    * Can also be used to request the status of an application.
    */
    class LinuxStarter : public ApplicationStarter
    {
    public:
        /**
        * Starts an application. Forks, creates a daemon and starts the executable.
        * @param application application to be started
        */
        virtual void startApplication(Data_Structure::ApplicationInstancePtr application, StatusManager statusManager, const std::string& commandLineParameters = "", bool printOnly = false) override;
        virtual void startScenario(Data_Structure::ScenarioPtr scenario, StatusManager statusManager, const std::string& commandLineParameters = "", bool printOnly = false) override;

        virtual void deployApplication(Data_Structure::ApplicationInstancePtr application, StatusManager statusManager, const std::string& commandLineParameters = "", bool printOnly = false) override;
        virtual void deployScenario(Data_Structure::ScenarioPtr scenario, StatusManager statusManager, const std::string& commandLineParameters = "", bool printOnly = false) override;

        /**
        * Returns the status of an application.
        * @param application application whose status is returned
        * @return status of the application
        */
        virtual std::string getStatus(Data_Structure::ApplicationInstancePtr application, StatusManager statusManager) override;

        virtual bool isApplicationDeployed(Data_Structure::ApplicationInstancePtr application) override;
        virtual bool isScenarioDeployed(Data_Structure::ScenarioPtr scenario) override;
    private:
        std::future<void> startApplicationAsync(Data_Structure::ApplicationInstancePtr application, StatusManager statusManager, const std::string& commandLineParameters = "", bool printOnly = false);

        /**
         * @brief getSystemAppName returns either the Process name of the application with the given pid, or an empty string if there is no such application
         * @param application
         * @return
         */
        std::string getSystemAppName(Data_Structure::ApplicationInstancePtr application);

        /**
         * @brief getSystemAppStatus returns an SystemStatus of the given application Possible states are ["Running", "Zombie"] if there is no app status it returns "Stopped"
         * @param application
         * @return
         */
        std::string getSystemAppStatus(Data_Structure::ApplicationInstancePtr application);
    };

}

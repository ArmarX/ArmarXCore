/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nicola Miskowiec
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "IceStarter.h"
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/util/OnScopeExit.h>
#include <ArmarXCore/util/ScenarioManagerCommon/generator/IceGridXmlGenerator.h>
#include <algorithm>

using namespace ScenarioManager;
using namespace Exec;
using namespace Generator;
using namespace Data_Structure;

IceStarter::IceStarter(const IceGrid::AdminPrx admin) : admin(admin), statusReader(admin) //IceAdmin interrupts calls if they take longer than 3 minutes
{
}

void IceStarter::startApplication(ApplicationInstancePtr application, StatusManager statusManager, const std::string& commandLineParameters, bool printOnly)
{
    ARMARX_ON_SCOPE_EXIT
    {
        application->setStatusWriteBlock(false);
    };
    if (!application->getEnabled())
    {
        return;
    }

    //    IceGridXmlGenerator generator;

    //    IceGrid::ServerInstanceDescriptor serverInstanceDescriptor = generator.generateComponentNode(application);
    //    std::vector<std::string> appNames = admin->getAllApplicationNames();

    try
    {
        auto appId = IceStatusReader::GetApplicationId(application);
        //        IceGrid::ServerInfo serverInfo = admin->getServerInfo(appId);
        if (!admin->isServerEnabled(appId))
        {
            admin->enableServer(appId, true);
        }
        if (admin->getServerState(appId) != IceGrid::Active)
        {
            admin->startServer(appId);
        }
        try
        {
            application->setPid(admin->getServerPid(appId));

        }
        catch (...)
        {
            ARMARX_INFO << deactivateSpam(10, application->getName()) << "Could not get PID for " << appId;
            application->setPid(-1);
        }
    }
    catch (IceGrid::ServerNotExistException& ex)
    {
        ARMARX_ERROR_S << "There is no Server for " << application->getName() << " - please make sure the Scenario is correctly deployed";

    }
    catch (IceGrid::ServerStartException& ex)
    {
        ARMARX_ERROR_S << "Could not start Server " << application->getName() << " Reason: " << ex.reason;

    }
    catch (IceGrid::NodeUnreachableException& ex)
    {
        ARMARX_ERROR_S << "Could not connect to the Node of  " << application->getName() << " - please make sure the Scenario is correctly deployed";

    }
    catch (IceGrid::DeploymentException& ex)
    {
        ARMARX_ERROR_S << "Could not connect deploy  " << application->getName() << " - please make sure the Scenario is correctly deployed";

    }

}

void IceStarter::deployApplication(Data_Structure::ApplicationInstancePtr application, StatusManager statusManager, const std::string& commandLineParameters, bool printOnly)
{
    ARMARX_ON_SCOPE_EXIT
    {
        application->setStatusWriteBlock(false);
    };
    if (!application->getEnabled())
    {
        return;
    }

    IceGridXmlGenerator generator;

    IceGrid::ServerInstanceDescriptor serverInstanceDescriptor = generator.generateComponentNode(application);
    std::vector<std::string> appNames = admin->getAllApplicationNames();
    //There already is an IceApplication so just register this app and update
    if (std::find(appNames.begin(), appNames.end(), application->getScenario()->getName()) != appNames.end())
    {
        IceGrid::ApplicationInfo appInfo = admin->getApplicationInfo(application->getScenario()->getName());
        IceGrid::ApplicationDescriptor appDescriptor = appInfo.descriptor;

        //since we only want to update this app clear the server instance list
        appDescriptor.nodes[application->getEffectiveNodeName()].serverInstances.clear();
        appDescriptor.nodes[application->getEffectiveNodeName()].serverInstances.push_back(serverInstanceDescriptor);
        IceGrid::ApplicationUpdateDescriptor appUpdate = generator.generateUpdateDescriptor(appDescriptor);
        admin->updateApplication(appUpdate);
    }
    else   //in this case we need to create the IceApplication with just this app
    {
        ARMARX_INFO << application->getName() << " on node " << application->getEffectiveNodeName();

        IceGrid::ApplicationDescriptor appDescriptor;
        if (application->getScenario()->getScenarioDeploymentType() == ScenarioDeploymentType::Local)
        {
            appDescriptor = generator.generateEmptyLocalApplicationDescriptor(application->getScenario(), admin);
        }
        else
        {
            appDescriptor = generator.generateEmptyRemoteApplicationDescriptor(application->getScenario(), admin);
        }
        appDescriptor.nodes[application->getEffectiveNodeName()].serverInstances.push_back(serverInstanceDescriptor);
        admin->addApplication(appDescriptor);
        statusManager.setIceScenario(application->getScenario(), true);
    }

    try
    {
        application->setPid(admin->getServerPid(application->getName()));
    }
    catch (...)
    {
        ARMARX_INFO << deactivateSpam(10, application->getName()) << "Could not get PID for " << application->getName();
        application->setPid(-1);
    }

}

void IceStarter::deployScenario(Data_Structure::ScenarioPtr scenario, StatusManager statusManager, const std::string& commandLineParameters, bool printOnly)
{
    ARMARX_ON_SCOPE_EXIT
    {
        scenario->setStatusWriteBlock(false);
    };
    IceGridXmlGenerator generator;

    IceGrid::ApplicationDescriptor appDescriptor;
    if (scenario->getScenarioDeploymentType() == ScenarioDeploymentType::Local)
    {
        appDescriptor = generator.generateLocalApplicationDescriptor(scenario, admin);
    }
    else
    {
        appDescriptor = generator.generateRemoteApplicationDescriptor(scenario, admin);
    }

    try
    {
        std::vector<std::string> appNames = admin->getAllApplicationNames();
        std::vector<std::string> nodeNames = admin->getAllNodeNames();
        for (auto nodeName : scenario->getAllDeploymendNodeNames())
        {
            if (std::find(nodeNames.begin(), nodeNames.end(), nodeName) == nodeNames.end())
            {
                ARMARX_ERROR_S << "You are launching an Scenario with applications on an unlaunched Node. Either start the node or change the launch Nodes of the curresponding applications. Nodename is: " + nodeName;

                throw IceGrid::DeploymentException("You are launching an Scenario with applications on an unlaunched Node. Either start the node or change the launch Nodes of the curresponding applications. Nodename is: " + nodeName);
            }
        }

        if (std::find(appNames.begin(), appNames.end(), scenario->getName()) != appNames.end())
        {
            admin->updateApplication(generator.generateUpdateDescriptor(appDescriptor));
        }
        else
        {
            admin->addApplication(appDescriptor);
        }
        for (auto& node : appDescriptor.nodes)
        {
            for (IceGrid::ServerInstanceDescriptor& server : node.second.serverInstances)
            {
                auto name = server.parameterValues["componentId"];
                admin->enableServer(name, false);
            }
        }
    }
    catch (IceGrid::AccessDeniedException& ex)
    {
        ARMARX_ERROR_S << "Error while deploying Scenario " << scenario->getName() << " another application has the accesss rights to the IceGridAdmin. (Please try again)";

    }
    catch (IceGrid::DeploymentException& ex)
    {

        throw ex;
    }

    statusManager.setIceScenario(scenario, true);

}


void IceStarter::startScenario(Data_Structure::ScenarioPtr scenario, StatusManager statusManager, const std::string& commandLineParameters, bool printOnly)
{
    std::vector<std::string> appNames = admin->getAllApplicationNames();
    //std::vector<Ice::AsyncResultPtr> futures;
    auto admin = this->admin;
    std::thread
    {
        [admin, scenario, statusManager, commandLineParameters, printOnly, appNames]
        {
            ARMARX_ON_SCOPE_EXIT
            {
                scenario->setStatusWriteBlock(false);
            };
            std::map<Data_Structure::ApplicationInstancePtr, ::Ice::AsyncResultPtr> asyncResultMap;
            //Check if there is an deployed application
            if (std::find(appNames.begin(), appNames.end(), scenario->getName()) != appNames.end())
            {
                for (auto app : *scenario->getApplications())
                {
                    auto appId = IceStatusReader::GetApplicationId(app);
                    try
                    {
                        if (admin->getServerState(appId) == IceGrid::ServerState::Inactive)
                        {
                            ARMARX_DEBUG << "Enabling server";
                            admin->enableServer(appId, true);
                            ARMARX_DEBUG << "Starting server";
                            asyncResultMap[app] = admin->begin_startServer(appId);
                        }
                    }
                    catch (IceGrid::ServerNotExistException& ex)
                    {
                        ARMARX_INFO_S << "Could not start Server " << appId << " because it is not existent";
                    }
                    catch (IceGrid::ServerStartException& ex)
                    {
                        ARMARX_INFO_S << "Could not start Server " << appId << " because it had an starting error. \nReason :" << ex.reason;
                        if (scenario->getScenarioDeploymentType() == ScenarioDeploymentType::Remote)
                        {
                            IceGridXmlGenerator generator;
                            ARMARX_INFO_S << "Often this is related to an unknown launching path. The ScenarioManager thinks to find the binary on the host of " << app->getEffectiveNodeName() << " at: " <<  generator.getDefaultSyncFile() << "/bin";
                        }
                        throw ex;
                    }
                    catch (IceGrid::NodeUnreachableException& ex)
                    {
                        ARMARX_INFO_S << "Could not start Server " << appId << " because the node is unreachable";
                    }
                    catch (IceGrid::DeploymentException& ex)
                    {
                        ARMARX_INFO_S << "Could not start Server " << appId << " because there was an deployment error";
                    }
                }
                for (auto pair : asyncResultMap)
                {
                    auto app = pair.first;
                    auto appId = IceStatusReader::GetApplicationId(app);
                    try
                    {
                        admin->end_startServer(pair.second);
                        ARMARX_DEBUG << "Server " << appId << " started";

                    }
                    catch (IceGrid::ServerNotExistException& ex)
                    {
                        ARMARX_INFO_S << "Could not start Server " << appId << " because it is not existent";
                    }
                    catch (IceGrid::ServerStartException& ex)
                    {
                        ARMARX_INFO_S << "Could not start Server " << appId << " because it had an starting error. \nReason :" << ex.reason;
                        if (scenario->getScenarioDeploymentType() == ScenarioDeploymentType::Remote)
                        {
                            IceGridXmlGenerator generator;
                            ARMARX_INFO_S << "Often this is related to an unknown launching path. The ScenarioManager thinks to find the binary on the host of " << app->getEffectiveNodeName() << " at: " <<  generator.getDefaultSyncFile() << "/bin";
                        }
                        throw ex;
                    }
                    catch (IceGrid::NodeUnreachableException& ex)
                    {
                        ARMARX_INFO_S << "Could not start Server " << appId << " because the node is unreachable";
                    }
                    catch (IceGrid::DeploymentException& ex)
                    {
                        ARMARX_INFO_S << "Could not start Server " << appId << " because there was an deployment error";
                    }
                }
            }
            else   //in this case we need to deploy the IceApplication
            {
                ARMARX_ERROR_S << "There is no deployed IceApplication for scenario " << scenario->getName() << " - please deploy it before starting";
            }

            //    for (auto future : futures)
            //    {
            //        future->waitForCompleted();
            //    }
            for (auto app : *scenario->getApplications())
            {
                try
                {
                    app->setPid(admin->getServerPid(IceStatusReader::GetApplicationId(app)));
                    ARMARX_INFO << " PID of " << app->getName() << " is " << app->getPid();
                }
                catch (...)
                {
                    ARMARX_INFO << deactivateSpam(10, app->getName()) << "Could not get PID for " << IceStatusReader::GetApplicationId(app) + "Server";
                    app->setPid(-1);
                }
            }
        }
    }
    .detach();

}


std::string IceStarter::getStatus(ApplicationInstancePtr application, StatusManager statusManager)
{
    ARMARX_CHECK_EXPRESSION(application);
    // this async thread is completetly useless?!
    //    //Launch the fetching of the Status in a different Thread
    //    std::cout << "launching status reading in thread " << application->getName() << std::endl;
    //    std::packaged_task<void()> task(std::bind(&IceStatusReader::fetch, &statusReader, application));
    //    std::cout << "fetching future" << std::endl;
    //    std::future<void> result = task.get_future();
    //    std::cout << "got future" << std::endl;

    //    std::thread t(std::move(task));
    //    t.detach();
    //    std::cout << "thread detached" << std::endl;

    //Return the fetched Status
    return statusReader.getStatus(application);

}


bool IceStarter::isApplicationDeployed(Data_Structure::ApplicationInstancePtr application)
{
    std::vector<std::string> appNames = admin->getAllApplicationNames();
    //Check if there is an deployed application
    if (std::find(appNames.begin(), appNames.end(), application->getScenario()->getName()) != appNames.end())
    {
        IceGrid::ApplicationInfo info = admin->getApplicationInfo(application->getScenario()->getName());
        IceGrid::ServerInstanceDescriptorSeq serverInstances = info.descriptor.nodes[application->getEffectiveNodeName()].serverInstances;
        if (std::find_if(serverInstances.begin(), serverInstances.end(), [application](const IceGrid::ServerInstanceDescriptor & d)
    {
        return  d.parameterValues.at("component") == application->getName()
                    && d.parameterValues.at("componentId") == application->getInstanceName();
        }) != serverInstances.end())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

bool IceStarter::isScenarioDeployed(Data_Structure::ScenarioPtr scenario)
{
    std::vector<std::string> appNames = admin->getAllApplicationNames();
    //Check if there is an deployed application
    if (std::find(appNames.begin(), appNames.end(), scenario->getName()) != appNames.end())
    {
        return true;
    }
    else
    {
        return false;
    }
}


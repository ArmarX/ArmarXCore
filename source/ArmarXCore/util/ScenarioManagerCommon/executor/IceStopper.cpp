/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nicola Miskowiec
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "IceStatusReader.h"
#include "IceStopper.h"

#include "ArmarXCore/util/ScenarioManagerCommon/generator/IceGridXmlGenerator.h"

using namespace ScenarioManager;
using namespace Exec;
using namespace Data_Structure;
using namespace Generator;

IceStopper::IceStopper(const IceGrid::AdminPrx admin) : admin(admin)
{

}

void IceStopper::stop(ApplicationInstancePtr application)
{
    try
    {
        auto appId = IceStatusReader::GetApplicationId(application);
        ARMARX_VERBOSE << "Disabling " << appId;
        admin->enableServer(appId, false);
        ARMARX_VERBOSE << "Stopping " << appId;
        admin->stopServer(appId);
    }
    catch (IceGrid::ServerNotExistException& ex)
    {
        ARMARX_ERROR_S << "Unable to stop Application " << application->getName() << " because it is not existent";
    }
    catch (IceGrid::ServerStopException& ex)
    {
        ARMARX_ERROR_S << "Unable to stop Application " << application->getName() << " because the server does not react";
    }
    catch (IceGrid::NodeUnreachableException& ex)
    {
        ARMARX_ERROR_S << "Unable to stop Application " << application->getName() << " because the Server node is unreachable";
    }
    catch (IceGrid::DeploymentException& ex)
    {
        ARMARX_ERROR_S << "Unable to stop Application " << application->getName() << " because it is not deployed";
    }
}

void IceStopper::kill(ApplicationInstancePtr application)
{
    try
    {
        auto appId = IceStatusReader::GetApplicationId(application);
        ARMARX_VERBOSE << "Disabling " << appId;
        admin->enableServer(appId, false);
        ARMARX_VERBOSE << "Killing " << appId;
        admin->sendSignal(appId, "9");
    }
    catch (IceGrid::ServerNotExistException& ex)
    {
        ARMARX_ERROR_S << "Unable to kill Application " << application->getName() << " because it is not existent";
    }
    catch (IceGrid::BadSignalException& ex)
    {
        ARMARX_ERROR_S << "Unable to kill Application " << application->getName() << " because the signal is not recognized by the Server";
    }
    catch (IceGrid::NodeUnreachableException& ex)
    {
        ARMARX_ERROR_S << "Unable to kill Application " << application->getName() << " because the Server node is unreachable";
    }
    catch (IceGrid::DeploymentException& ex)
    {
        ARMARX_ERROR_S << "Unable to kill Application " << application->getName() << " because it is not deployed";
    }
}

void IceStopper::removeApplication(Data_Structure::ApplicationInstancePtr application, StatusManager statusManager)
{
    std::vector<std::string> appNames = admin->getAllApplicationNames();

    if (std::find(appNames.begin(), appNames.end(), application->getScenario()->getName()) != appNames.end())
    {
        try
        {
            IceGridXmlGenerator generator;
            IceGrid::ApplicationDescriptor descriptor = admin->getApplicationInfo(application->getScenario()->getName()).descriptor;
            IceGrid::ApplicationUpdateDescriptor appUpdate = generator.generateUpdateDescriptor(descriptor);
            auto location = std::find_if(appUpdate.nodes.begin(), appUpdate.nodes.end(), [application](const IceGrid::NodeUpdateDescriptor & d)
            {
                return d.name == application->getEffectiveNodeName();
            });
            if (location == appUpdate.nodes.end())
            {
                ARMARX_ERROR_S << "Error while removing an Application in an not exitend node";
                return;
            }
            //If there is only this server left remove the whole application
            if ((*location).serverInstances.size() == 1)
            {
                removeScenario(application->getScenario(), statusManager);
            }
            else
            {
                (*location).removeServers.push_back(IceStatusReader::GetApplicationId(application));
                auto loc = std::find_if((*location).serverInstances.begin(), (*location).serverInstances.end(), [application](const IceGrid::ServerInstanceDescriptor & d)
                {
                    return  d.parameterValues.at("component") == application->getName()
                            && d.parameterValues.at("componentId") == application->getInstanceName();
                });
                if (loc != (*location).serverInstances.end())
                {
                    (*location).serverInstances.erase(loc);
                }
                ARMARX_INFO << "Removing application " << application->getName();
                admin->updateApplication(appUpdate);
                ARMARX_INFO << "Removed application " << application->getName();
            }
        }
        catch (IceGrid::AccessDeniedException& ex)
        {
            ARMARX_ERROR_S << "Error while removing Application " << application->getName() << " another application has the accesss rights to the IceGridAdmin. (Please try again)";
        }
        catch (IceGrid::ApplicationNotExistException& ex)
        {
            ARMARX_ERROR_S << "ScenarioManager error: this should not happen";
        }
    }
}

void IceStopper::removeScenario(Data_Structure::ScenarioPtr scenario, StatusManager statusManager)
{
    std::vector<std::string> appNames = admin->getAllApplicationNames();
    if (std::find(appNames.begin(), appNames.end(), scenario->getName()) != appNames.end())
    {
        try
        {
            ARMARX_INFO << "Removing scenario " << scenario->getName();
            admin->removeApplication(scenario->getName());
            ARMARX_INFO << "Removed scenario " << scenario->getName();
            //statusManager.setIceScenario(scenario, false);
        }
        catch (IceGrid::AccessDeniedException& ex)
        {
            ARMARX_ERROR_S << "Error while deploying Scenario " << scenario->getName() << " another application has the accesss rights to the IceGridAdmin. (Please try again)";
        }
        catch (IceGrid::DeploymentException& ex)
        {
            ARMARX_ERROR_S << "Error while deploying Scenario" << scenario->getName();
        }
        catch (IceGrid::ApplicationNotExistException& ex)
        {
            ARMARX_ERROR_S << "ScenarioManager error: this should not happen";
        }
    }
}

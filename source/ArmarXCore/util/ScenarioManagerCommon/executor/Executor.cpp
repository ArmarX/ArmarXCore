/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nicola Miskowiec
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Executor.h"
#include "ArmarXCore/util/ScenarioManagerCommon/parser/iceparser.h"
#include "ArmarXCore/util/ScenarioManagerCommon/executor/IceStarter.h"
#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <filesystem>
#include <sys/types.h>
#include <signal.h>
#include <chrono>
#include <thread>

using namespace ScenarioManager;

Exec::Executor::Executor(Exec::StopStrategyPtr strategy, Exec::ApplicationStarterPtr starter)
{
    setDefaultStopStrategy(strategy);
    setDefaultStarter(starter);
}

std::future<void> Exec::Executor::startApplication(ApplicationInstancePtr application, bool printOnly, const std::string& commandLineParameters)
{
    if (application->getPid() != -1 || application->getStatusWriteBlock())
    {
        return std::future<void>();
    }

    application->setStatusWriteBlock(true);
    std::packaged_task<void()> task(std::bind(&ApplicationStarter::startApplication, getStarter(application->getScenario()).get(), application, statusManager, commandLineParameters, printOnly)); // wrap the function

    std::future<void> result = task.get_future();  // get a future
    std::thread t(std::move(task));
    if (printOnly)
    {
        t.join(); //if only the commands should be printed then we want sync behaviour
    }
    else
    {
        t.detach();
    }
    return result;
}

std::future<void> Exec::Executor::deployApplication(ApplicationInstancePtr application, bool printOnly, const std::string& commandLineParameters)
{
    if (application->getPid() != -1)
    {
        ARMARX_ERROR_S << "Could not deploy application " << application->getName() << " because it is still running. Please Stop it before deploying";
        return std::future<void>();
    }

    application->setStatusWriteBlock(true);
    std::packaged_task<void()> task(std::bind(&ApplicationStarter::deployApplication, getStarter(application->getScenario()).get(), application, statusManager, commandLineParameters, printOnly)); // wrap the function

    std::future<void> result = task.get_future();  // get a future
    std::thread t(std::move(task));
    t.detach();

    return result;
}


std::future<void> Exec::Executor::stopApplication(ApplicationInstancePtr application)
{
    if (application->getStatusWriteBlock())
    {
        return std::future<void>();
    }

    application->setStatusWriteBlock(true);
    std::packaged_task<void()> task(std::bind(&StopStrategy::stop, getStopStrategy(application->getScenario()).get(), application)); // wrap the function
    std::future<void> result = task.get_future();  // get a future
    std::thread(std::move(task)).detach(); // launch on a thread

    return result;
}

std::future<void> Exec::Executor::removeApplication(ApplicationInstancePtr application)
{
    if (application->getStatusWriteBlock())
    {
        return std::future<void>();
    }

    application->setStatusWriteBlock(true);
    std::packaged_task<void()> task(std::bind(&StopStrategy::removeApplication, getStopStrategy(application->getScenario()).get(), application, statusManager)); // wrap the function
    std::future<void> result = task.get_future();  // get a future
    std::thread(std::move(task)).detach(); // launch on a thread

    return result;
}

void Exec::Executor::asyncApplicationRestart(ApplicationInstancePtr application, bool printOnly)
{
    Data_Structure::ScenarioPtr scenario = application->getScenario();
    getStopStrategy(scenario)->stop(application);

    int waitCount = 0;
    auto state = getStarter(scenario)->getStatus(application, statusManager);
    while (state != ScenarioManager::Data_Structure::ApplicationStatus::Stopped &&
           state != ScenarioManager::Data_Structure::ApplicationStatus::Inactive)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        waitCount ++;
        //Try to stop for 20 secs
        if (waitCount == 200)
        {
            ARMARX_INFO_S << "The application " << application->getName() << " is not Stopping please force kill it or try again. Aborting restart";
            return;
        }
        state = getStarter(scenario)->getStatus(application, statusManager);
    }

    ARMARX_INFO << "Starting application `" << application->getName() << "` which is `" << application->getExecutableAbsPath() << "`";
    getStarter(scenario)->startApplication(application, statusManager, "", printOnly);
}

std::future<void> Exec::Executor::restartApplication(ApplicationInstancePtr application, bool printOnly)
{
    if (application->getStatusWriteBlock())
    {
        return std::future<void>();
    }

    application->setStatusWriteBlock(true);

    std::packaged_task<void()> task(std::bind(&Executor::asyncApplicationRestart, this, application, printOnly)); // wrap the function

    std::future<void> result = task.get_future();  // get a future
    std::thread(std::move(task)).detach(); // launch on a thread

    return result;
}

std::string Exec::Executor::getApplicationStatus(ApplicationInstancePtr application)
{
    if (application->getStatusWriteBlock())
    {
        return "Waiting";
    }
    return getStarter(application->getScenario())->getStatus(application, statusManager);
}

std::future<void> Exec::Executor::startScenario(std::shared_ptr<Data_Structure::Scenario> scenario, bool printOnly, const std::string& commandLineParameters)
{
    if (scenario->getStatusWriteBlock())
    {
        return std::future<void>();
    }

    scenario->setStatusWriteBlock(true);
    std::packaged_task<void()> task(std::bind(&ApplicationStarter::startScenario, getStarter(scenario).get(), scenario, statusManager, commandLineParameters, printOnly)); // wrap the function

    std::future<void> result = task.get_future();  // get a future
    std::thread t(std::move(task)); // launch on a thread
    if (printOnly)
    {
        t.join(); //if only the commands should be printed then we want sync behaviour
    }
    else
    {
        t.detach();
    }
    return result;
}

std::future<void> Exec::Executor::deployScenario(Data_Structure::ScenarioPtr scenario, bool printOnly, const std::string& commandLineParameters)
{
    if (scenario->getStatusWriteBlock())
    {
        return std::future<void>();
    }
    if (scenario->getStatus() == Data_Structure::ApplicationStatus::Running ||
        scenario->getStatus() == Data_Structure::ApplicationStatus::Waiting ||
        scenario->getStatus() == Data_Structure::ApplicationStatus::Mixed)
    {
        ARMARX_ERROR_S << "Could not deploy Scenario " << scenario->getName() << ". Please Stop the Scenario before deploying";
        return std::future<void>();
    }

    scenario->setStatusWriteBlock(true);
    std::packaged_task<void()> task(std::bind(&ApplicationStarter::deployScenario, getStarter(scenario).get(), scenario, statusManager, commandLineParameters, printOnly)); // wrap the function

    std::future<void> result = task.get_future();  // get a future
    std::thread t(std::move(task));
    t.detach();

    return result;
}

void Exec::Executor::asyncScenarioStop(Data_Structure::ScenarioPtr scenario)
{
    std::vector<std::future<void>> futures;

    std::vector<ApplicationInstancePtr> apps = *scenario->getApplications();
    for (auto it = apps.begin(); it != apps.end(); it++)
    {
        futures.push_back(stopApplication(*it));
    }

    for (auto future = futures.begin(); future != futures.end(); ++future)
    {
        future->wait();
    }
}

std::future<void> Exec::Executor::stopScenario(Data_Structure::ScenarioPtr scenario)
{
    std::packaged_task<void()> task(std::bind(&Executor::asyncScenarioStop, this, scenario)); // wrap the function
    std::future<void> result = task.get_future();  // get a future
    std::thread(std::move(task)).detach(); // launch on a thread

    return result;
}

std::future<void> Exec::Executor::removeScenario(Data_Structure::ScenarioPtr scenario)
{
    if (scenario->getStatusWriteBlock())
    {
        return std::future<void>();
    }

    scenario->setStatusWriteBlock(true);
    std::packaged_task<void()> task(std::bind(&StopStrategy::removeScenario, getStopStrategy(scenario).get(), scenario, statusManager)); // wrap the function
    std::future<void> result = task.get_future();  // get a future
    std::thread(std::move(task)).detach(); // launch on a thread

    return result;
}

void Exec::Executor::asyncScenarioRestart(Data_Structure::ScenarioPtr scenario, bool printOnly)
{
    std::vector<std::future<void>> futures;
    for (auto app : *scenario->getApplications())
    {
        futures.push_back(restartApplication(app, printOnly));
    }

    for (auto future = futures.begin(); future != futures.end(); ++future)
    {
        future->wait();
    }
}

std::future<void> Exec::Executor::restartScenario(Data_Structure::ScenarioPtr scenario, bool printOnly)
{
    std::packaged_task<void()> task(std::bind(&Executor::asyncScenarioRestart, this, scenario, printOnly)); // wrap the function
    std::future<void> result = task.get_future();  // get a future
    std::thread(std::move(task)).detach(); // launch on a thread

    return result;
}

void Exec::Executor::loadAndSetCachedProperties(Data_Structure::ApplicationPtr application, std::string path, bool reload, bool set)
{
    std::filesystem::path xmlFilePath = std::filesystem::path(path) / std::filesystem::path(application->getPackageName() + "." + application->getName() + ".xml");

    if (reload || !std::filesystem::exists(xmlFilePath))
    {
        std::filesystem::create_directories(path);

        std::string strCommand = application->getExecutableAbsPath().append(" -p -f xml -o ").append(xmlFilePath.string());
        int ret = system(strCommand.c_str());
        if (ret != 0)
        {
            ARMARX_WARNING << "Failed to generate properties xml for " << application->getName() << "\nCommand was: " << strCommand;
        }
    }
    else
    {
        application->updateFound();
        if (!application->getFound())
        {
            return;
        }
        //if executable more recent that xml reload xml
        auto xmlDate = (std::filesystem::last_write_time(xmlFilePath));
        
        auto execDate = std::filesystem::last_write_time(application->getPathToExecutable().append("/").append(application->getExecutableName()));
        
        if (execDate > xmlDate)
        {
            loadAndSetCachedProperties(application, path, true, set);
            return;
        }
    }

    if (set)
    {
        Parser::IceParser parser;
        application->setProperties(parser.loadFromXml(xmlFilePath.string()));
    }
}

void Exec::Executor::setDefaultStopStrategy(StopStrategyPtr strategy)
{
    defaultStopStrategy = strategy;
}
void Exec::Executor::setDefaultStarter(ApplicationStarterPtr starter)
{
    defaultStartStrategy = starter;
}

void Exec::Executor::setStopStrategy(StopStrategyPtr strategy, Data_Structure::ScenarioPtr scenario)
{
    this->stopStrategy[scenario] = strategy;
}

void Exec::Executor::setStarter(ApplicationStarterPtr starter, Data_Structure::ScenarioPtr scenario)
{
    this->starter[scenario] = starter;
}

Exec::ApplicationStarterPtr Exec::Executor::getStarter(Data_Structure::ScenarioPtr scenario)
{
    if (starter.count(scenario) == 0)
    {
        return defaultStartStrategy;
    }
    return starter[scenario];
}

Exec::StopStrategyPtr Exec::Executor::getStopStrategy(Data_Structure::ScenarioPtr scenario)
{
    if (stopStrategy.count(scenario) == 0)
    {
        return defaultStopStrategy;
    }
    return stopStrategy[scenario];
}

bool Exec::Executor::isApplicationDeployed(Data_Structure::ApplicationInstancePtr application)
{
    return getStarter(application->getScenario())->isApplicationDeployed(application);
}

bool Exec::Executor::isScenarioDeployed(Data_Structure::ScenarioPtr scenario)
{
    return getStarter(scenario)->isScenarioDeployed(scenario);
}

Exec::StopStrategyPtr Exec::Executor::getDefaultStopStrategy()
{
    return defaultStopStrategy;
}

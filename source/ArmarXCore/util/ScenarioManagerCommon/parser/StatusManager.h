#pragma once

#include "../data_structure/ApplicationInstance.h"

namespace ScenarioManager
{
    class StatusManager
    {
    public:
        StatusManager();

        int loadPid(Data_Structure::ApplicationInstancePtr app);
        void savePid(Data_Structure::ApplicationInstancePtr app);

        bool isIceScenario(Data_Structure::ScenarioPtr scenario);
        void setIceScenario(Data_Structure::ScenarioPtr scenario, bool state);



        static void clearCache();
    private:
    };
}

#include "../executor/Executor.h"


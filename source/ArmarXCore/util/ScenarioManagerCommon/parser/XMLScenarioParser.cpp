/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "XMLScenarioParser.h"

#include "StringUtil.hpp"
#include "../data_structure/Application.h"
#include "../data_structure/ApplicationInstance.h"

#include <ArmarXCore/util/CPPUtility/filesystem.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlWriter.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionContainerFormatter.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionFormatter.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionConfigFormatter.h>

#include <filesystem>

#include <stdio.h>
#include <string>
#include <time.h>
#include <iostream>
#include <iomanip>
#include <ctime>
#include <vector>
#include <fstream>

#define SCENARIOMIMETYPE ".scx"

using namespace rapidxml;
using namespace ScenarioManager;
using namespace Data_Structure;
using namespace Parser;
using namespace armarx;


// Get current date/time, format is YYYY-MM-DD.HH:mm:ss
const std::string currentDateTime()
{
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

    return buf;
}
template<class T>
const std::string formatTTime(T chronotp)
{
    auto sctp = std::chrono::time_point_cast<std::chrono::system_clock::duration>(chronotp - T::clock::now()
                + std::chrono::system_clock::now());
    time_t time = std::chrono::system_clock::to_time_t(sctp);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&time);
    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

    return buf;
}

std::vector<std::string> XMLScenarioParser::getScenariosFromFolder(std::string folder)
{
    using namespace std::filesystem;
    const path scenarioDir = remove_trailing_separator(folder);
    std::vector<std::string> result;
    if (exists(scenarioDir))
    {
        try
        {
            for (std::filesystem::recursive_directory_iterator end, dir(scenarioDir);
                 dir != end ; ++dir)
            {
                if (dir->path().extension() == ".scx")
                {
                    result.push_back(fs::relative(dir->path(), scenarioDir).parent_path().string());
                }
            }
        }
        catch (std::exception& e)
        {
            ARMARX_WARNING_S << "Invalid filepath: " << e.what();
        }
    }
    return result;
}


ScenarioPtr XMLScenarioParser::parseScenario(PackagePtr package, std::string scenName, std::string subfolder)
{
    if (package == nullptr)
    {
        return ScenarioPtr();
    }


    std::string appFolder = package->getScenarioPath();
    appFolder.append("/");
    if (!subfolder.empty())
    {
        appFolder.append(subfolder + "/");
    }
    appFolder.append(scenName);
    appFolder.append("/");

    std::string path = appFolder;
    path.append(scenName);
    path.append(SCENARIOMIMETYPE);

    if (!std::filesystem::exists(std::filesystem::path(path)))
    {
        return ScenarioPtr(nullptr);
    }

    RapidXmlReaderPtr doc = RapidXmlReader::FromFile(path);

    RapidXmlReaderNode root_node = doc->getRoot();

    std::string name = root_node.attribute_value("name");
    std::string creation = root_node.attribute_value("creation");
    std::string globalConfigName;
    try
    {
        globalConfigName = root_node.attribute_value("globalConfigName");
    }
    catch (...)
    {
        globalConfigName = "./config/global.cfg";
    }
    ScenarioDeploymentType deploymentType;
    try
    {
        std::string val = root_node.attribute_value("deploymentType");
        val == "local" ? deploymentType = ScenarioDeploymentType::Local : deploymentType = ScenarioDeploymentType::Remote;
    }
    catch (...)
    {
        deploymentType = ScenarioDeploymentType::Local;
    }

    auto nodeName = root_node.has_attribute("nodeName") ? root_node.attribute_value("nodeName") : "";
    //string package = root_node->first_attribute("package")->value();
    std::string lastWriteTime = formatTTime(std::filesystem::last_write_time(std::filesystem::path(path)));

    //Build Ice Env Variables if there are some
    std::map<std::string, std::string> iceEnvVariables;
    if (root_node.has_node("iceEnvVariablesNode"))
    {
        RapidXmlReaderNode iceEnvVariablesNode = root_node.first_node("iceEnvVariablesNode");
        for (RapidXmlReaderNode iceEnvVariableNode : iceEnvVariablesNode.nodes("variable"))
        {
            std::string varName = iceEnvVariableNode.attribute_value("name");
            std::string value = iceEnvVariableNode.value();
            iceEnvVariables[varName] = value;
        }
    }

    ScenarioPtr result(new Scenario(name, creation, lastWriteTime, package, globalConfigName, subfolder, deploymentType, iceEnvVariables));
    for (RapidXmlReaderNode application_node : root_node.nodes("application"))
    {
        std::string appInstanceStr = application_node.attribute_value("instance");
        std::string appName = application_node.attribute_value("name");
        std::string packageName = application_node.attribute_value("package");
        std::string nodeName;
        try
        {
            nodeName = application_node.attribute_value("nodeName");
        }
        catch (...)
        {
            nodeName = "";
        }

        bool enabled;
        try
        {
            enabled = application_node.attribute_as_bool("enabled", "true", "false");
        }
        catch (...)
        {
            enabled = true;
        }

        bool iceAutoRestart;
        try
        {
            iceAutoRestart = application_node.attribute_as_bool("iceAutoRestart", "true", "false");
        }
        catch (...)
        {
            iceAutoRestart = false;
        }

        CMakePackageFinder pFinder = CMakePackageFinderCache::GlobalCache.findPackage(packageName);

        namespace fs = std::filesystem;

        const fs::path executableDir = pFinder.getBinaryDir();

        // attribute executableName might not be available
        const std::string appExecutableName = [&]() -> std::string {
            try{
                std::string executableName = application_node.attribute_value("executableName"); // throws if attribute does not exist
                return executableName;
            }catch(...)
            {
                if(fs::exists(executableDir / (appName + "Run")))
                {
                    return appName + "Run";
                }

                if(fs::exists(executableDir / (appName + "AppRun")))
                {
                    return appName + "AppRun";
                }

                if(fs::exists(executableDir / (appName + "_run")))
                {
                    return appName + "_run";
                }

                ARMARX_WARNING << "Scenario '" + name + "': App `" + appName + "` could not be found.";
                ARMARX_WARNING << executableDir / (appName + "Run");
                return appName;
            }
        }();

        ApplicationPtr application(new Application(appExecutableName, pFinder.getBinaryDir(), packageName));

        ApplicationInstancePtr appInstance;
        if (appInstanceStr.empty())
        {
            appInstance = std::make_shared<ApplicationInstance>(*application, appInstanceStr, appFolder + "config/" + appName + ".cfg", result, nodeName, enabled, iceAutoRestart);
        }
        else
        {
            appInstance = std::make_shared<ApplicationInstance>(*application, appInstanceStr, appFolder + "config/" + appName + "." + appInstanceStr + ".cfg", result, nodeName, enabled, iceAutoRestart);
        }
        appInstance->setNodeName(nodeName);

        appInstance->updateFound();
        result->addApplication(appInstance);
    }

    //load Global conf
    Ice::PropertiesPtr cfgProperties = IceProperties::create();
    armarx::IceProperties* cfgInternal = static_cast<armarx::IceProperties*>(cfgProperties.get());
    cfgInternal->setInheritanceSolver(nullptr);

    if (result->isGlobalConfigFileexistent())
    {
        cfgProperties->load(result->getGlobalConfigPath());
    }

    Ice::PropertyDict dict = cfgProperties->getPropertiesForPrefix("");

    for (auto const& property : dict)
    {
        result->getGlobalConfig()->defineOptionalProperty<std::string>(property.first, "::NOT_DEFINED::", "Custom Property");
        result->getGlobalConfig()->getProperties()->setProperty(property.first, property.second);
    }

    return result;
}

ScenarioPtr XMLScenarioParser::parseScenario(ScenarioPtr scenario)
{
    return parseScenario(scenario->getPackage(), scenario->getName(), scenario->getSubfolder());
}

bool XMLScenarioParser::isScenarioexistent(std::string name, PackagePtr package, std::string subPath)
{
    std::vector<std::string> paths = getScenariosFromFolder(package->getScenarioPath());

    if (std::find(paths.begin(), paths.end(), subPath + name) != paths.end())
    {
        return true;
    }

    return false;
}

ScenarioPtr XMLScenarioParser::createNewScenario(std::string name, PackagePtr package)
{
    std::string scenarioDir = package->getScenarioPath();
    if (scenarioDir.empty())
    {
        throw LocalException("The scenarios directory variable of " + package->getName() + " is empty - did you run CMake successfully?");
    }
    if (!package->isScenarioPathWritable())
    {
        ARMARX_WARNING_S << "Warning: Scenario Path is not writable. Unable to create a new Scenario.";
        return ScenarioPtr(nullptr);
    }

    RapidXmlWriter doc;

    RapidXmlWriterNode root_node = doc.createRootNode("scenario");

    root_node.append_attribute("name", name.c_str());
    root_node.append_attribute("package", package->getName().c_str());

    std::string currentDateStr = currentDateTime();
    root_node.append_attribute("creation", currentDateStr.c_str());
    root_node.append_attribute("deploymentType", "local");


    if (!std::filesystem::create_directories(scenarioDir.append("/").append(name)) ||
        !std::filesystem::create_directories(scenarioDir + "/config"))
    {
        ARMARX_ERROR_S << "Can not create Scenario in package " << package->getName();
        return ScenarioPtr(nullptr);
    }

    doc.saveToFile(scenarioDir + "/" + name + SCENARIOMIMETYPE, true);

    ARMARX_INFO_S << "Saved scenario file: " << scenarioDir + "/" + name + SCENARIOMIMETYPE;


    std::ofstream out2(scenarioDir.append("/config/global.cfg"));
    out2 << "";
    out2.close();

    return parseScenario(package, name);
}

void XMLScenarioParser::saveScenario(ScenarioWPtr wScenario)
{
    ScenarioPtr scenario = wScenario.lock();

    RapidXmlWriter doc;

    RapidXmlWriterNode scenarioNode = doc.createRootNode("scenario");

    scenarioNode.append_attribute("name", scenario->getName());


    std::string currentDateStr = currentDateTime();
    scenario->setLastChangedTime(currentDateStr);

    scenarioNode.append_attribute("creation", scenario->getCreationTime().c_str());
    scenarioNode.append_attribute("globalConfigName", scenario->getGlobalConfigName());
    scenarioNode.append_attribute("package", scenario->getPackage()->getName());
    scenarioNode.append_attribute("deploymentType", scenario->getScenarioDeploymentType() ? "remote" : "local");
    scenarioNode.append_attribute("nodeName", scenario->getNodeName());

    //Save Ice Env Variables if there are some
    std::map<std::string, std::string> iceEnvVariables = scenario->getIceEnviromentVariables();
    if (iceEnvVariables.size() > 0)
    {
        RapidXmlWriterNode iceEnvVariablesNode = scenarioNode.append_node("iceEnvVariablesNode");
        for (auto var : iceEnvVariables)
        {
            RapidXmlWriterNode variableNode = iceEnvVariablesNode.append_node("variable");
            variableNode.append_attribute("name", var.first);
            variableNode.append_data_node(var.second);
        }
    }


    std::vector<ApplicationInstancePtr> applicationInstances = *scenario->getApplications();
    for (auto app : applicationInstances)
    {
        if (app->isConfigWritable())
        {
            app->save();
        }

        RapidXmlWriterNode applicationNode = scenarioNode.append_node("application");
        applicationNode.append_attribute("name", app->getName());
        // applicationNode.append_attribute("executable", app->getExecutableName());
        applicationNode.append_attribute("instance", app->getInstanceName());
        applicationNode.append_attribute("package", app->getPackageName());
        applicationNode.append_attribute("nodeName", app->getNodeName());
        applicationNode.append_bool_attribute("enabled", "true", "false", app->getEnabled());
        applicationNode.append_bool_attribute("iceAutoRestart", "true", "false", app->getIceAutoRestart());
    }


    std::string path = scenario->getPath();
    if (scenario->isScenarioFileWriteable())
    {
        doc.saveToFile(path, true);
    }


    //Save Globalconfig
    PropertyDefinitionsPtr props = scenario->getGlobalConfig();
    armarx::IceProperties* propsInternals = static_cast<armarx::IceProperties*>(props->getProperties().get());
    propsInternals->setInheritanceSolver(nullptr);

    std::string resultStr;

    PropertyDefinitionFormatter* defFormatter = new PropertyDefinitionConfigFormatter();
    PropertyDefinitionContainerFormatter*  pdcFormatter = new PropertyDefinitionContainerFormatter(*defFormatter);
    pdcFormatter->setProperties(props->getProperties());

    resultStr += pdcFormatter->formatPropertyDefinitionContainer(props);

    //filter Ice.Config
    size_t begin = resultStr.rfind("# Ice.Config:");
    size_t end = resultStr.rfind("Ice.Config = <set value!>") + 25;

    if (begin != std::string::npos)
    {
        if (end - 25 != std::string::npos)
        {
            resultStr.erase(begin, end - begin);
        }
        else
        {
            end = resultStr.rfind("Ice.Config = ::NOT_DEFINED::") + 28;
            if (end - 28 != std::string::npos)
            {
                resultStr.erase(begin, end - begin);
            }
        }
    }
    else
    {

    }

    std::ofstream cfgFile;
    cfgFile.open(scenario->getGlobalConfigPath(), std::ofstream::out | std::ofstream::trunc);
    if (cfgFile.fail() && scenario->isGlobalConfigWritable())
    {
        ARMARX_INFO_S << "Failed to write to Global Cfg file at " << scenario->getGlobalConfigPath();
        return;
    }
    cfgFile << resultStr;
    cfgFile.close();
}

PackagePtr XMLScenarioParser::getScenarioPackage(ScenarioPtr scenario, PackageVectorPtr packages)
{
    for (auto package : *packages)
    {
        CMakePackageFinder pFinder = CMakePackageFinderCache::GlobalCache.findPackage(package->getName());
        std::string scenarioDir = pFinder.getScenariosDir();
        if (scenario->getPath().find(scenarioDir) != std::string::npos)
        {
            return package;
        }
    }
    return PackagePtr();
}

std::string XMLScenarioParser::getPackageNameFromScx(std::string path)
{
    RapidXmlReaderPtr reader = RapidXmlReader::FromFile(path);

    if (!reader->getRoot("scenario").has_attribute("package"))
    {
        return "";
    }

    return reader->getRoot("scenario").attribute_value("package");
}

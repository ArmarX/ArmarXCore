#include "StatusManager.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/util/CPPUtility/filesystem.h>

#include <filesystem>

#include <fstream>

using namespace ScenarioManager;
using namespace Exec;
using namespace armarx;

StatusManager::StatusManager()
{
}


int StatusManager::loadPid(Data_Structure::ApplicationInstancePtr app)
{
    std::string cachePath = ArmarXDataPath::GetCachePath();
    if (cachePath.empty())
    {
        return -1;
    }

    cachePath.append("/pids/");
    auto dir = remove_trailing_separator(cachePath);

    if (!std::filesystem::exists(dir) && !std::filesystem::create_directories(dir))
    {
        ARMARX_WARNING_S << "Unable to create Cache directory for the ScenarioManager plugin at " << cachePath;
    }


    std::string filename = cachePath
                           + app->getPackageName() + "."
                           + app->getScenario()->getName() +  "."
                           + app->getName() + (app->getInstanceName().empty() ? "" : ".")
                           + app->getInstanceName() + ".pids";

    if (!std::filesystem::exists(filename))
    {
        return -1;
    }

    std::ifstream input(filename);

    int pid;
    input >> pid;

    return pid;
}

void StatusManager::savePid(Data_Structure::ApplicationInstancePtr app)
{
    std::string cachePath = ArmarXDataPath::GetCachePath();
    cachePath.append("/pids/");
    auto dir = remove_trailing_separator(cachePath);

    if (!std::filesystem::exists(dir) && !std::filesystem::create_directories(dir))
    {
        ARMARX_WARNING_S << "Unable to create Cache directory for the ScenarioManager plugin at " << cachePath;
    }

    std::string filename = cachePath
                           + app->getPackageName() + "."
                           + app->getScenario()->getName() +  "."
                           + app->getName() + (app->getInstanceName().empty() ? "" : ".")
                           + app->getInstanceName() + ".pids";

    if (app->getPid() == -1)
    {
        std::filesystem::remove(std::filesystem::path(filename));
        return;
    }

    std::ofstream output(filename);

    output << app->getPid();
    output.close();
}

bool StatusManager::isIceScenario(Data_Structure::ScenarioPtr scenario)
{
    std::string cachePath = ArmarXDataPath::GetCachePath();
    cachePath.append("/iceDeployment/");

    std::string filename = cachePath
                           + scenario->getPackage()->getName() + "."
                           + scenario->getName() +  ".ice";

    return std::filesystem::exists(filename);
}

void StatusManager::setIceScenario(Data_Structure::ScenarioPtr scenario, bool state)
{
    if (state)
    {
        std::string cachePath = ArmarXDataPath::GetCachePath();
        cachePath.append("/iceDeployment/");
        std::filesystem::path dir(cachePath);

        if (!std::filesystem::exists(dir) && !std::filesystem::create_directories(dir))
        {
            ARMARX_WARNING_S << "Unable to create Cache directory for the ScenarioManager plugin at " << cachePath;
        }

        std::string filename = cachePath
                               + scenario->getPackage()->getName() + "."
                               + scenario->getName() +  ".ice";

        if (!std::filesystem::exists(filename))
        {
            std::ofstream output(filename);
            output << "";
            output.close();
        }
    }
    else
    {
        std::string cachePath = ArmarXDataPath::GetCachePath();
        cachePath.append("/iceDeployment/");

        std::string filename = cachePath
                               + scenario->getPackage()->getName() + "."
                               + scenario->getName() +  ".ice";

        if (std::filesystem::exists(filename))
        {
            if (!std::filesystem::remove(filename))
            {
                ARMARX_WARNING_S << "Unable to remove the ice deployment file " << filename;
            }
        }
    }
}


void StatusManager::clearCache()
{
    std::string cachePath = ArmarXDataPath::GetCachePath();
    cachePath.append("/pids/");

    std::filesystem::remove_all(std::filesystem::path(cachePath));
}

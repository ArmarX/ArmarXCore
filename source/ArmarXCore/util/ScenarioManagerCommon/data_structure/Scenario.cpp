/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Dennis Weigelt
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Scenario.h"
#include "../parser/XMLScenarioParser.h"
#include <ArmarXCore/core/application/properties/IceProperties.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <filesystem>
#include <Ice/Properties.h>
#include <fstream>

using namespace ScenarioManager;


Data_Structure::Scenario::Scenario(std::string name, std::string creationTime, std::string lastChangedTime, PackagePtr package, std::string globalConfigName, std::string subfolder, ScenarioDeploymentType deploymentType, IceEnvMap iceEnvVariables)
    : name(name), creationTime(creationTime),
      lastChangedTime(lastChangedTime),
      globalConfigName(globalConfigName),
      subfolder(subfolder),
      deploymentType(deploymentType),
      package(package),
      applications(new std::vector<ApplicationInstancePtr>()),
      globalConfig(new armarx::PropertyDefinitionContainer(name)),
      iceEnviromentVariables(iceEnvVariables)
{
    Ice::PropertiesPtr cfgProperties = armarx::IceProperties::create();
    armarx::IceProperties* cfgInternal = static_cast<armarx::IceProperties*>(cfgProperties.get());
    cfgInternal->setInheritanceSolver(nullptr);
    nodeName = "NodeMain";
    globalConfig->setProperties(cfgProperties);
    globalConfig->setPrefix("");
    globalConfig->setDescription("Global Config from Scenario " + name);
}

std::string Data_Structure::Scenario::getName()
{
    return this->name;
}

void Data_Structure::Scenario::setName(std::string name)
{
    this->name = name;
}

std::string Data_Structure::Scenario::getCreationTime()
{
    return this->creationTime;
}

std::string Data_Structure::Scenario::getLastChangedTime()
{
    return this->lastChangedTime;
}

std::string Data_Structure::Scenario::getGlobalConfigName()
{
    return globalConfigName;
}

std::string Data_Structure::Scenario::getSubfolder()
{
    return subfolder;
}

std::string Data_Structure::Scenario::getPath()
{
    std::string scenarioPath = package.lock()->getScenarioPath();

    if (subfolder.empty())
    {
        scenarioPath.append("/").append(name).append("/").append(name).append(".scx");
    }
    else
    {
        scenarioPath.append("/").append(subfolder).append("/").append(name).append("/").append(name).append(".scx");
    }

    return scenarioPath;
}

std::string Data_Structure::Scenario::getFolder()
{
    std::string scenarioPath = package.lock()->getScenarioPath();

    scenarioPath.append("/").append(name);

    return scenarioPath;
}

bool Data_Structure::Scenario::isScenarioFileWriteable()
{
    std::ofstream ofs;
    ofs.open(getPath().c_str(), std::ofstream::out | std::ofstream::app);
    ARMARX_DEBUG << getPath() << " is writeable: " << ofs.is_open();

    return ofs.is_open();

    std::filesystem::file_status s = std::filesystem::status(std::filesystem::path(getPath()));
    if ((s.permissions() & std::filesystem::perms::owner_write) != std::filesystem::perms::none)
    {
        return true;
    }
    return false;
}

Data_Structure::PackagePtr Data_Structure::Scenario::getPackage()
{
    return package.lock();
}

std::string Data_Structure::Scenario::getStatus()
{
    std::string status = ApplicationStatus::Unknown;
    for (auto app : *applications)
    {
        if (!app->getEnabled())
        {
            continue;
        }
        if (status == ApplicationStatus::Unknown && app->getStatus() == ApplicationStatus::Running)
        {
            status = ApplicationStatus::Running;
        }
        else if (status == ApplicationStatus::Unknown && app->getStatus() == ApplicationStatus::Stopped)
        {
            status = ApplicationStatus::Stopped;
        }
        else if (status == ApplicationStatus::Unknown && app->getStatus() == ApplicationStatus::Inactive)
        {
            status = ApplicationStatus::Inactive;
        }
        else if (status == ApplicationStatus::Running && app->getStatus() == ApplicationStatus::Running)
        {
            status = ApplicationStatus::Running;
        }
        else if (status == ApplicationStatus::Stopped && app->getStatus() == ApplicationStatus::Stopped)
        {
            status = ApplicationStatus::Stopped;
        }
        else if (status == ApplicationStatus::Inactive && app->getStatus() == ApplicationStatus::Inactive)
        {
            status = ApplicationStatus::Inactive;
        }
        else if ((status == ApplicationStatus::Running && app->getStatus() == ApplicationStatus::Stopped)
                 || (status == ApplicationStatus::Stopped && app->getStatus() == ApplicationStatus::Running)
                 || (status == ApplicationStatus::Stopped && app->getStatus() == ApplicationStatus::Inactive)
                 || (status == ApplicationStatus::Running && app->getStatus() == ApplicationStatus::Inactive)
                 || (status == ApplicationStatus::Inactive && app->getStatus() == ApplicationStatus::Running)
                 || (status == ApplicationStatus::Inactive && app->getStatus() == ApplicationStatus::Stopped))
        {
            status = ApplicationStatus::Mixed;
            break;
        }
        else if (app->getStatus() == ApplicationStatus::Waiting)
        {
            status = ApplicationStatus::Waiting;
            break;
        }
        else if (app->getStatus() == ApplicationStatus::Unknown)
        {
            status = ApplicationStatus::Unknown;
            break;
        }
        else
        {
            status = ApplicationStatus::Unknown;
            break;
        }
    }

    return status;
}

void Data_Structure::Scenario::setLastChangedTime(std::string time)
{
    this->lastChangedTime = time;
}

void Data_Structure::Scenario::setGlobalConfigName(std::string name)
{
    globalConfigName = name;
}

Data_Structure::ApplicationInstanceVectorPtr Data_Structure::Scenario::getApplications()
{
    return applications;
}

Data_Structure::ApplicationInstancePtr Data_Structure::Scenario::getApplicationByName(std::string name)
{
    for (auto app : *applications)
    {
        if (app->getName() == name)
        {
            return app;
        }
    }

    return ApplicationInstancePtr(nullptr);
}

void Data_Structure::Scenario::addApplication(Data_Structure::ApplicationInstancePtr application)
{
    applications->push_back(application);
}

void Data_Structure::Scenario::removeApplication(Data_Structure::ApplicationInstancePtr application)   //?
{
    for (std::vector<ApplicationInstancePtr>::iterator iter = applications->begin(); iter != applications->end(); ++iter)
    {
        if (iter->get() == application.get())
        {
            applications->erase(iter);
            return;
        }
    }
}

void Data_Structure::Scenario::updateApplicationByName(std::string name)   //gehört diese Methode hier rein? evtl signal/slot mit Controller
{
    // TODO - implement Scenario::updateApplicationByName
    //throw "Not yet implemented";
}

void Data_Structure::Scenario::updateApplication(Data_Structure::ApplicationInstancePtr application)
{
    // TODO - implement Scenario::updateApplication
    //throw "Not yet implemented";
}

void Data_Structure::Scenario::save()
{
    Parser::XMLScenarioParser parser;
    parser.saveScenario(shared_from_this());
}

void Data_Structure::Scenario::reloadAppInstances()
{
    for (auto it : *applications)
    {
        it->load();
    }
}

void Data_Structure::Scenario::reloadGlobalConf()
{
    if (isGlobalConfigFileexistent())
    {
        globalConfig->getProperties()->load(getGlobalConfigPath());
    }
}

armarx::PropertyDefinitionsPtr Data_Structure::Scenario::getGlobalConfig()
{
    return globalConfig;
}

std::string Data_Structure::Scenario::getGlobalConfigPath()
{
    std::filesystem::path scenarioPath;
    if (subfolder.empty())
    {
        scenarioPath = std::filesystem::path(package.lock()->getScenarioPath().append("/").append(name).append("/"));
    }
    else
    {
        scenarioPath = std::filesystem::path(package.lock()->getScenarioPath().append("/").append(subfolder + "/").append(name).append("/"));
    }

    scenarioPath = scenarioPath / std::filesystem::path(globalConfigName.empty() ? "./config/global.cfg" : globalConfigName);
    return scenarioPath.string();
}

bool Data_Structure::Scenario::isGlobalConfigWritable()
{
    std::ofstream ofs;
    ofs.open(getGlobalConfigPath().c_str(), std::ofstream::out | std::ofstream::app);
    ARMARX_DEBUG << getGlobalConfigPath() << " is writeable: " << ofs.is_open();

    return ofs.is_open();
}

bool Data_Structure::Scenario::isGlobalConfigFileexistent()
{
    return std::filesystem::exists(std::filesystem::path(getGlobalConfigPath()));
}

bool Data_Structure::Scenario::getStatusWriteBlock()
{
    for (auto app : *applications)
    {
        if (app->getStatusWriteBlock())
        {
            return true;
        }
    }
    return false;
}

void Data_Structure::Scenario::setStatusWriteBlock(bool state)
{
    for (auto app : *applications)
    {
        app->setStatusWriteBlock(state);
    }
}

Data_Structure::ScenarioDeploymentType Data_Structure::Scenario::getScenarioDeploymentType()
{
    return deploymentType;
}

void Data_Structure::Scenario::setScenarioDeploymentType(ScenarioDeploymentType type)
{
    deploymentType = type;
}

std::vector<std::string> Data_Structure::Scenario::getAllDeploymendNodeNames()
{
    std::vector<std::string> result;
    for (auto app : *applications)
    {
        result.push_back(app->getEffectiveNodeName());
    }
    return result;
}

bool Data_Structure::Scenario::allApplicationsFound()
{
    for (auto app : *applications)
    {
        app->updateFound();
        if (!app->getFound())
        {
            return false;
        }
    }
    return true;
}

void Data_Structure::Scenario::addIceEnviromentVariable(std::string name, std::string variable)
{
    iceEnviromentVariables[name] = variable;
}

void Data_Structure::Scenario::clearIceEnviromentVariable(std::string name)
{
    iceEnviromentVariables[name] = "";
}

std::map<std::string, std::string> Data_Structure::Scenario::getIceEnviromentVariables()
{
    return iceEnviromentVariables;
}

std::string Data_Structure::Scenario::getNodeName() const
{
    return nodeName;
}

void Data_Structure::Scenario::setNodeName(const std::string& value)
{
    nodeName = value;
}

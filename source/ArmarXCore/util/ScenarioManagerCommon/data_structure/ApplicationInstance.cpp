/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Dennis Weigelt
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "ApplicationInstance.h"

#include "../parser/iceparser.h"
#include <ArmarXCore/core/application/properties/PropertyDefinitionConfigFormatter.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionFormatter.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionContainerFormatter.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionConfigFormatter.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <filesystem>


using namespace ScenarioManager;
using namespace armarx;

const std::string Data_Structure::ApplicationStatus::Stopped = "Stopped";
const std::string Data_Structure::ApplicationStatus::Running = "Running";
const std::string Data_Structure::ApplicationStatus::Waiting = "Waiting";
const std::string Data_Structure::ApplicationStatus::Mixed = "Mixed";
const std::string Data_Structure::ApplicationStatus::Unknown = "Unknown";
const std::string Data_Structure::ApplicationStatus::Inactive = "Inactive";
const std::string Data_Structure::ApplicationStatus::Missing = "Missing";

std::string Data_Structure::ApplicationInstance::getConfigDomain() const
{
    return configDomain;
}

void Data_Structure::ApplicationInstance::setConfigDomain(const std::string& value)
{
    ARMARX_CHECK_EXPRESSION(!value.empty());
    configDomain = value;
}

Data_Structure::ApplicationInstance::ApplicationInstance(std::string executableName, std::string executablePath, std::string instanceName, std::string configPath, std::string packageName, ScenarioPtr scenario, std::string node, bool enabled, bool iceAutoRestart)
    : Application(executableName, executablePath, packageName), instanceName(instanceName)
    , configPath(configPath)
    , status(ApplicationStatus::Unknown), nodeName(node), scenario(scenario), pid(-1), statusWriteBlock(false), enabled(enabled), iceAutoRestart(iceAutoRestart)
{
}


Data_Structure::ApplicationInstance::ApplicationInstance(Application application, std::string instanceName, std::string configPath, ScenarioPtr scenario, std::string node, bool enabled, bool iceAutoRestart)
    : Application(application),
      instanceName(instanceName),
      configPath(configPath),
      status(ApplicationStatus::Unknown),
      nodeName(node),
      scenario(scenario),
      pid(-1),
      statusWriteBlock(false),
      enabled(enabled),
      iceAutoRestart(iceAutoRestart)
{
}

Data_Structure::ScenarioPtr Data_Structure::ApplicationInstance::getScenario()
{
    return scenario;
}

std::string Data_Structure::ApplicationInstance::getInstanceName()
{
    return this->instanceName;
}

void Data_Structure::ApplicationInstance::setInstanceName(std::string newName)
{
    this->instanceName = newName;
}

std::string Data_Structure::ApplicationInstance::getConfigPath()
{
    return this->configPath;
}

void Data_Structure::ApplicationInstance::setConfigPath(std::string configPath)
{
    this->configPath = configPath;
}

void Data_Structure::ApplicationInstance::resetConfigPath()
{
    std::filesystem::path scenariosFolder(scenario->getPackage()->getScenarioPath());
    std::filesystem::path scenarioFolder = scenariosFolder / std::filesystem::path("./" + scenario->getName());
    std::filesystem::path scenarioCfgFolder = scenarioFolder / std::filesystem::path("./config");

    std::filesystem::path configPath = scenarioCfgFolder;

    if (!this->instanceName.empty() || !this->instanceName.compare("\"\"") || !this->instanceName.compare("\" \""))
    {
        configPath = configPath / std::filesystem::path("./" + this->getName() + "." + this->getInstanceName() + ".cfg");
    }
    else
    {
        configPath = configPath / std::filesystem::path("./" + this->getName() + ".cfg");;
    }

    std::filesystem::remove(std::filesystem::path(this->configPath));
    this->configPath = configPath.string();
}

std::string Data_Structure::ApplicationInstance::getStatus()
{
    return this->status;
}

bool Data_Structure::ApplicationInstance::setStatus(const std::string& status)
{
    bool result = status != this->status;
    if (result)
    {
        this->status = status;
    }
    return result;
}

int Data_Structure::ApplicationInstance::getPid()
{
    return this->pid;
}

void Data_Structure::ApplicationInstance::setPid(int pid)   // if PID < -1 Error?
{
    this->pid = pid;
}

void Data_Structure::ApplicationInstance::modifyProperty(std::string name, std::string value)
{
    if (!properties->isPropertySet(name))
    {
        properties->defineOptionalProperty(name, std::string("::NOT_DEFINED::"), "Custom Property");
    }
    properties->getProperties()->setProperty(name, value);
}

void Data_Structure::ApplicationInstance::addProperty(std::string name, std::string value)
{
    if (!properties->isPropertySet(name))
    {
        properties->defineOptionalProperty(name, std::string("::NOT_DEFINED::"), "Custom Property");
    }
    properties->getProperties()->setProperty(name, value);
}

void Data_Structure::ApplicationInstance::save()
{
    Parser::IceParser parser;
    parser.saveCfg(shared_from_this());
}

void Data_Structure::ApplicationInstance::load(bool firstLoad)
{
    if (!std::filesystem::exists(configPath))
    {
        ARMARX_WARNING_S << "Cannot find ApplicationInstance Config at:" << configPath;
    }
    else
    {
        Parser::IceParser parser;
        setProperties(parser.mergeXmlAndCfg(shared_from_this(), firstLoad));
    }
}

bool Data_Structure::ApplicationInstance::isConfigWritable()
{
    std::ofstream ofs;

    ofs.open(configPath.c_str(), std::ofstream::out | std::ofstream::app);
    ARMARX_DEBUG << configPath << " is writeable: " << ofs.is_open();

    return ofs.is_open();
}

bool Data_Structure::ApplicationInstance::getStatusWriteBlock()
{
    return statusWriteBlock;
}

void Data_Structure::ApplicationInstance::setStatusWriteBlock(bool blocked)
{
    statusWriteBlock = blocked;
}

std::string Data_Structure::ApplicationInstance::getNodeName() const
{
    return nodeName;
}

std::string Data_Structure::ApplicationInstance::getEffectiveNodeName() const
{
    return nodeName.empty() && scenario ? scenario->getNodeName() : nodeName;
}

void Data_Structure::ApplicationInstance::setNodeName(std::string nodeName)
{
    this->nodeName = nodeName;
}

bool Data_Structure::ApplicationInstance::getEnabled()
{
    return enabled;
}

void Data_Structure::ApplicationInstance::setEnabled(bool enabled)
{
    this->enabled = enabled;
}

bool Data_Structure::ApplicationInstance::getIceAutoRestart()
{
    return iceAutoRestart;
}

void Data_Structure::ApplicationInstance::setIceAutoRestart(bool enabled)
{
    this->iceAutoRestart = enabled;
}
